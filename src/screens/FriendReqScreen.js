import React, { Component } from 'react';
import { View, Text,FlatList} from 'react-native';
import { FriendContactCard } from '../components/FriendContactCard'
import {get,post} from '../api';
import Loading from '../components/loading';
export default class ChatListScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
        isLoading:true,request:[]
    };
    this.accepInvite = this.accepInvite.bind(this);
  }
  componentDidMount(){
   this.init();
  }
  async init(){
    let response= await get("friend/invite/touser");
    this.setState({isLoading:false});
    if(response ){
        this.setState({request:response.invite});
    }
  }
  async accepInvite(id){
    let response= await post("friend/invite/accept/"+id,{});
    this.setState({isLoading:false});
    if(response ){
        await this.init();
    }
  }
 
    renderFriendReqItem = ({ item }) => (
        <FriendContactCard
            imgUri={{uri:item.picture}}
            name={item.nickname}
            desc={item.desc}
            actionType='request'
            confirm={()=>this.accepInvite(item.friend_invite_id)}
            data={item}
        />
    )
  render() {
    return (
      <View style={{flex:1}}>
             <FlatList
                data={this.state.request}
                renderItem={this.renderFriendReqItem}
                keyExtractor={(item, index) => index.toString()}
            />
            <Loading visible={this.state.isLoading} />
      </View>
    );
  }
}
