import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { CartItemListCard } from '../components/CartItemListCard'
import { observer, inject } from 'mobx-react';
@inject('naviStore')
@observer
export default class MarketCartScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }
    confirm(){
        this.props.naviStore.navigation.dismissLightBox();
        this.props.naviStore.navigation.push({
            screen: "esgn.MarketDealResultScreen", // unique ID registered with Navigation.registerScreen
            passProps: {},
            style: {
                backgroundBlur: 'dark', // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
                tapBackgroundToDismiss: true // dismisses LightBox on background taps (optional)
            }
        })
    }
    renderContent() {
        let itemList = {
            totalCost: '1,590',
            unit: '฿',
            items: [
                {
                    imgUri: require('../sources/images/item_img01.png'),
                    title: 'ธนูเทพแสงแปดมาร',
                    prize: '790',
                    total: '1',
                },
                {
                    imgUri: require('../sources/images/item_img01.png'),
                    title: 'ธนูเทพแสงแปดมาร',
                    prize: '790',
                    total: '1',
                },
            ]
        }

        return (
            <CartItemListCard
                confirm={()=>this.confirm()}
                itemList={itemList}
                style={styles.cartItemListCardStyle}
            />
        )
    }


    render() {

        return (
            <View style={styles.marketCartScreenContainerStyle}>
                {/* <Headers
                    title='Market'
                    leftName='home'
                    rightName='searchData'
                /> */}
                {this.renderContent()}
            </View>
        )
    }
}

const styles = {
    marketCartScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    cartItemListCardStyle: {
        marginTop: responsiveHeight(4),
    }
}