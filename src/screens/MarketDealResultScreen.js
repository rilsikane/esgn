import React, { Component } from 'react'
import { View, Text, FlatList, ScrollView, TouchableOpacity, Image } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { ItemTitleCard } from '../components/ItemTitleCard'
import { observer, inject } from 'mobx-react';
import {convertCurrency,convertNumber} from '../components/utils/number';
@inject('naviStore')
@observer
export default class MarketDealResultScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderItemList() {
       

        return (
            <FlatList
                data={this.props.itemList ? [...this.props.itemList,{}]:[]}
                renderItem={this.renderGameItem}
                keyExtractor={(item, index) => index.toString()}
                numColumns={2}
            />
        )
    }
    giveFeedBack(item){
        this.props.navigator.showLightBox({
            screen: "esgn.ItemRatingScreen", // unique ID registered with Navigation.registerScreen
            passProps: {data:item},
            style: {
                backgroundBlur: 'dark', // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
                tapBackgroundToDismiss: true // dismisses LightBox on background taps (optional)
            }
        })
    }
    renderGameItem = ({ item }) => (
       
        <ItemTitleCard
            feedback={()=>this.giveFeedBack(item)}
            imgUri={{uri:item.image}}
            title={item.name}
            desc={item.description}
            isFavorite={item.is_addedwish}
            prize={convertNumber(item.price)}
            rating={Number(item.avg_rate)}
            mark={item.highlight!="0"}
            bought={true}
            viewType={'item'}
            style={styles.ItemTitleCardStyle}
            isReview={this.props.type=="recent"}
        />
    )

    render() {
        return (
            <View style={styles.marketDealResultScreenContainerStyle}>
                <Headers
                    title='Market'
                    leftName='home'
                    rightName='searchData'
                />
                <Text style={styles.titleTextStyle}>ซื้อขายสำเร็จแล้ว</Text>
                <View style={styles.itemListContainerStyle}>
                    {this.renderItemList()}
                </View>
                <View style={styles.footerContainerStyle}>
                    <TouchableOpacity style={styles.footerButtonStyle} onPress={()=>{
                        this.props.naviStore.navigation.resetTo({
                            screen: 'esgn.InventoryHomeScreen', // unique ID registered with Navigation.registerScreen
                            title: undefined, // navigation bar title of the pushed screen (optional)
                            titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
                            animated: true, // does the push have transition animation or does it happen immediately (optional)
                            backButtonTitle: undefined, // override the back button title (optional)
                            backButtonHidden: false, // hide the back button altogether (optional)
                            animationType: 'fade',
                        })
                     }}>
                        <Image
                            source={require('../sources/icons/inventory_icon01.png')}
                            resizeMode='contain'
                            style={styles.footerIconStyle}
                        />
                        <Text style={styles.footerTextStyle}>  Go Inventory</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.footerButtonStyle} onPress={()=>{
                        this.props.naviStore.navigation.resetTo({
                            screen: 'esgn.MarketHomeScreen', // unique ID registered with Navigation.registerScreen
                            title: undefined, // navigation bar title of the pushed screen (optional)
                            titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
                            animated: true, // does the push have transition animation or does it happen immediately (optional)
                            backButtonTitle: undefined, // override the back button title (optional)
                            backButtonHidden: false, // hide the back button altogether (optional)
                            animationType: 'fade',
                        })
                     }}>
                        <Image
                            source={require('../sources/icons/market_icon01.png')}
                            resizeMode='contain'
                            style={styles.footerIconStyle}
                        />
                        <Text style={styles.footerTextStyle}>  Shop More</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = {
    marketDealResultScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff",
        textAlign: 'center',
        marginTop: responsiveHeight(2),
    },
    itemListContainerStyle: {
        alignItems: 'center',
        flex: 1
    },
    gameMarketListContainerStyle: {
        paddingBottom: responsiveHeight(2),
        marginTop: responsiveHeight(1),
    },
    gameMarketContainerStyle: {
        marginBottom: responsiveHeight(1),
    },
    itemTitleCardStyle: {
        margin: responsiveWidth(1),
        width:responsiveWidth(40)
    },
    footerContainerStyle: {
        flexDirection: 'row',
        borderTopWidth: 1,
        borderColor: '#231f20',
    },
    footerButtonStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        height: responsiveHeight(10),
    },
    footerIconStyle: {
        width: responsiveHeight(2.9),
        height: responsiveHeight(2.9),
    },
    footerTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#ffffff"
    },

}