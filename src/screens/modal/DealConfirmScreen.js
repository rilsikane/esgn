import React, { Component } from 'react';
import { View, Text, TouchableWithoutFeedback } from 'react-native';
import { DealConfirmCard } from '../../components/DealConfirmCard'
import {get,post} from '../../api';
import Loading from '../../components/loading';
import { observer, inject } from 'mobx-react';
import { responsiveHeight,responsiveWidth } from 'react-native-responsive-dimensions';
@inject('naviStore')
@observer
export default class DealConfirmScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
        prizeList :{}
    };
  }
  componentDidMount(){
    let prizeList = {};
    prizeList.fee = Number(this.props.data.fee);
    prizeList.unit = '฿';
    let item = {};
    item.title = 'ราคาไอเทม';
    item.prize = Number(this.props.data.price);
    prizeList.prizes=[...[],item];
    prizeList.total = prizeList.fee+ prizeList.prizes[0].prize;
   // item_id
    this.setState({prizeList:prizeList})
  }
  async buy(){
    let response = await post(`market/buy_mpay/${this.props.data.item_id}`);
    if(response){
        console.log(response)
        this.props.naviStore.navigation.dismissLightBox({
            animationType: 'slide-down' // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
        });
        let data = {...this.props.data};
        data.invoice_id = response.invoice_id;
        setTimeout(()=>{
            this.props.naviStore.navigation.push({
                screen: "esgn.MPayScreen", // unique ID registered with Navigation.registerScreen
                passProps: {itemList:[...[],data],invoice_id:response.invoice_id,mpayUrl:response.url},
                style: {
                    backgroundBlur: 'dark', // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
                    tapBackgroundToDismiss: true // dismisses LightBox on background taps (optional)
                }
            })
        },500)
       
       
    }else{
        this.props.naviStore.navigation.dismissLightBox({
            animationType: 'slide-down' // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
        });
    }
  }

  
  render() {
  
    return (
    <TouchableWithoutFeedback  onPress={() => this.props.naviStore.navigation.dismissLightBox()}>
                <View style={{ width: responsiveWidth(100),
                height: responsiveHeight(100),
                backgroundColor: 'transparent',
                justifyContent: 'flex-end'}}>
                {this.state.prizeList.prizes && this.state.prizeList.prizes.length>0 && <DealConfirmCard
                    prizeList={this.state.prizeList} buy={()=>this.props.buy?this.props.buy():this.buy()}
                />}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}
