import React, { Component } from 'react';
import { View, Text,TouchableWithoutFeedback } from 'react-native';
import {ContactActionCard} from '../../components/ContactActionCard';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import { observer, inject } from 'mobx-react';

@inject('naviStore')
@observer
export default class FriendsActionScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  componentDidMount(){

  }

  render() {
    return (
        <TouchableWithoutFeedback onPress={() => this.props.naviStore.navigation.dismissLightBox()}>
            <View style={styles.multiTeamListModalContainerStyle}>
                <View style={styles.multiTeamListModalStyle}>
                    <ContactActionCard onSelect={(val)=>this.props.onSelect(val)}
                    actionType="user" title={this.props.data.nickname} isFriend={this.props.isFriend}/>
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
  }
}
const styles = {
    multiTeamListModalContainerStyle: {
        width: responsiveWidth(100),
        height: responsiveHeight(100),
        backgroundColor: 'transparent',
        justifyContent: 'flex-end',
    },
    multiTeamListModalStyle: {
        height: responsiveHeight(45),
        width: '100%',
        backgroundColor: 'transparent',
    },
    

}