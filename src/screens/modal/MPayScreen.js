import React, { Component } from 'react';
import { View, Text,WebView } from 'react-native';
import {Headers} from '../../components/Headers'
import { observer, inject } from 'mobx-react';
import { responsiveHeight } from 'react-native-responsive-dimensions';
import {get,post} from '../../api';
@inject('naviStore')
@observer
export default class MPayScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  async cancel(){
    let response = await get(`market/ispaid/${this.props.invoice_id ? this.props.invoice_id:this.props.transaction_id}`);
    if(response.is_paid==1){
        if(this.props.invoice_id){
            this.props.naviStore.navigation.resetTo({
                screen: "esgn.MarketDealResultScreen", // unique ID registered with Navigation.registerScreen
                passProps: {itemList:this.props.itemList},
                style: {
                    backgroundBlur: 'dark', // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
                    tapBackgroundToDismiss: true // dismisses LightBox on background taps (optional)
                }
            })
        }
    }else{
        this.props.naviStore.navigation.pop();
    }
  }

  render() {
    return (
        <View style={{flex:1}}>
             <Headers
                title={"Mpay"}
                leftName='cancel'
                cancel={()=>this.props.cancel ?this.props.cancel():this.cancel()}
            />
            <WebView 
            ref={(ref) => { this.videoPlayer = ref;}}
            allowsInlineMediaPlayback={true}
            mediaPlaybackRequiresUserAction={false}
            scalesPageToFit={true}
            style={{backgroundColor:"#fff",flex:1}} 
            source={{uri:this.props.mpayUrl}} //for iOS
            />
        </View>
    );
  }
}
