import React, { Component } from 'react';
import { View, Text,TouchableWithoutFeedback,TouchableOpacity } from 'react-native';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import { observer, inject } from 'mobx-react';
import { LabelInput } from '../../components/LabelInput'
@inject('naviStore')
@observer
export default class DonateScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
        gold:0
    };
  }
  renderTitle(actionType, title) {
    return (
        <View style={styles.titleContainerStyle}>
            <Text style={styles.labelTextStyle}>กรอก จำนวน Gold ที่ต้องการที่จะ Donate</Text>
        </View>
    )
}

  render() {
    return (
        <TouchableWithoutFeedback onPress={() => this.props.naviStore.navigation.dismissLightBox()}>
            <View style={styles.multiTeamListModalContainerStyle}>
                <View style={styles.multiTeamListModalStyle}>
                    <View style={styles.contactActionCardContainerStyle}>
                        {this.renderTitle()}
                        <View style={{flex:1,alignItems: 'center',}}>
                            <View style={styles.promoCodeContainerStyle}>
                                <Text style={styles.codeTitleTextStyle}>Gold</Text>
                                <LabelInput keyboardType='numeric' onChangeText={(val)=>this.setState({gold:val})} value={this.state.gold}/>
                            </View>
                            <TouchableOpacity onPress={()=>this.props.donate(this.state.gold)} style={styles.sellButtonStyle}>
                                <Text style={styles.submitButtonTextStyle}>{'Donate'}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
  }
}
const styles = {
    multiTeamListModalContainerStyle: {
        width: responsiveWidth(100),
        height: responsiveHeight(100),
        backgroundColor: 'transparent',
        justifyContent: 'center',
    },
    multiTeamListModalStyle: {
        height: responsiveHeight(45),
        width: '100%',
        backgroundColor: 'transparent',
    },
    contactActionCardContainerStyle: {
        width: responsiveWidth(100),
        height: responsiveHeight(30),
        backgroundColor: "#f6f6f6",
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
        padding: responsiveWidth(2.5),
    },
    titleContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: responsiveWidth(4),
        marginBottom: responsiveHeight(2.5),
    },
    labelTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#212221"
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#212221"
    },
    buttonStyle: {
        flexDirection: 'row',
        marginLeft: responsiveWidth(4),
        paddingBottom: responsiveHeight(2.5),
        paddingTop: responsiveHeight(2.5),
    },
    iconStyle: {
        width: responsiveHeight(2.9),
        height: responsiveHeight(2.9),
    },
    buttonTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#212221",
        marginLeft: responsiveWidth(10),
    },
    lineStyle: {
        height: 1,
        width: '100%',
        backgroundColor: "#212221"
    },
    sellButtonStyle: {
        width: responsiveWidth(50),
        height: responsiveHeight(6),
        backgroundColor: "#e42526",
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 3.3,
        justifyContent: 'center',
        marginLeft:5,
    },
    inputStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    promoCodeContainerStyle: {
        marginTop: responsiveHeight(2),
    },
    codeTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#414042",
        textAlign: 'center'
    },
    submitButtonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff"
    },
}
