import React, { Component } from 'react';
import { View, Text,TouchableWithoutFeedback,TouchableOpacity } from 'react-native';
import {ContactActionCard} from '../../components/ContactActionCard';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import { observer, inject } from 'mobx-react';

@inject('naviStore')
@observer
export default class FilterActionScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  componentDidMount(){

  }
  renderTitle(actionType, title) {
        return (
            <View style={styles.titleContainerStyle}>
                <Text style={styles.labelTextStyle}>Sort By</Text>
            </View>
        )
 }

renderActionList(actionType) {
    let userAction = userAction = [ {
        title: 'New coming',
        value:"new comming"
    },
    {
        title: 'Market price high to low',
        value:"price desc"
    },
    {
        title: 'Market price low to hight',
        value:"price asc"
    },
    {
        title: 'Name A-Z',
        value:"name asc"
    },
    {
        title: 'Name Z-A',
        value:"name desc"
    },
    ];
   
    

        return userAction.map((data, index) =>
            <View key={index}>
                <View style={styles.lineStyle} />
                <TouchableOpacity style={styles.buttonStyle} onPress={()=>this.props.onSelect(data.value)}>
                    <Text style={styles.buttonTitleTextStyle}>{data.title}</Text>
                </TouchableOpacity>
            </View>
        )
}

  render() {
    return (
        <TouchableWithoutFeedback onPress={() => this.props.naviStore.navigation.dismissLightBox()}>
            <View style={styles.multiTeamListModalContainerStyle}>
                <View style={styles.multiTeamListModalStyle}>
                    <View style={styles.contactActionCardContainerStyle}>
                        {this.renderTitle()}
                        {this.renderActionList()}
                    </View>
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
  }
}
const styles = {
    multiTeamListModalContainerStyle: {
        width: responsiveWidth(100),
        height: responsiveHeight(100),
        backgroundColor: 'transparent',
        justifyContent: 'center',
    },
    multiTeamListModalStyle: {
        height: responsiveHeight(45),
        width: '100%',
        backgroundColor: 'transparent',
    },
    contactActionCardContainerStyle: {
        width: responsiveWidth(100),
        height: responsiveHeight(50),
        backgroundColor: "#f6f6f6",
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
        padding: responsiveWidth(2.5),
    },
    titleContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: responsiveWidth(4),
        marginBottom: responsiveHeight(2.5),
    },
    labelTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#212221"
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#212221"
    },
    buttonStyle: {
        flexDirection: 'row',
        marginLeft: responsiveWidth(4),
        paddingBottom: responsiveHeight(2.5),
        paddingTop: responsiveHeight(2.5),
    },
    iconStyle: {
        width: responsiveHeight(2.9),
        height: responsiveHeight(2.9),
    },
    buttonTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#212221",
        marginLeft: responsiveWidth(10),
    },
    lineStyle: {
        height: 1,
        width: '100%',
        backgroundColor: "#212221"
    },

}