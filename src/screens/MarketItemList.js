import React, { Component } from 'react'
import { View, Text, Image, FlatList } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { MainTabs } from '../components/MainTabs'
import { FilterItemTitle } from '../components/FilterItemTitle'
import { ItemTitleCard } from '../components/ItemTitleCard'
import { GameTitleCard } from '../components/GameTitleCard'
import { observer, inject } from 'mobx-react';
import Loading from '../components/loading';
import {get,post} from '../api';
import {convertCurrency,convertNumber} from '../components/utils/number';
@inject('naviStore')
@observer
export default class MarketItemListHomeScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            viewType : 'list',isLoading:true,tabSelect:0,
            changeView:false,items:[]
        }
        this.onChangeViewType = this.onChangeViewType.bind(this);
    }
    componentDidMount(){
        this.init();    
    };
    async init(){
        if(this.props.type=="recent"){
            await this.getReviewItem();
        }else{
            await this.getTopRateItem();
        }
        
    }
    async getReviewItem(){
        this.setState({isLoading:true});
        let response = await get("market/recently_review_items");
        this.setState({isLoading:false});
        if(response){
            this.setState({items:response.items})
        }
    }
    async getTopRateItem(){
        this.setState({isLoading:true});
        let response = await get("market/top_rating_items");
        this.setState({isLoading:false});
        if(response){
            this.setState({items:response.items})
        }
    }
    
    
    onChangeViewType(){
        this.setState({changeView:true,isLoading:true});
        setTimeout(()=>{
            if(this.state.viewType=='list'){
                this.setState({viewType:'item',changeView:false,isLoading:false});
            }else{
                this.setState({viewType:'list',changeView:false,isLoading:false});
            }
        },100)
        
    }
    dealConfirm(item) {
        this.props.naviStore.navigation.showLightBox({
            screen: "esgn.DealConfirmScreen", // unique ID registered with Navigation.registerScreen
            passProps: {data:item},
            style: {
                tapBackgroundToDismiss: true,
                backgroundBlur: 'dark', // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
            }
        })
    }
    
   
    goToDetail(link,item){
        if(link)
        {
            this.props.naviStore.navigation.push({
                screen: link, // unique ID registered with Navigation.registerScreen
                title: undefined, // navigation bar title of the pushed screen (optional)
                titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
                animated: true, // does the push have transition animation or does it happen immediately (optional)
                backButtonTitle: undefined, // override the back button title (optional)
                backButtonHidden: false, // hide the back button altogether (optional)
                animationType: 'fade',
                passProps:{item:item}
            })
        }
    }
    renderItemList() {
        console.log("marketItemList",this.state.items);
        return (
            <FlatList
                data={this.state.items}
                renderItem={this.renderGameItem}
                keyExtractor={(item, index) => index.toString()}
                numColumns={this.state.viewType=='item'?2:1}
            />
        )

    }
    async wishlistItem(item){
       
        if(item.is_addedwish==0){
            let response = await post(`market/add_to_wishlist/${item.item_id}`);
            this.setState({isLoading:false});
            if(response){
                let tmpItems = [...this.state.items];
                let index = this.state.items.findIndex(tmp=>tmp.item_id==item.item_id);
                tmpItems[index].is_addedwish = 1;
                this.setState({items:tmpItems}); 
            }
        }else{
            let response =  await post(`market/delete_from_wishlist/${item.item_id}`);
            this.setState({isLoading:false});
            if(response){
                let tmpItems = [...this.state.items];
                let index = this.state.items.findIndex(tmp=>tmp.item_id==item.item_id);
                tmpItems[index].is_addedwish = 0;
                this.setState({items:tmpItems}); 
            }
        }
    }
    renderGameItem = ({ item }) => (
        <ItemTitleCard
            onPress={()=>!this.props.isReview && this.goToDetail("esgn.MarketItemDetailScreen",item)}
            imgUri={{uri:item.image}}
            title={item.name}
            desc={item.description}
            isFavorite={item.is_addedwish==1}
            prize={convertNumber(item.price)}
            rating={Number(item.avg_rate)}
            mark={item.highlight!="0"}
            bought={item.bought}
            viewType={this.state.viewType}
            style={styles.ItemTitleCardStyle}
            isReview={this.props.isReview}
            isOwner={this.props.type=="recent" || item.owner==1}
            onBuySellPress={()=>this.dealConfirm(item)}
            wishlist={()=>this.wishlistItem(item)}
        />
    )
    
    renderContent(){
        return (<View style={styles.itemListContainerStyle}>
            <View style={styles.gameItemListContainerStyle}>
                {!this.state.changeView && this.renderItemList()}
            </View>
        </View>)
    }
   
    render() {
        return (
            <View style={styles.marketItemListHomeScreenContainerStyle}>
                {!this.state.changeView && this.renderContent()}
                <Loading visible={this.state.isLoading} />
            </View>
        )
    }
}

const styles = {
    marketItemListHomeScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    tabContentContainerStyle: {
        paddingTop: 0,
    },
    gameCoverImgStyle: {
        width: responsiveWidth(100),
        height: responsiveHeight(30),
    },
    filterTitleStyle: {
    },
    itemListContainerStyle: {
        paddingLeft: responsiveWidth(2.5),
        paddingRight: responsiveWidth(2.5),
    },
    ItemTitleCardStyle: {
        margin: responsiveWidth(1),
    },
    gameItemListContainerStyle: {
        alignItems: 'center',
    },
    byGameTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#ffffff",
        marginTop: responsiveHeight(2),
    },
    gameTitleCardContainerStyle: {
        marginRight: responsiveWidth(4),
        height:responsiveHeight(2)
    },

}