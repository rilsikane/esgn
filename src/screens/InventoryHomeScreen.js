import React, { Component } from 'react'
import { View, Text, FlatList, ScrollView,Alert } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { MenuButtonGroup } from '../components/MenuButtonGroup'
import { GameTitleCard } from '../components/GameTitleCard'
import { ItemTitleCard } from '../components/ItemTitleCard'
import { FilterItemTitle } from '../components/FilterItemTitle'
import { observer, inject } from 'mobx-react';
import Loading from '../components/loading';
import {get,post} from '../api';
@inject('naviStore')
@observer
export default class InventoryHomeScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            changeView:false,
            isLoading:false,
            viewType : 'item',
            inventory:{},cats:[],game_id:0
        }
        this.onChangeViewType = this.onChangeViewType.bind(this);
    }
    async componentDidMount(){
        this.setState({isLoading:true});
        let [response,catRes] = await Promise.all([get("inventory/items?sort_by=new "),get("game/getgames")]);
        this.setState({isLoading:false});
        if(response){
            this.setState({inventory:response})
        }
        if(catRes){
            this.setState({cats:catRes.result})
        }
    };
    async initInventory(){
        this.setState({isLoading:true});
        let response = await get("inventory/items?sort_by=new ");
        this.setState({isLoading:false});
        if(response){
            this.setState({inventory:response})
        }
    }
    renderMenuButtonList() {
        let menus = [
            {
                iconUri: require('../sources/icons/live_icon01.png')
            },
            {
                iconUri: require('../sources/icons/trophy_icon01.png')
            },
            {
                iconUri: require('../sources/icons/market_icon01.png')
            },
            {
                iconUri: require('../sources/icons/news_icon01.png')
            },
        ]

        return (
            < MenuButtonGroup
                buttonList={menus}
                headingType='image'
            />
        )

    }

    renderGameList() {
        
        return (
            <FlatList
                data={this.state.cats}
                renderItem={this.renderGameTitleItem}
                keyExtractor={(item, index) => index.toString()}
                horizontal
                contentContainerStyle={styles.gameListContainerStyle}
                style={styles.gameContainerStyle}
            />
        )
    }

    renderGameTitleItem = ({ item }) => (
        <GameTitleCard
            onPress={()=>this.filterByGame(item)}
            imgUri={{uri:item.image_url}}
            iconUri={require('../sources/icons/inventory_icon02.png')}
            title={item.name}
            count={item.count}
            countUnit='item'
            style={styles.gameTitleCardContainerStyle}
        />
    )


    renderItemList() {
        
        return (
            <FlatList
                data={this.state.inventory.items}
                renderItem={this.renderGameItem}
                keyExtractor={(item, index) => index.toString()}
                numColumns={this.state.viewType=='item'?2:1}
                contentContainerStyle={styles.gameItemListContainerStyle}

            /> 
        )

    }
    buyMore(){
        this.props.naviStore.navigation.showLightBox({
            screen: "esgn.BuyMoreModal", // unique ID registered with Navigation.registerScreen
            passProps: {},
            style: {
                backgroundBlur: 'dark', // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
                tapBackgroundToDismiss: true // dismisses LightBox on background taps (optional)
            }
        })
    }
    renderGameItem = ({ item }) => (
        <ItemTitleCard
            onPress={()=>item.buyMore ? this.buyMore():this.goToDetail("esgn.InventoryItemDetailScreen",item)}
            imgUri={{uri:item.image}}
            title={item.name}
            desc={item.description}
            viewType={this.state.viewType}// list,item
            moduleType='inventory'
            timeLeft={item.timeLeft}
            style={styles.ItemTitleCardStyle}
            buyMore={item.buyMore}
            onBuySellPress={()=>this.sell(item)}
            status={item.status}
        />
    )
    goToDetail(link,item){
        if(link)
        {
            this.props.naviStore.navigation.push({
                screen: link, // unique ID registered with Navigation.registerScreen
                title: undefined, // navigation bar title of the pushed screen (optional)
                titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
                animated: true, // does the push have transition animation or does it happen immediately (optional)
                backButtonTitle: undefined, // override the back button title (optional)
                backButtonHidden: false, // hide the back button altogether (optional)
                animationType: 'fade',
                passProps:{item:item}
            })
        }
    }
    onChangeViewType(){
        this.setState({changeView:true,isLoading:true});
        setTimeout(()=>{
            if(this.state.viewType=='list'){
                this.setState({viewType:'item',changeView:false,isLoading:false});
            }else{
                this.setState({viewType:'list',changeView:false,isLoading:false});
            }
        },100)
        
    }
    async onSearch(val){
        this.setState({isLoading:true});
        let response = await get(`inventory/items?search_name=${val}`);
        this.setState({isLoading:false});
        if(response){
            this.setState({inventory:response})
        }
    }
    async filterByGame(item){
        this.setState({isLoading:true});
        let response = {};
        if(item.game_id!="0"){
            response = await get(`inventory/items?games=["${item.game_id}"]`);
        }else{
            response = await get(`inventory/items?sort_by=new`);
        }
        this.setState({isLoading:false});
        if(response){
            this.setState({inventory:response,game_id:item.game_id})
        }
    }
    async filterBy(val){
        this.props.naviStore.navigation.dismissLightBox();
        this.setState({isLoading:true});
        let response = {};
        response = await get(`inventory/items?sort_by=${val}`);
        this.setState({isLoading:false});
        if(response){
            this.setState({inventory:response})
        }
    }
    sell(item){
        if(item.status!="selling"){
            this.props.naviStore.navigation.showLightBox({
                screen: "esgn.InventorySellingModal", // unique ID registered with Navigation.registerScreen
                passProps: {data : item,complete:()=>{
                    this.props.naviStore.navigation.dismissLightBox();
                    setTimeout(()=>{
                        this.initInventory();
                    },100)
                }},
                style: {
                    backgroundBlur: 'dark', // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
                    tapBackgroundToDismiss: true // dismisses LightBox on background taps (optional)
                }
            })
        }else{
            Alert.alert(
                " ",
                "Do yo want to cancel selling this item?",
                [
                {text: "Close"},
                {text: "Confirm", onPress: ()=> {
                    this.cancelItem(item);
                }},
                ],
                { cancelable: false }
              )
        }
    }
    
    async cancelItem(item){
        let response = await post(`market/cancel_sell/${item.item_id}`);
        console.log(response);
        if(response){
            Alert.alert(
                " ",
                "Cancel Completed",
            [
            {text: "Ok", onPress: ()=> {
                this.initInventory();
            }},
            ],
            { cancelable: false }
            )
        }
    }
    async redeemItem(val){
        this.props.naviStore.navigation.dismissLightBox();
        let response = await post(`inventory/redeem?code=${val}`);
        this.setState({isLoading:false});
        if(response){
            Alert.alert(
                " ",
                "Redeem completed",
                [
                {text: "Ok", onPress: ()=> {
                    this.filterByGame({game_id:"0"})
                }},
                ],
                { cancelable: false }
            )
        }
    }
    render() {
        return (
            <View style={styles.inventoryHomeScreenContainerStyle}>
                 {!this.props.isHome && <Headers
                    title='Inventory'
                    leftName='home'
                    rightName='searchData'
                    onSearch={(val)=>this.onSearch(val)}
                />}
                
                    {/* {this.renderMenuButtonList()} */}
                    <View style={styles.inventoryHomeContainerStyle}>
                        <Text style={styles.byGameTitleTextStyle}>Inventory By : Game</Text>
                        <View style={{height:responsiveHeight(20)}}>
                            {this.renderGameList()}
                        </View>
                        <FilterItemTitle
                            title={`ALL INVENTORY (${this.state.inventory.item_count||0}/${this.state.inventory.inventory_slot||0}) : >`}
                            viewType={this.state.viewType} // item, list
                            isInventory={true}
                            onChangeViewType={this.onChangeViewType}
                            onFilter={()=>{
                                this.props.naviStore.navigation.showLightBox({
                                    screen: 'esgn.FilterActionScreen',
                                    passProps: {
                                       onSelect:(val)=>this.filterBy(val)
                                    },
                                    style: {
                                        backgroundBlur: "dark",
                                        tapBackgroundToDismiss: true,
                                    },
                                })
                            }}
                            addItem={()=>{
                                this.props.naviStore.navigation.showLightBox({
                                    screen: "esgn.ReedeemCodeScreen", // unique ID registered with Navigation.registerScreen
                                    passProps:{redeemItem:(val)=>this.redeemItem(val)},
                                    style: {
                                        backgroundBlur: 'dark', // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
                                        tapBackgroundToDismiss: true // dismisses LightBox on background taps (optional)
                                    }
                                })
                            }}
                        />
                        <ScrollView style={styles.itemListContainerStyle}>
                            {!this.state.changeView && this.renderItemList()}
                        </ScrollView>
                    </View>
                    <Loading visible={this.state.isLoading} />
            </View>
        )
    }
}

const styles = {
    inventoryHomeScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    inventoryHomeContainerStyle: {
        padding: responsiveWidth(2.5),
    },
    gameTitleCardContainerStyle: {
        marginRight: responsiveWidth(4),
    },
    byGameTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#ffffff",
        marginTop: responsiveHeight(2),
    },
    gameListContainerStyle: {
        paddingBottom: responsiveHeight(2),
        marginTop: responsiveHeight(1),
    },
    gameContainerStyle: {
        marginBottom: responsiveHeight(1),
    },
    itemListContainerStyle: {
        height:responsiveHeight(60)
    },
    gameItemListContainerStyle: {
        paddingBottom: responsiveHeight(2),
        flex:1
    },
    ItemTitleCardStyle: {
        margin: responsiveWidth(1),
    },

}