import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image, ScrollView } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { TeamMatchCard } from '../components/TeamMatchCard'
import { SocialButtonGroup } from '../components/SocialButtonGroup'
import { MainTabs } from '../components/MainTabs'
import { TeamMatchingCard } from '../components/TeamMatchingCard'
import { CommentInput } from '../components/CommentInput'
import { ImageCustom } from '../components/ImageCustom'
import { CommentCard } from '../components/CommentCard'
import { ReportForm } from '../components/ReportForm'
import {get,post} from '../api';
import moment from 'moment';
import Carousel from 'react-native-looped-carousel';
import Loading from '../components/loading';
export default class MatchDetailScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isLoading:false,match_comments:[],appointments:[]
        }
        this.onChangeTab = this.onChangeTab.bind(this);
    }
    componentDidMount(){
        this.init();
    }
    async init(){
        this.setState({isLoading:true});
        let [commentResp,matchResp] = await Promise.all([get(`match/${this.props.data.match_id}/comments`)
                                      ,get(`match/${this.props.data.match_id}/appointments`)]);
        this.setState({isLoading:false});
        if(commentResp){
            this.setState({match_comments:commentResp.match_comments});
        }
        if(matchResp){
            this.setState({appointments:matchResp.appointment_messages});
        }
    }

    renderMatchDetailTabList() {
        let tabData = [
            {
                title: 'Fixture',
            },
            {
                title: 'Result',
            },
            {
                title: 'Report',
            },
        ]

        return (
            <MainTabs
                tabData={tabData}
                child={this.renderTabChild()}
                headingType='text'
                onChangeTab={this.onChangeTab}
            />
        )
    }
    async onChangeTab(index){
        
    }

    renderTabChild() {
        return [
            <ScrollView style={styles.fixtureContainerStyle}>
                {this.state.appointments.length >0 &&<View style={styles.fixtureTitleContainerStyle}>
                    <Text style={styles.fixtureTitleTextStyle}>นัดแข่ง</Text>
                    {/* <TouchableOpacity style={styles.addGameNoContainerStyle}>
                        <Image
                            source={require('../sources/icons/add_icon01.png')}
                            resizeMode='contain'
                            style={styles.addGameNoIconStyle}
                        />
                        <Text style={styles.addGameNoTextStyle}>Add No. of GAME</Text>
                    </TouchableOpacity> */}
                </View>}
                {/* {this.state.appointments.length >0 && this.renderTeamMatching()} */}
                {this.state.appointments.length >0 && this.renderMatchComments()}
                {/* <CommentInput
                    style={styles.commentInputStyle}
                /> */}
                <Loading visible={this.state.isLoading}/>
            </ScrollView>,
            <ScrollView style={styles.resultContainerStyle}>
                {/* <Text style={styles.resultTitleTextStyle}>CHOOSE THE WINNER</Text>
                <TeamMatchCard
                    coverImgUri={require('../sources/images/game_cover_img04.png')}
                    t1ImgUri={require('../sources/images/team_img01.png')}
                    t1Name='Team OT'
                    t1Desc='Vestibulum augue'
                    t2ImgUri={require('../sources/images/team_img02.png')}
                    t2Name='Team HF'
                    t2Desc='Vestibulum augue'
                    // winnerName
                    // winnerImgUri
                    // winnerDesc
                    //renderWinner
                    hideCover={true}
                /> */}
                <View style={styles.resultTitleContainerStyle}>
                    <Text style={styles.fixtureTitleTextStyle}>MATCH SCORE</Text>
                    <TouchableOpacity style={styles.addGameNoContainerStyle}>
                        <Image
                            source={require('../sources/icons/add_icon01.png')}
                            resizeMode='contain'
                            style={styles.addGameNoIconStyle}
                        />
                        <Text style={styles.addGameNoTextStyle}>Add No. of GAME</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.resultContentStyle}>
                    {this.renderMatchScoreList()}
                    {/* <View style={styles.screenShortContainerStyle}>
                        <Text style={styles.screenShortTitleTextStyle}>SCREENSHOT</Text>
                        <View style={styles.scrShortImgStyle}>
                            <Carousel
                                delay={2000}
                                style={{flex:1}}
                                autoplay
                                onAnimateNextPage={(p) => console.log(p)}
                                >
                                <Image
                                    source={require('../sources/images/scrshort_img01.png')}
                                    style={{width:responsiveWidth(100),height:responsiveHeight(40)}}
                                />
                            </Carousel>
                        </View> */}
                        {/* {this.renderScreenShortImageButton()}
                        <Text style={styles.commentTitleTextStyle}>Comment</Text>
                        {this.renderResultCommentList()}
                        <CommentInput
                            style={styles.commentInputStyle}
                        /> */}
                    {/* </View> */}
                </View>
                <Loading visible={this.state.isLoading}/>
            </ScrollView>,
            <ScrollView>
                <ReportForm />
            </ScrollView>
        ]
    }

    renderResultCommentList() {
        let commentList = [
            {
                userImgUri: require('../sources/icons/avatar_icon01.png'),
                userName: 'Lankdard',
                timeLeft: '7 hours ago',
                desc: 'Suspendisse vestibulum scelerisque metus, ut biben dum lorem pharetra nec. Phasellus vitae faucibus nisi.'
            },
            {
                userImgUri: require('../sources/icons/avatar_icon01.png'),
                userName: 'Lankdard',
                timeLeft: '7 hours ago',
                desc: 'Suspendisse vestibulum scelerisque metus, ut biben dum lorem pharetra nec. Phasellus vitae faucibus nisi.'
            },
            {
                userImgUri: require('../sources/icons/avatar_icon01.png'),
                userName: 'Lankdard',
                timeLeft: '7 hours ago',
                desc: 'Suspendisse vestibulum scelerisque metus, ut biben dum lorem pharetra nec. Phasellus vitae faucibus nisi.'
            },
        ]

        return commentList.map((data, index) =>
            <CommentCard
                key={index}
                userImgUri={data.userImgUri}
                userName={data.userName}
                timeLeft={data.timeLeft}
                desc={data.desc}
                style={styles.resultCommentCardStyle}
            />
        )
    }

    renderScreenShortImageButton() {
        let imgList = [
            {
                imgUri: require('../sources/images/scrshort_img01.png'),
                title: 'Result Game#01',
                size: '345 KB',
            },
        ]

        return (
            <ImageCustom
                imgList={imgList}
            />
        )
    }

    renderMatchScoreList() {

        // let matchScore = [
        //     {
        //         t1Img: require('../sources/icons/team_icon03.png'),
        //         t1Name: 'Team GT',
        //         t1Rank: '#Ranking Team',
        //         t2Img: require('../sources/icons/team_icon04.png'),
        //         t2Name: 'Team WQ',
        //         t2Rank: '#Ranking Team',
        //         status: 'score',
        //         statusTime: '5h 43m',
        //         date: '15 July \'17',
        //         time: '19:00',
        //         gameCount: '1',
        //         t1Score: '3',
        //         t2Score: '0',
        //     },
        // ]
        let data = this.props.data;
        return (this.props.data.result && this.props.data.result.length>0) && this.props.data.result.games.map((item, index) =>
            <TeamMatchingCard
                key={index}
                t1Img={{uri:data.contestant[0].image}}
                t1Name={data.contestant[0].name}
                t1Rank={''}
                t2Img={{uri:data.contestant[1].image}}
                t2Name={data.contestant[1].name}
                t2Rank={''}
                status={"score"}
                statusTime={data.statusTime}
                date={moment(data.start_at).format("DD MMM YYYY, HH:mm")}
                time={moment(data.start_at).format("HH:mm")}
                gameCount={item.game_number}
                t1Score={item.first_contestant_score}
                t2Score={item.second_contestant_score}
            />
        )
    }

    renderTeamMatching() {
        let match = [
            {
                t1Img: require('../sources/icons/team_icon03.png'),
                t1Name: 'Team GT',
                t1Rank: '#Ranking Team',
                t2Img: require('../sources/icons/team_icon04.png'),
                t2Name: 'Team WQ',
                t2Rank: '#Ranking Team',
                status: 'coming',
                statusTime: '5h 43m',
                date: '15 July \'17',
                time: '19:00',
                gameCount: '1'

            },
        ]

        return this.state.appointments.map((data, index) =>
            <TeamMatchingCard
                key={index}
                t1Img={data.t1Img}
                t1Name={data.t1Name}
                t1Rank={data.t1Rank}
                t2Img={data.t2Img}
                t2Name={data.t2Name}
                t2Rank={data.t2Rank}
                status={data.status}
                statusTime={data.statusTime}
                date={data.date}
                time={data.time}
                gameCount={data.gameCount}
                canExpand={true}
                commentList={data.commentList}
            />
        )
    }
    renderMatchComments(){
        return this.state.appointments.map((data, index) =>
            <CommentCard
                key={index}
                userImgUri={{uri:data.picture}}
                userName={`${data.username} (${data.contestant.name})`}
                timeLeft={moment(data.send_at).fromNow()}
                desc={data.message}
                style={styles.resultCommentCardStyle}
            />
        )
    }

    render() {
        const data = this.props.data;
        return (
            <View style={styles.matchDetailScreenContainerStyle}>
                <Headers
                    title='Matchs'
                    leftName='back'
                    rightName='searchData'
                />
                <ScrollView>
                    <TeamMatchCard
                        coverImgUri={require('../sources/images/game_cover_img04.png')}
                        t1ImgUri={{uri:data.contestant[0].image}}
                        t1Name={data.contestant[0].name}
                        t1Desc=''
                        t2ImgUri={{uri:data.contestant[1].image}}
                        t2Name={data.contestant[1].name}
                        t2Desc=''
                        winner={data.winner_id==data.contestant[0].id ? "t1":"t2"}
                    //hideCover={true}
                    />
                    {/* <SocialButtonGroup
                        style={styles.socialButtonGroupContainerStyle}
                    /> */}
                    <View style={{marginLeft: 10,}}>
                        <View style={{flexDirection: 'row',}}>
                            <Text style={styles.matchDetailLabel}>
                                MATCH_ID
                            </Text>
                            <Text style={[styles.matchDetailLabel,{color:"#f8981d"}]}>
                                {`  ${data.match_id}`}
                            </Text>
                        </View>
                        <Text style={styles.matchDetailLabel}>
                            {`Date: ${moment(data.start_at).format("DD MM YYYY, HH:mm")} (${moment(data.start_at).fromNow()})`}
                        </Text>
                    </View>
                   
                    {this.renderMatchDetailTabList()}
                </ScrollView>
            </View >
        )
    }
}

const styles = {
    matchDetailScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    socialButtonGroupContainerStyle: {
        marginTop: responsiveHeight(1),
    },
    matchDetailLabel:{
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff",
    },
    matchDetailTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.6),
        color: "#ffffff",
        marginTop: responsiveHeight(1),
        marginLeft: responsiveWidth(2.5),
        marginRight: responsiveWidth(2.5),

    },
    fixtureContainerStyle: {
        width: responsiveWidth(100),
        paddingLeft: responsiveWidth(2.5),
        paddingRight: responsiveWidth(2.5),
    },
    resultContainerStyle: {
        flex:1
    },
    resultTitleContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: responsiveWidth(2.5),
        paddingRight: responsiveWidth(2.5),
    },
    resultTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff",
        marginLeft: responsiveWidth(2.5),
    },
    resultContentStyle: {
        alignItems: 'center',
    },
    screenShortContainerStyle: {
        width: '100%',

    },
    screenShortTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff",
        marginLeft: responsiveWidth(2.5),
        marginBottom: responsiveHeight(1),
    },
    scrShortImgStyle: {
        width: responsiveWidth(100),
        height: responsiveHeight(31)
    },
    commentTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff",
        marginLeft: responsiveWidth(2.5),
        marginTop: responsiveHeight(1),
    },
    fixtureTitleContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    fixtureTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff"
    },
    addGameNoContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    addGameNoIconStyle: {
        width: responsiveHeight(1.7),
        height: responsiveHeight(1.7),
    },
    addGameNoTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(1.4),
        color: "#1b75bc",
        marginLeft: responsiveWidth(2),
    },
    commentInputStyle: {
        marginTop: responsiveHeight(1),
        marginBottom: responsiveHeight(1),
        alignSelf: 'center',
    },
    resultCommentCardStyle: {
        alignSelf: 'center',
    }

}