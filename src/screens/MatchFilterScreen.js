import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image, ScrollView } from 'react-native'
import { CheckBox, Button } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'

export default class MatchFilterScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderFilterList() {
        let filters = [
            {
                title: 'Register Ends soon',
                type: 'checkbox',
            },
            {
                title: 'Hot Tournament',
                type: 'touchable',
                iconUri: require('../sources/icons/trophy_icon03.png'),
            },
            {
                title: 'Highest Prize',
                type: 'touchable',
                iconUri: require('../sources/icons/prize_icon01.png')
            },
        ]

        return filters.map((data, index) =>
            data.type == 'checkbox' ?
                <View key={data.title} style={styles.filterItemContainerStyle}>
                    <Text style={styles.filterTitleTextStyle}>{data.title}</Text>
                    <CheckBox
                        checked={true}
                        color='#e42526'
                        style={styles.checBoxStyle}
                    />
                </View> :
                <TouchableOpacity key={data.title} style={styles.filterItemContainerStyle}>
                    <Text style={styles.filterTitleTextStyle}>{data.title}</Text>
                    <Image
                        source={data.iconUri}
                        resizeMode='contain'
                        style={styles.filterIconStyle}
                    />
                </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={styles.matchFilterScreenContainerStyle}>
                <Headers
                    title='Filter'
                    leftName='home'
                    rightName='searchData'
                />
                <View style={styles.titleContainerStyle}>
                    <Text style={styles.sortLabelTextStyle}>Sorting by</Text>
                </View>
                <ScrollView style={styles.filterListContainerStyle}>
                    {this.renderFilterList()}
                </ScrollView>
                <Button rounded style={styles.submitButtonStyle}>
                    <Text style={styles.submitButtonTextStyle}>Submit</Text>
                </Button>
            </View>
        )
    }
}

const styles = {
    matchFilterScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    titleContainerStyle: {
        height: responsiveHeight(10),
        backgroundColor: "#414042",
        justifyContent: 'center',
    },
    sortLabelTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff",
        marginLeft: responsiveWidth(2.5),
    },
    filterListContainerStyle: {
        flex: 1,
    },
    filterItemContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: responsiveHeight(10),
        borderBottomWidth: 1,
        borderColor: '#474647',
        paddingLeft: responsiveWidth(2.5),
        paddingRight: responsiveWidth(2.5),

    },
    filterTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    checBoxStyle: {
        height: responsiveHeight(4),
        width: responsiveHeight(4),
        justifyContent: 'center',
        alignItems: 'center',
        left: 0,
        paddingLeft: 0,
    },
    filterIconStyle: {
        height: responsiveHeight(4),
        width: responsiveHeight(4),
    },
    submitButtonStyle: {
        width: responsiveWidth(85),
        height: responsiveHeight(8),
        backgroundColor: "#e42526",
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        marginBottom: responsiveHeight(5),
        marginTop: responsiveHeight(2),
    },
    submitButtonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    }
}