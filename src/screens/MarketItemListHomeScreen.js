import React, { Component } from 'react'
import { View, Text, Image, FlatList,ScrollView } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { MainTabs } from '../components/MainTabs'
import { FilterItemTitle } from '../components/FilterItemTitle'
import { ItemTitleCard } from '../components/ItemTitleCard'
import { GameTitleCard } from '../components/GameTitleCard'
import { observer, inject } from 'mobx-react';
import Loading from '../components/loading';
import {get,post} from '../api';
import {convertCurrency,convertNumber} from '../components/utils/number';
import MarketItemList from './MarketItemList';
@inject('naviStore')
@observer
export default class MarketItemListHomeScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            viewType : 'list',isLoading:true,tabSelect:0,game_id:0,
            changeView:false,items:[], cats:[],reviews:[],topRate:[]
        }
        this.onChangeViewType = this.onChangeViewType.bind(this);
    }
    componentDidMount(){
        this.init();    
    };
    async init(){
        let [response,catRes] = await Promise.all([get("market/items"),get("game/getgames")]);
        this.setState({isLoading:false});
        if(response){
            console.log(response.items);
            this.setState({items:response.items})
        }
        if(catRes){
            this.setState({cats:catRes.result})
        }
    }
    async getReviewItem(){
        this.setState({isLoading:true});
        let response = await get("market/recently_review_items");
        this.setState({isLoading:false});
        if(response){
            this.setState({reviews:response.items})
        }
    }
    async getTopRateItem(){
        this.setState({isLoading:true});
        let response = await get("market/top_rating_items");
        this.setState({isLoading:false});
        if(response){
            this.setState({topRate:response.items})
        }
    }
    onChangeTab(){
        this.setState({isLoading:true});
        this.setState({isLoading:false});
    }
    renderTabContent() {
        let tabData = [
            {
                title: 'New Listing',
                iconUri: require('../sources/icons/new_icon01.png'),
            },
            {
                title: 'Recent Reviews',
                iconUri: require('../sources/icons/category_icon01.png'),
            },
            {
                title: 'Top Rate',
                
                iconUri: require('../sources/icons/top_icon01.png'),
            },
        ]

        return (
            <MainTabs
                tabData={tabData}
                child={this.renderTabChild()}
                headingType='text-icon'
                contentContainerStyle={styles.tabContentContainerStyle}
                onChangeTab={()=>this.onChangeTab()}
                isNotScroll={true}
            />
        )
    }
    
    onChangeViewType(){
        this.setState({changeView:true,isLoading:true});
        setTimeout(()=>{
            if(this.state.viewType=='list'){
                this.setState({viewType:'item',changeView:false,isLoading:false});
            }else{
                this.setState({viewType:'list',changeView:false,isLoading:false});
            }
        },100)
        
    }
    renderMarketByGameList() {
        return (
            <FlatList
                data={this.state.cats}
                renderItem={this.renderGameMarkettItem}
                keyExtractor={(item, index) => index.toString()}
                horizontal
                contentContainerStyle={styles.gameMarketListContainerStyle}
                style={styles.gameMarketContainerStyle}
            />
        )
    }
    filterByGame(item){
        this.setState({isLoading:true});
        setTimeout(async ()=>{
            let response = {};
            if(item.game_id!="0"){
                response = await get(`market/search_items?games=["${item.game_id}"]`);
            }else{
                response = await get(`market/items`);
            }
            this.setState({isLoading:false});
            if(response){
                this.setState({items:response.items,game_id:item.game_id})
            }
        },200)
        
    }
    async filterBy(val){
        this.props.naviStore.navigation.dismissLightBox();
        this.setState({isLoading:true});
        let response = {};
        response = await get(`market/search_items?sort_by=${val}`);
        this.setState({isLoading:false});
        if(response){
            this.setState({items:response.items})
        }
    }
    renderGameMarkettItem = ({ item }) => (
        <GameTitleCard
            onPress={()=>this.filterByGame(item)}
            imgUri={{uri:item.image_url}}
            iconUri={require('../sources/icons/market_icon02.png')}
            title={item.name}
            count={item.itemCount}
            countUnit='item'
            style={styles.gameTitleCardContainerStyle}
        />
    )
    renderTabChild() {
        return [
            <View>
                
                <View style={styles.itemListContainerStyle}>
                <Text style={styles.byGameTitleTextStyle}>Market By : Game</Text>
                    <View style={{height:responsiveHeight(22)}}>
                        {this.renderMarketByGameList()}
                    </View>
                    <FilterItemTitle
                        title={`ALL Item : ${this.state.cats.length>0 && this.state.cats.find(cat=>cat.game_id==this.state.game_id).name}>`}
                        viewType={this.state.viewType} // item, list
                        style={styles.filterTitleStyle}
                        onChangeViewType={this.onChangeViewType}
                        onFilter={()=>{
                            this.props.naviStore.navigation.showLightBox({
                                screen: 'esgn.FilterActionScreen',
                                passProps: {
                                   onSelect:(val)=>this.filterBy(val)
                                },
                                style: {
                                    backgroundBlur: "dark",
                                    tapBackgroundToDismiss: true,
                                },
                            })
                        }}
                    />
                    <View style={styles.gameItemListContainerStyle}>
                        {!this.state.isLoading && !this.state.changeView && this.renderItemList()}
                    </View>
                </View>
            </View>,
             <MarketItemList type="recent" isReview={true}/>,
             <MarketItemList type="top"/>,
        ]
    }
    goToDetail(link,item){
        if(link)
        {
            this.props.naviStore.navigation.push({
                screen: link, // unique ID registered with Navigation.registerScreen
                title: undefined, // navigation bar title of the pushed screen (optional)
                titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
                animated: true, // does the push have transition animation or does it happen immediately (optional)
                backButtonTitle: undefined, // override the back button title (optional)
                backButtonHidden: false, // hide the back button altogether (optional)
                animationType: 'fade',
                passProps:{item:item}
            })
        }
    }
    renderItemList() {
      
        return (
            <FlatList
                data={this.state.items}
                renderItem={this.renderGameItem}
                keyExtractor={(item, index) => index.toString()}
                numColumns={this.state.viewType=='item'?2:1}
            />
        )

    }
    dealConfirm(item) {
        this.props.naviStore.navigation.showLightBox({
            screen: "esgn.DealConfirmScreen", // unique ID registered with Navigation.registerScreen
            passProps: {data:item},
            style: {
                tapBackgroundToDismiss: true,
                backgroundBlur: 'dark', // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
            }
        })
    }
   
    async wishlistItem(item){
        if(item.is_addedwish==0){
            let response = await post(`market/add_to_wishlist/${item.item_id}`);
            this.setState({isLoading:false});
            if(response){
                let tmpItems = [...this.state.items];
                let index = this.state.items.findIndex(tmp=>tmp.item_id==item.item_id);
                tmpItems[index].is_addedwish = 1;
                this.setState({items:tmpItems}); 
            }
        }else{
            let response =  await post(`market/delete_from_wishlist/${item.item_id}`);
            this.setState({isLoading:false});
            if(response){
                let tmpItems = [...this.state.items];
                let index = this.state.items.findIndex(tmp=>tmp.item_id==item.item_id);
                tmpItems[index].is_addedwish = 0;
                this.setState({items:tmpItems}); 
            }
        }
    }

    renderGameItem = ({ item }) => (
        <ItemTitleCard
            onPress={()=>this.goToDetail("esgn.MarketItemDetailScreen",item)}
            onBuySellPress={()=>this.dealConfirm(item)}
            wishlist={()=>this.wishlistItem(item)}
            imgUri={{uri:item.image}}
            title={item.name}
            desc={item.description}
            isFavorite={item.is_addedwish==1}
            prize={convertNumber(item.price)}
            rating={Number(item.avg_rate)}
            mark={item.highlight!="0"}
            viewType={this.state.viewType}
            style={styles.ItemTitleCardStyle}
            isOwner={item.owner==1}
        />
    )
    renderRecentItem = ({ item }) => (
        <ItemTitleCard
            wishlist={()=>this.wishlistItem(item)}
            imgUri={{uri:item.image}}
            title={item.name}
            desc={item.description}
            isFavorite={item.is_addedwish==1}
            prize={convertNumber(item.price)}
            rating={Number(item.avg_rate)}
            mark={item.highlight!="0"}
            bought={item.owner==1}
            viewType={this.state.viewType}
            style={styles.ItemTitleCardStyle}
            isReview={true}
            isOwner={item.owner==1}
        />
    )
    async onSearch(val){
        this.setState({isLoading:true});
        let response = await get(`market/search_items?search_name=${val}`);
        this.setState({isLoading:false});
        if(response){
            this.setState({items:response.items})
        }
    }
    render() {
        return (
            <View style={styles.marketItemListHomeScreenContainerStyle}>
                <Headers
                    title='Market'
                    leftName='home'
                    rightName='searchData'
                    onSearch={(val)=>this.onSearch(val)}
                />
                {!this.state.isLoading && !this.state.changeView && this.renderTabContent()}
                <Loading visible={this.state.isLoading} />
            </View>
        )
    }
}

const styles = {
    marketItemListHomeScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    tabContentContainerStyle: {
        paddingTop: 0,
    },
    gameCoverImgStyle: {
        width: responsiveWidth(100),
        height: responsiveHeight(30),
    },
    filterTitleStyle: {
    },
    itemListContainerStyle: {
        paddingLeft: responsiveWidth(2.5),
        paddingRight: responsiveWidth(2.5),
    },
    ItemTitleCardStyle: {
        margin: responsiveWidth(1),
    },
    gameItemListContainerStyle: {
        // alignItems: 'center',
        height:responsiveHeight(50)
    },
    byGameTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#ffffff",
        marginTop: responsiveHeight(2),
    },
    gameTitleCardContainerStyle: {
        marginRight: responsiveWidth(4),
        height:responsiveHeight(2)
    },
    gameMarketContainerStyle:{
        marginBottom: responsiveHeight(1),
    }

}