import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {ItemNotifyCard} from '../components/ItemNotifyCard';
import { observer, inject } from 'mobx-react';
@inject('naviStore')
@observer
export default class PromocodeModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  complete(){
   setTimeout(()=>{
    this.props.naviStore.navigation.resetTo({
      passProps: {itemList:[...[],this.props.data]},
        screen: "esgn.InventoryHomeScreen", // unique ID registered with Navigation.registerScreen
        title: undefined, // navigation bar title of the pushed screen (optional)
        titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
        animated: true, // does the push have transition animation or does it happen immediately (optional)
        backButtonTitle: undefined, // override the back button title (optional)
        backButtonHidden: false, // hide the back button altogether (optional)
        animationType: 'fade'
    })
  },500)
  }

  render() {
    return (
      <View style={{flex:1}}>
        <ItemNotifyCard title={this.props.item.name} imgUri={{uri:this.props.item.image}} complete={()=>this.complete()}
        viewType='check-code' code={this.props.code} desc="You can check serial code anytime in history"/>
      </View>
    );
  }
}
