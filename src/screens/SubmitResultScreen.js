import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { SubmitResultCard } from '../components/SubmitResultCard'

export default class SubmitResultScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { headerText, resultTitle } = this.props

        return (
            <View style={styles.submitResultScreenContainerStyle}>
                <Headers
                    title={headerText || 'HEADER'}
                    leftName='home'
                    rightName='searchData'
                />
                <SubmitResultCard
                    iconUri={require('../sources/icons/notify_icon01.png')}
                    title={resultTitle || 'Your order has been placed'}
                    style={styles.submitResultCardStyle}
                    //headerColor='red'
                />
            </View>
        )
    }
}

const styles = {
    submitResultScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    submitResultCardStyle: {
        marginTop: responsiveHeight(10),
    },

}
