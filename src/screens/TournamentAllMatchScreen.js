import React, { Component } from 'react'
import { View, Text, ScrollView, FlatList } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { GameHighLightCard } from '../components/GameHighLightCard'
import { TeamMatchingCard } from '../components/TeamMatchingCard'
import {get,post} from '../api';
import Loading from '../components/loading';
import moment from 'moment';
import { observer, inject } from 'mobx-react';
@inject('naviStore')
@observer
export default class TournamentAllMatchScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isLoading:false,matches:[]
        }
        this.goToDetail = this.goToDetail.bind(this);
    }
    componentDidMount = () => {
        this.init();
      
    };
    async init(){
        this.setState({isLoading:true});
        let matchResp = await get(`tournament/bracket/${this.props.tourId}/get_matches`);
        this.setState({isLoading:false});
        if(matchResp){
            this.setState({matches:matchResp.matches});
        }
    }

    renderMatchList() {
       

        return (
            <FlatList
                data={this.state.matches}
                renderItem={this.renderMatchItem}
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }

    renderMatchItem = ({ item }) => (
            <TeamMatchingCard
            onPress={()=>this.goToDetail("esgn.MatchDetailScreen",item)}
            isHome={true}
            t1Img={{uri:item.contestant[0].image}}
            t1Name={item.contestant[0].name}
            t1Rank={''}
            t2Img={{uri:item.contestant[1].image}}
            t2Name={item.contestant[1].name}
            t2Rank={''}
            status={item.status}
            />
    )
    goToDetail(link,item,isHot){
        if(link)
        {
            this.props.naviStore.navigation.push({
                screen: link, // unique ID registered with Navigation.registerScreen
                title: undefined, // navigation bar title of the pushed screen (optional)
                titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
                animated: true, // does the push have transition animation or does it happen immediately (optional)
                backButtonTitle: undefined, // override the back button title (optional)
                backButtonHidden: false, // hide the back button altogether (optional)
                animationType: 'fade',
                passProps:{data:item,type:isHot?"hot":undefined}
            })
        }
    }

    render() {
        return (
            <View style={styles.tournamentAllMatchScreenContainerStyle}>
                {/* <Headers
                    title='Matches'
                    leftName='back'
                    rightName='searchData'
                /> */}
                {/* <ScrollView style={{flex:1}}> */}
                    {/* <GameHighLightCard
                        imgUri={require('../sources/images/game_cover_img01.png')}
                        prize={'10,000 ฿'}
                        countdownTime={'22 Hrs 15 mins 54 sec'}
                        iconUri={require('../sources/icons/game_icon01.png')}
                        disabled
                        style={styles.gameCoverImgStyle}
                    /> */}
                    {/* <View style={styles.titleContainerStyle}>
                        <Text style={styles.titleTextStyle}>All Match</Text>
                        <Text style={styles.totalTournamentTextStyle}>Tournament ESGN#00344 (64)</Text>
                    </View> */}
                        {this.renderMatchList()}
                    <Loading visible={this.state.isLoading}/>
                {/* </ScrollView> */}
            </View>
        )
    }
}

const styles = {
    tournamentAllMatchScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    gameCoverImgStyle: {
        borderBottomWidth: 1,
        borderColor: '#6d6e71',
    },
    titleContainerStyle: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        paddingLeft: responsiveWidth(2.5),
        marginBottom: responsiveHeight(2),
    },
    totalTournamentContainerStyle: {
        flexDirection: 'row',
        alignItems: 'flex-end',
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff",
    },
    totalTournamentTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.4),
        color: "#bcbec0",
        marginLeft: responsiveWidth(2),
    },
    matchListContainerStyle: {
        alignItems: 'center',
        flex:1
    },

}
