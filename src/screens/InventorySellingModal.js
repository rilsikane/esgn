import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { ItemSellingCard } from '../components/ItemSellingCard';
import { observer, inject } from 'mobx-react';
import {get,post} from '../api';
@inject('naviStore')
@observer
export default class InventorySellingModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      totalPrice:0
    };
  }
  onInputPrice(val){
    this.setState({totalPrice:val})
  }
  async sell(){
    let response = await post(`inventory/sell/${this.props.data.item_id}?price=${this.state.totalPrice}`);
    if(response){
      this.props.complete();
    }
  }
  render() {
    const data = this.props.data;
    return (
      <View style={{flex:1}}>
        <ItemSellingCard totalPrice={this.state.totalPrice} onPress={this.props.onPress} title={data.name} imgUri={{uri:data.image}} 
        itemDesc={data.description} feeTotal={0.0}  feeCharge={0.0} onChangeText={(val) =>this.onInputPrice(val)} sell={()=>this.sell()}/>
      </View>
    );
  }
}
