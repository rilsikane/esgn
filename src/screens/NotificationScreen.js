import React, { Component } from 'react'
import { View, Text, FlatList } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { MainTabs } from '../components/MainTabs'
import { NotifyCard } from '../components/NotifyCard'
import { observer, inject } from 'mobx-react';
import store from 'react-native-simple-store'
import {get,post} from '../api';
import Loading from '../components/loading';
@inject('notiStore','naviStore')
@observer
export default class NotificationScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isLoading:false
        }
    }
    async componentDidMount() {
        this.setState({isLoading:true});
        let notiRes = await get("notification/my");
        this.setState({isLoading:false});
        if(notiRes){
            this.props.notiStore.unwatch = notiRes.unwatch_notfication_count;
            this.props.notiStore.notifications = notiRes.notifications;
        }
    }

    renderTabContent() {
        let tabData = [
            {
                title: 'Notification',
                iconUri: require('../sources/icons/notify_icon02.png')
            },
            {
                title: 'Chat',
                iconUri: require('../sources/icons/community_icon01.png')
            },
        ]

        return (
            <MainTabs
                tabData={tabData}
                child={this.renderTabChild()}
                headingType='text-icon'
            />
        )
    }

    renderTabChild() {
        return [
            <View style={styles.tabContainerStyle}>
                <Text style={styles.titleTextStyle}>Your notification In</Text>
                <Text style={styles.subTitleTextStyle}>All Activities on</Text>
                {this.renderNotifyList()}
            </View>,

        ]
    }

    renderNotifyList() {
        
        return (
            <FlatList
                data={this.props.notiStore.notifications}
                renderItem={this.renderNotifyItem}
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }
    async onNotiPress(item){
        let link = ''
        if(item.noti_type == "FRIEND_INVITE"){
            this.props.naviStore.navigation.resetTo({
                screen: 'esgn.ChatHomeScreen', // unique ID registered with Navigation.registerScreen
                title: undefined, // navigation bar title of the pushed screen (optional)
                titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
                animated: true, // does the push have transition animation or does it happen immediately (optional)
                backButtonTitle: undefined, // override the back button title (optional)
                backButtonHidden: false, // hide the back button altogether (optional)
                animationType: 'fade',
            })
        }else if(item.noti_type == "LIVE"){
            this.props.naviStore.navigation.resetTo({
                screen: 'esgn.LiveHomeScreen', // unique ID registered with Navigation.registerScreen
                title: undefined, // navigation bar title of the pushed screen (optional)
                titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
                animated: true, // does the push have transition animation or does it happen immediately (optional)
                backButtonTitle: undefined, // override the back button title (optional)
                backButtonHidden: false, // hide the back button altogether (optional)
                animationType: 'fade',
            })
        }else if(item.noti_type=="CHAT"){
            this.setState({isLoading:true});
            const user = await store.get("user");
            let response = await post(`chat/open/one_on_one?friend_id=${item.reference_id.replace(`-${user.user_id}`,'').split("-")[1]}`)
            this.setState({isLoading:false});
            if(response && response!=""){
                setTimeout(()=>{
                    this.props.naviStore.navigation.push({
                        passProps:{data:response.result,item:{nickname:item.message.split(" ")[0]}},
                        screen: 'esgn.ChatTalkScreen', // unique ID registered with Navigation.registerScreen
                        title: undefined, // navigation bar title of the pushed screen (optional)
                        titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
                        animated: true, // does the push have transition animation or does it happen immediately (optional)
                        backButtonTitle: undefined, // override the back button title (optional)
                        backButtonHidden: false, // hide the back button altogether (optional)
                        animationType: 'fade',
                    })
                },500)
            }
        }
    }

    renderNotifyItem = ({ item }) => (
        <NotifyCard
            imgUri={{uri:item.act_user_image}}
            title={item.act_username}
            desc={item.message}
            isOnline={item.isOnline}
            onPress={()=>this.onNotiPress(item)}
        />
    )

    render() {
        return (
            <View style={styles.notificationScreenContainerStyle}>
                <Headers
                    title='Notification'
                    leftName='home'
                    rightName='searchData'
                />
                {/* {this.renderTabContent()} */}
                <View style={styles.tabContainerStyle}>
                    <Text style={styles.titleTextStyle}>Your notification In</Text>
                    <Text style={styles.subTitleTextStyle}>All Activities on</Text>
                    {this.renderNotifyList()}
                </View>
                <Loading visible={this.state.isLoading} />
            </View>
        )
    }
}

const styles = {
    notificationScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    tabContainerStyle: {
        paddingLeft: responsiveWidth(2.5),
        paddingRight: responsiveWidth(2.5),
        width: '100%',
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff"
    },
    subTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#bcbec0"
    },

}