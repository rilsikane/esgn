import React, { Component } from 'react'
import { View, Image, Text } from 'react-native'
import { Button } from 'native-base'
import Carousel from 'react-native-looped-carousel'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import {getBasic} from '../api';
import FastImage from 'react-native-fast-image'
import Loading from '../components/loading'

export default class AppIntroScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            images:[],isLoading:true
        }
    }
    async componentDidMount(){
        let response = await getBasic("tutorial/get");
        if(response){
            let images = response.tutorials
            this.setState({images:images});
            setTimeout(()=>{
                this.setState({isLoading:false});
            },500)
        }
    }

    renderIntroImageList() {
       
        return this.state.images.map((data, i) =>
            <FastImage key={i} source={{uri:data.image_url}} resizeMode='stretch' style={styles.introImageStyle} />
        )
    }

    render() {
        return (
            <View style={styles.appIntroScreenContainerStyle}>
                <Loading visible={this.state.isLoading}></Loading>
                <View style={styles.appIntroCarouselContainerStyle}>
                    {this.state.images.length > 0 && 
                    <Carousel
                        delay={1000}
                        isLooped={false}
                        style={styles.carouselContainerStyle}
                        autoplay={false}
                        bullets
                        bulletStyle={styles.bulletStyle}
                        chosenBulletStyle={styles.chosenBulletStyle}
                        bulletsContainerStyle={styles.bulletsContainerStyle}
                    >
                        {this.renderIntroImageList()}
                    </Carousel>}
                </View>
                <Button style={styles.skipButtonStyle} onPress={()=>{
                        this.props.navigator.resetTo({
                            screen: 'esgn.StartScreen', // unique ID registered with Navigation.registerScreen
                            title: undefined, // navigation bar title of the pushed screen (optional)
                            passProps: {}, // simple serializable object that will pass as props to the pushed screen (optional)
                            animated: true, // does the resetTo have transition animation or does it happen immediately (optional)
                            animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the resetTo have different transition animation (optional)
                            navigatorStyle: {}, // override the navigator style for the pushed screen (optional)
                            navigatorButtons: {} // override the nav buttons for the pushed screen (optional)
                          });
                    }}>
                    <Text style={styles.skipButtonTextStyle}>SKIP ></Text>
                </Button>
            </View>
        )
    }
}

const styles = {
    appIntroScreenContainerStyle: {
        flex: 1,
        backgroundColor: 'rgb(33,34,33)',
        paddingLeft: responsiveWidth(2),
        paddingRight: responsiveWidth(2),
        paddingTop: responsiveHeight(5),
        alignItems: 'center',
    },
    appIntroCarouselContainerStyle: {
        width: responsiveWidth(96),
        height: responsiveHeight(85),
        alignContent: 'center',
    },
    carouselContainerStyle: {
        flex: 1
    },
    bulletStyle: {
        width: responsiveHeight(1.5),
        height: responsiveHeight(1.5),
        backgroundColor: "#c0bfbf",
        borderWidth: 0,
    },
    chosenBulletStyle: {
        backgroundColor: 'rgb(253,98,98)',
        width: responsiveHeight(1.5),
        height: responsiveHeight(1.5),
    },
    bulletsContainerStyle: {
        height: responsiveHeight(1),
    },
    introImageStyle: {
        width: responsiveWidth(96),
        height: responsiveHeight(80),
        borderWidth: 1,
        borderColor: '#C4C4C4',
    },
    skipButtonStyle: {
        width: responsiveWidth(33),
        height: responsiveHeight(5),
        borderRadius: 2,
        backgroundColor: "#e42526",
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: responsiveHeight(2),
    },
    skipButtonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    }
}
