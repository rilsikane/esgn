import React, { Component } from 'react'
import { View, Text,Alert } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { ItemPlaceOrderCard } from '../components/ItemPlaceOrderCard'
import {get,post} from '../api';
import { observer, inject } from 'mobx-react';
import momet from 'moment';
@inject('naviStore')
@observer
export default class MarketItemPlaceOrderScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            price :0
        }
    }
    async placeOrder(){
        let response = await post(`market/request_to_buy/${this.props.data.blueprint_id}
        ?price=${this.state.price}&expired_at=${momet().add(1,"week").toString}`)
       
        if(response){
            console.log(response);
            this.props.naviStore.navigation.dismissLightBox();
            Alert.alert(
                " ",
                "Place order completed",
                [
                {text: "Ok", onPress: ()=> {
                }},
                ],
                { cancelable: false }
              )
           
        }else{
            this.props.naviStore.navigation.dismissLightBox();
        }
    }
    render() {
        const {name,image,description,price,avg_rate} = this.props.data;
        return (
            <View style={styles.marketItemPlaceOrderScreenContainerStyle}>
                {/* <Headers
                    title='Market'
                    leftName='home'
                    rightName='searchData'
                /> */}

                <ItemPlaceOrderCard
                    state='buy' //buy,sell
                    title={name}
                    imgUri={{uri:image}}
                    rating={Number(avg_rate)}
                    info={`${description}`}
                    label='THB'
                    remark={`Minimum Price : ${price} THB`}
                    style={styles.itemPlaceOrderCardStyle}
                    onChangeText={(val)=>this.setState({price:val})}
                    placeOrder={()=>this.placeOrder()}
                />

            </View>
        )
    }
}

const styles = {
    marketItemPlaceOrderScreenContainerStyle: {
        flex: 1,
        // backgroundColor: '#212221',
    },
    itemPlaceOrderCardStyle: {
        marginTop: -responsiveHeight(7),
    },

}