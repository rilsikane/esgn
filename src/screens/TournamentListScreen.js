import React, { Component } from 'react'
import { View, Text, FlatList, TouchableOpacity } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { GameHighLightCard } from '../components/GameHighLightCard'
import { observer, inject } from 'mobx-react';
import {get,post} from '../api';
import Loading from '../components/loading';
import moment from 'moment';
import { MainTabs } from '../components/MainTabs';
import TournamentHilight from './TournamentHighLightListScreen';
@inject('naviStore')
@observer
export default class TournamentListScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading:false,
            tours:[]
        }
        this.goToDetail = this.goToDetail.bind(this);
    }
    componentDidMount = () => {
      this.init();
    };
    
    async init(){
        // this.setState({isLoading:true});
        // let tourResp = await get(`tournament/hot`);
        // this.setState({isLoading:false});
        // if(tourResp){
        //     console.log(tourResp);
        //     this.setState({tours:tourResp.tournaments});
        // }
        // if(matchResp){
        //     this.setState({appointments:matchResp.appointment_messages});
        // }
    }
    renderTabContent() {
        let tabData = [
            {
                title: '• Current',
            },
            {
                title: '• Upcoming',
            },
            {
                title: '• Past',
            },
        ]

        return (
            <MainTabs
                tabData={tabData}
                child={this.renderTabChild()}
                headingType='text'
            />
        )
    }
    renderTabChild() {

        return [
            <TournamentHilight type="ongoing" gameId={this.props.data.game_id}/>,
            <TournamentHilight type="upcoming" gameId={this.props.data.game_id}/>,
            <TournamentHilight type="completed" gameId={this.props.data.game_id}/>,
        ]
    }
    
    openDetail(){
        
    }
    renderGameHighLightList() {
        
        return (
            <FlatList
                data={this.state.tours}
                renderItem={this.renderGameHighLightItem}
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }
    goToDetail(item){
        this.props.naviStore.navigation.push({
            screen: "esgn.TournamentDetailScreen", // unique ID registered with Navigation.registerScreen
            title: undefined, // navigation bar title of the pushed screen (optional)
            titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
            animated: true, // does the push have transition animation or does it happen immediately (optional)
            backButtonTitle: undefined, // override the back button title (optional)
            backButtonHidden: false, // hide the back button altogether (optional)
            animationType: 'fade',
            passProps:{data:item}
        })
    }
    renderGameHighLightItem = ({ item }) => (
        <GameHighLightCard
            gotoDetail={()=>this.goToDetail(item)}
            imgUri={{uri:item.information.logo}}
            prize={item.information.prizepool}
            countdownTime={this.getCountDownTime(item.information.register_end_at)}
        />
    )
    getCountDownTime(time){
        let remain = "";
        remain = moment(time).fromNow();
        return `${remain.replace("ago","")}`;
    }

    render() {
        return (
            <View style={styles.tournamentHighLightListScreenContainerStyle}>
                <Headers
                    title='Tournament'
                    leftName='back'
                    rightName='searchData'
                />
                 {this.renderTabContent()}
            </View>
        )
    }
}

const styles = {
    tournamentHighLightListScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    titleContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: responsiveWidth(2.5),
        paddingRight: responsiveWidth(2.5),
        paddingTop: responsiveHeight(2),
        marginBottom: responsiveHeight(2),
    },
    totalTournamentContainerStyle: {
        flexDirection: 'row',
        alignItems: 'flex-end',
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff",
    },
    totalTournamentTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.4),
        color: "#bcbec0",
        marginLeft: responsiveWidth(2),
    },
    seeAllTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#5782c2"
    },
}