import React, { Component } from 'react';
import { View, Text,FlatList} from 'react-native';
import { FriendContactCard } from '../components/FriendContactCard'
import {get,post} from '../api';
import Loading from '../components/loading';
import store from 'react-native-simple-store'
import { observer, inject } from 'mobx-react';
import TimerMixin from 'react-timer-mixin';
import SocketIOClient from 'socket.io-client';
@inject('naviStore','notiStore')
@observer
export default class ChatroomScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
        isLoading:false,chats:[]
    };
   
    
   
  }
  async componentDidMount(){
    const user = await store.get("user");
   
    this.socket = SocketIOClient('https://dev-socket-apis.esgn-tc.com:13337');
    console.log(user.user_id);
    this.socket.on(`chatroom/${user.user_id}`, async (message) =>{
        let chatsTmp = [...this.state.chats];
        chatsTmp.map(c=>{
            if(c.chat_channel_id == message.chat_channel_id){
                c.unread_message_count += 1;
            }
            
        })
        
        this.setState({chats:chatsTmp});
        let notiRes = await get("notification/my");
        if(notiRes){
            this.props.notiStore.unwatch = notiRes.unwatch_notfication_count;
            this.props.notiStore.notifications = notiRes.notifications;
        }
    })
    await this.init();
    // this.timer = TimerMixin.setInterval( async () => {
    //     await this.init();
    // }, 5000);
    
  }
  componentWillUnmount(){
    // TimerMixin.clearInterval(this.timer);
  }
  async init(){

    let response = await get("chat/chatroom");
    if(response ){
        console.log(response);
        this.setState({chats:response.chatroom['one-on-one']});
    }
  }
 
  async _onPress(item){
    const user = await store.get("user");
    this.setState({isLoading:true});
    let response = await post(`chat/open/one_on_one?friend_id=${item.chat_channel_id.replace(`-${user.user_id}`,'').split("-")[1]}`)
    this.setState({isLoading:false});
    if(response && response!=""){
        setTimeout(()=>{
            this.props.naviStore.navigation.push({
                passProps:{data:response.result,item:item},
                screen: 'esgn.ChatTalkScreen', // unique ID registered with Navigation.registerScreen
                title: undefined, // navigation bar title of the pushed screen (optional)
                titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
                animated: true, // does the push have transition animation or does it happen immediately (optional)
                backButtonTitle: undefined, // override the back button title (optional)
                backButtonHidden: false, // hide the back button altogether (optional)
                animationType: 'fade',
            })
        },500)
        this.init();
    }
    
}
    _renderItem = ({ item }) => (
        <FriendContactCard
            imgUri={{uri:item.image}}
            name={item.name}
            desc={item.latest_message}
            data={item}
            onPress={()=>this._onPress(item)}
            badge={item.unread_message_count}
            actionType="chat"
        />
    )
  render() {
    return (
      <View style={{flex:1}}>
             <FlatList
                data={this.state.chats}
                renderItem={this._renderItem}
                keyExtractor={(item, index) => index.toString()}
            />
            <Loading visible={this.state.isLoading} />
      </View>
    );
  }
}
