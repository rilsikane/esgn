import React, { Component } from 'react'
import { View, Text, Image, ScrollView, FlatList } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { FilterItemTitle } from '../components/FilterItemTitle'
import { ItemTitleCard } from '../components/ItemTitleCard'

export default class InventoryByGameScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderItemList() {
        let item = [
            {
                imgUri: require('../sources/images/item_img01.png'),
                title: 'ดาบเจ็ดสีมณีเจ็ดแสง',
                desc: 'เลือกอย่างใด อย่างหนึ่ง',
                timeLeft: '08:05:24'
            },
            {
                imgUri: require('../sources/images/item_img02.png'),
                title: 'ดาบเจ็ดสีมณีเจ็ดแสง',
                desc: 'เลือกอย่างใด อย่างหนึ่ง',
                timeLeft: '08:05:24'
            },
            {
                imgUri: require('../sources/images/item_img01.png'),
                title: 'ดาบเจ็ดสีมณีเจ็ดแสง',
                desc: 'เลือกอย่างใด อย่างหนึ่ง',
                timeLeft: '08:05:24'
            },
        ]
        return (
            <FlatList
                data={item}
                renderItem={this.renderGameItem}
                keyExtractor={(item, index) => index.toString()}
                //numColumns={2} // viewType = list remove numColumns
                contentContainerStyle={styles.gameItemListContainerStyle}
            />
        )

    }

    renderGameItem = ({ item }) => (
        <ItemTitleCard
            imgUri={item.imgUri}
            title={item.title}
            desc={item.desc}
            viewType='list' // list,item
            moduleType='inventory'
            timeLeft={item.timeLeft}
            style={styles.ItemTitleCardStyle}
        />
    )

    render() {
        return (
            <View style={styles.inventoryByGameScreenContainerStyle}>
                <Headers
                    title='Inventory'
                    leftName='home'
                    rightName='searchData'
                />
                <ScrollView>
                    <View style={styles.coverImgStyle}>
                        <Image
                            source={require('../sources/images/game_cover_img10.png')}
                            style={styles.coverImgStyle}
                        />
                    </View>
                    <View style={styles.inventoryHomeContainerStyle}>
                        <FilterItemTitle
                            title='Inventory : BattleFi... (12/30)'
                            viewType='list' // item, list
                        />
                        <View style={styles.itemListContainerStyle}>
                            {this.renderItemList()}
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = {
    inventoryByGameScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    coverImgStyle: {
        width: responsiveWidth(100),
        height: responsiveHeight(25),
        alignItems: 'center',
    },
    inventoryHomeContainerStyle: {
        padding: responsiveWidth(2.5),
    },
    itemListContainerStyle: {
        alignItems: 'center'
    },
    gameItemListContainerStyle: {
        paddingBottom: responsiveHeight(2),
    },
    ItemTitleCardStyle: {
        margin: responsiveWidth(1),
    },
    
}