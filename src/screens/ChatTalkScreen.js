import React from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';

import {GiftedChat, Actions, Bubble, SystemMessage} from 'react-native-gifted-chat';
import CustomActions from '../components/CustomActions';
import { Headers } from '../components/Headers'
import store from 'react-native-simple-store'
import SocketIOClient from 'socket.io-client';
import {get,post} from '../api';
import moment from 'moment'
export default class ChatTalkScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      loadEarlier: true,
      typingText: null,
      isLoadingEarlier: false,
      user:{}
    };

    this._isMounted = false;
    this.onSend = this.onSend.bind(this);
    this.onReceive = this.onReceive.bind(this);
    this.renderCustomActions = this.renderCustomActions.bind(this);
    this.renderBubble = this.renderBubble.bind(this);
    this.renderSystemMessage = this.renderSystemMessage.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
    this.onLoadEarlier = this.onLoadEarlier.bind(this);

    this._isAlright = null;
    this.socket = SocketIOClient('https://dev-socket-apis.esgn-tc.com:13337');
    this.socket.on(this.props.data.chat_channel.chat_channel_id, (message) =>{
      console.log(message);
      if(message.writer_id!=this.state.user.user_id){
        this.setState({messages:[...this.state.messages,this.buildChat(undefined,message)]});
        
      }
    })
  }

  async componentDidMount() {
    this._isMounted = true;
    
    const user = await store.get("user");
    this.setState({messages:this.buildChat(this.props.data.history),user:user})
    
   
  }
  

  componentWillUnmount() {
    this._isMounted = false;
  }

  onLoadEarlier() {
    this.setState((previousState) => {
      return {
        isLoadingEarlier: true,
      };
    });

    setTimeout(() => {
      if (this._isMounted === true) {
        this.setState((previousState) => {
          return {
            messages: GiftedChat.prepend(previousState.messages, require('../data/old_messages.js')),
            loadEarlier: false,
            isLoadingEarlier: false,
          };
        });
      }
    }, 1000); // simulating network
  }

  async onSend(messages = []) {
    this.setState((previousState) => {
      return {
        messages: GiftedChat.append(messages,previousState.messages),
      };
    });
    console.log(messages);
    post(`chat/message/${this.props.data.chat_channel.chat_channel_id}?message=${messages[0].text}&message_type=1`);
  }
  buildChat(chatList,chat){
    if(chatList){
      console.log("chatList",chatList);
      let chats = chatList.map(c=>{
        let tmpChat ={
          _id: c.message_number,
          text: c.message,
          createdAt: moment(c.message_time,'yyyy-MM-dd HH:mm:ss').toDate(),
          user: {
            _id: c.writer_id,
            name: c.writer_name,
            avatar: c.writer_picture
          }
        }
        return tmpChat
      })
      return chats;
    }else if(chat){
      let tmpChat={
        _id: chat.message_number,
        text: chat.message,
        createdAt: moment(chat.message_time,'yyyy-MM-dd HH:mm:ss').toDate(),
        user: {
          _id: chat.writer_id,
          name: chat.writer_name,
          avatar: chat.writer_picture
        }
      }
      return tmpChat
    }
    
  }

  answerDemo(messages) {
    if (messages.length > 0) {
      if ((messages[0].image || messages[0].location) || !this._isAlright) {
        this.setState((previousState) => {
          return {
            typingText: 'ESGN is typing'
          };
        });
      }
    }

    setTimeout(() => {
      if (this._isMounted === true) {
        if (messages.length > 0) {
          if (messages[0].image) {
            this.onReceive('Nice picture!');
          } else if (messages[0].location) {
            this.onReceive('My favorite place');
          } else {
            if (!this._isAlright) {
              this.onReceive('เยี่ยมมาก');
            }
          }
        }
      }

      this.setState((previousState) => {
        return {
          typingText: null,
        };
      });
    }, 1000);
  }

  onReceive(text) {
    this.setState((previousState) => {
      return {
        messages: GiftedChat.append(previousState.messages, {
          _id: Math.round(Math.random() * 1000000),
          text: text,
          createdAt: new Date(),
          user: {
            _id: 2,
            name: 'ESGN',
            // avatar: 'https://facebook.github.io/react/img/logo_og.png',
          },
        }),
      };
    });
  }

  renderCustomActions(props) {
    if (Platform.OS === 'ios') {
      return (
        <CustomActions
          {...props}
        />
      );
    }
    const options = {
      'Action 1': (props) => {
        alert('option 1');
      },
      'Action 2': (props) => {
        alert('option 2');
      },
      'Cancel': () => {},
    };
    return (
      <Actions
        {...props}
        options={options}
      />
    );
  }

  renderBubble(props) {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          left: {
            backgroundColor: '#f0f0f0',
          }
        }}
      />
    );
  }

  renderSystemMessage(props) {
    return (
      <SystemMessage
        {...props}
        containerStyle={{
          marginBottom: 15,
        }}
        textStyle={{
          fontSize: 14,
        }}
      />
    );
  }

  renderCustomView(props) {
    return (
      <CustomView
        {...props}
      />
    );
  }

  renderFooter(props) {
    if (this.state.typingText) {
      return (
        <View style={styles.footerContainer}>
          <Text style={styles.footerText}>
            {this.state.typingText}
          </Text>
        </View>
      );
    }
    return null;
  }

  render() {
    return (
     <View style={{flex:1}}>
     <Headers
        leftName='back'
        title={this.props.item.nickname?this.props.item.nickname:this.props.data.chat_channel.name}
      />
      <View style={{backgroundColor: '#212221',flex:1}}>
        <GiftedChat
          style={{backgroundColor:'#212221'}}
          messages={this.state.messages.slice().reverse()}
          onSend={this.onSend}
          user={{
            _id: this.state.user.user_id,
            name:this.state.user.username
          }}
          // renderActions={this.renderCustomActions}
          renderBubble={this.renderBubble}
          renderFooter={this.renderFooter}
        />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  footerContainer: {
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    height:53
  },
  footerText: {
    fontSize: 14,
    color: '#aaa',
  },
});