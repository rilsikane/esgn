import React, { Component } from 'react'
import { View, Text, Image, ScrollView,Alert } from 'react-native'
import { Button } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { ItemShortDetailCard } from '../components/ItemShortDetailCard'
import { MarketTabs } from '../components/MarketTabs'
import { GraphCard } from '../components/GraphCard'
import { SocialButtonGroup } from '../components/SocialButtonGroup'
import { ItemDealDetailCard } from '../components/ItemDealDetailCard'
import { DealConfirmCard } from '../components/DealConfirmCard'
import { observer, inject } from 'mobx-react';
import {get,post} from '../api';
import Loading from '../components/loading';
import {convertCurrency,convertNumber} from '../components/utils/number';
@inject('naviStore')
@observer
export default class CasterItemDetailScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isLoading:false,itemDetail:{}
        }
    }
    async componentDidMount(){
        // const user = await store.get("user");
        // this.setState({isLoading:true});
        // // let response= await get("market/detail/"+this.props.item.item_id);
        // this.setState({isLoading:false,user:user});
        // if(response ){
        //     console.log(response);
        //     this.setState({itemDetail:response.item});
        // }
    }
    async cancel(transaction_id){
        let response = await get(`market/ispaid/${transaction_id}`);
        this.props.naviStore.navigation.dismissModal({
            animationType: 'slide-down' // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
        });
        if(response.is_paid==1){
            Alert.alert(
                " ",
                "ซื้อไอเทมสำเร็จ",
                [
                {text: "Ok", onPress: ()=> {
                   
                }},
                ],
                { cancelable: false }
              )
        }else{
            Alert.alert(
                "เกิดข้อผิดพลาด",
                "ไม่สามารถซื้อไอเทมได้",
                [
                {text: "Ok", onPress: ()=> {
                   
                }},
                ],
                { cancelable: false }
              )
        }
    }
    async buy(){
        this.props.naviStore.navigation.dismissLightBox({
            animationType: 'slide-down' // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
        });
        this.setState({isLoading:true});
        let response = await post(`caster_shop/buy/${this.props.item.caster_shop_id}
        ?qty=1&shipping_number=1`);
        this.setState({isLoading:false});
        if(response){
            console.log(response);
            setTimeout(()=>{
                this.props.naviStore.navigation.showModal({
                    screen: "esgn.MPayScreen", // unique ID registered with Navigation.registerScreen
                    passProps: {mpayUrl:response.url,cancel:()=>this.cancel(response.transaction.transaction_id)},
                    navigatorStyle: {}, // override the navigator style for the screen, see "Styling the navigator" below (optional)
                    navigatorButtons: {}, // override the nav buttons for the screen, see "Adding buttons to the navigator" below (optional)
                    animationType: 'slide-up' // 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
                })
            },500)
           
           
        }else{
            this.props.naviStore.navigation.dismissLightBox({
                animationType: 'slide-down' // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
            });
        }
      }
    dealConfirm(item) {
        let tmp = {...item};
        tmp.fee=0;
        this.props.navigator.showLightBox({
            screen: "esgn.DealConfirmScreen", // unique ID registered with Navigation.registerScreen
            passProps: {data:tmp,buy:()=>this.buy()},
            style: {
                tapBackgroundToDismiss: true,
                backgroundBlur: 'dark', // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
            }
        })
    }
    placeOrder(item){
        console.log(item);
        this.props.navigator.showLightBox({
            screen: "esgn.MarketItemPlaceOrderScreen", // unique ID registered with Navigation.registerScreen
            passProps: {data:item},
            style: {
                tapBackgroundToDismiss: true,
                marginTop:-20,
                backgroundBlur: 'dark', // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
            }
        })
    }
    
    render() {
        let item = this.props.item;
        console.log(this.props)
        return (
            <View style={styles.marketItemDetailScreenContainerStyle}>
                <Headers
                    title={item.name ? item.name : ''}
                    fontSize={responsiveFontSize(1.8)}
                    leftName='back'
                    rightName='searchData'
                />
                <ScrollView>
                    <ItemShortDetailCard
                        onPress={()=>this.addToCart()}
                        viewType='selling' // bought,selling
                        coverImgUri={{uri:item.image1}}
                        prize={convertNumber(item.price)}
                        itemName={item.name}
                        itemInfo={item.description}
                        canBuy={false}
                        isCaster={!this.props.isOwner}
                        dealConfirm={()=>this.dealConfirm(item)}
                        //timeLeft='22 Hrs 15 mins 54 sec'
                    />
                    
                    {/* {this.renderConfirm()} */}
                    
                    {/* {this.renderTabContent()} */}
                    <Loading visible={this.state.isLoading} />
                </ScrollView>
            </View>
        )
    }
}

const styles = {
    marketItemDetailScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    submitButtonStyle: {
        width: responsiveWidth(85),
        height: responsiveHeight(8),
        backgroundColor: "#e42526",
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        marginBottom: responsiveHeight(1),
        marginTop: responsiveHeight(2),
    },
    submitButtonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    socialButtonGroupContainerStyle: {
        marginBottom: responsiveHeight(2),

    }

}