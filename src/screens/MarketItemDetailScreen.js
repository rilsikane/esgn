import React, { Component } from 'react'
import { View, Text, Image, ScrollView } from 'react-native'
import { Button } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { ItemShortDetailCard } from '../components/ItemShortDetailCard'
import { MarketTabs } from '../components/MarketTabs'
import { GraphCard } from '../components/GraphCard'
import { SocialButtonGroup } from '../components/SocialButtonGroup'
import { ItemDealDetailCard } from '../components/ItemDealDetailCard'
import { DealConfirmCard } from '../components/DealConfirmCard'
import { observer, inject } from 'mobx-react';
import {get,post} from '../api';
import Loading from '../components/loading';
import {convertCurrency,convertNumber} from '../components/utils/number';
import store from 'react-native-simple-store'
@inject('naviStore')
@observer
export default class MarketItemDetailScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isLoading:false,itemDetail:{},offers:[]
        }
    }
    async componentDidMount(){
        const user = await store.get("user");
        this.setState({isLoading:true});
        let response= await get("market/detail/"+this.props.item.item_id);
        this.setState({isLoading:false,user:user});
        if(response ){
            console.log(response);
               let offerRes =  await get("market/blueprint_request/"+this.props.item.blueprint_id);
                if(offerRes && offerRes.offers_price_summary){
                    let offers = [];
                    if(offerRes.offers_price_summary.length>5){
                       let reverseOffers = offerRes.offers_price_summary.slice().reverse();
                       for(let i=0;i<reverseOffers.length;i++){
                           if(i<4){
                                offers.push({price:reverseOffers[i].price,number_offer:Number(reverseOffers[i].number_offer)})
                           }else if(i==4){
                                offers.push({price:`${reverseOffers[i].price} or more`,number_offer:Number(reverseOffers[i].number_offer)})
                           }else{
                                offers[4].number_offer+=Number(reverseOffers[i].number_offer);
                           }
                       }
                    }else{
                        offers = offerRes.offers_price_summary
                    }
                    this.setState({offers:offers});
                }
            
            this.setState({itemDetail:response.item});
        }
    }

    renderTabContent() {
        let tabData = [
          
            {
                title: 'Offer',
            },
        ]

        return (
            <MarketTabs
                tabData={tabData}
                child={this.renderTabChild()}
                headingType='text'
                activeIndex={0}
                contentContainerStyle={styles.tabContentContainerStyle}
            />
        )
    }

    renderTabChild() {
        
        return [
           
            <View>
                <ItemDealDetailCard
                    status='buy'
                    reqCount='23940'
                    prizeAt='0.60'
                    prizeUnit='$'
                    prizeList={this.state.offers}
                />
                
            </View>
        ]
    }

    dealConfirm(item) {
        this.props.navigator.showLightBox({
            screen: "esgn.DealConfirmScreen", // unique ID registered with Navigation.registerScreen
            passProps: {data:item},
            style: {
                tapBackgroundToDismiss: true,
                backgroundBlur: 'dark', // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
            }
        })
    }
    placeOrder(item){
        console.log(item);
        this.props.navigator.showLightBox({
            screen: "esgn.MarketItemPlaceOrderScreen", // unique ID registered with Navigation.registerScreen
            passProps: {data:item},
            style: {
                tapBackgroundToDismiss: true,
                marginTop:-20,
                backgroundBlur: 'dark', // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
            }
        })
    }
    addToCart(){
        this.props.navigator.showLightBox({
            screen: "esgn.MarketCartScreen", // unique ID registered with Navigation.registerScreen
            passProps: {},
            style: {
                tapBackgroundToDismiss: true,
                backgroundBlur: 'dark', // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
            }
        })
    }

    render() {
        let item = this.state.itemDetail;
        return (
            <View style={styles.marketItemDetailScreenContainerStyle}>
                <Headers
                    title={item.name ? item.name : ''}
                    fontSize={responsiveFontSize(1.8)}
                    leftName='back'
                    rightName='searchData'
                />
                <ScrollView>
                    <ItemShortDetailCard
                        onPress={()=>this.addToCart()}
                        viewType='selling' // bought,selling
                        coverImgUri={{uri:item.image}}
                        gameName={item.game_name}
                        ratingStar={Number(item.avg_rate)}
                        ratingCount={undefined}
                        ratingTotal={undefined}
                        prize={convertNumber(item.price)}
                        itemName={item.name}
                        itemDesc={item.seller_nickname}
                        itemInfo={item.description}
                        canBuy={this.state.user&& this.state.user.user_id!=item.seller_id}
                        dealConfirm={()=>this.dealConfirm(item)}
                        placeOrder={()=>this.placeOrder(item)}
                        //timeLeft='22 Hrs 15 mins 54 sec'
                    />
                    {/* {this.renderConfirm()} */}
                    
                    {this.state.offers.length > 0 && this.renderTabChild()}
                    <Loading visible={this.state.isLoading} />
                </ScrollView>
            </View>
        )
    }
}

const styles = {
    marketItemDetailScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    submitButtonStyle: {
        width: responsiveWidth(85),
        height: responsiveHeight(8),
        backgroundColor: "#e42526",
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        marginBottom: responsiveHeight(1),
        marginTop: responsiveHeight(2),
    },
    submitButtonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    socialButtonGroupContainerStyle: {
        marginBottom: responsiveHeight(2),

    }

}