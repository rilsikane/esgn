import React, { Component } from 'react'
import { View, Text, ScrollView } from 'react-native'
import { Button,Card } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { GameHighLightCard } from '../components/GameHighLightCard'
import { MainTabs } from '../components/MainTabs'
import { TournamentDetailCard } from '../components/TournamentDetailCard'
import moment from 'moment';
import {convertNumber} from '../components/utils/number';
import TounamentAllMatchScreen from './TournamentAllMatchScreen';
import TournamentAllTeamScreen from './TournamentAllTeamScreen';
export default class TournamentDetailScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isOverview:true
        }
        this.onChangeTab = this.onChangeTab.bind(this);
    }

    renderSubmitButton() {
        let status = 'Join now' //Joined, All Match
        return (
            <Button style={[styles.submitButtonStyle, status == 'Joined' && styles.joinedButtonStyle]}>
                <Text style={styles.submitButtonTextStyle}>{status}</Text>
            </Button>
        )
    }

    renderDetailTabList() {
        let tabData = [
            {
                title: 'Overview',
            },
            {
                title: 'Match',
            },
            {
                title: 'Team',
            },
        ]

        return (
            <MainTabs
                tabData={tabData}
                child={this.renderTabChild()}
                headingType='text'
                onChangeTab={this.onChangeTab}
            />
        )
    }
    async onChangeTab(index){
        if(index==0){
            this.setState({isOverview:true});
        }else{
            this.setState({isOverview:false});
        }
    }

    renderTabChild() {
        let data = this.props.data;
        return [
            <View style={{flex:1}}>
                 <Card style={styles.tournamentDetailCardContainerStyle}>
                    <View style={{flex:1,flexDirection: 'row',}}>
                        <View style={{flex:1}}>
                            <Text style={styles.titleTextStyle}>TOURNAMENT DETAILS</Text>
                            <View style={styles.detailItemContainerStyle}>
                                <Text style={styles.detailTextStyle}>Match Type : {data.information.bracket_format}</Text>
                            </View>
                            <View style={styles.detailItemContainerStyle}>
                                <Text style={styles.detailTextStyle}>Contestants : {data.information.contestant_type}</Text>
                            </View>
                            <View style={styles.detailItemContainerStyle}>
                                <Text style={styles.detailTextStyle}>Schedule Frequency : {data.information.game_frequency}</Text>
                            </View>
                        </View>
                        <View style={{flex:0.7,paddingLeft: responsiveWidth(8),}}>
                            <Text style={styles.titleTextStyle}>PRIZE</Text>
                                <View style={styles.detailItemContainerStyle}>
                                    <Text style={styles.detailTextStyle}>1st : {convertNumber(data.prize[0].prize)} ฿</Text>
                                </View>
                                <View style={styles.detailItemContainerStyle}>
                                    <Text style={styles.detailTextStyle}>2nd : {convertNumber(data.prize[1].prize)} ฿</Text>
                                </View>
                                <View style={styles.detailItemContainerStyle}>
                                    <Text style={styles.detailTextStyle}>3rd : {convertNumber(data.prize[2].prize)} ฿</Text>
                                </View>
                        </View>
                    </View>
                </Card>
                
            </View>,
            <TounamentAllMatchScreen tourId={data.information.tournament_id}/>,
            <TournamentAllTeamScreen tourId={data.information.tournament_id}/>
        ]
    }
    getCountDownTime(time){
        let remain = "";
        remain = moment(time).fromNow();
        return `${remain.replace("ago","")}`;
    }
    render() {
        let data = this.props.data;
        return (
            <View style={styles.tournamentDetailScreenContainerStyle}>
                <Headers
                    title='Tournament'
                    leftName='back'
                    rightName='searchData'
                />
                <ScrollView>
                    <GameHighLightCard
                        imgUri={{uri:data.information.logo}}
                        prize={data.information.prizepool}
                        countdownTime={this.getCountDownTime(data.information.register_end_at)}
                        iconUri={{uri:data.games[0].image_url}}
                        disabled
                        style={styles.gameCoverImgStyle}
                    />
                    {this.state.isOverview ? <View style={styles.tournamentDetailContainerStyle}>
                        <View style={styles.detailContainerStyle}>
                            <View style={styles.tournamentDateContainerStyle}>
                                <Text style={styles.dateTextStyle}>{moment(data.information.start_date).format("dd")}</Text>
                                <Text style={styles.dayTextStyle}>{moment(data.information.start_date).format("DD")}</Text>
                                <Text style={styles.dateTextStyle}>{moment(data.information.start_date).format("MMM YYYY")}</Text>
                            </View>
                            <View style={styles.detailSectionStyle}>
                                <Text style={styles.detailTitleTextStyle}>{data.information.name}</Text>
                                <Text numberOfLines={4} style={styles.detailTextStyle}>{data.information.description}</Text>
                            </View>
                        </View>
                        {/* {(data.is_join==0 &&data.is_match_done==0)&& this.renderSubmitButton()} */}
                    </View>:null}
                   
                    {this.renderDetailTabList()}
                </ScrollView>
            </View>
        )
    }
}

const styles = {
    tournamentDetailScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    gameCoverImgStyle: {
        borderBottomWidth: 1,
        borderColor: '#6d6e71',
    },
    tournamentDetailContainerStyle: {

    },
    detailContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: responsiveWidth(2),
    },
    tournamentDateContainerStyle: {
        flex: 0.2,
        alignItems: 'center',
    },
    dateTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.4),
        color: "#e6e6e5",
        textAlign: 'center',
    },
    dayTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(5),
        color: "#e6e6e5",
    },
    detailTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#e6e6e5",
    },
    detailTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.4),
        color: "#e6e6e5",
        textAlign: 'right',
        width: responsiveWidth(60),
    },
    detailSectionStyle: {
        flex: 0.8,
        alignItems: 'flex-end',
    },
    submitButtonStyle: {
        width: responsiveWidth(85),
        height: responsiveHeight(8),
        backgroundColor: "#e42526",
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        marginBottom: responsiveHeight(2),
        marginTop: responsiveHeight(2),
    },
    joinedButtonStyle: {
        backgroundColor: '#89c140',
    },
    submitButtonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    tournamentDetailCardContainerStyle: {
        borderWidth: 0.5,
        borderColor: "#cdcccc",
        borderRadius: 1.8,
        backgroundColor: "transparent",
        height: responsiveHeight(30),
        width: responsiveWidth(95),
        padding: responsiveWidth(2),
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    detailItemContainerStyle:{
        marginBottom: responsiveHeight(1),
    },
    detailTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.5),
        color: "#ffffff"
    }
}
