import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image, FlatList } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { UserShortProfileCard } from '../components/UserShortProfileCard'
import { ItemHistoryCard } from '../components/ItemHistoryCard'

export default class UserProfileHistoryScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderHistoryCardList() {
        let history = [
            {
                imgUri: require('../sources/images/item_img01.png'),
                title: 'ธนูเทพแสงแปดมาร',
                date: '02/09/2017',
                prize: '79',
                type: 'buy',
            },
            {
                imgUri: require('../sources/images/item_img02.png'),
                title: 'ธนูเทพแสงแปดมาร',
                date: '02/09/2017',
                prize: '79',
                type: 'buy',
            },
            {
                imgUri: require('../sources/images/item_img01.png'),
                title: 'ธนูเทพแสงแปดมาร',
                date: '02/09/2017',
                prize: '79',
                type: 'sell',
            },
        ]

        return (
            <FlatList
                data={history}
                renderItem={this.renderHistoryItem}
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }

    renderHistoryItem = ({ item }) => (
        <ItemHistoryCard
            imgUri={item.imgUri}
            title={item.title}
            date={item.date}
            prize={item.prize}
            type={item.type}
        />
    )

    render() {
        return (
            <View style={styles.userProfileHistoryScreenContainerStyle}>
                <Headers
                    title='My Profile'
                    leftName='home'
                    rightName='searchData'
                />
                <UserShortProfileCard
                    //buttonType='setting'
                    imgUri={require('../sources/icons/user_icon02.png')}
                    name='Jasonson Steheim'
                    desc='Donutlover'
                    friendCount='45'
                    videoCount='2'
                    creditCount='1005.00'
                />
                <View style={styles.userProfileHistoryContainerStyle}>
                    <View style={styles.titleContainerStyle}>
                        <Text style={styles.titleTextStyle}>History</Text>
                        <TouchableOpacity>
                            <Image
                                source={require('../sources/icons/filter_icon01.png')}
                                resizeMode='contain'
                                style={styles.filterIconStyle}
                            />
                        </TouchableOpacity>
                    </View>
                    {this.renderHistoryCardList()}
                </View>
            </View>
        )
    }
}

const styles = {
    userProfileHistoryScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    userProfileHistoryContainerStyle: {
        padding: responsiveWidth(2.5),
        flex: 1,
    },
    titleContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    filterIconStyle: {
        width: responsiveHeight(2.7),
        height: responsiveHeight(2.7),
    },

}