import React, { Component } from 'react'
import { View, Text } from 'react-native'

import { Headers } from '../components/Headers'
import { ItemRatingCard } from '../components/ItemRatingCard'
import { observer, inject } from 'mobx-react';
import {get,post} from '../api';
@inject('naviStore')
@observer
export default class ItemRatingScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            star:0,note:""
        }
    }
    onSelectedStart(rating){
        console.log(rating);
        this.setState({star:rating})
    }
    async confirm(){
        let response = await post(`market/review/${this.props.data.item_id}?rate=${this.state.star}&notes=${this.state.note}`)
        if(response){
            this.props.naviStore.navigation.dismissLightBox();
            setTimeout(()=>{
                this.props.naviStore.navigation.resetTo({
                    screen: 'esgn.InventoryHomeScreen', // unique ID registered with Navigation.registerScreen
                    title: undefined, // navigation bar title of the pushed screen (optional)
                    titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
                    animated: true, // does the push have transition animation or does it happen immediately (optional)
                    backButtonTitle: undefined, // override the back button title (optional)
                    backButtonHidden: false, // hide the back button altogether (optional)
                    animationType: 'fade',
                })
            },500)
          
        }
    }
    
    renderContent() {
      

        return (
            <ItemRatingCard
                onChangeText={(value)=>this.setState({note:value})}
                editRating={true}
                title={this.props.data.name}
                imguri={{uri:this.props.data.image}}
                rating={this.state.star}
                confirm={()=>this.confirm()}
                onSelectedStart={(rating)=>this.onSelectedStart(rating)}
            />
        )

    }

    render() {
        return (
            <View style={styles.itemRatingScreenContainerStyle}>
                {/* <Headers
                    title='Market'
                    leftName='home'
                    rightName='searchData'
                /> */}
                {this.renderContent()}
            </View>
        )
    }
}

const styles = {
    itemRatingScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    }
}