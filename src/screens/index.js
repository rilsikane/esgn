import {Navigation} from 'react-native-navigation';
import Provider from '../lib/MobxRnnProvider';
import Store from '../stores/store';
import LoginScreen from './LoginScreen';
import AppIntoScreen from './AppIntroScreen';
import StartScreen from './StartScreen';
import HomeSceen from './HomeScreen';
import MenuScreen from './MenuScreen';
import AppRegisterScreen from './AppRegisterScreen';
import AppPolicyScreen from './AppPolicyScreen';
import ForgotPasswordScreen from './ForgotPasswordScreen';
import LiveHomeScreen from './LiveHomeScreen';
import TournamentHomeScreen from './TournamentHomeScreen';
import MatchDetailScreen from './MatchDetailScreen';
import MatchFilterScreen from './MatchFilterScreen';
import TeamDetailScreen from './TeamDetailScreen';
import TournamentAllMatchScreen from './TournamentAllMatchScreen';
import TournamentAllTeamScreen from './TournamentAllTeamScreen';
import TournamentDetailScreen from './TournamentDetailScreen';
import TournamentHighLightListScreen from './TournamentHighLightListScreen';
import TournamentMatchListScreen from './TournamentMatchListScreen';
import TournamentListScreen from './TournamentListScreen';
import MarketCartScreen from './MarketCartScreen';
import MarketDealResultScreen from './MarketDealResultScreen';
import MarketFilterScreen from './MarketFilterScreen';
import MarketGameListScreen from './MarketGameListScreen';
import MarketHomeScreen from './MarketHomeScreen';
import MarketItemListHomeScreen from './MarketItemListHomeScreen';
import MarketItemDetailScreen from './MarketItemDetailScreen';
import MarketItemPlaceOrderScreen from './MarketItemPlaceOrderScreen';
import ItemRatingScreen from './ItemRatingScreen';
import ChatHomeScreen from './ChatHomeScreen';
import InventoryByGameScreen from './InventoryByGameScreen';
import InventoryHomeScreen from './InventoryHomeScreen';
import InventoryItemDetailScreen from './InventoryItemDetailScreen';
import NotificationScreen from './NotificationScreen';
import UserProfileScreen from './UserProfileScreen';
import UserProfileHistoryScreen from './UserProfileHistoryScreen';
import LiveListScreen from './LiveListScreen';
import LivingScreen from './LivingScreen';
import ChatTalkScreen from './ChatTalkScreen';
import UserProfileDetail from './UserProfileDetail';
import UserProfileEditScreen from './UserProfileEditScreen';
import CasterItemDetailScreen from './CasterItemDetailScreen';

//Modal
import InventorySellingModal from './InventorySellingModal';
import PromocodeModal from './PromocodeModal';
import BuyMoreModal from './BuyMoreModal';
import DealConfirmScreen from './modal/DealConfirmScreen';
import FriendsActionScreen from './modal/FriendsActionScreen';
import FilterActionScreen from './modal/FilterActionScreen';
import ReedeemCodeScreen from './modal/ReedeemCodeScreen';
import MPayScreen from './modal/MPayScreen';
import DonateScreen from './modal/DonateScreen'





export function registerScreens() {
    Navigation.registerComponent('esgn.LoginScreen', () => LoginScreen, Store, Provider);
    Navigation.registerComponent('esgn.AppIntoScreen', () => AppIntoScreen, Store, Provider);
    Navigation.registerComponent('esgn.HomeScreen', () => HomeSceen, Store, Provider);
    Navigation.registerComponent('esgn.MenuScreen', () => MenuScreen, Store, Provider);
    Navigation.registerComponent('esgn.AppRegisterScreen', () => AppRegisterScreen, Store, Provider);
    Navigation.registerComponent('esgn.AppPolicyScreen', () => AppPolicyScreen, Store, Provider);
    Navigation.registerComponent('esgn.ForgotPasswordScreen', () => ForgotPasswordScreen, Store, Provider);
    Navigation.registerComponent('esgn.LiveHomeScreen', () => LiveHomeScreen, Store, Provider);
    Navigation.registerComponent('esgn.TournamentHomeScreen', () => TournamentHomeScreen, Store, Provider);
    Navigation.registerComponent('esgn.StartScreen', () => StartScreen, Store, Provider);

    Navigation.registerComponent('esgn.MatchDetailScreen', () => MatchDetailScreen, Store, Provider);
    Navigation.registerComponent('esgn.MatchFilterScreen', () => MatchFilterScreen, Store, Provider);
    Navigation.registerComponent('esgn.TeamDetailScreen', () => TeamDetailScreen, Store, Provider);
    Navigation.registerComponent('esgn.TournamentAllMatchScreen', () => TournamentAllMatchScreen, Store, Provider);
    Navigation.registerComponent('esgn.TournamentAllTeamScreen', () => TournamentAllTeamScreen, Store, Provider);
    Navigation.registerComponent('esgn.TournamentDetailScreen', () => TournamentDetailScreen, Store, Provider);
    Navigation.registerComponent('esgn.TournamentHighLightListScreen', () => TournamentHighLightListScreen, Store, Provider);
    Navigation.registerComponent('esgn.TournamentMatchListScreen', () => TournamentMatchListScreen, Store, Provider);
    Navigation.registerComponent('esgn.TournamentListScreen', () => TournamentListScreen, Store, Provider);
    

    Navigation.registerComponent('esgn.MarketCartScreen', () => MarketCartScreen, Store, Provider);
    Navigation.registerComponent('esgn.MarketDealResultScreen', () => MarketDealResultScreen, Store, Provider);
    Navigation.registerComponent('esgn.MarketFilterScreen', () => MarketFilterScreen, Store, Provider);
    Navigation.registerComponent('esgn.MarketGameListScreen', () => MarketGameListScreen, Store, Provider);
    Navigation.registerComponent('esgn.MarketHomeScreen', () => MarketHomeScreen, Store, Provider);
    Navigation.registerComponent('esgn.MarketItemListHomeScreen', () => MarketItemListHomeScreen, Store, Provider);
    Navigation.registerComponent('esgn.MarketItemDetailScreen', () => MarketItemDetailScreen, Store, Provider);
    Navigation.registerComponent('esgn.MarketItemPlaceOrderScreen', () => MarketItemPlaceOrderScreen, Store, Provider);
    Navigation.registerComponent('esgn.ItemRatingScreen', () => ItemRatingScreen, Store, Provider);
    Navigation.registerComponent('esgn.DealConfirmScreen', () => DealConfirmScreen, Store, Provider);
    

    Navigation.registerComponent('esgn.ChatHomeScreen', () => ChatHomeScreen, Store, Provider);
    Navigation.registerComponent('esgn.ChatTalkScreen', () => ChatTalkScreen, Store, Provider);

    Navigation.registerComponent('esgn.InventoryHomeScreen', () => InventoryHomeScreen, Store, Provider);
    Navigation.registerComponent('esgn.InventoryByGameScreen', () => InventoryByGameScreen, Store, Provider);
    Navigation.registerComponent('esgn.InventoryItemDetailScreen', () => InventoryItemDetailScreen, Store, Provider);

    Navigation.registerComponent('esgn.NotificationScreen', () => NotificationScreen, Store, Provider);
    Navigation.registerComponent('esgn.UserProfileScreen', () => UserProfileScreen, Store, Provider);
    Navigation.registerComponent('esgn.UserProfileHistoryScreen', () => UserProfileHistoryScreen, Store, Provider);
    Navigation.registerComponent('esgn.UserProfileDetail', () => UserProfileDetail, Store, Provider)
    Navigation.registerComponent('esgn.UserProfileEditScreen', () => UserProfileEditScreen, Store, Provider)
    

    Navigation.registerComponent('esgn.LiveListScreen', () => LiveListScreen, Store, Provider);
    Navigation.registerComponent('esgn.LivingScreen', () => LivingScreen, Store, Provider);
    Navigation.registerComponent('esgn.CasterItemDetailScreen', () => CasterItemDetailScreen, Store, Provider);
    

    Navigation.registerComponent('esgn.InventorySellingModal', () => InventorySellingModal, Store, Provider)
    Navigation.registerComponent('esgn.PromocodeModal', () => PromocodeModal, Store, Provider)
    Navigation.registerComponent('esgn.BuyMoreModal', () => BuyMoreModal, Store, Provider)
    Navigation.registerComponent('esgn.FriendsActionScreen', () => FriendsActionScreen, Store, Provider)
    Navigation.registerComponent('esgn.FilterActionScreen', () => FilterActionScreen, Store, Provider)
    Navigation.registerComponent('esgn.ReedeemCodeScreen', () => ReedeemCodeScreen, Store, Provider)
    Navigation.registerComponent('esgn.MPayScreen', () => MPayScreen, Store, Provider)
    Navigation.registerComponent('esgn.DonateScreen', () => DonateScreen, Store, Provider)
    
    
    
}
export function registerScreenVisibilityListener() {
    new ScreenVisibilityListener();
}