import React, { Component } from 'react'
import { View, Text, FlatList, Image, ImageBackground, TouchableOpacity, ScrollView,WebView,Alert } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { LiveHeader } from '../components/LiveHeader'
import { LiveTabs } from '../components/LiveTabs'
import { LiveTitleCard } from '../components/LiveTitleCard'
import * as Progress from '../components/react-native-progress'
import { CommentInput } from '../components/CommentInput'
import {Headers} from '../components/Headers'
import YouTube from 'react-native-youtube'
import Loading from '../components/loading';
import {get,post} from '../api';
import {convertCurrency,convertNumber} from '../components/utils/number';
import { ItemTitleCard } from '../components/ItemTitleCard'
import { observer, inject } from 'mobx-react';
import store from 'react-native-simple-store'
@inject('naviStore')
@observer
export default class LivingScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {

        }
    }
    async componentDidMount(){
        const user = await store.get("user");
        this.setState({isLoading:true});
        let response = await get(`caster_shop/${this.props.data.caster_id}`);
        this.setState({isLoading:false,user:user});
        if(response){
            console.log(response.caster_shop);
            this.setState({items:response.caster_shop})
        }
    }
    renderButtonList() {
        let buttons = [
            {
                iconUri: require('../sources/icons/contact_icon03.png')
            },
            {
                iconUri: require('../sources/icons/share_icon02.png')
            },
            {
                iconUri: require('../sources/icons/setting_icon03.png')
            },
            {
                iconUri: require('../sources/icons/gift_icon01.png')
            },
            {
                iconUri: require('../sources/icons/heart_icon03.png')
            },
        ]

        return buttons.map((data, index) =>
            <TouchableOpacity key={index} style={styles.buttonStyle}>
                <Image
                    source={data.iconUri}
                    resizeMode='contain'
                    style={styles.viewerIconStyle}
                />
            </TouchableOpacity>
        )
    }

    renderMessageList() {
        let messages = [
            {
                title: 'Suspendisse viverra mauris eget tortor imperdiet'
            },
            {
                title: 'Suspendisse viverra mauris eget tortor imperdiet'
            },
            {
                title: 'Suspendisse viverra mauris eget tortor imperdiet'
            },
            {
                title: 'mauris sit amet magna suscipit hend merit non sed ligula. Vivamus rus odio,'
            },
            {
                title: 'Suspendisse viverra mauris eget tortor imperdiet'
            },
        ]

        return (
            <FlatList
                data={messages}
                renderItem={this.renderMessageItem}
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }

    renderMessageItem = ({ item }) => (
        <View style={styles.messageItemStyle}>
            <View style={styles.bulletStyle} />
            <Text style={styles.messageTextStyle}>{item.title}</Text>
        </View>
    )
    renderVideo(platform,link){
        if(platform=="twitch"){
            return(<WebView 
                ref={(ref) => { this.videoPlayer = ref;}}
                allowsInlineMediaPlayback={true}
                mediaPlaybackRequiresUserAction={false}
                scalesPageToFit={true}
                style={{backgroundColor:"transparent",height:responsiveHeight(31)}} 
                source={{ html: `<html><meta content="width=device-width,user-scalable=0" name="viewport" />
                <iframe
                src="https://player.twitch.tv/?channel=${link}"
                height="${responsiveHeight(31.4)}"
                width="${responsiveWidth(94)}"
                frameborder="1"
                scrolling="no"
                autoplay=true
                playsinline=true
                allowfullscreen="no">
                </iframe></html>`}} //for iOS
            />)
        }else if(platform=="twitchvdo"){
            return(<WebView 
                ref={(ref) => { this.videoPlayer = ref;}}
                allowsInlineMediaPlayback={true}
                mediaPlaybackRequiresUserAction={false}
                scalesPageToFit={true}
                style={{backgroundColor:"transparent",height:responsiveHeight(31)}} 
                source={{ html: `<html><meta content="width=device-width,user-scalable=0" name="viewport" />
                <iframe
                src="https://player.twitch.tv/?video=${link}"
                height="${responsiveHeight(30)}"
                width="${responsiveWidth(94)}"
                frameborder="1"
                scrolling="no"
                autoplay=true
                playsinline=true
                allowfullscreen="no">
                </iframe></html>`}} //for iOS
            />)
        }else if(platform=="youtube"){
            // <iframe width="560" height="315" src="https://www.youtube.com/embed/nwe76N7J0EI?rel=0&autoplay=1&mute=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

            return(<YouTube
                videoId={link}   // The YouTube video ID
                play={true}             // control playback of video with true/false
                loop={false}             // control whether the video should loop when ended
                style={{height:responsiveHeight(31.4) }}
              />)
        }
    }
    renderItemList() {
        
        return (
            <FlatList
                data={this.state.items}
                renderItem={this.renderGameItem}
                keyExtractor={(item, index) => index.toString()}
                numColumns={2}
                contentContainerStyle={styles.gameMarketListContainerStyle}
                style={styles.gameItemListStyle}
            />
        )

    }
    goToDetail(link,item){
        if(link)
        console.log(this.state.user.user_id,item.user_id)
        {
            this.props.naviStore.navigation.push({
                screen: link, // unique ID registered with Navigation.registerScreen
                title: undefined, // navigation bar title of the pushed screen (optional)
                titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
                animated: true, // does the push have transition animation or does it happen immediately (optional)
                backButtonTitle: undefined, // override the back button title (optional)
                backButtonHidden: false, // hide the back button altogether (optional)
                animationType: 'fade',
                passProps:{item:item,isOwner:this.state.user.user_id==item.user_id}
            })
        }
    }
    renderGameItem = ({ item }) => (
        <ItemTitleCard
            onPress={()=>this.goToDetail("esgn.CasterItemDetailScreen",item)}
            wishlist={()=>this.wishlistItem(item)}
            imgUri={{uri:item.image1}}
            title={item.name}
            desc={item.description}
            isFavorite={item.is_addedwish}
            prize={convertNumber(item.price)}
            rating={Number(item.avg_rate)}
            bought={item.owner==1}
            viewType={"item"}
            style={styles.ItemTitleCardStyle}
            isOwner={true}
            isReview={false}
            isLive={true}
        />
    )
    onDonatePress(){
        this.props.naviStore.navigation.showLightBox({
            screen: "esgn.DonateScreen", // unique ID registered with Navigation.registerScreen
            passProps:{donate:(val)=>this.donate(val)},
            style: {
                backgroundBlur: 'dark', // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
                tapBackgroundToDismiss: true // dismisses LightBox on background taps (optional)
            }
        })
    }
    async donate(amount){
        let response = await post(`channel/${this.props.data.caster_id}/donate?amount=${amount}`);
        this.setState({isLoading:false});
        if(response){
            this.props.naviStore.navigation.dismissLightBox();
            setTimeout(()=>{
                Alert.alert(
                    " ",
                    "Donate Success",
                    [
                    {text: "OK", },
                    ],
                    { cancelable: false }
                  )
            },500)
           
        }
    }

    render() {
        const data = this.props.data;
        return (
            <View style={styles.liveListScreenContainerStyle}>
                {/* <LiveHeader
                    title='MOBA#1'
                    leftName='home'
                    rightName='share'
                /> */}
                <Headers
                    title={data.title}
                    leftName='back'
                    fontSize={responsiveFontSize(1.7)}
                />
                {/* <ImageBackground
                    source={require('../sources/images/live_img01.png')}
                    style={styles.coverImgStyle}
                >
                    <View style={styles.buttonGroupContainerStyle}>
                        {this.renderButtonList()}
                    </View>
                    <View style={styles.viewerContainerStyle}>
                        <Text style={styles.viewTextStyle}>{data.view_count}</Text>
                        <Image
                            source={require('../sources/icons/view_icon01.png')}
                            resizeMode='contain'
                            style={styles.viewerIconStyle}
                        />
                    </View>
                </ImageBackground> */}
                <View style={{height:responsiveHeight(32)}}>
                {this.renderVideo(data.platform,data.archive_video_id?data.video_link1:data.live_url)}
                </View>
                <View style={styles.titleContainerStyle}>
                    <Image
                        source={require('../sources/icons/avatar_icon01.png')}
                        style={styles.casterImgStyle}
                    />
                    <View style={styles.detailContainerStyle}>
                        <Text style={styles.casterNameTextStyle}>{data.caster_name}</Text>
                        <Text numberOfLines={2} style={styles.descTextStyle}>{data.title}</Text>
                    </View>
                    {this.state.user && this.state.user.user_id!=data.caster_id && !data.archive_video_id &&<TouchableOpacity style={styles.sellButtonStyle} onPress={()=>this.onDonatePress()}>
                        <Text style={styles.sellTextStyle}>DONATE</Text>
                    </TouchableOpacity>}
                </View>
                {/* <Progress.Bar
                    progress={0.7}
                    width={responsiveWidth(95)}
                    color='#939598'
                    height={responsiveHeight(2)}
                    borderRadius={10}
                    style={styles.barStyle}
                    unfilledColor='#FFF'
                /> */}
                {/* <ScrollView style={{ flex: 1,backgroundColor: '#dbdcdc' }}>
                    {this.renderMessageList()}
                </ScrollView> */}
                {/* <CommentInput
                    style={styles.commentInputStyle}
                    textColor='#a7a9ac'
                /> */}
               {!data.archive_video_id &&<View style={styles.itemListContainerStyle}>
                    <Text style={styles.byGameTitleTextStyle}>Caster Shop</Text>
                    <View style={{height:responsiveHeight(47)}}>
                        {this.renderItemList()}
                    </View>
                    
                </View>}
                <Loading visible={this.state.isLoading} />
            </View>
        )
    }
}

const styles = {
    liveListScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    tabContainerStyle: {
        paddingLeft: responsiveWidth(2.5),
        paddingRight: responsiveWidth(2.5),
        width: '100%',
    },
    coverImgStyle: {
        width: responsiveWidth(100),
        height: responsiveHeight(31.4),
        justifyContent: 'space-between',
        alignItems: 'flex-end',
    },
    viewerContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingRight: responsiveWidth(4),
    },
    buttonStyle: {
        marginRight: responsiveWidth(4),
    },
    viewerIconStyle: {
        width: responsiveHeight(2.5),
        height: responsiveHeight(2.5)
    },
    viewTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.4),
        color: "#f5f7f9"
    },
    buttonGroupContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: responsiveHeight(2),
    },
    titleContainerStyle: {
        width: responsiveWidth(100),
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#dbdcdc',
        paddingTop:10,
        paddingBottom: 10,
    },
    casterImgStyle: {
        width: responsiveHeight(4.8),
        height: responsiveHeight(4.8),
        borderRadius: responsiveHeight(2.4),
        marginLeft: responsiveWidth(2.5),
    },
    detailContainerStyle: {
        flex: 1,
        marginLeft: responsiveWidth(5)
    },
    casterNameTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#939598"
    },
    descTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.4),
        color: "#a7a9ac"
    },
    messageItemStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: responsiveHeight(2),
        marginLeft: responsiveWidth(4),
    },
    bulletStyle: {
        width: responsiveHeight(2.4),
        height: responsiveHeight(2.4),
        borderRadius: responsiveHeight(1.2),
        backgroundColor: "#a7a9ac"
    },
    messageTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.5),
        color: "#58595b",
        marginLeft: responsiveWidth(5),
    },
    barStyle: {
        alignSelf: 'center',
        marginTop: responsiveHeight(2),
        marginBottom: responsiveHeight(2),
    },
    commentInputStyle: {
        marginTop: responsiveHeight(1),
        marginBottom: responsiveHeight(1),
        alignSelf: 'center',
        backgroundColor: '#FFF',
    },
    sellButtonStyle: {
        width: responsiveWidth(21.6),
        height: responsiveHeight(4.2),
        backgroundColor: "#e42526",
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 3.3,
        justifyContent: 'center',
        marginRight: 10,
    },
    sellTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    marketHomeContainerStyle: {
        paddingLeft: responsiveWidth(2.5),
        paddingRight: responsiveWidth(2.5),
        marginTop: 10,
        alignItems: 'center'
    },
    gameMarketContainerStyle:{
    },
    gameItemListContainerStyle: {
        height:responsiveHeight(90),
        
    },
    gameMarketListContainerStyle: {
        paddingBottom: responsiveHeight(2),
        marginTop: responsiveHeight(1),
    },
    itemListContainerStyle: {
        alignItems: 'center'
    },
    ItemTitleCardStyle: {
        margin: responsiveWidth(1),
    },
    byGameTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#ffffff",
        marginTop: responsiveHeight(2),
    },
}