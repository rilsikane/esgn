import React, { Component } from 'react'
import { View, Text, FlatList } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import { LiveTitleCard } from '../components/LiveTitleCard'
import { observer, inject } from 'mobx-react';
import {get} from '../api';
import Loading from '../components/loading';
@inject('naviStore')
@observer
export default class LiveListTab2Screen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            lives:[]
        }
    }

    async componentDidMount(){
        this.setState({isLoading:true});
        let liveRes = await get("channel/video/all");
        if(liveRes){
            this.setState({lives:liveRes.videos})
            console.log(liveRes.videos);
        }
        this.setState({isLoading:false});
    };
    

    renderLiveList() {
        return (
            <FlatList
                data={this.state.lives}
                renderItem={this.renderLiveItem}
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }
    gotoLive(item){
        this.props.naviStore.navigation.push({
            screen: 'esgn.LivingScreen', // unique ID registered with Navigation.registerScreen
            title: undefined, // navigation bar title of the pushed screen (optional)
            titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
            animated: true, // does the push have transition animation or does it happen immediately (optional)
            backButtonTitle: undefined, // override the back button title (optional)
            backButtonHidden: false, // hide the back button altogether (optional)
            animationType: 'fade',
            passProps:{data:item}
        })
    }

    renderLiveItem = ({ item }) => (
        <LiveTitleCard
            onPress={()=>this.gotoLive(item)}
            imgUri={{uri:item.thumbnail}}
            casterName={item.caster_name}
            casterImgUri={{uri:item.caster_image}}
            desc={item.title}
            viewer={item.view_count}
        />
    )

    render() {
        return (
            <View style={styles.liveListScreenContainerStyle}>
               
                {this.renderLiveList()}
                <Loading visible={this.state.isLoading} />
            </View>
        )
    }
}

const styles = {
    liveListScreenContainerStyle: {
        flex: 1,
    },
    tabContainerStyle: {
        paddingLeft: responsiveWidth(2.5),
        paddingRight: responsiveWidth(2.5),
        width: '100%',
    },

}