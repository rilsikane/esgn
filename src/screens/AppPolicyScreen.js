import React, { Component } from 'react'
import { View, Text, Image } from 'react-native'
import { Button } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import CheckBox from '../components/react-native-check-box'

export default class AppPolicyScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            termChecked: false,

        }
    }

    onCheckBoxPress = () => {
        this.setState({ termChecked: !this.state.termChecked })
    }

    render() {
        const { termChecked } = this.state

        return (
            <View style={styles.appPolicyScreenContainerStyle}>
                <Image
                    source={require('../sources/icons/app_icon02.png')}
                    resizeMode='contain'
                    style={styles.appIconStyle}
                />
                <Text style={styles.termTitleTextStyle}>TERMS AND CONDITIONS</Text>
                <Text style={styles.termSubTitleTextStyle}>{'\t'}Family Sharing Beta Terms and Conditions For Purchases and Downloads</Text>
                <Text style={styles.termsDetailTextStyle}>{policyDetail}</Text>
                <CheckBox
                    onClick={this.onCheckBoxPress}
                    isChecked={termChecked}
                    rightText='I have read and agree to the Terms and Condition.'
                    rightTextStyle={styles.checkBoxTextStyle}
                    style={styles.checkboxContainerStyle}
                    checkBoxColor='#B0AFAF'
                />
                <Button disabled={!termChecked} rounded style={styles.submitButtonStyle}>
                    <Text style={styles.submitTextStyle}>Submit</Text>
                </Button>
            </View>
        )
    }
}

const policyDetail = `${'\t'}Family Sharing permits you to share eligible iTunes, App Store, 
Mac App Store, iBooks Store, and iCloud products with a maximum 
of five other members of your single family (e.g., adults and chi-
dren) by allowing family members to view and download each 
others’ eligible products to their Associated Devices. Family Shar-
ing is for personal, non-commercial use, and is not available for 
commercial or educational institutions. The "organizer" of a Family 
must be an adult with an eligible payment method on file with 
iTunes and must be a parent or legal guardian of any minor added 
to the Family. The organizer can invite other members, and the 
organizer’s payment method will be used to pay for any purchas-
es initiated by Family members in excess of any iTunes Store 
credit in the initiating member’s account. Family members are 
acting as agents for the organizer when the organizer's payment 
method is used. The content purchased by Family members will 
be associated with the Apple ID of the Family member who initi-
ates the transaction. By adding Family members to Family`

const styles = {
    appPolicyScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#e6e7e8',
        alignItems: 'center',
    },
    appIconStyle: {
        height: responsiveHeight(12),
        marginTop: responsiveHeight(10),
        marginBottom: responsiveHeight(5),
    },
    termTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#414042",
        marginBottom: responsiveHeight(2),
    },
    termSubTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.5),
        color: 'rgb(27,40,56)',
    },
    termsDetailTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.5),
        color: '#676569',
        marginLeft: responsiveWidth(1),
    },
    checkBoxTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.5),
        color: "#808285"
    },
    checkboxContainerStyle: {
        marginTop: responsiveHeight(2),
    },
    submitButtonStyle: {
        width: responsiveWidth(76),
        height: responsiveHeight(8),
        backgroundColor: "#6a85c3",
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        marginTop: responsiveHeight(2),
    },
    submitTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    }

}