import React, { Component } from 'react';
import { View, Text,FlatList,Alert} from 'react-native';
import { ChatTitleCard } from '../components/ChatTitleCard'
import {get,post} from '../api';
import Loading from '../components/loading';
import { observer, inject } from 'mobx-react';

@inject('naviStore')
@observer
export default class ChatListScreen extends Component {
        
  constructor(props) {
    super(props);
    this.state = {
        isLoading:true,chatData:[],teams:[],groups:[],friends:[],friendReqs:[],
    };
    this._renderItem = this._renderItem.bind(this);
  }
  async componentDidMount(){
    this.init();
  
  }
  async init(){
    this.setState({isLoading:true});
    let [response,friendRes,friendReqRes] = await Promise.all([get("chat/chatroom"),get("friend/getfriends"),get("friend/invite/touser")]);
    this.setState({isLoading:false});
    if(response){
        console.log(response);
        this.setState({teams:response.chatroom.team||[],groups:response.chatroom.group||[]});
    }
    if(friendRes){
        
        this.setState({friends:friendRes.friends});
    }
    if(friendReqRes){
        this.setState({friendReqs:friendReqRes.invite});
    }
  }
  async getRequester(){
    this.setState({isLoading:true});
      let response = await get("friend/invite/touser");
      this.setState({isLoading:false});
      if(response){
        this.setState({friendReqs:response.invite});
    }
  }
  async getFriends(){
    let response = await get("friend/getfriends");
    this.setState({isLoading:false});
    if(response){
      this.setState({friends:response.friends});
  }
}
  _renderItem = ({ item }) => (
        <ChatTitleCard
        total={item.total}
        chatTitle={item.title}
        chatList={item.chats}
        canExpand
        isFriend={item.isFriend}
        />
   )
    renderChatTitle() {
        
        // this.setState({chatData:chatData});
        return <FlatList
                data={this.state.chatData}
                renderItem={this._renderItem}
                keyExtractor={(item, index) => index.toString()}
        />
      
    }
    async accepInvite(id){
        //this.setState({isLoading:true});
        let response= await post("friend/invite/accept/"+id,{});
        //this.setState({isLoading:false});
        if(response ){
            await this.getRequester();
        }
    }
    async rejectInvite(id){
        //this.setState({isLoading:true});
        let response= await post("friend/invite/reject/"+id,{});
        //this.setState({isLoading:false});
        if(response ){
            await this.getRequester();
        }
    }
    async onSelect(val,item){
        this.props.naviStore.navigation.dismissLightBox();
        if("delete"==val){
            this.confirmAction("Do you want to delete this user",async ()=>{
                this.setState({isLoading:true});
                let response= await post("friend/delete/"+item.friend_id,{});
                this.setState({isLoading:false});
                if(response ){
                    await this.getFriends();
                }
            })
        }else if("block"==val){
            this.confirmAction("Do you want to block this user",async ()=>{
                this.setState({isLoading:true});
                let response= await post("friend/block/"+item.friend_id,{});
                this.setState({isLoading:false});
                if(response ){
                    await this.getFriends();
                }
            })
        }
    }
    confirmAction(msg,callback){
        setTimeout(()=>{
            Alert.alert(
                " ",
                msg,
                [
                {text: "Close"},
                {text: "Confirm", onPress: ()=> {
                    callback();
                }},
                ],
                { cancelable: false }
              )
        },200)
        
    }
    async openChat(item,type){
        this.setState({isLoading:true});
        let response = {};
        if(type=="friend"){
            response = await post(`chat/open/one_on_one?friend_id=${item.friend_id}`)
        }else if(type=="group"){
            response = await post(`chat/open/group/${item.chat_channel_id}`)
        }else{
            response = await post(`chat/open/team`)
            
        }
        this.setState({isLoading:false});
        if(response && response!=""){
            setTimeout(()=>{
                this.props.naviStore.navigation.push({
                    passProps:{data:response.result,item:item},
                    screen: 'esgn.ChatTalkScreen', // unique ID registered with Navigation.registerScreen
                    title: undefined, // navigation bar title of the pushed screen (optional)
                    titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
                    animated: true, // does the push have transition animation or does it happen immediately (optional)
                    backButtonTitle: undefined, // override the back button title (optional)
                    backButtonHidden: false, // hide the back button altogether (optional)
                    animationType: 'fade',
                })
            },500)
            
        }
    }
  render() {
    return (
      <View style={{flex:1}}>
            {/* {this.renderChatTitle()} */}
            {this.state.friendReqs.length >0 &&<View style={{flex:1}}>
                <ChatTitleCard
                    isChat={false}
                    total={this.state.friendReqs.length}
                    chatTitle={"All Request"}
                    chatList={this.state.friendReqs}
                    canExpand
                    isRequest={true}
                    isFriend={true}
                    confirm={(id)=>this.accepInvite(id)}
                    reject={(id)=>this.rejectInvite(id)}
                    expanded={true}
                />
            </View>}
            {this.state.teams.length >0 &&<View style={{flex:1}}>
                <ChatTitleCard
                    isChat={true}
                    total={this.state.teams.length}
                    chatTitle={"Team Chat"}
                    chatList={this.state.teams}
                    canExpand
                    isFriend={false}
                    openChat={(item)=>this.openChat(item,'team')}
                />
            </View>}
            {this.state.groups.length >0 &&<View style={{flex:1}}>
                <ChatTitleCard
                    isChat={true}
                    total={this.state.groups.length}
                    chatTitle={"Group Chat"}
                    chatList={this.state.groups}
                    canExpand
                    isFriend={false}
                    openChat={(item)=>this.openChat(item,'group')}
                />
            </View>}
            {this.state.friends.length >0 &&<View style={{flex:1}}>
                <ChatTitleCard
                    total={this.state.friends.length}
                    chatTitle={"Friends"}
                    chatList={this.state.friends}
                    canExpand
                    isFriend={true}
                    openChat={(item)=>this.openChat(item,'friend')}
                    openSetting={(item)=>{
                        this.props.naviStore.navigation.showLightBox({
                            screen: 'esgn.FriendsActionScreen',
                            passProps: {
                               data:item,onSelect:(val)=>this.onSelect(val,item),
                               isFriend:true
                            },
                            style: {
                                backgroundBlur: "dark",
                                tapBackgroundToDismiss: true,
                            },
                        })
                    }}
                />
            </View>}
           
            <Loading visible={this.state.isLoading} />
           
      </View>
    );
  }
}
