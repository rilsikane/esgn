import React, { Component } from 'react';
import { View, Text,FlatList,ScrollView } from 'react-native';
import { MainTabs } from '../components/MainTabs'
import { FriendContactCard } from '../components/FriendContactCard'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import Loading from '../components/loading';
import {get,post} from '../api';
import { observer, inject } from 'mobx-react';
@inject('naviStore')
@observer
export default class CommunityTabScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
        isLoading :false,follows:[],followings:[]
    };
    this.renderFollowerItem = this.renderFollowerItem.bind(this);
    this.renderSubscriberItem = this.renderSubscriberItem.bind(this);
  }
  async componentDidMount(){
    this.init();
  }
  async init(){
    this.setState({isLoading:true});
    let [followRes,followingRes] = await Promise.all([get("channel/followers"),get("channel/following")]);
   
    if(followRes){
        this.setState({follows:followRes.followers})
    }
    if(followingRes){
        this.setState({followings:followingRes.following})
    }
    this.setState({isLoading:false});
  }
  renderCommunityTab() {
    let tabData = [
        {
            title: 'Follower',
            iconUri: require('../sources/icons/follow_icon01.png')
        },
        {
            title: 'Subscribe',
            iconUri: require('../sources/icons/contact_icon02.png')
        },
    ]

    return (
        <MainTabs
            tabData={tabData}
            child={this.renderCommunityTabChild()}
            headingType='text-icon'
        />
    )
}
renderCommunityTabChild() {
    return [
        <View style={styles.tabContainerStyle}>
            <View style={styles.friendTitleContainerStyle}>
                <Text style={styles.tabSectionTitleTextStyle}>All Follower </Text>
                <Text style={styles.tabSectionSubTitleTextStyle}>{`(${this.state.follows.length})`}</Text>
            </View>
            <ScrollView style={{height:responsiveHeight(50)}}>
                {this.renderFollowerList()}
            </ScrollView>
        </View>,
        <View style={styles.tabContainerStyle}>
            <View style={styles.friendTitleContainerStyle}>
                <Text style={styles.tabSectionTitleTextStyle}>All Subscriber </Text>
                <Text style={styles.tabSectionSubTitleTextStyle}>(0)</Text>
            </View>
            <ScrollView style={{height:responsiveHeight(50)}}>    
                {this.renderSubscriberList()}
            </ScrollView>
        </View>
    ]
}
renderSubscriberItem = ({ item }) => (
    <FriendContactCard
        imgUri={item.imgUri}
        name={item.name}
        desc={item.desc}
        actionType='subscribe'
    />
)

renderFollowerList() {
    return (
        <FlatList
            data={this.state.follows}
            renderItem={this.renderFollowerItem}
            keyExtractor={(item, index) => index.toString()}
        />
    )
}
renderSubscriberList() {
    let follower = []

    return (
        <FlatList
            data={follower}
            renderItem={this.renderSubscriberItem}
            keyExtractor={(item, index) => index.toString()}
        />
    )

}

renderFollowerItem = ({ item }) => (
    <FriendContactCard
        follow={()=>this.follow(item)}
        imgUri={{uri:item.picture}}
        name={item.nickname}
        desc={item.username}
        actionType={this.state.followings.find(f=>f.caster_id==item.follower_id)==undefined?'follow':undefined}
    />
)
async follow(item){
    this.setState({isLoading:true});
    let response = await post(`channel/${item.follower_id}/follow`);
    
    if(response){
        await this.init();
        this.setState({isLoading:false});
    }
    this.setState({isLoading:false});
}
  render() {
    return (
      <View style={{flex:1}}>
        {!this.state.isLoading && this.renderCommunityTab()}
        <Loading visible={this.state.isLoading}/>
      </View>
    );
  }
}
const styles = {
    userProfileScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    tabsHeaderStyle: {
        width: responsiveWidth(25),
    },
    tabContainerStyle: {
        paddingLeft: responsiveWidth(2.5),
        paddingRight: responsiveWidth(2.5),
        width: '100%',
    },
    whitelistTitleContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    friendTitleContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    expTitleContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    tabSectionTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    expIconStyle: {
        width: responsiveHeight(4.6),
        height: responsiveHeight(4.6)
    },
    tabSectionSubTitleTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(1.4),
        color: "#d1d3d4",
        justifyContent:'center'
    },
    listViewIconStyle: {
        width: responsiveHeight(2.7),
        height: responsiveHeight(2.7),
    },
    questListContainerStyle: {
        paddingBottom: responsiveHeight(2),
        marginTop: responsiveHeight(1),
    },
    questContainerStyle: {
        marginBottom: responsiveHeight(1),
    },
    questTitleCardContainerStyle: {
        marginRight: responsiveWidth(4),
    },
    itemListContainerStyle: {
        alignItems: 'center'
    },
    gameItemListContainerStyle: {
        paddingBottom: responsiveHeight(2),
    },
    ItemTitleCardStyle: {
        margin: responsiveWidth(1),
    },

}
