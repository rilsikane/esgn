import React, { Component } from 'react'
import { View, Text, FlatList } from 'react-native'

import { Headers } from '../components/Headers'
import { TeamMatchingCard } from '../components/TeamMatchingCard'
import { observer, inject } from 'mobx-react';
@inject('naviStore')
@observer
export default class TournamentMatchListScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderMatchList() {
        let match = [
            {
                t1Img: require('../sources/icons/team_icon01.png'),
                t1Name: 'Team AB',
                t1Rank: '#Ranking Team',
                t2Img: require('../sources/icons/team_icon02.png'),
                t2Name: 'Team CD',
                t2Rank: '#Ranking Team',
                status: 'living',
                statusTime: '5h 43m',
                gameImg: require('../sources/images/game_cover_img01.png'),
                showImg: true,
            },
            {
                t1Img: require('../sources/icons/team_icon01.png'),
                t1Name: 'Team AB',
                t1Rank: '#Ranking Team',
                t2Img: require('../sources/icons/team_icon02.png'),
                t2Name: 'Team CD',
                t2Rank: '#Ranking Team',
                status: 'ended',
                statusTime: '5h 43m',
                gameImg: require('../sources/images/game_cover_img01.png'),
                showImg: true,
            },
            {
                t1Img: require('../sources/icons/team_icon03.png'),
                t1Name: 'Team AB',
                t1Rank: '#Ranking Team',
                t2Img: require('../sources/icons/team_icon04.png'),
                t2Name: 'Team CD',
                t2Rank: '#Ranking Team',
                status: 'ended',
                statusTime: '5h 43m',
                gameImg: require('../sources/images/game_cover_img01.png'),
                showImg: true,
            },
        ]

        return (
            <FlatList
                data={match}
                renderItem={this.renderMatchItem}
                keyExtractor={(item, index) => index.toString()}
                contentContainerStyle={styles.tournamentMatchListContainerStyle}
            />
        )
    }
    goToDetail(link,item){
        if(link)
        {
            this.props.naviStore.navigation.push({
                screen: link, // unique ID registered with Navigation.registerScreen
                title: undefined, // navigation bar title of the pushed screen (optional)
                titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
                animated: true, // does the push have transition animation or does it happen immediately (optional)
                backButtonTitle: undefined, // override the back button title (optional)
                backButtonHidden: false, // hide the back button altogether (optional)
                animationType: 'fade',
                passProps:{data:item}
            })
        }
    }
    renderMatchItem = ({ item }) => (
        <TeamMatchingCard
            onPress={()=>this.goToDetail("esgn.TournamentDetailScreen",item)}
            t1Img={item.t1Img}
            t1Name={item.t1Name}
            t1Rank={item.t1Rank}
            t2Img={item.t2Img}
            t2Name={item.t2Name}
            t2Rank={item.t2Rank}
            status={item.status}
            statusTime={item.statusTime}
            gameImg={item.gameImg}
            showImg={item.showImg}
        />
    )

    render() {
        return (
            <View style={styles.tournamentMatchListScreenContainerStyle}>
                <Headers
                    title='Matches'
                    leftName='back'
                    rightName='searchData'
                />
                {this.renderMatchList()}
            </View>
        )
    }
}

const styles = {
    tournamentMatchListScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    tournamentMatchListContainerStyle: {
        alignItems: 'center',
    },

}