import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { UserShortMemberCard } from '../components/UserShortMemberCard';
import { Headers } from '../components/Headers'
import Loading from '../components/loading'
import {get} from '../api'
export default class UserProfileDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
        isLoading:false,
        user:{}
    };
  }
  async componentDidMount(){
    this.setState({isLoading:true});
    let response = await get("user/getprofile/"+this.props.user.friend_id);
    console.log(response)
    this.setState({isLoading:false});
    if(response && response.result){
        this.setState({user:response.result})
    }
  }

  render() {
    return (
        <View style={styles.userProfileScreenContainerStyle}>
            <Headers
                title='Profile'
                leftName='back'
            />
            <Loading visible={this.state.isLoading} />
            <UserShortMemberCard
                imgUri={{uri:this.state.user.picture}}
                name={`${this.state.user.nickname}`}
                status='Active'
                desc={this.state.user.user_type}
                walletCount={this.state.user.wallet}
                daimonCount={this.state.user.diamond}
                goldCount={this.state.user.gold}
                level={this.state.user.level && this.state.user.level.level}
                memberClass={this.state.user.member_class_namel}
                classImgUri={require('../sources/icons/member_level_icon01.png')}
                isFriend={true}
            />
        </View>
    );
  }
}
const styles = {
    userProfileScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    }
}