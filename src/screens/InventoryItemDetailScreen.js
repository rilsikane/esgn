import React, { Component } from 'react'
import { View, Text, Image, ScrollView,Alert} from 'react-native'
import { Button } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { ItemShortDetailCard } from '../components/ItemShortDetailCard'
import { MarketTabs } from '../components/MarketTabs'
import { GraphCard } from '../components/GraphCard'
import { SocialButtonGroup } from '../components/SocialButtonGroup'
import { ItemDealDetailCard } from '../components/ItemDealDetailCard'
import { DealConfirmCard } from '../components/DealConfirmCard'
import { observer, inject } from 'mobx-react';
import Loading from '../components/loading';
import {convertCurrency,convertNumber} from '../components/utils/number';
import {get,post} from '../api';
@inject('naviStore')
@observer
export default class InventoryItemDetailScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isLoading:false,itemDetail:{}
        }
    }
    async componentDidMount(){
        this.init();
    }
    async init(){
        this.setState({isLoading:true});
        let response= await get("inventory/detail/"+this.props.item.item_id);
        this.setState({isLoading:false});
        if(response ){
            console.log(response);
            this.setState({itemDetail:response.item});
        }
    }

    renderTabContent() {
        let tabData = [
            {
                title: 'Market Price',
            },
        ]

        return (
            <MarketTabs
                tabData={tabData}
                child={this.renderTabChild()}
                headingType='text'
                activeIndex={0}
                headerStyle={styles.tabHeaderStyle}
            />
        )
    }

    renderTabChild() {
        let prizeList = [
            {
                prize: '92.52',
                unit: '฿',
                qty: '123'
            },
            {
                prize: '92.52',
                unit: '฿',
                qty: '123'
            },
            {
                prize: '93.52 ot more',
                unit: '฿',
                qty: '123'
            },

        ]

        return [
            <View>
                <GraphCard
                    imgUri={require('../sources/images/graph_img01.png')}
                />
                <SocialButtonGroup
                    style={styles.socialButtonGroupContainerStyle}
                />
            </View>,
            <View>
                <ItemDealDetailCard
                    status='buy'
                    reqCount='23940'
                    prizeAt='0.60'
                    prizeUnit='$'
                    prizeList={prizeList}
                />
                <ItemDealDetailCard
                    status='sell'
                    reqCount='23940'
                    prizeAt='0.60'
                    prizeUnit='$'
                    prizeList={prizeList}
                />
            </View>
        ]
    }

    renderConfirm() {
        let prizeList = {
            total: '95.52',
            fee: '00.00',
            unit: '฿',
            prizes: [
                {
                    title: 'ราคาไอเทม',
                    prize: '93.52'
                },
                {
                    title: 'ราคาไอเทม',
                    prize: '93.52'
                },
            ]
        }

        return (
            <DealConfirmCard
                prizeList={prizeList}
            />
        )
    }
    sell(){
        this.props.naviStore.navigation.showLightBox({
            screen: "esgn.InventorySellingModal", // unique ID registered with Navigation.registerScreen
            passProps: {data : this.state.itemDetail,complete:()=>{
                this.props.naviStore.navigation.resetTo({
                    screen: 'esgn.InventoryHomeScreen', // unique ID registered with Navigation.registerScreen
                    title: undefined, // navigation bar title of the pushed screen (optional)
                    passProps: {}, // simple serializable object that will pass as props to the pushed screen (optional)
                    animated: true, // does the resetTo have transition animation or does it happen immediately (optional)
                    animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the resetTo have different transition animation (optional)
                    navigatorStyle: {}, // override the navigator style for the pushed screen (optional)
                    navigatorButtons: {} // override the nav buttons for the pushed screen (optional)
                  });
            }},
            style: {
                backgroundBlur: 'dark', // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
                tapBackgroundToDismiss: true // dismisses LightBox on background taps (optional)
            }
        })
    }
    cancelSell(){
        Alert.alert(
            " ",
            "Do yo want to cancel selling this item?",
            [
            {text: "Close"},
            {text: "Confirm", onPress: ()=> {
                this.cancelItem();
            }},
            ],
            { cancelable: false }
          )
    }
    async cancelItem(){
        let response = await post(`market/cancel_sell/${this.props.item.item_id}`);
        console.log(response);
        if(response){
            Alert.alert(
                " ",
                "Cancel Completed",
            [
            {text: "Ok", onPress: ()=> {
                this.init();
            }},
            ],
            { cancelable: false }
            )
        }
    }
    delete(){
        Alert.alert(
            " ",
            "Do yo want to delete this item?",
            [
            {text: "Close"},
            {text: "Confirm", onPress: ()=> {
                this.deleteItem();
            }},
            ],
            { cancelable: false }
          )
    }
    async deleteItem(){
        this.setState({isLoading:true});
        let response = await(`inventory/delete/${this.props.item.item_id}`);
        this.setState({isLoading:false});
        if(response){
            Alert.alert(
                " ",
                "Delete Completed",
            [
            {text: "Ok", onPress: ()=> {
                setTimeout(()=>{
                    this.props.naviStore.navigation.resetTo({
                        screen: "esgn.InventoryHomeScreen", // unique ID registered with Navigation.registerScreen
                        title: undefined, // navigation bar title of the pushed screen (optional)
                        titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
                        animated: true, // does the push have transition animation or does it happen immediately (optional)
                        backButtonTitle: undefined, // override the back button title (optional)
                        backButtonHidden: false, // hide the back button altogether (optional)
                        animationType: 'fade'
                    })
                },500)
                
            }},
            ],
            { cancelable: false }
            )
        }
    }
    async code(){
        this.setState({isLoading:true});
        let response= await post("inventory/code/"+this.props.item.item_id);
        this.setState({isLoading:false});
        if(response ){
            console.log(response);
            setTimeout(()=>{
                this.props.naviStore.navigation.showLightBox({
                    screen: "esgn.PromocodeModal", // unique ID registered with Navigation.registerScreen
                    passProps: {code:response.code,item:this.props.item},
                    style: {
                        backgroundBlur: 'dark', // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
                        tapBackgroundToDismiss: true // dismisses LightBox on background taps (optional)
                    }
                })
            },500)
           
        }
       
    }
    render() {
        let item = this.state.itemDetail;
        return (
            <View style={styles.inventoryItemDetailScreenContainerStyle}>
                <Headers
                    title={item.name ? item.name : ''}
                    fontSize={responsiveFontSize(1.8)}
                    leftName='back'
                    rightName='searchData'
                />
                <ScrollView>
                    <ItemShortDetailCard
                        onPress={()=>{
                            this.sell();
                        }}
                        cancelPress={()=>{
                            this.cancelSell();
                        }}
                        promoPress={()=>{
                            this.code();
                        }}
                        deletePress={()=>this.delete()}
                        viewType='bought' // bought,selling
                        coverImgUri={{uri:item.image}}
                        gameName={item.game_name}
                        ratingStar={Number(item.avg_rate)}
                        ratingCount={undefined}
                        ratingTotal={undefined}
                        prize={convertNumber(item.base_price)}
                        itemName={item.name}
                        itemDesc={item.seller_nickname}
                        itemInfo={item.description}
                        status={item.status}
                    />
                    {/* {this.renderTabContent()} */}
                    <Loading visible={this.state.isLoading} />
                </ScrollView>
            </View>
        )
    }
}

const styles = {
    inventoryItemDetailScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    submitButtonStyle: {
        width: responsiveWidth(85),
        height: responsiveHeight(8),
        backgroundColor: "#e42526",
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        marginBottom: responsiveHeight(5),
        marginTop: responsiveHeight(2),
    },
    submitButtonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    socialButtonGroupContainerStyle: {
        marginBottom: responsiveHeight(2),

    },
    tabHeaderStyle:{
        marginTop: responsiveHeight(1)
    }
}