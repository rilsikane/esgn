import React, { Component } from 'react'
import { View, Text, TouchableOpacity, FlatList, ScrollView } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { TeamCard } from '../components/TeamCard'
import {get,post} from '../api';
import Loading from '../components/loading';
import { observer, inject } from 'mobx-react';
@inject('naviStore')
@observer
export default class TournamentAllTeamScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isLoading:false,teams:[]
        }
    }
    componentDidMount = () => {
        this.init();
      
    };
    async init(){
        this.setState({isLoading:true});
        let teamResp = await get(`tournament/${this.props.tourId}/get_contestants`);
        this.setState({isLoading:false});
        if(teamResp && teamResp.contestants){
            this.setState({teams:teamResp.contestants.contestants});
        }
    }

    renderTeamList() {

        return (
            <FlatList
                data={this.state.teams}
                renderItem={this.renderTeamItem}
                keyExtractor={(item, index) => index.toString()}
                numColumns={3}
            />
        )

    }

    renderTeamItem = ({ item }) => (
        <TeamCard
            onPress={()=>this.goToDetail("esgn.TeamDetailScreen",item)}
            imgUri={{uri:item.logo}}
            teamName={item.name}
            teamDesc={''}
            style={styles.teamCardStyle}
        />
    )
    goToDetail(link,item){
        if(link)
        {
            this.props.naviStore.navigation.push({
                screen: link, // unique ID registered with Navigation.registerScreen
                title: undefined, // navigation bar title of the pushed screen (optional)
                titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
                animated: true, // does the push have transition animation or does it happen immediately (optional)
                backButtonTitle: undefined, // override the back button title (optional)
                backButtonHidden: false, // hide the back button altogether (optional)
                animationType: 'fade',
                passProps:{data:item}
            })
        }
    }

    render() {
        return (
            <ScrollView style={styles.tournamentAllTeamScreen}>
                {/* <Headers
                    title='Team'
                    leftName='back'
                    rightName='searchData'
                /> */}
                    {/* <View style={styles.titleContainerStyle}>
                        <Text style={styles.titleTextStyle}>All Teams</Text>
                        <Text style={styles.totalTournamentTextStyle}>Tournament ESGN#00344 (64)</Text>
                    </View> */}
                    <View style={styles.teamListContainerStyle}>
                        {this.renderTeamList()}
                    </View>
                    <Loading visible={this.state.isLoading}/>
            </ScrollView>
        )
    }
}

const styles = {
    tournamentAllTeamScreen: {
        flex: 1,
        backgroundColor: '#212221',
    },
    titleContainerStyle: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        marginTop: responsiveHeight(1),
        paddingLeft: responsiveWidth(2.5),
        paddingRight: responsiveWidth(2.5),
        marginBottom: responsiveHeight(1),
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff",
    },
    totalTournamentTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.4),
        color: "#bcbec0",
        marginLeft: responsiveWidth(2),
    },
    teamListContainerStyle: {
        alignItems: 'center',
        flex:1
    },
    teamCardStyle: {
        margin: responsiveWidth(1)
    }
}