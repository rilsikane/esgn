import React, { Component } from 'react'
import { View, Text, ScrollView, FlatList } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { GameTitleCard } from '../components/GameTitleCard'
import { FilterItemTitle } from '../components/FilterItemTitle'
import { ItemTitleCard } from '../components/ItemTitleCard'
import { observer, inject } from 'mobx-react';
import Loading from '../components/loading';
import {get,post} from '../api';
import {convertCurrency,convertNumber} from '../components/utils/number';
@inject('naviStore')
@observer
export default class MarketHomeScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            viewType : 'item',
            changeView:false,
            isLoading:true,
            items:[],
            cats:[]
        }
        this.onChangeViewType = this.onChangeViewType.bind(this);
    }
    async componentDidMount(){
        let [response,catRes] = await Promise.all([get("market/items"),get("game/getgames")]);
        this.setState({isLoading:false});
        if(response){
            this.setState({items:response.items})
        }
        if(catRes){
            this.setState({cats:catRes.result})
        }
    };
    
    filterByGame(item){
        this.setState({isLoading:true});
        setTimeout(async ()=>{
            let response = {};
            if(item.game_id!="0"){
                response = await get(`market/search_items?games=["${item.game_id}"]`);
            }else{
                response = await get(`market/items`);
            }
            this.setState({isLoading:false});
            if(response){
                this.setState({items:response.items,game_id:item.game_id})
            }
        },200)
    }
    renderMarketByGameList() {
        return (
            <FlatList
                
                data={this.state.cats}
                renderItem={this.renderGameMarkettItem}
                keyExtractor={(item, index) => index.toString()}
                horizontal
                contentContainerStyle={styles.gameMarketListContainerStyle}
                style={styles.gameMarketContainerStyle}
            />
        )
    }

    renderGameMarkettItem = ({ item }) => (
        <GameTitleCard
            onPress={()=>this.filterByGame(item)}
            imgUri={{uri:item.image_url}}
            iconUri={require('../sources/icons/market_icon02.png')}
            title={item.name}
            count={item.itemCount}
            countUnit='item'
            style={styles.gameTitleCardContainerStyle}
        />
    )

    renderItemList() {
        
        return (
            <FlatList
                data={this.state.items}
                renderItem={this.renderGameItem}
                keyExtractor={(item, index) => index.toString()}
                numColumns={this.state.viewType=='item'?2:1}
                contentContainerStyle={styles.gameItemListContainerStyle}
                style={styles.gameItemListStyle}
            />
        )

    }

    renderGameItem = ({ item }) => (
        <ItemTitleCard
            onPress={()=>this.goToDetail("esgn.MarketItemDetailScreen",item)}
            wishlist={()=>this.wishlistItem(item)}
            imgUri={{uri:item.image}}
            title={item.name}
            desc={item.description}
            isFavorite={item.is_addedwish}
            prize={convertNumber(item.price)}
            rating={Number(item.avg_rate)}
            mark={item.highlight!="0"}
            bought={item.owner==1}
            viewType={this.state.viewType}
            style={styles.ItemTitleCardStyle}
            isOwner={item.owner==1}
            isReview={true}
        />
    )
    goToDetail(link,item){
        if(link)
        {
            this.props.naviStore.navigation.push({
                screen: link, // unique ID registered with Navigation.registerScreen
                title: undefined, // navigation bar title of the pushed screen (optional)
                titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
                animated: true, // does the push have transition animation or does it happen immediately (optional)
                backButtonTitle: undefined, // override the back button title (optional)
                backButtonHidden: false, // hide the back button altogether (optional)
                animationType: 'fade',
                passProps:{item:item}
            })
        }
    }
    async filterBy(val){
        this.props.naviStore.navigation.dismissLightBox();
        this.setState({isLoading:true});
        let response = {};
        response = await get(`market/search_items?sort_by=${val}`);
        this.setState({isLoading:false});
        if(response){
            this.setState({items:response.items})
        }
    }
    onChangeViewType(){
        this.setState({changeView:true,isLoading:true});
        setTimeout(()=>{
            if(this.state.viewType=='list'){
                this.setState({viewType:'item',changeView:false,isLoading:false});
            }else{
                this.setState({viewType:'list',changeView:false,isLoading:false});
            }
        },100)
        
    }
    async wishlistItem(item){
        if(item.is_addedwish==0){
            let response = await post(`market/add_to_wishlist/${item.item_id}`);
            this.setState({isLoading:false});
            if(response){
                let tmpItems = [...this.state.items];
                let index = this.state.items.findIndex(tmp=>tmp.item_id==item.item_id);
                tmpItems[index].is_addedwish = 1;
                this.setState({items:tmpItems}); 
            }
        }else{
            let response =  await post(`market/delete_from_wishlist/${item.item_id}`);
            this.setState({isLoading:false});
            if(response){
                let tmpItems = [...this.state.items];
                let index = this.state.items.findIndex(tmp=>tmp.item_id==item.item_id);
                tmpItems[index].is_addedwish = 0;
                this.setState({items:tmpItems}); 
            }
        }
    }
    render() {
        return (
            <View style={styles.marketHomeScreenContainerStyle}>
                {!this.props.isHome && <Headers
                    title='Market'
                    leftName='home'
                    rightName='searchData'
                />}
                <ScrollView contentContainerStyle={styles.marketHomeContainerStyle}>
                    <Text style={styles.byGameTitleTextStyle}>Market By : Game</Text>
                    {this.renderMarketByGameList()}
                    <FilterItemTitle
                        title='ALL Item : >'
                        viewType={this.state.viewType} // item, list
                        onChangeViewType={this.onChangeViewType}
                        onFilter={()=>{
                            this.props.naviStore.navigation.showLightBox({
                                screen: 'esgn.FilterActionScreen',
                                passProps: {
                                   onSelect:(val)=>this.filterBy(val)
                                },
                                style: {
                                    backgroundBlur: "dark",
                                    tapBackgroundToDismiss: true,
                                },
                            })
                        }}
                    />
                    <View style={styles.itemListContainerStyle}>
                        {!this.state.changeView && this.renderItemList()}
                    </View>
                    <Loading visible={this.state.isLoading} />
                </ScrollView>
            </View>
        )
    }
}

const styles = {
    marketHomeScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    marketHomeContainerStyle: {
        paddingLeft: responsiveWidth(2.5),
        paddingRight: responsiveWidth(2.5),
    },
    byGameTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#ffffff",
        marginTop: responsiveHeight(2),
    },
    gameMarketListContainerStyle: {
        paddingBottom: responsiveHeight(2),
        marginTop: responsiveHeight(1),
    },
    gameMarketContainerStyle: {
        marginBottom: responsiveHeight(1),
    },
    gameTitleCardContainerStyle: {
        marginRight: responsiveWidth(4),
    },
    itemListContainerStyle: {
        alignItems: 'center'
    },
    gameItemListContainerStyle: {
        paddingBottom: responsiveHeight(2),
    },
    gameItemListStyle: {

    },
    ItemTitleCardStyle: {
        margin: responsiveWidth(1),
    }
}