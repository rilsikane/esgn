import React, { Component } from 'react'
import { View, Text, Image,TouchableOpacity} from 'react-native'
import { Button } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import * as Animatable from 'react-native-animatable'

import { ErrorInput } from '../components/ErrorInput'

export default class ForgotPasswordScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            email: '',
            errorEmail: false,
            errorEmailText: '',
            resetComplete: false,
        }
    }

    onEmailChangeText(email) {
        this.setState({ email, errorEmail: false, })
    }

    onSubmit = () => {
        const { email } = this.state
        if (email.length == 0) {
            this.setState({ errorEmail: true, errorEmailText: 'Please enter your email.' })
        } else if (email.indexOf('@') == -1) {
            this.setState({ errorEmail: true, errorEmailText: 'Invalid email.' })
        }

    }

    renderResetResult() {
        return (
            <View style={styles.forgotPasswordScreenContainerStyle}>
                <Animatable.View animation='bounceIn' style={styles.emailSentContainerStyle}>
                    <Image
                        source={require('../sources/icons/email_icon01.png')}
                        resizeMode='contain'
                        style={styles.emailIconStyle}
                    />
                    <View style={styles.borderLineStyle} />
                    <Text style={styles.emailSentTextStyle}>Email has been sent.</Text>
                </Animatable.View>
            </View>
        )
    }

    render() {
        const { email, errorEmail, errorEmailText, resetComplete } = this.state

        return !resetComplete ? (
            <View style={styles.forgotPasswordScreenContainerStyle}>
                <Image
                    source={require('../sources/icons/app_icon01.png')}
                    resizeMode='contain'
                    style={styles.appIconStyle}
                />
                <Text style={styles.descTextStyle}>Enter your Email Address and we will send the reset password link to your email.</Text>
                <ErrorInput
                    error={errorEmail}
                    value={email}
                    onChangeText={(email) => this.onEmailChangeText(email)}
                    placeholder='Email'
                    errorText={errorEmailText}
                />
                <Button style={styles.submitButtonStyle} onPress={this.onSubmit}>
                    <Text style={styles.submitTextStyle}>Submit</Text>
                </Button>
                <TouchableOpacity style={{paddingTop:5}} onPress={()=>{
                        this.props.navigator.resetTo({
                            screen: 'esgn.LoginScreen', // unique ID registered with Navigation.registerScreen
                            title: undefined, // navigation bar title of the pushed screen (optional)
                            passProps: {}, // simple serializable object that will pass as props to the pushed screen (optional)
                            animated: true, // does the resetTo have transition animation or does it happen immediately (optional)
                            animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the resetTo have different transition animation (optional)
                            navigatorStyle: {}, // override the navigator style for the pushed screen (optional)
                            navigatorButtons: {} // override the nav buttons for the pushed screen (optional)
                          });
                    }}>
                        <Text style={styles.checkBoxTextStyle}>Cancel</Text>
                    </TouchableOpacity>
            </View>
        ) : this.renderResetResult()
    }
}

const styles = {
    forgotPasswordScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#e6e7e8',
        alignItems: 'center',
    },
    emailSentContainerStyle: {
        width: responsiveWidth(90),
        borderRadius: 4,
        backgroundColor: "#ffffff",
        height: responsiveHeight(50),
        marginTop: responsiveHeight(20),
        alignItems: 'center',
    },
    emailIconStyle: {
        height: responsiveHeight(29),
        marginTop: responsiveHeight(5),
    },
    borderLineStyle: {
        height: 1,
        backgroundColor: '#bcbec0',
        width: '95%',
        marginTop: responsiveHeight(5),
        marginBottom: responsiveHeight(3),
    },
    emailSentTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#414042",
    },
    appIconStyle: {
        height: responsiveHeight(12),
        marginTop: responsiveHeight(14),
    },
    descTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        textAlign: "center",
        color: "#414042",
        width: responsiveWidth(85),
        marginTop: responsiveHeight(10),
        marginBottom: responsiveHeight(10),

    },
    submitButtonStyle: {
        width: responsiveWidth(85),
        height: responsiveHeight(8),
        borderRadius: 4,
        backgroundColor: "#414042",
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: responsiveHeight(8),
    },
    submitTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    checkBoxTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.5),
        color: "#808285"
    },
}

