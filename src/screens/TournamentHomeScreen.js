import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image, FlatList,ScrollView,WebView } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { MainTabs } from '../components/MainTabs'
import { GameTitleCard } from '../components/GameTitleCard'
import { TeamMatchingCard } from '../components/TeamMatchingCard'
import { observer, inject } from 'mobx-react';
import YouTube from 'react-native-youtube'
import Loading from '../components/loading';
import {get,post} from '../api';
import FastImage from 'react-native-fast-image';

@inject('naviStore')
@observer
export default class TournamentHomeScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isLoading:false,matches:[],games:[],live:{}
        }
        //this.props.naviStore.navigation = this.props.navigator;
    }
    async componentDidMount(){

        this.setState({isLoading:true});
        let [response,gameResponse,liveRes]= await Promise.all([get("match/allmatches?start_date=2016-10-11&end_date=2018-12-31&status=completed")
        ,get("game/getgames"),get("live/all?limit=1")]);
        this.setState({isLoading:false});
        if(response ){
            console.log(response);
            this.setState({matches:response.matches.slice(0,5)});
        }
        if(gameResponse ){
            this.setState({games:gameResponse.result})
        }
        if(liveRes){
            this.setState({live:liveRes.lives[0]})
        }
        //match/allmatches?start_date=2016-10-11&end_date=2018-12-31&status=completed

    }

    renderTabContent() {
        let tabData = [
            {
                title: '• Current',
            },
            // {
            //     title: '• Upcoming',
            // },
            // {
            //     title: '• Past',
            // },
        ]

        return (
            <MainTabs
                tabData={tabData}
                child={this.renderTabChild()}
                headingType='text'
            />
        )
    }

    renderTournamentByGameList() {
        
        return (
            <FlatList
                data={this.state.games}
                renderItem={this.renderGameTournamentItem}
                keyExtractor={(item, index) => index.toString()}
                horizontal
                contentContainerStyle={styles.gameTournamentListContainerStyle}
                style={styles.gameTournamentContainerStyle}
            />
        )
    }
    

    renderGameTournamentItem = ({ item }) => (
        <GameTitleCard
            onPress={()=>this.goToDetail("esgn.TournamentListScreen",item)}
            imgUri={{uri:item.image_url}}
            iconUri={require('../sources/icons/trophy_icon02.png')}
            title={item.name}
            count={'-'}
            countUnit='Tournament'
            style={styles.gameTitleCardContainerStyle}
        />
    )

    renderTeamMatchingList() {
       

        return this.state.matches.map((data, index) =>
            <TeamMatchingCard
                onPress={()=>this.goToDetail("esgn.MatchDetailScreen",data)}
                isHome={true}
                key={index}
                t1Img={{uri:data.contestant[0].image}}
                t1Name={data.contestant[0].name}
                t1Rank={''}
                t2Img={{uri:data.contestant[1].image}}
                t2Name={data.contestant[1].name}
                t2Rank={''}
                status={data.status}
                // statusTime={data.statusTime}
            //date={data.date}
            //time={data.time}
            //gameCount={data.gameCount}
            //t1Score={data.t1Score}
            //t2Score={data.t2Score}
            //gameImg={data.gameImg}
            //showImg={data.showImg}
            />
        )
    }
    goToDetail(link,item,isHot){
        if(link)
        {
            this.props.naviStore.navigation.push({
                screen: link, // unique ID registered with Navigation.registerScreen
                title: undefined, // navigation bar title of the pushed screen (optional)
                titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
                animated: true, // does the push have transition animation or does it happen immediately (optional)
                backButtonTitle: undefined, // override the back button title (optional)
                backButtonHidden: false, // hide the back button altogether (optional)
                animationType: 'fade',
                passProps:{data:item,type:isHot?"hot":undefined}
            })
        }
    }

    renderTabChild() {

        return 
            <View style={styles.tabChildContainerStyle}>
                <View style={styles.titleContainerStyle}>
                    <Text style={styles.titleTextStyle}>Highlight</Text>
                    <TouchableOpacity onPress={()=>this.goToDetail("esgn.TournamentHighLightListScreen",undefined,true)}>
                        <Text style={styles.seeAllTextStyle}>SEE ALL</Text>
                    </TouchableOpacity>
                </View>
                <Text style={styles.descTextStyle}>Tournament {this.state.live.title}</Text>
                
                
                <Text style={styles.byGameTitleTextStyle}>Tournament by : Game</Text>
                <View style={{height:responsiveHeight(15)}}>
                {this.renderTournamentByGameList()}
                </View>
                <View style={styles.titleContainerStyle}>
                    <View style={styles.matchPredictTitleContainerStyle}>
                        <Text style={styles.titleTextStyle}>Match Predict</Text>
                        <Image
                            source={require('../sources/icons/live_icon02.png')}
                            resizeMode='contain'
                            style={styles.liveTitleIconStyle}
                        />
                    </View>
                    <TouchableOpacity onPress={()=>this.goToDetail("esgn.TournamentMatchListScreen")}>
                        <Text style={styles.seeAllTextStyle}>SEE ALL</Text>
                    </TouchableOpacity>
                </View>
                {this.renderTeamMatchingList()}
                <Loading visible={this.state.isLoading} />
            </View>
            // <View />,
            // <View />,
            // <View />,
        
    }
    renderVideo(platform,link){
        if(platform=="twitch"){
            return(<WebView 
                ref={(ref) => { this.videoPlayer = ref;}}
                allowsInlineMediaPlayback={true}
                mediaPlaybackRequiresUserAction={false}
                scalesPageToFit={true}
                style={{backgroundColor:"transparent",height:responsiveHeight(31)}} 
                source={{ html: `<html><meta content="width=device-width,user-scalable=0" name="viewport" />
                <iframe
                src="https://player.twitch.tv/?channel=${link}"
                height="${responsiveHeight(31.4)}"
                width="${responsiveWidth(94)}"
                frameborder="1"
                scrolling="no"
                autoplay=true
                playsinline=true
                allowfullscreen="no">
                </iframe></html>`}} //for iOS
            />)
        }else if(platform=="twitchvdo"){
            return(<WebView 
                ref={(ref) => { this.videoPlayer = ref;}}
                allowsInlineMediaPlayback={true}
                mediaPlaybackRequiresUserAction={false}
                scalesPageToFit={true}
                style={{backgroundColor:"transparent",height:responsiveHeight(31)}} 
                source={{ html: `<html><meta content="width=device-width,user-scalable=0" name="viewport" />
                <iframe
                src="https://player.twitch.tv/?video=${link}"
                height="${responsiveHeight(30)}"
                width="${responsiveWidth(94)}"
                frameborder="1"
                scrolling="no"
                autoplay=true
                playsinline=true
                allowfullscreen="no">
                </iframe></html>`}} //for iOS
            />)
        }else if(platform=="youtube"){
            // <iframe width="560" height="315" src="https://www.youtube.com/embed/nwe76N7J0EI?rel=0&autoplay=1&mute=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

            return(<YouTube
                videoId={link}   // The YouTube video ID
                play={true}             // control playback of video with true/false
                loop={false}             // control whether the video should loop when ended
                style={{height:responsiveHeight(31.4) }}
              />)
        }
    }
    gotoLive(item){
        this.props.naviStore.navigation.push({
            screen: 'esgn.LivingScreen', // unique ID registered with Navigation.registerScreen
            title: undefined, // navigation bar title of the pushed screen (optional)
            titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
            animated: true, // does the push have transition animation or does it happen immediately (optional)
            backButtonTitle: undefined, // override the back button title (optional)
            backButtonHidden: false, // hide the back button altogether (optional)
            animationType: 'fade',
            passProps:{data:item}
        })
    }

    render() {
        return (
            <View style={styles.tournamentHomeScreenContainerStyle}>
                {!this.props.isHome && <Headers
                    title='Tournament'
                    leftName='home'
                    rightName='searchData'
                />}
               <ScrollView style={styles.tabChildContainerStyle}>
                <View style={styles.titleContainerStyle}>
                    <Text style={styles.titleTextStyle}>Highlight</Text>
                    <TouchableOpacity onPress={()=>this.goToDetail("esgn.TournamentHighLightListScreen",undefined,true)}>
                        <Text style={styles.seeAllTextStyle}>SEE ALL</Text>
                    </TouchableOpacity>
                </View>
                <Text numberOfLines={2} style={styles.descTextStyle}>Tournament {this.state.live.title}</Text>
                <TouchableOpacity onPress={()=>this.gotoLive(this.state.live)} style={styles.mainLiveContainerStyle}>
                    {/* <Image
                        source={require('../sources/images/live_img01.png')}
                        style={styles.mainLiveImgStyle}
                    /> */}
                    <FastImage
                        source={{uri:this.state.live.thumbnail}}
                        style={styles.mainLiveImgStyle}
                    />
                    <View style={[styles.mainLiveImgStyle,{position:'absolute',alignItems: 'center'
                    ,justifyContent: 'center',}]}>
                        <Image source={require('../sources/icons/live_icon04.png')}/>
                    </View>
                    <View style={[styles.mainLiveImgStyle,{position:'absolute',alignItems: 'flex-end'
                    ,justifyContent: 'flex-end',}]}>
                        <Image source={require('../sources/icons/live_icon03.png')} 
                        style={{height:responsiveHeight(5),width:responsiveHeight(10) 
                        ,marginBottom:responsiveHeight(1) ,}}/>
                    </View>
                    
                </TouchableOpacity>
                <Text style={styles.byGameTitleTextStyle}>Tournament by : Game</Text>
                {this.renderTournamentByGameList()}
                <View style={styles.titleContainerStyle}>
                    <View style={styles.matchPredictTitleContainerStyle}>
                        <Text style={styles.titleTextStyle}>Match Predict</Text>
                        <Image
                            source={require('../sources/icons/live_icon02.png')}
                            resizeMode='contain'
                            style={styles.liveTitleIconStyle}
                        />
                    </View>
                    {/* <TouchableOpacity onPress={()=>this.goToDetail("esgn.TournamentMatchListScreen")}>
                        <Text style={styles.seeAllTextStyle}>SEE ALL</Text>
                    </TouchableOpacity> */}
                </View>
                {this.renderTeamMatchingList()}
                <Loading visible={this.state.isLoading}/>
            </ScrollView>

            </View>
        )
    }
}

const styles = {
    tournamentHomeScreenContainerStyle: {
        flex: 1,

    },
    tabChildContainerStyle: {
        width: '100%',
        paddingLeft: responsiveWidth(2.5),
        paddingRight: responsiveWidth(2.5),
        backgroundColor:"#212221"
    },
    titleContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    matchPredictTitleContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff",
    },
    liveTitleIconStyle: {
        height: responsiveHeight(3),
        width: responsiveHeight(3),
        marginLeft: responsiveWidth(2),
    },
    seeAllTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#5782c2"
    },
    descTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#bcbec0"
    },
    liveBannerContainerStyle: {
        height: responsiveHeight(31),
        width: responsiveWidth(95),
    },
    liveBannerImgStyle: {
        height: responsiveHeight(31),
        width: responsiveWidth(95),
    },
    byGameTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#ffffff",
        marginTop: responsiveHeight(2),
    },
    gameTournamentListContainerStyle: {
        paddingBottom: responsiveHeight(2),
        marginTop: responsiveHeight(1),
    },
    gameTournamentContainerStyle: {
        marginBottom: responsiveHeight(1),
    },
    gameTitleCardContainerStyle: {
        marginRight: responsiveWidth(4),
        height:responsiveHeight(18)
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    mainLiveContainerStyle: {
        width: responsiveWidth(95),
        height: responsiveHeight(31),
    },
    mainLiveImgStyle: {
        width: responsiveWidth(95),
        height: responsiveHeight(31),
    },
    videoListSectionStyle: {
        marginTop: responsiveHeight(2),
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    descTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.6),
        color: "#ffffff",
        width:responsiveWidth(80)
    },
}