import React, { Component } from 'react';
import { View, Text,FlatList,ScrollView,Alert } from 'react-native';
import {get,post} from '../api';
import Loading from '../components/loading';
import { observer, inject } from 'mobx-react';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import {Input} from 'native-base'
import { FriendContactCard } from '../components/FriendContactCard';
@inject('naviStore')
@observer
export default class FriendsSearchListScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
        isLoading:false,friends:[],searchVal:""
    };
  }
  componentDidMount(){
    this.init();
  }
  async init(){
    this.setState({isLoading:true});
    let response = await get("friend/search_user?limit=60");
    this.setState({isLoading:false});
    if(response){
        console.log(response.result);
        this.setState({friends:response.result});
    }
 }
 async onSearch(){
    this.setState({isLoading:true});
    let response = await get(`friend/search_user?search_text=${this.state.searchVal}&limit=60`);
    this.setState({isLoading:false});
    if(response){
        this.setState({friends:response.result});
    }
 }
 async invite(item){
    this.props.naviStore.navigation.dismissLightBox();
    this.setState({isLoading:true});
    let response = await post(`friend/invite/${item.user_id}`);
    this.setState({isLoading:false});
    if(response){
        setTimeout(()=>{
            Alert.alert(
                " ",
                "ส่งคำขอเป็นเพื่อนแล้ว",
                [
                {text: "Ok", onPress: ()=> {
                    this.onSearch();
                }},
                ],
                { cancelable: false }
              )
        },500)
       
    }
 }
 async cancel(item){
    this.props.naviStore.navigation.dismissLightBox();
    this.setState({isLoading:true});
    let response = await post(`friend/invite/cancel/${item.user_id}`);
    this.setState({isLoading:false});
    if(response){
        setTimeout(()=>{
            Alert.alert(
                " ",
                "ยกเลิกคำขอเป็นเพื่อนแล้ว",
                [
                {text: "Ok", onPress: ()=> {
                    this.onSearch();
                }},
                ],
                { cancelable: false }
              )
        },500)
       
    }
 }
 openFriendDetail(item){
    this.props.naviStore.navigation.push({
        screen: 'esgn.UserProfileDetail', // unique ID registered with Navigation.registerScreen
        title: undefined, // navigation bar title of the pushed screen (optional)
        titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
        animated: true, // does the push have transition animation or does it happen immediately (optional)
        backButtonTitle: undefined, // override the back button title (optional)
        backButtonHidden: false, // hide the back button altogether (optional)
        animationType: 'fade',
        passProps:{user:item}
    })
}
  renderFriendItem = ({ item }) => (
    <FriendContactCard
        imgUri={{uri:item.picture}}
        name={item.username}
        desc={item.user_type}
        status={item.friend_status}
        //lastActive={item.lastActive}
        actionType='add'
        add={()=>this.invite(item)}
        cancel={()=>this.cancel(item)}
    //isOnLine
    />
 )
  render() {
    return (
        <View style={{flex:1}}>
            <View  style={{marginLeft:20,marginRight:20,height:responsiveHeight(5)}}>
                    <Input
                        placeholder="ค้นหา"
                        returnKeyLabel="search"
                        returnKeyType="search"
                        value={this.state.searchVal}
                        onChangeText={(val) => this.setState({searchVal:val})}
                        style={styles.inputStyle}
                        onSubmitEditing={()=>this.onSearch()}
                    />
             </View>
            <ScrollView style={{height:responsiveHeight(80),marginTop:10}}>
                <FlatList
                    data={this.state.friends}
                    renderItem={this.renderFriendItem}
                    keyExtractor={(item, index) => index.toString()}
                />
            </ScrollView>
            <Loading visible={this.state.isLoading} />
        </View>
    );
  }
}
const styles = {

    inputStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#212221",
        backgroundColor:"#fff",
        borderRadius: 5,
    },
}