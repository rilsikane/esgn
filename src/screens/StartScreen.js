import React, { Component } from 'react'
import { ImageBackground, Text } from 'react-native'
import { Button } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import store from 'react-native-simple-store';
import app from '../stores/app'
export default class StartScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
        this.app = app;
    }

    render() {
        return (
            <ImageBackground source={require('../sources/images/start_img01.png')} style={styles.startScreenContainerStyle}>
                <Button style={styles.startButtonStyle} onPress={async ()=>{
                        await store.save("tutorial",true);
                        this.app.logout();
                    }}>
                    <Text style={styles.startButtonTextStyle}>PRESS START</Text>
                </Button>
            </ImageBackground>
        )
    }
}

const styles = {
    startScreenContainerStyle: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    startButtonStyle: {
        width: responsiveWidth(33),
        height: responsiveHeight(5),
        borderRadius: 2,
        backgroundColor: "#e42526",
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: responsiveHeight(10),
    },
    startButtonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    }
}