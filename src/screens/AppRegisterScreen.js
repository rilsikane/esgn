import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity } from 'react-native'
import { Tab, Tabs, Button, Item } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import FontAwesome from 'react-native-vector-icons/FontAwesome'

import { ErrorInput } from '../components/ErrorInput'
import { BirthDatePicker } from '../components/BirthDatePicker'
import CheckBox from '../components/react-native-check-box'
import { PhonePicker } from '../components/PhonePicker'
import { CreditCardDetect } from '../components/CreditCardDetect'
import creditcardutils from '../components/creditcardutils'


export default class AppRegisterScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            tabIndex: 0,
            userId: '',
            password: '',
            rePassword: '',
            email: '',
            reEmail: '',
            birthDate: '',
            birthMonth: '',
            birthYear: '',
            country: '',
            telNo: '',
            nameOfCard: '',
            bankOfCard: '',
            cardNumber: '',
            cardExpDate: '',
            cardSecCode: '',
            errorUserId: false,
            errorPassword: false,
            errorRePassword: false,
            errorEmail: false,
            errorReEmail: false,
            errorBirthDate: false,
            errorTel: false,
            errorNameOfCard: false,
            errorBankOfCard: false,
            errorCardNumber: false,
            errorCardExpDate: false,
            errorCardSecCode: false,
            errorPasswordText: undefined,
            errorEmailText: undefined,
            errorCardNumberText: undefined,
            errorCardSecCodeText: undefined,
            userChecked: false,
            paymentChecked: false,
            activeCardType: null,
        }
    }

    renderSubmit(index, userChecked, paymentChecked) {
        return (
            <View style={styles.submitContainerStyle}>
                <CheckBox
                    onClick={() => this.onCheckBoxPress(index)}
                    isChecked={index == 0 ? userChecked : paymentChecked}
                    rightText='I have read and agree to the Terms and Condition.'
                    rightTextStyle={styles.checkBoxTextStyle}
                    style={styles.checkboxContainerStyle}
                    checkBoxColor='#B0AFAF'
                />
                <View style={{ alignItems: 'center' }}>
                    <Button onPress={() => this.onSubmit(index)} rounded style={styles.submitButtonStyle}>
                        <Text style={styles.submitTextStyle}>{index == 0 ? 'Sign Up' : 'Submit'}</Text>
                    </Button>
                    <TouchableOpacity style={{ paddingTop: 5 }} onPress={() => {
                        this.props.navigator.resetTo({
                            screen: 'esgn.LoginScreen', // unique ID registered with Navigation.registerScreen
                            title: undefined, // navigation bar title of the pushed screen (optional)
                            passProps: {}, // simple serializable object that will pass as props to the pushed screen (optional)
                            animated: true, // does the resetTo have transition animation or does it happen immediately (optional)
                            animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the resetTo have different transition animation (optional)
                            navigatorStyle: {}, // override the navigator style for the pushed screen (optional)
                            navigatorButtons: {} // override the nav buttons for the pushed screen (optional)
                        });
                    }}>
                        <Text style={styles.checkBoxTextStyle}>Cancel</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )

    }

    onSubmit(index) {
        if (index == 0) {
            this.validateUserInfoForm()
            if (!this.state.errorUserId && !this.state.errorPassword && !this.state.errorRePassword &&
                !this.state.errorEmail && !this.state.errorReEmail && !this.state.errorBirthDate &&
                !this.state.errorTel
            ) {
                //submit
            }
        } else {
            this.validatePaymentForm()
            if (!this.state.errorNameOfCard && !this.state.errorBankOfCard && !this.state.errorCardNumber &&
                !this.state.errorCardExpDate && !this.state.errorCardSecCode
            ) {
                //submit
            }
        }

    }


    validateUserInfoForm() {
        !this.state.userChecked && alert('Please accept the Terms and Condition.')
        if (this.state.userChecked) {
            this.state.userId == '' && this.setState({ errorUserId: true, })
            this.state.password == '' && this.setState({ errorPassword: true, })
            this.state.rePassword == '' && this.setState({ errorRePassword: true, })
            this.state.email == '' && this.setState({ errorEmail: true, })
            this.state.reEmail == '' && this.setState({ errorReEmail: true, })
            this.state.birthDate == '' && this.setState({ errorBirthDate: true, })
            this.state.birthMonth == '' && this.setState({ errorBirthDate: true, })
            this.state.birthYear == '' && this.setState({ errorBirthDate: true, })
            this.state.country == '' && this.setState({ errorTel: true, })
            this.state.telNo == '' && this.setState({ errorTel: true, })
        }

    }

    validatePaymentForm() {
        !this.state.paymentChecked && alert('Please accept the Terms and Condition.')
        if (this.state.paymentChecked) {
            this.state.nameOfCard == '' && this.setState({ errorNameOfCard: true, })
            this.state.bankOfCard == '' && this.setState({ errorBankOfCard: true, })
            this.state.cardNumber == '' && this.setState({ errorCardNumber: true, errorCardNumberText: undefined })
            this.state.cardExpDate == '' && this.setState({ errorCardExpDate: true, })
            this.state.cardSecCode == '' && this.setState({ errorCardSecCode: true, errorCardSecCodeText: undefined })
        }
    }

    onCheckBoxPress = (index) => {
        if (index == 0) {
            this.setState({ userChecked: !this.state.userChecked })
        } else {
            this.setState({ paymentChecked: !this.state.paymentChecked })
        }

    }

    _onChangeTab = (index) => {
        this.setState({ tabIndex: index.i })
    }

    validateMatching(field1, field2, fieldName) {
        if (fieldName == 'password') {
            if (field1.length < 8) {
                this.setState({
                    errorRePassword: true,
                    errorPasswordText: 'Password must be more than 8 characters.'
                })
            } else {
                if (field1 != field2) {
                    this.setState({ errorRePassword: true, errorPasswordText: 'Password does not match.' })
                }
            }
        } else if (fieldName == 'email') {
            if (field1.indexOf('@') == -1) {
                this.setState({
                    errorReEmail: true,
                    errorEmailText: 'Invalid email.'
                })
            } else if (field1 != field2 && field2.length > 0) {
                this.setState({
                    errorReEmail: true,
                    errorEmailText: 'Email does not match.'
                })
            }
        }



    }

    validateCard(cardNumber) {
        if (!creditcardutils.validateCardNumber(cardNumber)) {
            this.setState({
                errorCardNumber: true,
                errorCardNumberText: 'Invalid card number.',
            })
        } else {
            let activeCardType = creditcardutils.parseCardType(cardNumber)
            this.setState({ activeCardType })
        }

    }

    onCardNumberChange(cardNumber) {
        this.setState({ cardNumber: creditcardutils.formatCardNumber(cardNumber), errorCardNumber: false })
    }

    validateCardSecCode(code, cardType) {
        if (!creditcardutils.validateCardCVC(code, cardType)) {
            this.setState({
                errorCardSecCode: true,
                errorCardSecCodeText: 'Invalid Security Code.'
            })
        }

    }

    render() {
        const { userId, password, rePassword, email, reEmail, birthDate, birthMonth, birthYear, errorUserId, errorPassword, errorRePassword,
            errorEmail, errorReEmail, errorBirthDate, country, telNo, errorTel, tabIndex, userChecked, paymentChecked,
            nameOfCard, bankOfCard, cardNumber, cardExpDate, cardSecCode, errorNameOfCard, errorBankOfCard, errorCardNumber, errorCardExpDate,
            errorCardSecCode, errorPasswordText, errorEmailText, activeCardType, errorCardNumberText, errorCardSecCodeText,

        } = this.state

        return (
            <View style={styles.appRegisterScreenContainerStyle}>
                <Tabs
                    locked={true}
                    tabBarUnderlineStyle={styles.tabBarUnderlineStyle}
                    tabContainerStyle={styles.tabsContainerStyle}
                    onChangeTab={this._onChangeTab}
                    initialPage={0}
                >
                    <Tab
                        heading="User Info"
                        tabStyle={styles.tabStyle}
                        activeTabStyle={styles.activeTabStyle}
                        activeTextStyle={styles.activeTextStyle}
                        textStyle={styles.tabTextStyle}
                    >
                        <ScrollView contentContainerStyle={styles.tabChildContainerStyle}>
                            <ErrorInput
                                error={errorUserId}
                                value={userId}
                                onChangeText={(userId) => this.setState({ userId, errorUserId: false })}
                                placeholder='USER ID'
                                errorText='Enter your USER ID'
                                style={styles.errorInputStyle}
                            />
                            <ErrorInput
                                error={errorPassword}
                                value={password}
                                onChangeText={(password) => this.setState({ password, errorPassword: false, errorRePassword: false })}
                                placeholder='PASSWORD'
                                errorText='Enter your PASSWORD'
                                style={styles.errorInputStyle}
                                secureTextEntry
                                onBlur={() => this.validateMatching(password, rePassword, 'password')}
                                maxLength={16}
                            />
                            <ErrorInput
                                error={errorRePassword}
                                value={rePassword}
                                onChangeText={(rePassword) => this.setState({ rePassword, errorRePassword: false })}
                                placeholder='Re-PASSWORD'
                                errorText={errorPasswordText || 'Re-enter yout password'}
                                style={styles.errorInputStyle}
                                secureTextEntry
                                onBlur={() => this.validateMatching(rePassword, password, 'password')}
                                maxLength={16}
                            />
                            <ErrorInput
                                error={errorEmail}
                                value={email}
                                onChangeText={(email) => this.setState({ email, errorEmail: false, errorReEmail: false })}
                                placeholder='Email'
                                errorText='Enter your Email'
                                style={styles.errorInputStyle}
                                onBlur={() => this.validateMatching(email, reEmail, 'email')}
                                keyboardType='email-address'
                            />
                            <ErrorInput
                                error={errorReEmail}
                                value={reEmail}
                                onChangeText={(reEmail) => this.setState({ reEmail, errorReEmail: false })}
                                placeholder='Re-Email'
                                errorText={errorEmailText || 'Re-enter your email'}
                                style={styles.errorInputStyle}
                                onBlur={() => this.validateMatching(reEmail, email, 'email')}
                                keyboardType='email-address'
                            />
                            <BirthDatePicker
                                error={errorBirthDate}
                                errorText='Enter your birth date'
                                dateValue={birthDate}
                                monthValue={birthMonth}
                                yearValue={birthYear}
                            // onDatePress={}
                            // onMonthPress={}
                            // onYearPress={}
                            />
                            <PhonePicker
                                error={errorTel}
                                errorText='Enter your Telephone'
                                countryValue={country}
                                telValue={telNo}
                                onTelChangeText={(telNo) => this.setState({ telNo, errorTel: false })}
                            />
                            {this.renderSubmit(tabIndex, userChecked, paymentChecked)}
                        </ScrollView>
                    </Tab>
                    <Tab
                        heading="Payment Info"
                        tabStyle={styles.tabStyle}
                        activeTabStyle={styles.activeTabStyle}
                        activeTextStyle={styles.activeTextStyle}
                        textStyle={styles.tabTextStyle}
                    >
                        <View style={styles.tabChildContainerStyle}>
                            <CreditCardDetect
                                style={styles.creditCardDetectStyle}
                                cardType={activeCardType}
                            />
                            <TouchableOpacity style={styles.skipButtonContainerStyle}>
                                <Text style={styles.skipButtonTextStyle}>SKIP THIS STEP ></Text>
                            </TouchableOpacity>
                            <ErrorInput
                                error={errorNameOfCard}
                                value={nameOfCard}
                                onChangeText={(nameOfCard) => this.setState({ nameOfCard })}
                                placeholder='Name on Card'
                                errorText='Enter your Name on Card'
                                style={styles.errorInputStyle}
                            />
                            <View style={styles.bankOfCardContainerStyle}>
                                <Item error={errorBankOfCard}>
                                    <TouchableOpacity style={styles.mainInputContainerStyle}>
                                        <Text style={styles.labelTextStyle}>{bankOfCard != '' ? bankOfCard : 'Bank of Card'}</Text>
                                        <FontAwesome name='caret-down' color='#818181' size={responsiveFontSize(2)} />
                                    </TouchableOpacity>
                                </Item>
                                {errorBankOfCard ? <Text style={styles.errorTextStyle}>Enter your Bank of Card</Text> : null}
                            </View>
                            <ErrorInput
                                error={errorCardNumber}
                                value={cardNumber}
                                onChangeText={(cardNumber) => this.onCardNumberChange(cardNumber)}
                                placeholder='Card Number'
                                errorText={errorCardNumberText || 'Enter your Card Number'}
                                style={styles.errorInputStyle}
                                onBlur={() => this.validateCard(cardNumber)}
                                keyboardType='numeric'
                                maxLength={19}
                            />
                            <View style={styles.dateAndCodeContainerStyle}>
                                <View>
                                    <Item error={errorCardExpDate}>
                                        <TouchableOpacity style={styles.cardExpDateInputContainerStyle}>
                                            <Text style={styles.labelTextStyle}>{cardExpDate != '' ? cardExpDate : 'Expiration Date'}</Text>
                                            <FontAwesome name='caret-down' color='#818181' size={responsiveFontSize(2)} />
                                        </TouchableOpacity>
                                    </Item>
                                    {errorCardExpDate ? <Text style={styles.errorTextStyle}>Enter your Expiration Date</Text> : null}
                                </View>
                                <ErrorInput
                                    error={errorCardSecCode}
                                    value={cardSecCode}
                                    onChangeText={(cardSecCode) => this.setState({ cardSecCode, errorCardSecCode: false })}
                                    placeholder='Security Code'
                                    errorText={errorCardSecCodeText || 'Enter your Security Code'}
                                    style={styles.secCodeInputStyle}
                                    maxLength={4}
                                    onBlur={() => this.validateCardSecCode(cardSecCode, activeCardType)}
                                    keyboardType='numeric'
                                />
                            </View>
                            <View style={styles.termTitleContainerStyle}>
                                <View style={styles.titleLineStyle} />
                                <Text style={styles.termTitleTextStyle}>Term & Condition</Text>
                                <View style={styles.titleLineStyle} />
                            </View>
                            <Text style={styles.termDetailTextStyle}>{termDetail}</Text>
                            {this.renderSubmit(tabIndex, userChecked, paymentChecked)}
                        </View>
                    </Tab>
                </Tabs>
            </View>
        )
    }
}

const termDetail = `A Terms and Conditions agreement or a Privacy Policy are legally binding 
agreements between you (the company, the mobile app developer, the website 
owner, the e-commerce store owner etc.) and users using your website, mobile 
app, Facebook app etc.`

const styles = {
    appRegisterScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#e6e7e8',
    },
    tabBarUnderlineStyle: {
        backgroundColor: '#a51e23',
    },
    tabsContainerStyle: {
        height: responsiveHeight(10),
    },
    tabStyle: {

    },
    activeTabStyle: {
        backgroundColor: '#1b2838',
    },
    activeTextStyle: {
        color: '#FFF',
        fontFamily: "BentonSans",
        fontSize: responsiveFontSize(2),
    },
    tabTextStyle: {
        color: '#525353',
        fontFamily: "BentonSans",
        fontSize: responsiveFontSize(2),
    },
    tabChildContainerStyle: {
        flex: 1,
        backgroundColor: '#e6e7e8',
        alignItems: 'center',
        paddingBottom: 50,
    },
    creditCardDetectStyle: {
        marginTop: responsiveHeight(5),
    },
    skipButtonContainerStyle: {
        alignSelf: 'flex-end',
        marginTop: responsiveHeight(3),
        marginRight: responsiveWidth(2.5),
    },
    skipButtonTextStyle: {
        fontFamily: "BentonSans",
        fontSize: responsiveFontSize(1.4),
        color: "#525353"
    },
    errorInputStyle: {
        marginBottom: responsiveHeight(1.5),
    },
    dateAndCodeContainerStyle: {
        width: responsiveWidth(90),
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    cardExpDateInputContainerStyle: {
        height: responsiveHeight(8),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: responsiveWidth(47),
    },
    bankOfCardContainerStyle: {
        marginBottom: responsiveHeight(1.5),
    },
    mainInputContainerStyle: {
        height: responsiveHeight(8),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: responsiveWidth(90),
    },
    labelTextStyle: {
        fontFamily: 'Kanit',
        color: '#808285',
        fontSize: responsiveFontSize(2),
    },
    errorTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.6),
        color: "#e42526",
        marginTop: responsiveHeight(0.5),
    },
    secCodeInputStyle: {
        width: responsiveWidth(36),
    },
    termTitleContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: responsiveHeight(5),
    },
    titleLineStyle: {
        width: responsiveWidth(26),
        height: 0.8,
        backgroundColor: "#939598"
    },
    termTitleTextStyle: {
        fontFamily: "BentonSans",
        fontSize: responsiveFontSize(1.5),
        color: "#939598",
        marginLeft: responsiveWidth(2),
        marginRight: responsiveWidth(2),
    },
    termDetailTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(1),
        color: "#939598",
        width: responsiveWidth(80),
        marginBottom: responsiveHeight(4),
    },
    checkBoxTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.5),
        color: "#808285"
    },
    checkboxContainerStyle: {
        marginTop: responsiveHeight(2),
    },
    submitContainerStyle: {
        alignItems: 'center',
    },
    submitButtonStyle: {
        width: responsiveWidth(76),
        height: responsiveHeight(8),
        backgroundColor: "#6a85c3",
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        marginTop: responsiveHeight(2),
    },
    submitTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    }

}