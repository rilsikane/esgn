import React, { Component } from 'react'
import { View, Text, FlatList, TouchableOpacity, Image } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { MainTabs } from '../components/MainTabs'
import { ChatTitleCard } from '../components/ChatTitleCard'
import { FriendContactCard } from '../components/FriendContactCard'
import { ContactActionCard } from '../components/ContactActionCard'
import ChatList from './ChatListScreen';
import ChatroomScreen from './ChatroomScreen';
import FriendsSearchListScreen from './FriendsSearchListScreen';

export default class ChatHomeScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderTabContent() {
        let tabData = [
            {
                iconUri: require('../sources/icons/user_icon04.png')
            },
            {
                iconUri: require('../sources/icons/feedback_icon01.png')
            },
            {
                iconUri: require('../sources/icons/search_icon02.png')
            },
        ]

        return (
            <MainTabs
                tabData={tabData}
                child={this.renderTabChild()}
                headingType='image'
            />
        )
    }

    renderTabChild() {
        return [
            <View style={styles.containerStyle}>
                <ChatList />
            </View>,
            <View style={styles.containerStyle}>
                <ChatroomScreen />
            </View>,
            <View style={styles.containerStyle}>
                <FriendsSearchListScreen />
            </View>
        ]
    }

    renderButtonList() {
        let buttons = [
            {
                iconUri: require('../sources/icons/search_icon03.png')
            },
            {
                iconUri: require('../sources/icons/facebook_icon02.png')
            },
            {
                iconUri: require('../sources/icons/twitter_icon02.png')
            },
            {
                iconUri: require('../sources/icons/google_icon02.png')
            },
        ]

        return buttons.map((data, index) =>
            <TouchableOpacity key={index} style={styles.searchButtonContainerStyle}>
                <Image
                    source={data.iconUri}
                    resizeMode='contain'
                    style={styles.searchButtonStyle}
                />
            </TouchableOpacity>
        )
    }

    renderFriendReq() {
        let reqList = [
            {
                imgUri: require('../sources/icons/avatar_icon02.png'),
                name: 'Skewfried',
                desc: 'EZ32@echo',
            },
            {
                imgUri: require('../sources/icons/avatar_icon02.png'),
                name: 'Skewfried',
                desc: 'EZ32@echo',
            },
            {
                imgUri: require('../sources/icons/avatar_icon02.png'),
                name: 'Skewfried',
                desc: 'EZ32@echo',
            },
        ]

        return (
            <FlatList
                data={reqList}
                renderItem={this.renderFriendReqItem}
                keyExtractor={(item, index) => index.toString()}
            />
        )

    }

    renderFriendReqItem = ({ item }) => (
        <FriendContactCard
            imgUri={item.imgUri}
            name={item.name}
            desc={item.desc}
            actionType='request'
        />
    )

    renderChatTitle() {
        let chatData = [
            {
                title: 'Team Chat',
                chats: [
                    {
                        imgUri: require('../sources/icons/chat_icon01.png'),
                        name: 'INSANITY',
                        desc: '# Sub Team Info.',
                    }
                ]
            },
            {
                title: 'Group Chat',
                total: '4',
                chats: [
                    {
                        imgUri: require('../sources/icons/chat_icon01.png'),
                        name: 'ROV ฮาเฮ',
                        users: '27',
                    }
                ]
            },
            {
                title: 'Online Friend',
                total: '3/235',
                chats: [
                    {
                        imgUri: require('../sources/icons/chat_icon01.png'),
                        name: 'ROV ฮาเฮ',
                        desc: '# Sub Team Info.',
                        isOnLine: true,
                    },
                    {
                        imgUri: require('../sources/icons/chat_icon01.png'),
                        name: 'ROV ฮาเฮ',
                        desc: '# Sub Team Info.',
                        isOnLine: true,
                    },
                    {
                        imgUri: require('../sources/icons/chat_icon01.png'),
                        name: 'ROV ฮาเฮ',
                        desc: '# Sub Team Info.',
                        isOnLine: true,
                    },
                ]
            },
            {
                title: 'Offline Friend',
                chats: [
                    {
                        imgUri: require('../sources/icons/chat_icon01.png'),
                        name: 'ROV ฮาเฮ',
                        desc: '# Sub Team Info.',
                        lastActive: '1hrs 33m ago'
                    },
                    {
                        imgUri: require('../sources/icons/chat_icon01.png'),
                        name: 'ROV ฮาเฮ',
                        desc: '# Sub Team Info.',
                        lastActive: '1hrs 33m ago'
                    },
                ]
            },

        ]

        return chatData.map((data, index) =>
            <ChatTitleCard
                key={index}
                total={data.total}
                chatTitle={data.title}
                chatList={data.chats}
                canExpand
            />
        )

    }

    render() {
        return (
            <View style={styles.chatHomeScreenContainerStyle}>
                <Headers
                    title='Friend'
                    leftName='home'
                    rightName='searchData'
                />
                {this.renderTabContent()}
            </View>
        )
    }
}

const styles = {
    chatHomeScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    containerStyle: {
        width: '100%',
        paddingLeft: responsiveWidth(2.5),
        paddingRight: responsiveWidth(2.5),
    },
    titleLabelStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff",
        marginBottom: responsiveHeight(2),
    },
    searchButtonContainerStyle: {
        alignSelf: 'center',
        marginBottom: responsiveHeight(2.5),
    },
    searchButtonStyle: {
        width: responsiveHeight(14.6),
        height: responsiveHeight(14.6),
    },

}