import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, Image } from 'react-native'
import { Button } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'

export default class MarketFilterScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderFilterList() {
        let filters = [
            {
                title: 'Heroes',
                iconUri: require('../sources/icons/filter_icon02.png'),
            },
            {
                title: 'Arcana',
                iconUri: require('../sources/icons/filter_icon03.png'),
            },
            {
                title: 'Skins',
                iconUri: require('../sources/icons/filter_icon04.png')
            },
            {
                title: 'Others',
                iconUri: require('../sources/icons/filter_icon05.png')
            },
        ]

        return filters.map((data, index) =>
            <TouchableOpacity key={data.title} style={styles.filterItemContainerStyle}>
                <Text style={styles.filterTitleTextStyle}>{data.title}</Text>
                <Image
                    source={data.iconUri}
                    resizeMode='contain'
                    style={styles.filterIconStyle}
                />
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={styles.marketFilterScreenContainerStyle}>
                <Headers
                    title='Category'
                    leftName='home'
                    rightName='searchData'
                />
                <View style={styles.titleContainerStyle}>
                    <Text style={styles.sortLabelTextStyle}>Category</Text>
                </View>
                <ScrollView style={styles.filterListContainerStyle}>
                    {this.renderFilterList()}
                </ScrollView>
                <Button rounded style={styles.submitButtonStyle}>
                    <Text style={styles.submitButtonTextStyle}>Submit</Text>
                </Button>
            </View>
        )
    }
}

const styles = {
    marketFilterScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    titleContainerStyle: {
        height: responsiveHeight(10),
        backgroundColor: "#414042",
        justifyContent: 'center',
    },
    sortLabelTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff",
        marginLeft: responsiveWidth(2.5),
    },
    filterListContainerStyle: {
        flex: 1,
    },
    filterItemContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: responsiveHeight(10),
        borderBottomWidth: 1,
        borderColor: '#474647',
        paddingLeft: responsiveWidth(2.5),
        paddingRight: responsiveWidth(2.5),

    },
    filterTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    submitButtonStyle: {
        width: responsiveWidth(85),
        height: responsiveHeight(8),
        backgroundColor: "#e42526",
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        marginBottom: responsiveHeight(5),
        marginTop: responsiveHeight(2),
    },
    submitButtonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    filterIconStyle: {
        height: responsiveHeight(4),
        width: responsiveHeight(4),
    },
}