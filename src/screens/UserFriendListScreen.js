import React, { Component } from 'react';
import { View, Text,FlatList,Alert } from 'react-native';
import { UserShortMemberCard } from '../components/UserShortMemberCard'
import Loading from '../components/loading';
import {get,post} from '../api';
import { FriendContactCard } from '../components/FriendContactCard';
import { observer, inject } from 'mobx-react';
@inject('naviStore')
@observer
export default class UserFreindListScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
        isLoading:true,
        friends:[]
    };
  }
  
  componentDidMount(){
    this.init();
  }
  async init(){
    let friendRes = await get("friend/getfriends");
    this.setState({isLoading:false});
    if(friendRes){
        this.setState({friends:friendRes.friends})
    }
  }
  openFriendDetail(item){
    this.props.naviStore.navigation.push({
        screen: 'esgn.UserProfileDetail', // unique ID registered with Navigation.registerScreen
        title: undefined, // navigation bar title of the pushed screen (optional)
        titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
        animated: true, // does the push have transition animation or does it happen immediately (optional)
        backButtonTitle: undefined, // override the back button title (optional)
        backButtonHidden: false, // hide the back button altogether (optional)
        animationType: 'fade',
        passProps:{user:item}
    })
}
async onSelect(val,item){
    this.props.naviStore.navigation.dismissLightBox();
    if("delete"==val){
        this.confirmAction("Do you want to delete this user",async ()=>{
            this.setState({isLoading:true});
            let response= await post("friend/delete/"+item.friend_id,{});
            this.setState({isLoading:false});
            if(response ){
                await this.init();
            }
        })
    }else if("block"==val){
        this.confirmAction("Do you want to block this user",async ()=>{
            this.setState({isLoading:true});
            let response= await post("friend/block/"+item.friend_id,{});
            this.setState({isLoading:false});
            if(response ){
                await this.init();
            }
        })
    }
}
confirmAction(msg,callback){
    setTimeout(()=>{
        Alert.alert(
            " ",
            msg,
            [
            {text: "Close"},
            {text: "Confirm", onPress: ()=> {
                callback();
            }},
            ],
            { cancelable: false }
          )
    },200)
}
  renderFriendItem = ({ item }) => (
    <FriendContactCard
        onPress={()=>this.openFriendDetail(item)}
        imgUri={{uri:item.picture}}
        name={item.username}
        desc={item.user_type}
        //lastActive={item.lastActive}
        actionType='setting'
        openSetting={()=>{
            this.props.naviStore.navigation.showLightBox({
                screen: 'esgn.FriendsActionScreen',
                passProps: {
                   data:item,onSelect:(val)=>this.onSelect(val,item),isFriend:true
                },
                style: {
                    backgroundBlur: "dark",
                    tapBackgroundToDismiss: true,
                },
            })
        }}
    //isOnLine
    />
 )
  render() {
    return( 
        <View style={{flex:1}}>
            <FlatList
                data={this.state.friends}
                renderItem={this.renderFriendItem}
                keyExtractor={(item, index) => index.toString()}
            />
             <Loading visible={this.state.isLoading} />
        </View>
    )
  }
}
