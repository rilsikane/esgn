import React, { Component } from 'react'
import { View, Text, Image, ScrollView, TouchableOpacity } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import { observer, inject } from 'mobx-react';
import store from 'react-native-simple-store';
import FastImage from 'react-native-fast-image'
import app from '../stores/app';
@inject('naviStore','notiStore')
@observer
export default class MenuScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            user:{}
        }
        this.app = app;
    }
    async componentDidMount(){
        let user = await store.get("user");
        this.setState({user:user});
    }
    closeToggle(){
        this.props.naviStore.navigation.toggleDrawer({
            side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
            animated: true, // does the toggle have transition animation or does it happen immediately (optional)
            to: 'close' // optional, 'open' = open the drawer, 'closed' = close it, missing = the opposite of current state
          });
    }
    gotoMenu(link){
        if(link){
            this.props.naviStore.selectMenu = link;
            this.props.naviStore.navigation.resetTo({
                screen: link, // unique ID registered with Navigation.registerScreen
                title: undefined, // navigation bar title of the pushed screen (optional)
                titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
                animated: true, // does the push have transition animation or does it happen immediately (optional)
                backButtonTitle: undefined, // override the back button title (optional)
                backButtonHidden: false, // hide the back button altogether (optional)
                animationType: 'fade'
            })
        }
        this.closeToggle();
    }
    logout(){
        this.closeToggle();
        // setTimeout(()=>{
        //     this.props.naviStore.navigation.resetTo({
        //         screen: "esgn.LoginScreen", // unique ID registered with Navigation.registerScreen
        //         title: undefined, // navigation bar title of the pushed screen (optional)
        //         titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
        //         animated: true, // does the push have transition animation or does it happen immediately (optional)
        //         backButtonTitle: undefined, // override the back button title (optional)
        //         backButtonHidden: false, // hide the back button altogether (optional)
        //         animationType: 'fade'
        //     })
        // },800)
        this.app.logout();
       
    }
    renderMenuList() {
        let menus = [
            {
                iconUri: require('../sources/icons/live_icon01.png'),
                title: 'Live!',
                link:'esgn.LiveHomeScreen'
            },
            {
                iconUri: require('../sources/icons/market_icon01.png'),
                title: 'Market',
                link:'esgn.MarketItemListHomeScreen'
            },
            {
                iconUri: require('../sources/icons/trophy_icon01.png'),
                title: 'Tournament',
                link:'esgn.TournamentHomeScreen'
            },
            {
                iconUri: require('../sources/icons/inventory_icon01.png'),
                title: 'Inventory',
                link:'esgn.InventoryHomeScreen'
            },
            {
                iconUri: require('../sources/icons/community_icon01.png'),
                title: 'Community',
                link:'esgn.ChatHomeScreen'
            },
            {
                iconUri: require('../sources/icons/notify_icon02.png'),
                title: 'Notifications',
                link:'esgn.NotificationScreen',
                badge:true
            },
        ]

        return menus.map((data, index) =>
            <View key={index}>
                <TouchableOpacity onPress={()=>this.gotoMenu(data.link)} style={[styles.normalMenuContainerStyle, data.link ==this.props.naviStore.selectMenu && styles.activeMenuStyle]}>
                    <FastImage
                        source={data.iconUri}
                        resizeMode='contain'
                        style={styles.menuIconStyle}
                    />
                    <Text style={styles.normalMenuLabelTextStyle}>{data.title}</Text>
                    {data.badge && <View style={styles.badgeTextContainerStyle}>
                        <Text style={styles.badgeTextStyle}>{this.props.notiStore ? this.props.notiStore.unwatch :0}</Text>
                    </View>}
                </TouchableOpacity>
                <View style={styles.menuLineStyle} />
            </View>
        )
    }

    render() {
        return (
            <View style={styles.menuScreenContainerStyle}>
                {/* <Image
                    source={require('../sources/icons/app_icon03.png')}
                    resizeMode='contain'
                    style={styles.appIconStyle}
                /> */}
                <TouchableOpacity onPress={()=>this.gotoMenu('esgn.UserProfileScreen')}
                    style={{flexDirection:'row',marginTop:30,marginBottom:20,alignItems:'flex-start',justifyContent:'flex-start',marginLeft:-25}}>
                        <FastImage
                            source={{uri:this.state.user.picture}}
                            resizeMode='contain'
                            style={styles.userIcon02Style}
                        />
                    
                    <View>
                        <Text style={styles.jasonson}>{`${this.state.user.nickname}`}</Text>
                        <Text style={styles.donutlover}>{this.state.user.user_type}</Text>
                    </View>
                </TouchableOpacity>
                <ScrollView style={{ flex: 1, }} contentContainerStyle={styles.menuListContainerStyle}>
                    <View style={styles.menuLineStyle} />
                    {this.renderMenuList()}
                </ScrollView>
                <View style={styles.menuLineStyle} />
                <View>
                    <Text style={{color:"#fff",fontFamily:"Kanit"}}>V.1.3.4</Text>
                </View>
                <TouchableOpacity style={styles.loginButtonContainerStyle} onPress={()=>{
                    this.logout()
                }}>
                    <FastImage
                        source={require('../sources/icons/login_icon01.png')}
                        resizeMode='contain'
                        style={styles.menuIconStyle}
                    />
                    <Text style={styles.menuLabelTextStyle}>Logout</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = {
    menuScreenContainerStyle: {
        flex: 1,
        backgroundColor: "#212221",
        alignItems: 'center',
        paddingLeft: responsiveWidth(2.5),
        paddingRight: responsiveWidth(2.5)
    },
    appIconStyle: {
        height: responsiveHeight(10),
        marginTop: responsiveHeight(8),
        marginBottom: responsiveHeight(5),
    },
    loginButtonContainerStyle: {
        width: responsiveWidth(95),
        height: responsiveHeight(8),
        borderRadius: 3,
        backgroundColor: "#e42526",
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: responsiveHeight(1),
        marginTop: responsiveHeight(1),
    },
    menuIconStyle: {
        height: responsiveHeight(4.5),
        width: responsiveHeight(4.5),
    },
    menuLabelTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(1.8),
        color: "#ffffff"
    },
    normalMenuLabelTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(1.8),
        color: "#ffffff",
        marginLeft: responsiveWidth(8),
    },
    normalMenuContainerStyle: {
        width: responsiveWidth(95),
        height: responsiveHeight(8),
        borderRadius: 3,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        paddingLeft: responsiveWidth(6),
    },
    activeMenuStyle: {
        backgroundColor: '#e42526',
        borderRadius: 3,
    },
    menuListContainerStyle: {
        alignItems: 'center',
    },
    menuLineStyle: {
        width: responsiveWidth(95),
        height: 1,
        backgroundColor: "#e6e7e8",

    },
    userIcon02Style: {
        height: responsiveHeight(8),
        width: responsiveHeight(8),
    },
    jasonson : {
        fontFamily: "Prompt",
        fontSize: 15,
        fontWeight: "500",
        fontStyle: "normal",
        lineHeight: 22,
        letterSpacing: 0,
        textAlign: "left",
        color: "#ffffff",
        marginLeft:19
    },
    donutlover :{
        fontFamily: "Prompt",
        fontSize: 11,
        fontWeight: "300",
        fontStyle: "italic",
        lineHeight: 17,
        letterSpacing: 0.22,
        textAlign: "left",
        color: "#ffffff",
        marginLeft:19
    },
    badgeTextContainerStyle:{
        backgroundColor: '#fbb040',
        width: responsiveHeight(3.5),
        height: responsiveHeight(3.5),
        borderRadius: responsiveHeight(3.5),
        justifyContent: 'center',
        zIndex: 5,
        marginLeft: responsiveWidth(5),

    },
    badgeTextStyle:{
        color: '#FFF',
        textAlign: 'center',
        textAlignVertical: 'center',


    },
}