import React, { Component } from 'react'
import { View, Text, FlatList } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { LiveHeader } from '../components/LiveHeader'
import { LiveTabs } from '../components/LiveTabs'
import { LiveTitleCard } from '../components/LiveTitleCard'
import { observer, inject } from 'mobx-react';
import { Headers } from '../components/Headers';
import LiveListTab1Screen from './LiveListTab1Screen';
import LiveListTab2Screen from './LiveListTab2Screen';
@inject('naviStore')
@observer
export default class LiveListScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderTabContent() {
        let tabData = [
            {
                title: 'Live',
                iconUri: require('../sources/icons/live_icon03.png')
            },
            {
                title: 'Video',
                iconUri: require('../sources/icons/live_icon01.png')
            },
            // {
            //     title: 'HOT',
            //     iconUri: require('../sources/icons/hot_icon01.png')
            // },
        ]

        return (
            <LiveTabs
                tabData={tabData}
                child={this.renderTabChild()}
                headingType='text-icon'
                activeIndex={0}
                
            />
        )

    }

    renderTabChild() {
        return [
            <View style={styles.tabContainerStyle}>
                <LiveListTab1Screen />
            </View>,
            <View style={styles.tabContainerStyle}>
                <LiveListTab2Screen />
        </View>
        ]
    }

    renderLiveList() {
        let lives = [
            {
                imgUri: require('../sources/images/live_img01.png'),
                casterName: 'Caster Name',
                casterImgUri: require('../sources/icons/avatar_icon02.png'),
                desc: 'Game Title / Map / Stage',
                viewer: '1,987',
            },
            {
                imgUri: require('../sources/images/live_img01.png'),
                casterName: 'Caster Name',
                casterImgUri: require('../sources/icons/avatar_icon02.png'),
                desc: 'Game Title / Map / Stage',
                viewer: '1,987',
            },
            {
                imgUri: require('../sources/images/live_img01.png'),
                casterName: 'Caster Name',
                casterImgUri: require('../sources/icons/avatar_icon02.png'),
                desc: 'Game Title / Map / Stage',
                viewer: '1,987',
            },

        ]

        return (
            <FlatList
                data={lives}
                renderItem={this.renderLiveItem}
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }


    renderLiveItem = ({ item }) => (
        <LiveTitleCard
            state='live'
            imgUri={item.imgUri}
            casterName={item.casterName}
            casterImgUri={item.casterImgUri}
            desc={item.desc}
            viewer={item.viewer}
        />
    )

    render() {
        return (
            <View style={styles.liveListScreenContainerStyle}>
                {/* <LiveHeader
                    title='MOBA#1'
                    leftName='home'
                    rightName='share'
                /> */}
                <Headers
                    title='All Videos'
                    leftName='back'
                    rightName='searchData'
                />
                {this.renderTabContent()}
            </View>
        )
    }
}

const styles = {
    liveListScreenContainerStyle: {
        flex: 1,

    },
    tabContainerStyle: {
        paddingLeft: responsiveWidth(2.5),
        paddingRight: responsiveWidth(2.5),
        width: '100%',
    },

}