import React, { Component } from 'react'
import { View, Text, FlatList, Image, TouchableOpacity } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { UserShortProfileCard } from '../components/UserShortProfileCard'
import { MainTabs } from '../components/MainTabs'
import { UserShortMemberCard } from '../components/UserShortMemberCard'
import { FriendContactCard } from '../components/FriendContactCard'

import { MembershipPlanCard } from '../components/MembershipPlanCard'
import { GameTitleCard } from '../components/GameTitleCard'
import { ExpRewardTitleCard } from '../components/ExpRewardTitleCard'
import { ItemTitleCard } from '../components/ItemTitleCard'
import store from 'react-native-simple-store';
import {get} from '../api'
import Loading from '../components/loading';
import { observer, inject } from 'mobx-react';
import UserFriendListScreen from './UserFriendListScreen';
import ItemWishlistScreen from './ItemWishlistScreen';
import CommunityTabScreen from './CommunityTabScreen';
@inject('naviStore')
@observer
export default class UserProfileScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            user:{},
            isLoading:false,
            friends:[]
        }
    }
    async componentDidMount(){
        let user = await store.get("user");
        console.log(user);

        let userRes = await get(`user/getprofile/${user.user_id}`);

        // this.setState({isLoading:false});
        // if(friendRes){
        //     this.setState({friends:friendRes.friends})
        // }
        this.setState({user:userRes.result});
    }

    renderTabContent() {
        let tabData = [
            {
                iconUri: require('../sources/icons/home_icon01.png')
            },
            {
                iconUri: require('../sources/icons/user_icon03.png')
            },
            // {
            //     iconUri: require('../sources/icons/contact_icon01.png')
            // },
            // {
            //     iconUri: require('../sources/icons/gift_icon01.png')
            // },
            {
                iconUri: require('../sources/icons/rating_icon01.png')
            },
            // {
            //     iconUri: require('../sources/icons/trophy_icon04.png')
            // },
            {
                iconUri: require('../sources/icons/contact_icon02.png')
            },
        ]

        return (
            <MainTabs
                tabData={tabData}
                child={this.renderTabChild()}
                headingType='image'
                scrollable={true}
                headerStyle={styles.tabsHeaderStyle}
            />
        )
    }

    renderTabChild() {
        return [
            <View>
                <UserShortMemberCard
                    imgUri={{uri:this.state.user.picture}}
                    name={this.state.user.nickname}
                    status='Active'
                    desc={this.state.user.user_type}
                    walletCount={this.state.user.wallet}
                    daimonCount={this.state.user.diamond}
                    goldCount={this.state.user.gold}
                    level={this.state.user.level && this.state.user.level.level}
                    memberClass={this.state.user.member_class_namel}
                    classImgUri={require('../sources/icons/member_level_icon01.png')}
                    onPress={()=>{
                        this.props.naviStore.navigation.push({
                            screen: 'esgn.UserProfileEditScreen', // unique ID registered with Navigation.registerScreen
                            title: undefined, // navigation bar title of the pushed screen (optional)
                            titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
                            animated: true, // does the push have transition animation or does it happen immediately (optional)
                            backButtonTitle: undefined, // override the back button title (optional)
                            backButtonHidden: false, // hide the back button altogether (optional)
                            animationType: 'fade',
                            passProps:{user:this.state.user}
                        })
                    }}
                />
            </View>,
            <View style={styles.tabContainerStyle}>
                {/* <View style={styles.friendTitleContainerStyle}>
                    <Text style={styles.tabSectionTitleTextStyle}>All Friend </Text>
                    <Text style={styles.tabSectionSubTitleTextStyle}>(3/45)</Text>
                </View>
                {this.renderFriendList()} */}
                <UserFriendListScreen />
            </View>,
            // <View style={styles.tabContainerStyle}>
            //     <Text style={styles.tabSectionTitleTextStyle}>Your Membership Plan</Text>
            //     {this.renderShipPlanList()}
            // </View>,
            // <View style={styles.tabContainerStyle}>
            //     <Text style={styles.tabSectionTitleTextStyle}>Quest by : Type</Text>
            //     {this.renderQuestList()}
            //     <View style={styles.expTitleContainerStyle}>
            //         <Text style={styles.tabSectionTitleTextStyle}>Best EXP Reward  </Text>
            //         <Image
            //             source={require('../sources/icons/exp_icon01.png')}
            //             resizeMode='contain'
            //             style={styles.expIconStyle}
            //         />
            //     </View>
            //     {this.renderExpRewardList()}
            // </View>,
            <View style={styles.tabContainerStyle}>
                <ItemWishlistScreen />
            </View>,
            // <View />,
            <View>
               <CommunityTabScreen />
            </View>
        ]
    }

    renderCommunityTab() {
        let tabData = [
            {
                title: 'Follower',
                iconUri: require('../sources/icons/follow_icon01.png')
            },
            {
                title: 'Subscribe',
                iconUri: require('../sources/icons/contact_icon02.png')
            },
        ]

        return (
            <MainTabs
                tabData={tabData}
                child={this.renderCommunityTabChild()}
                headingType='text-icon'
            />
        )
    }

    renderCommunityTabChild() {
        return [
            <View style={styles.tabContainerStyle}>
                <View style={styles.friendTitleContainerStyle}>
                    <Text style={styles.tabSectionTitleTextStyle}>All Follower </Text>
                    <Text style={styles.tabSectionSubTitleTextStyle}>(38)</Text>
                </View>
                {this.renderFollowerList()}
            </View>,
            <View style={styles.tabContainerStyle}>
                <View style={styles.friendTitleContainerStyle}>
                    <Text style={styles.tabSectionTitleTextStyle}>All Subscriber </Text>
                    <Text style={styles.tabSectionSubTitleTextStyle}>(38)</Text>
                </View>
                {this.renderSubscriberList()}
            </View>
        ]
    }

    renderSubscriberList() {
        let follower = [
            {
                imgUri: require('../sources/icons/avatar_icon02.png'),
                name: 'Skewfried',
                desc: 'EZ32@echo',
            },
            {
                imgUri: require('../sources/icons/avatar_icon02.png'),
                name: 'Skewfried',
                desc: 'EZ32@echo',
            },
            {
                imgUri: require('../sources/icons/avatar_icon02.png'),
                name: 'Skewfried',
                desc: 'EZ32@echo',
            },
        ]

        return (
            <FlatList
                data={follower}
                renderItem={this.renderSubscriberItem}
                keyExtractor={(item, index) => index.toString()}
            />
        )

    }

    renderSubscriberItem = ({ item }) => (
        <FriendContactCard
            imgUri={item.imgUri}
            name={item.name}
            desc={item.desc}
            actionType='subscribe'
        />
    )

    renderFollowerList() {
        let follower = [
            {
                imgUri: require('../sources/icons/avatar_icon02.png'),
                name: 'Skewfried',
                desc: 'EZ32@echo',
            },
            {
                imgUri: require('../sources/icons/avatar_icon02.png'),
                name: 'Skewfried',
                desc: 'EZ32@echo',
            },
            {
                imgUri: require('../sources/icons/avatar_icon02.png'),
                name: 'Skewfried',
                desc: 'EZ32@echo',
            },
        ]

        return (
            <FlatList
                data={follower}
                renderItem={this.renderFollowerItem}
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }

    renderFollowerItem = ({ item }) => (
        <FriendContactCard
            imgUri={item.imgUri}
            name={item.name}
            desc={item.desc}
            actionType='follow'
        />
    )

    renderItemWitshlist() {
       
        return (
            <FlatList
                data={item}
                renderItem={this.renderWitshlistItem}
                keyExtractor={(item, index) => index.toString()}
                numColumns={2} // viewType = list remove numColumns
                contentContainerStyle={styles.gameItemListContainerStyle}
                style={styles.gameItemListStyle}
            />
        )

    }

    renderWitshlistItem = ({ item }) => (
        <ItemTitleCard
            imgUri={item.imgUri}
            title={item.title}
            isFavorite={item.isFavorite}
            desc={item.desc}
            prize={item.prize}
            rating={item.rating}
            mark={item.mark}
            bought={item.bought}
            viewType='item'
            style={styles.ItemTitleCardStyle}
        />
    )

    renderExpRewardList() {
        let expList = [
            {
                title: 'Share join Tournament 1 time',
                expCount: '10',
            },
            {
                title: 'Share join Tournament 1 time',
                expCount: '10',
            },
            {
                title: 'Share join Tournament 1 time',
                expCount: '10',
            }
        ]

        return (
            <FlatList
                data={expList}
                renderItem={this.renderExpItem}
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }

    renderExpItem = ({ item }) => (
        <ExpRewardTitleCard
            title={item.title}
            expCount={item.expCount}
        />
    )

    renderQuestList() {
        let quests = [
            {
                imgUri: require('../sources/images/game_img01.png'),
                title: 'SELL ‘n BUY',
                count: '10',
            },
            {
                imgUri: require('../sources/images/game_img01.png'),
                title: 'SELL ‘n BUY',
                count: '10',
            },
            {
                imgUri: require('../sources/images/game_img01.png'),
                title: 'SELL ‘n BUY',
                count: '10',
            },
            {
                imgUri: require('../sources/images/game_img01.png'),
                title: 'SELL ‘n BUY',
                count: '10',
            },
            {
                imgUri: require('../sources/images/game_img01.png'),
                title: 'SELL ‘n BUY',
                count: '10',
            },
        ]

        return (
            <FlatList
                data={quests}
                renderItem={this.renderQuestItem}
                keyExtractor={(item, index) => index.toString()}
                horizontal
                contentContainerStyle={styles.questListContainerStyle}
                style={styles.questContainerStyle}
            />
        )
    }

    renderQuestItem = ({ item }) => (
        <GameTitleCard
            imgUri={item.imgUri}
            iconUri={require('../sources/icons/trophy_icon02.png')}
            title={item.title}
            count={item.count}
            countUnit='Quest'
            style={styles.questTitleCardContainerStyle}
        />
    )

    renderShipPlanList() {
        let shipPlan = [
            {
                title: 'is BROZE',
                iconUri: require('../sources/icons/member_level_icon02.png'),
                free: true,
                owned: true,
                benefits: [
                    {
                        title: 'Access FACEIT subscriber-only competitions',
                        type: 'checked',
                    },
                    {
                        title: 'Access FACEIT subscriber-only competitions',
                        type: 'checked'
                    },
                    {
                        title: 'Access FACEIT subscriber-only competitions',
                        type: 'locked',
                        highlight: true,
                    },
                    {
                        title: 'Access FACEIT subscriber-only competitions',
                        type: 'locked',
                        highlight: true,
                    },
                ]
            },
            {
                title: 'SILVER',
                iconUri: require('../sources/icons/member_level_icon01.png'),
                prize: '158฿ / month',
                benefits: [
                    {
                        title: 'Access FACEIT subscriber-only competitions',
                        type: 'checked'
                    },
                    {
                        title: 'Access FACEIT subscriber-only competitions',
                        type: 'checked'
                    },
                    {
                        title: 'Access FACEIT subscriber-only competitions',
                        type: 'locked'
                    },
                    {
                        title: 'Access FACEIT subscriber-only competitions',
                        type: 'locked'
                    },
                ]
            },
            {
                title: 'GOLD',
                iconUri: require('../sources/icons/member_level_icon03.png'),
                prize: '299฿ / month',
                isFavorite: true,
                benefits: [
                    {
                        title: 'Access FACEIT subscriber-only competitions',
                        type: 'checked'
                    },
                    {
                        title: 'Access FACEIT subscriber-only competitions',
                        type: 'checked'
                    },
                    {
                        title: 'Access FACEIT subscriber-only competitions',
                        type: 'locked'
                    },
                    {
                        title: 'Access FACEIT subscriber-only competitions',
                        type: 'locked'
                    },
                    {
                        title: 'Access FACEIT subscriber-only competitions',
                        type: 'locked',
                        highlight: true,
                    },
                ]
            },
        ]

        return (
            <FlatList
                data={shipPlan}
                renderItem={this.renderShipPlanItem}
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }

    renderShipPlanItem = ({ item }) => (
        <MembershipPlanCard
            free={item.free}
            iconUri={item.iconUri}
            title={item.title}
            owned={item.owned}
            benefits={item.benefits}
            prize={item.prize}
            isFavorite={item.isFavorite}
        />
    )

    renderFriendList() {
        
        

        return (
            <FlatList
                data={this.state.friends}
                renderItem={this.renderFriendItem}
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }
    

    renderFriendItem = ({ item }) => (
        <FriendContactCard
            onPress={()=>this.openFriendDetail(item)}
            imgUri={{uri:item.picture}}
            name={item.username}
            desc={item.user_type}
            //lastActive={item.lastActive}
            actionType='setting'
        //isOnLine
        />
    )

    render() {
        return (
            <View style={styles.userProfileScreenContainerStyle}>
                <Headers
                    title='My Profile'
                    leftName='home'
                    rightName='searchData'
                />
                
                <UserShortProfileCard
                    //buttonType='setting'
                    imgUri={this.state.user.picture ? {uri:this.state.user.picture}:null}
                    name={this.state.user.nickname}
                    desc={this.state.user.user_type}
                    friendCount={this.state.user.friend_count}
                    videoCount={this.state.user.video_count}
                    creditCount={this.state.user.wallet}
                />
                {this.renderTabContent()}
            </View>
        )
    }
}

const styles = {
    userProfileScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    tabsHeaderStyle: {
        width: responsiveWidth(25),
    },
    tabContainerStyle: {
        paddingLeft: responsiveWidth(2.5),
        paddingRight: responsiveWidth(2.5),
        width: '100%',
    },
    whitelistTitleContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    friendTitleContainerStyle: {
        flexDirection: 'row',
        alignItems: 'flex-end',
    },
    expTitleContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    tabSectionTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    expIconStyle: {
        width: responsiveHeight(4.6),
        height: responsiveHeight(4.6)
    },
    tabSectionSubTitleTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(1.4),
        color: "#d1d3d4"
    },
    listViewIconStyle: {
        width: responsiveHeight(2.7),
        height: responsiveHeight(2.7),
    },
    questListContainerStyle: {
        paddingBottom: responsiveHeight(2),
        marginTop: responsiveHeight(1),
    },
    questContainerStyle: {
        marginBottom: responsiveHeight(1),
    },
    questTitleCardContainerStyle: {
        marginRight: responsiveWidth(4),
    },
    itemListContainerStyle: {
        alignItems: 'center'
    },
    gameItemListContainerStyle: {
        paddingBottom: responsiveHeight(2),
    },
    ItemTitleCardStyle: {
        margin: responsiveWidth(1),
    },

}
