import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity,WebView } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { MainTabs } from '../components/MainTabs'
import { VideoListSection } from '../components/VideoListSection'

import TournamentHomeScreen from './TournamentHomeScreen';
import MarketHomeScreen from './MarketHomeScreen';
import InventoryHomeScreen from './InventoryHomeScreen';
import { observer, inject } from 'mobx-react';
import Loading from '../components/loading';
import {get} from '../api';
import YouTube from 'react-native-youtube'
import Video from 'react-native-af-video-player'
import FastImage from 'react-native-fast-image';
// import KSYVideo from 'react-native-ksyvideo';
@inject('naviStore','notiStore')
@observer
export default class LiveHomeScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            recommended:[],isLoading:true,live:{}
        }
        if(!this.props.naviStore.navigation){
            this.props.naviStore.navigation = this.props.navigator;
        }
        this.props.naviStore.selectMenu = "esgn.LiveHomeScreen"
    }

    renderTabContent() {
        let tabData = [
            {
                iconUri: require('../sources/icons/live_icon01.png')
            },
            {
                iconUri: require('../sources/icons/trophy_icon01.png')
            },
            {
                iconUri: require('../sources/icons/market_icon01.png')
            },
            {
                iconUri: require('../sources/icons/inventory_icon01.png')
            },
        ]

        return (
            <MainTabs
                headingType='image'
                tabData={tabData}
                child={this.renderTabChild()}
                headingType='image'
            />
        )
    }
    async componentDidMount(){
        this.props.naviStore.navigation = this.props.navigator;
        let [livesHomeRes,liveRes,notiRes] = await Promise.all([get("channel/video/all?limit=6")
        ,get("live/all?limit=1"),get("notification/my")]);
        this.setState({isLoading:false});
        if(livesHomeRes){
            console.log(livesHomeRes.videos);
            this.setState({recommended:livesHomeRes.videos})
        }
        if(liveRes && liveRes.lives && liveRes.lives.length > 0){
            console.log(liveRes.lives);
            this.setState({live:liveRes.lives[0]})
        }
        if(notiRes){
            this.props.notiStore.unwatch = notiRes.unwatch_notfication_count;
            this.props.notiStore.notifications = notiRes.notifications;
        }
    }


    renderTabChild() {
        return [
            <View style={styles.tabChildContainerStyle}>
                <TouchableOpacity onPress={()=>this.gotoLive(this.state.live)} style={styles.mainLiveContainerStyle}>
                    {/* <Image
                        source={require('../sources/images/live_img01.png')}
                        style={styles.mainLiveImgStyle}
                    /> */}
                    <FastImage
                        source={{uri:this.state.live.thumbnail}}
                        style={styles.mainLiveImgStyle}
                    />
                    <View style={[styles.mainLiveImgStyle,{position:'absolute',alignItems: 'center'
                    ,justifyContent: 'center',}]}>
                        <Image source={require('../sources/icons/live_icon04.png')}/>
                    </View>
                    <View style={[styles.mainLiveImgStyle,{position:'absolute',alignItems: 'flex-end'
                    ,justifyContent: 'flex-end',}]}>
                        <Image source={require('../sources/icons/live_icon03.png')} 
                        style={{height:responsiveHeight(5),width:responsiveHeight(10) 
                        ,marginBottom:responsiveHeight(1) ,}}/>
                    </View>
                    
                </TouchableOpacity>
                <View>
                 <Text style={styles.descTextStyle}>{this.state.live.title}</Text>
                 <Text style={styles.descTextStyle}>{this.state.live.total_viewer} Watching now</Text>
                </View>
                <VideoListSection
                    title='Recommended'
                    desc='Based on your History'
                    videoList={this.state.recommended}
                    style={styles.videoListSectionStyle}
                    onPress={(item)=>this.gotoLive(item)}
                    seeAllVideo={()=>this.gotoLiveAll()}
                />
               
                <Loading visible={this.state.isLoading} />
            </View>,
            <View style={{flex:1}}>
                <TournamentHomeScreen isHome={true}/>
            </View>,
            <View style={{flex:1}}>
                <MarketHomeScreen isHome={true}/>
            </View>,
            <View style={{flex:1}}>
                <InventoryHomeScreen isHome={true}/>
            </View>,
        ]
    }
   

    render() {
        return (
            <View style={styles.lveHomeScreenContainerStyle}>
                <Headers
                    leftName='home'
                    rightName='searchData'
                />
                {this.renderTabContent()}
            </View>
        )
    }
    gotoLive(item){
        this.props.naviStore.navigation.push({
            screen: 'esgn.LivingScreen', // unique ID registered with Navigation.registerScreen
            title: undefined, // navigation bar title of the pushed screen (optional)
            titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
            animated: true, // does the push have transition animation or does it happen immediately (optional)
            backButtonTitle: undefined, // override the back button title (optional)
            backButtonHidden: false, // hide the back button altogether (optional)
            animationType: 'fade',
            passProps:{data:item}
        })
    }
    gotoLiveAll(){
        this.props.naviStore.navigation.push({
            screen: 'esgn.LiveListScreen', // unique ID registered with Navigation.registerScreen
            title: undefined, // navigation bar title of the pushed screen (optional)
            titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
            animated: true, // does the push have transition animation or does it happen immediately (optional)
            backButtonTitle: undefined, // override the back button title (optional)
            backButtonHidden: false, // hide the back button altogether (optional)
            animationType: 'fade'
        })
    }
}

const styles = {
    lveHomeScreenContainerStyle: {
        flex: 1,
    },
    tabChildContainerStyle: {
        flex: 1,
    },
    mainLiveContainerStyle: {
        width: responsiveWidth(95),
        height: responsiveHeight(31),
    },
    mainLiveImgStyle: {
        width: responsiveWidth(95),
        height: responsiveHeight(31),
    },
    videoListSectionStyle: {
        marginTop: responsiveHeight(2),
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    descTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.6),
        color: "#ffffff",
        width:responsiveWidth(80)
    },
}