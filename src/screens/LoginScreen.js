import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity,Keyboard,KeyboardAvoidingView,TextInput } from 'react-native'
import { Input, Item, Button, Icon } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import app from '../stores/app'
import store from 'react-native-simple-store'
import {authen} from '../api';
import Loading from '../components/loading'
import DeviceInfo from 'react-native-device-info';
import { Navigation } from 'react-native-navigation';
import { observer, inject } from 'mobx-react';
import {FBLoginManager} from 'react-native-facebook-login';
import RCTDeviceEventEmitter from 'RCTDeviceEventEmitter';
import { GoogleSignin, statusCodes } from 'react-native-google-signin';

@inject('naviStore')
@observer
export default class LoginScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            userPassword: '',
            idError: false,
            passwordError: false,
            isLoading:false
        }
        this.app = app;
        this.onLoginPress = this.onLoginPress.bind(this);
        if(!this.props.naviStore.navigation){
            this.props.naviStore.navigation = this.props.navigator;
        }
        GoogleSignin.configure({
            scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
            iosClientId: "813899615351-cpqijgmkmuoo2qgrrrcu2ingco10grup.apps.googleusercontent.com", // only for iOS
            webClientId: "813899615351-cpqijgmkmuoo2qgrrrcu2ingco10grup.apps.googleusercontent.com", // only for iOS
            offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
            hostedDomain: '', // specifies a hosted domain restriction 
          })
    }

    async onLoginPress(){
        let isError = false;
        if (this.state.userId.length == 0) {
            this.setState({
                idError: true,
            })
            isError = true;
        }
        if (this.state.userPassword.length == 0) {
            this.setState({
                passwordError: true,
            })
            isError = true;
        }
        if(isError){
          
        }else{
            const uniqueId = DeviceInfo.getUniqueID();
            // this.setState({isLoading:true});
            let param = {};
            param.username = this.state.userId;
            param.password = this.state.userPassword;
            param.uuid = uniqueId;
            let response = await authen("user/loginasuser",param);
            
            if(response && response.result){
                let user = response.result.user;
                user.token = response.result.token;
                await store.save("user",user);
                setTimeout(()=>{
                    this.app.login();
                    // this.setState({isLoading:false});
                },500)
                
            }else{
                this.setState({isLoading:false});
            }
            // this.setState({isLoading:false});
        }
       

    }
    async loginFacebook(){
        FBLoginManager.setLoginBehavior(FBLoginManager.LoginBehaviors.Web);
        FBLoginManager.loginWithPermissions(["email","user_friends","public_profile"],(error,data)=>this.callLoginFacebook(data,error))
    }
    async loginGoogle(){
        try {
            // Add any configuration settings here:
        
            const userInfo = await GoogleSignin.signIn();
            console.log(JSON.stringify(userInfo));
            const uniqueId = DeviceInfo.getUniqueID();
            const {user} = userInfo;
            let param = {};
            param.uuid = uniqueId;
            param.email = user.email;
            param.nickname = user.name;
            param.firstname = user.givenName;
            param.lastname = user.familyName;
            param.image_url = user.photo;
            param.google_id = user.id;
            param.username = user.id;
            let response = await authen("user/loginwithgoogle_notoken",param);
            if(response && response.result){
                let user = response.result.user;
                user.token = response.result.token;
                await store.save("user",user);
                setTimeout(()=>{
                    this.app.login();
                    // this.setState({isLoading:false});
                },500)
            }

          } catch (e) {
            console.error(e);
          }
    }
   async callLoginFacebook(data,error){
            if (!error) {
                console.log(data);
                const uniqueId = DeviceInfo.getUniqueID();
                let param = {};
                param.uuid = uniqueId;
                param.facebook_id = data.credentials.userId;
                param.access_token = data.credentials.token
                let response = await authen("user/loginwithfacebook",param);
               
                if(response && response.result){
                    let user = response.result.user;
                    let fbResponse = await fetch(`https://graph.facebook.com/v2.3/${data.credentials.userId}/picture?width=${200}&redirect=false&access_token=${data.credentials.token}`);
                    let dataParse = await  fbResponse.json();
                    user.picture = dataParse.data.url;
                    user.token = response.result.token;
                    await store.save("user",user);
                    setTimeout(()=>{
                        this.app.login();
                    },500)
                    
                }
            } else {
                console.log("Error: ", error);
            }
            
    }
    

    render() {

        const { userId, userPassword, idError, passwordError } = this.state

        return (
            <KeyboardAvoidingView behavior="padding" style={styles.loginScreenContainerStyle}>
                {this.state.isLoading && <Loading visible={this.state.isLoading} />}
                <Image
                    source={require('../sources/icons/app_icon01.png')}
                    resizeMode='contain'
                    style={styles.appIconStyle}
                />
                <View style={styles.loginContainerStyle}>
                    <Item error={idError}>
                        <Input
                            value={userId}
                            placeholder='ESGN ID'
                            placeholderTextColor='#808285'
                            style={styles.loginInputStyle}
                            onChangeText={(userId) => this.setState({ userId, idError: false })}
                            blurOnSubmit={ false }
                        />
                    </Item>
                    {idError ? <Text style={styles.errorTextStyle}>Enter your ID</Text> : <Text style={styles.errorTextStyle} />}
                    <Item error={passwordError}>
                        <Input
                            secureTextEntry={true}
                            value={userPassword}
                            placeholder='PASSWORD'
                            placeholderTextColor='#808285'
                            style={styles.loginInputStyle}
                            onChangeText={(userPassword) => this.setState({ userPassword, passwordError: false })}
                            blurOnSubmit={ true }
                        />
                    </Item>
                    {passwordError ? <Text style={styles.errorTextStyle}>Enter your password</Text> : <Text style={styles.errorTextStyle} />}
                    {/* <TouchableOpacity style={styles.forgetPasswordButtonStyle} onPress={()=>{
                           this.props.navigator.resetTo({
                            screen: 'esgn.ForgotPasswordScreen', // unique ID registered with Navigation.registerScreen
                            title: undefined, // navigation bar title of the pushed screen (optional)
                            passProps: {}, // simple serializable object that will pass as props to the pushed screen (optional)
                            animated: true, // does the resetTo have transition animation or does it happen immediately (optional)
                            animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the resetTo have different transition animation (optional)
                            navigatorStyle: {}, // override the navigator style for the pushed screen (optional)
                            navigatorButtons: {} // override the nav buttons for the pushed screen (optional)
                          });
                        }}>
                        <Text style={styles.forgetPasswordTextStyle}>Forget your Password?</Text>
                    </TouchableOpacity> */}
                    <Button style={styles.loginButtonStyle} onPress={this.onLoginPress}>
                        <Text style={styles.loginButtonTextStyle}>LOG IN</Text>
                    </Button>
                    <View style={styles.signupButtonContainerStyle}>
                        {/* <Text style={styles.signupDescTextStyle}>Dont’t have an Account?</Text> */}
                        {/* <TouchableOpacity onPress={()=>{
                           this.props.navigator.resetTo({
                            screen: 'esgn.AppRegisterScreen', // unique ID registered with Navigation.registerScreen
                            title: undefined, // navigation bar title of the pushed screen (optional)
                            passProps: {}, // simple serializable object that will pass as props to the pushed screen (optional)
                            animated: true, // does the resetTo have transition animation or does it happen immediately (optional)
                            animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the resetTo have different transition animation (optional)
                            navigatorStyle: {}, // override the navigator style for the pushed screen (optional)
                            navigatorButtons: {} // override the nav buttons for the pushed screen (optional)
                          });
                        }}>
                            <Text style={styles.signupButtonTextStyle}>SIGN UP</Text>
                        </TouchableOpacity> */}
                    </View>
                    <Text style={styles.forgetPasswordTextStyle}>Or</Text>
                    <Button onPress={()=>this.loginFacebook()} iconLeft style={styles.facebookButtonStyle}>
                        <Icon type='EvilIcons' name='sc-facebook' style={styles.facebookIconStyle} />
                        <Text style={styles.loginOtherTextStyle}>Login with Facebook</Text>
                    </Button>
                    <Button onPress={()=>this.loginGoogle()} iconLeft style={styles.googleButtonStyle}>
                        <Icon type='Entypo' name='google-' style={styles.googleIconStyle} />
                        <Text style={styles.loginOtherTextStyle}>Login with Google</Text>
                    </Button>
                </View>
            </KeyboardAvoidingView>
        )
    }
}

const styles = {
    loginScreenContainerStyle: {
        flex: 1,
        backgroundColor: "#e6e7e8",
        alignItems: 'center',
    },
    loginContainerStyle: {
        width: responsiveWidth(87),
        marginTop: responsiveHeight(5),
    },
    errorTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.6),
        color: "#e42526",
        marginTop: responsiveHeight(0.5),
        marginBottom: responsiveHeight(2),
    },
    forgetPasswordButtonStyle: {
        width: responsiveWidth(45),
        alignSelf: 'center',
    },
    forgetPasswordTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.6),
        textAlign: "center",
        color: "#707171"
    },
    facebookButtonStyle: {
        width: responsiveWidth(85),
        height: responsiveHeight(7),
        borderRadius: 4,
        backgroundColor: "#3757a7",
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        marginTop: responsiveHeight(2),
    },
    facebookIconStyle: {
        fontSize: responsiveFontSize(5),
        marginRight: responsiveWidth(5),
        marginLeft: 0,
    },
    googleIconStyle: {
        fontSize: responsiveFontSize(3.5),
        marginRight: responsiveWidth(8),
        marginLeft: 0,
    },
    googleButtonStyle: {
        width: responsiveWidth(85),
        height: responsiveHeight(7),
        borderRadius: 4,
        backgroundColor: "#e42526",
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        marginTop: responsiveHeight(2),
    },
    loginOtherTextStyle: {
        fontFamily: "BentonSans",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    appIconStyle: {
        height: responsiveHeight(12),
        marginTop: responsiveHeight(10),
    },
    loginInputStyle: {
        fontFamily: 'Kanit',
        color: '#808285',
        fontSize: responsiveFontSize(2),
        paddingLeft: 0,
    },
    loginButtonStyle: {
        width: responsiveWidth(85),
        height: responsiveHeight(7),
        borderRadius: 4,
        backgroundColor: "#414042",
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: responsiveHeight(2),
    },
    loginButtonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    signupButtonContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: responsiveHeight(2),
        marginBottom: responsiveHeight(2),
    },
    signupDescTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#414042",
        marginRight: responsiveWidth(3),
    },
    signupButtonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#557ebf",
        textDecorationLine: 'underline'
    }
}