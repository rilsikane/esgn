import React, { Component } from 'react'
import { View, Text, FlatList } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { GameItemCoverCard } from '../components/GameItemCoverCard'

export default class MarketGameListScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }
  
    

    renderGameList() {
        let game = [
            {
                imgUri: require('../sources/images/game_cover_img05.png'),
            },
            {
                imgUri: require('../sources/images/game_cover_img06.png'),
            },
            {
                imgUri: require('../sources/images/game_cover_img07.png'),
            },
            {
                imgUri: require('../sources/images/game_cover_img08.png'),
            },
        ]
        return (
            <FlatList
                data={game}
                renderItem={this.renderGameItem}
                keyExtractor={(item, index) => index.toString()}
                contentContainerStyle={styles.marketGameListContainerStyle}
            />
        )

    }

    renderGameItem = ({ item }) => (
        <GameItemCoverCard
            imgUri={item.imgUri}
        />
    )

    render() {
        return (
            <View style={styles.marketGameListScreenContainerStyle}>
                <Headers
                    title='Market'
                    leftName='home'
                    rightName='searchData'
                />
                {this.renderGameList()}
            </View>
        )
    }
}

const styles = {
    marketGameListScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    marketGameListContainerStyle: {

    }
}
