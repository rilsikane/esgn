import React, { Component } from 'react';
import { View, Text,FlatList } from 'react-native';
import { ItemTitleCard } from '../components/ItemTitleCard'
import Loading from '../components/loading';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import {get,post} from '../api';
import { observer, inject } from 'mobx-react';
import {convertCurrency,convertNumber} from '../components/utils/number';
@inject('naviStore')
@observer
export default class ItemWishlistScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading:false,items:[]
    };
  }
  async componentDidMount(){
    this.setState({isLoading:true});
    let response = await get("market/wishlist_items");
    this.setState({isLoading:false});
    if(response){
      this.setState({items:response.items})
    }
  }
  goToDetail(link,item){
    if(link)
    {
        this.props.naviStore.navigation.push({
            screen: link, // unique ID registered with Navigation.registerScreen
            title: undefined, // navigation bar title of the pushed screen (optional)
            titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
            animated: true, // does the push have transition animation or does it happen immediately (optional)
            backButtonTitle: undefined, // override the back button title (optional)
            backButtonHidden: false, // hide the back button altogether (optional)
            animationType: 'fade',
            passProps:{item:item}
        })
    }
}
  renderWitshlistItem = ({ item }) => (
    <ItemTitleCard
    onPress={()=>this.goToDetail("esgn.MarketItemDetailScreen",item)}
    wishlist={()=>this.wishlistItem(item)}
    imgUri={{uri:item.image}}
    title={item.name}
    desc={item.description}
    prize={convertNumber(item.current_price)}
    rating={Number(item.avg_rate)}
    viewType={'item'}
    style={styles.ItemTitleCardStyle}
    isReview={true}
    isFavorite={true}
  />
)
async wishlistItem(item){
        
  let response = await post(`market/delete_from_wishlist/${item.item_id}`);
  if(response){
    let response2 = await get("market/wishlist_items");
    this.setState({isLoading:false});
    if(response2){
      this.setState({items:response2.items})
    }
  }
}
  render() {
    return (
      <View style={{flex:1}}>
        <View style={styles.whitelistTitleContainerStyle}>
            <View style={styles.friendTitleContainerStyle}>
                <Text style={styles.tabSectionTitleTextStyle}>Wishlist </Text>
                <Text style={styles.tabSectionSubTitleTextStyle}>{`(${this.state.items.length})`}</Text>
            </View>
            {/* <TouchableOpacity>
                <Image
                    source={require('../sources/icons/list_icon01.png')}
                    resizeMode='contain'
                    style={styles.listViewIconStyle}
                />
            </TouchableOpacity> */}
        </View>
        <View style={styles.itemListContainerStyle}>
        
        <FlatList
            data={this.state.items}
            renderItem={this.renderWitshlistItem}
            keyExtractor={(item, index) => index.toString()}
            numColumns={2} // viewType = list remove numColumns
            contentContainerStyle={styles.gameItemListContainerStyle}
            style={styles.gameItemListStyle}
        />
        </View>
        <Loading visible={this.state.isLoading}/>
      </View>
    )
  }
}
const styles = {
  gameItemListContainerStyle: {
    paddingBottom: responsiveHeight(2),
  },
  ItemTitleCardStyle: {
      margin: responsiveWidth(1),
  },
  whitelistTitleContainerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  friendTitleContainerStyle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
 tabSectionTitleTextStyle: {
  fontFamily: "Kanit",
  fontSize: responsiveFontSize(2),
  color: "#ffffff"
 },
 tabSectionSubTitleTextStyle: {
  fontFamily: "Prompt",
  fontSize: responsiveFontSize(1.4),
  color: "#d1d3d4",
  alignItems:'center'
 },
 itemListContainerStyle: {
  alignItems: 'flex-start'
 },
}
