import React, { Component } from 'react'
import { View, Text, ScrollView, Image } from 'react-native'
import { Thumbnail } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Headers } from '../components/Headers'
import { MainTabs } from '../components/MainTabs'
import { TeamDetailCard } from '../components/TeamDetailCard'
import Loading from '../components/loading';
import {get,post} from '../api';
import FastImage from 'react-native-fast-image';
import {convertNumber}  from '../components/utils/number';
export default class TeamDetailScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isLoading:false,teamInfo:{}
        }
    }
    componentDidMount() {
        // contestant_id
        this.init();
    }
    async init(){
        this.setState({isLoading:true});
        let teamResp = await get(`team/${this.props.data.contestant_id}/getinfo`);
        this.setState({isLoading:false});
        if(teamResp && teamResp.result){
            this.setState({teamInfo:teamResp.result.team});
        }
    }

    renderTabContent() {
        let tabData = [
            {
                title: 'Info',
            },
            {
                title: 'Stats',
            },
            {
                title: 'Member',
            },
            {
                title: 'Match',
            },
        ]

        return (
            <MainTabs
                tabData={tabData}
                child={this.renderTabChild()}
                headingType='text'
            />
        )
    }

    renderTabChild() {
        return [
            <TeamDetailCard 
                desc={this.state.teamInfo.team_info.description}
            />
        ]
    }

    render() {
        return (
            <View style={styles.teamDetailScreenContainerStyle}>
                <Headers
                    title='Team'
                    leftName='back'
                    rightName='searchData'
                />
                {this.state.teamInfo.team_info && <ScrollView contentContainerStyle={styles.teamDetailContainerStyle}>
                    <View style={styles.teamThumbContainerStyle}>
                        <FastImage
                            source={{uri:this.state.teamInfo.team_info.logo}}
                            style={styles.teamThumbStyle}
                        />
                    </View>
                    <Text style={styles.teamNameTextStyle}>{this.state.teamInfo.team_info.name}</Text>
                    <Text style={styles.teamInfoTextStyle}># Sub Team Info.</Text>
                    <Text style={styles.teamDescTextStyle}>{this.state.teamInfo.team_info.tag}</Text>
                    <View style={styles.teamRankContainerStyle}>
                        {/* <View style={styles.rankSectionLeftStyle}>
                            <View style={styles.rankStateContainerStyle}>
                                <Image
                                    source={require('../sources/icons/rank_up_icon01.png')}
                                    resizeMode='contain'
                                    style={styles.rankStateIconStyle}
                                />
                                <Text style={styles.rankStateTextStyle}>1.836</Text>
                            </View>
                            <Text style={styles.rankStateLabelStyle}>Team Ranking</Text>
                        </View> */}
                        <View style={styles.rankSectionRightStyle}>
                            {/* <View style={styles.rightRankSectionStyle}>
                                <Text style={styles.rankLabelTextStyle}>Rank :</Text>
                                <Text style={styles.rankNumberTextStyle}>56</Text>
                            </View> */}
                            <View style={styles.rightRankSectionStyle}>
                                <Text style={styles.rankLabelTextStyle}>Total Prize :</Text>
                                <Text style={styles.rankPointTextStyle}>{convertNumber(this.state.teamInfo.prize.total_prize)} ฿</Text>
                            </View>
                        </View>
                    </View>
                    {this.renderTabContent()}
                    <Loading visible={this.state.isLoading}/>
                </ScrollView>}
            </View>
        )
    }
}

const styles = {
    teamDetailScreenContainerStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    teamDetailContainerStyle: {
        alignItems: 'center',
    },
    teamThumbContainerStyle: {
        width: responsiveHeight(19),
        height: responsiveHeight(19),
        borderRadius: responsiveHeight(9.5),
        marginTop: responsiveHeight(5),
    },
    teamThumbStyle: {
        width: responsiveHeight(19),
        height: responsiveHeight(19),
        borderRadius: responsiveHeight(9.5),
    },
    teamNameTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(4),
        color: "#ffffff"
    },
    teamInfoTextStyle: {
        fontFamily: "BentonSans",
        fontSize: responsiveFontSize(2),
        color: "#989898"
    },
    teamDescTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#f1f0f0",
        marginTop: responsiveHeight(5),
        marginRight: responsiveWidth(2.5),
        width: responsiveWidth(70),
        alignSelf: 'flex-end',
    },
    teamRankContainerStyle: {
        flexDirection: 'row',
    },
    rankSectionLeftStyle: {
        flex: 0.5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    rankSectionRightStyle: {
        flex: 0.5,
        justifyContent: 'center',
    },
    rightRankSectionStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        width: '100%',
    },
    rankLabelTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#f1f0f0",
        marginRight: responsiveWidth(2),
    },
    rankPointTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#f1f0f0"
    },
    rankNumberTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#d81f2a"
    },
    rankStateContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    rankStateIconStyle: {
        width: responsiveHeight(1.8),
        height: responsiveHeight(1.8),
        marginRight: responsiveWidth(1),
    },
    rankStateTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(4.5),
        color: "#f1f0f0"
    },
    rankStateLabelStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#f1f0f0"
    }
}