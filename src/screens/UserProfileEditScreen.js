import React, { Component } from 'react'
import { View, Text, ImageBackground, TouchableOpacity, Image, ScrollView } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import { Button } from 'native-base'

import { ErrorInput } from '../components/ErrorInput'
import { AuthSwitch } from '../components/AuthSwitch'
import { BirthDatePicker } from '../components/BirthDatePicker'
import { PhonePicker } from '../components/PhonePicker'
import { observer, inject } from 'mobx-react';
@inject('naviStore')
@observer
export default class UserProfileEditScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            profileEditable: false,
            turnOnSec: false,
            accId: '',
            accName: '',
            password: '',
            rePassword: '',
            email: '',
            confirmEmail: '',
            fullName: '',
            gender: '',
            address: '',
            telNo: '',
            country: '',
            picture:undefined,
            accNameError: false,
            passwordError: false,
            rePasswordError: false,
            emailError: false,
            fullNameError: false,
            genderError: false,
            addressError: false,
            telNoError: false,
            errorPasswordText: undefined,
            errorEmailText: undefined,
        }
    }
    componentDidMount(){
        const user = this.props.user;
        this.setState({
            picture:user.picture,
            accId: user.username,
            accName: user.nickname,
            email: user.email,
            fullName: user.firstname  ? `${user.firstname} ${user.lastname}`:undefined,
            telNo:user.phone_number,
            address:user.address,
            gender:user.gender
        })

    }

    onSubmit = () => {
        if (!this.state.profileEditable) {
            if (!this.state.accNameError && !this.state.rePasswordError && !this.state.emailError &&
                !this.state.fullNameError && !this.state.genderError && !this.state.addressError &&
                !this.state.telNoError
            ) {

            }
        }
        this.setState({
            profileEditable: !this.state.profileEditable
        })
    }

    onCancelEdit = () => {
        this.setState({
            profileEditable: false,
        })
    }

    validateMatching(field1, field2, fieldName) {
        if (fieldName == 'password') {
            if (field1.length < 8) {
                this.setState({
                    rePasswordError: true,
                    errorPasswordText: 'Password must be more than 8 characters.'
                })
            } else {
                if (field1 != field2) {
                    this.setState({ rePasswordError: true, errorPasswordText: 'Password does not match.' })
                }
            }
        } else if (fieldName == 'email') {
            if (field1.indexOf('@') == -1) {
                this.setState({
                    emailError: true,
                    errorEmailText: 'Invalid email.'
                })
            } else if (field1 != field2 && field2.length > 0) {
                this.setState({
                    emailError: true,
                    errorEmailText: 'Email does not match.'
                })
            }
        }



    }

    render() {
        const { profileEditable, turnOnSec, accId, accName, password, rePassword, email, fullName, gender, address,
            telNo, country, accNameError, passwordError, rePasswordError, fullNameError, genderError, addressError,
            telNoError, errorPasswordText, emailError, confirmEmail, errorEmailText,picture
        } = this.state
        let genders = [{ value: 'male', }, { value: 'female', },]

        return (
            <View style={styles.userEditProfileScreenContainerStyle}>
                <View style={styles.headersContainerStyle}>
                    <View style={styles.leftItemContainerStyle}>          
                        <TouchableOpacity onPress={(e)=> this.props.naviStore.navigation.pop()}>
                            <Text style={styles.backTextStyle}>{'<'} Back</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <ScrollView style={{ flex: 1 }} contentContainerStyle={styles.contentContainerStyle}>
                    <TouchableOpacity disabled={!profileEditable} style={styles.userThumbContainerStyle}>
                        <ImageBackground
                            source={picture ? {uri:picture}:require('../sources/icons/user_icon02.png')}
                            style={styles.userThumbStyle}
                        >
                            {profileEditable &&
                                <Image
                                    source={require('../sources/icons/camera_icon02.png')}
                                    resizeMode='contain'
                                    style={styles.cameraIconStyle}
                                />
                            }
                        </ImageBackground>
                    </TouchableOpacity>
                    <View style={styles.detailContainerStyle}>
                        <Text style={styles.labelTextStyle}>ACCOUNT INFO</Text>
                        <ErrorInput
                            value={accId}
                            placeholder='ACCOUNT ID'
                            iconName='pen'
                            editable={false}
                            errorText={'Error'}
                        />
                        <ErrorInput
                            value={accName}
                            error={accNameError}
                            placeholder='ACCOUNT NAME'
                            iconName='pen'
                            editable={profileEditable}
                            errorText={'Error'}
                            onChangeText={(accName) => this.setState({ accName, accNameError: false })}
                        />
                        <ErrorInput
                            value={password}
                            placeholder='PASSWORD'
                            iconName='pen'
                            editable={profileEditable}
                            errorText={errorPasswordText}
                            secureTextEntry
                            onChangeText={(password) => this.setState({ password, passwordError: false, rePasswordError: false })}
                            onBlur={() => this.validateMatching(password, rePassword, 'password')}
                        />
                        {profileEditable &&
                            <ErrorInput
                                value={rePassword}
                                error={rePasswordError}
                                placeholder='Re-PASSWORD'
                                iconName='pen'
                                editable={profileEditable}
                                errorText={errorPasswordText}
                                secureTextEntry
                                onChangeText={(rePassword) => this.setState({ rePassword, rePasswordError: false })}
                                onBlur={() => this.validateMatching(rePassword, password, 'password')}
                            />
                        }
                        <ErrorInput
                            value={email}
                            placeholder='Email'
                            iconName='pen'
                            editable={profileEditable}
                            errorText={'Invalid Email.'}
                            onBlur={() => this.validateMatching(email, confirmEmail, 'email')}
                            onChangeText={(email) => this.setState({ email, emailError: false, })}
                            keyboardType='email-address'
                        />
                        {profileEditable &&
                            <ErrorInput
                                error={emailError}
                                value={confirmEmail}
                                placeholder='Re-Email'
                                iconName='pen'
                                editable={profileEditable}
                                errorText={errorEmailText}
                                onBlur={() => this.validateMatching(confirmEmail, email, 'email')}
                                onChangeText={(confirmEmail) => this.setState({ confirmEmail, emailError: false, })}
                                keyboardType='email-address'
                            />
                        }
                        <Text style={styles.labelTextStyle}>ACCOUNT SECURITY</Text>
                        <AuthSwitch
                            labelText='2 STEPS AUTHENTICATION'
                            state={turnOnSec}
                            onValueChange={(turnOnSec) => this.setState({ turnOnSec })}
                            disabled={!profileEditable}
                        />
                        <Text style={styles.labelTextStyle}>PERSONAL DETAILS</Text>
                        <ErrorInput
                            value={fullName}
                            error={fullNameError}
                            placeholder='NAME - SURNAME'
                            iconName='pen'
                            editable={profileEditable}
                            errorText={'Error'}
                            onChangeText={(fullName) => this.setState({ fullName, fullNameError: false })}
                        />
                        <ErrorInput
                            value={gender}
                            placeholder='GENDER'
                            iconName='pen'
                            editable={profileEditable}
                            error={genderError}
                            errorText={'Enter your gender.'}
                            inputType='dropdown'
                            dropdownData={genders}
                            onChangeText={(genders) => this.setState({ genders })}
                        />
                        <BirthDatePicker
                            error={false}
                            errorText='Enter your Birth-Date.'
                            dateValue={''}
                            monthValue={''}
                            yearValue={''}
                            style={styles.birthDatePickerStyle}
                        // onDatePress={}
                        // onMonthPress={}
                        // onYearPress={}
                        />
                        <ErrorInput
                            value={address}
                            error={addressError}
                            placeholder='ADDRESS'
                            iconName='pen'
                            editable={profileEditable}
                            errorText={'Enter your address.'}
                            onChangeText={(address) => this.setState({ address, addressError: false })}
                        />
                        <PhonePicker
                            error={telNoError}
                            errorText='Error'
                            countryValue={country}
                            telValue={telNo}
                            style={styles.birthDatePickerStyle}
                            editable={profileEditable}
                            onTelChangeText={(telNo) => this.setState({ telNo, telNoError: false })}
                        />
                        {profileEditable &&
                            < Button onPress={this.onCancelEdit} rounded style={[styles.submitButtonStyle, styles.cancelButton]}>
                                <Text style={styles.submitButtonTextStyle}>Cancel</Text>
                            </Button>
                        }
                        <Button onPress={this.onSubmit} rounded style={[styles.submitButtonStyle,]}>
                            <Text style={styles.submitButtonTextStyle}>{profileEditable ? 'Submit' : 'Edit'}</Text>
                        </Button>
                    </View>
                </ScrollView>
            </View >
        )
    }
}

const styles = {
    userEditProfileScreenContainerStyle: {
        flex: 1,
        backgroundColor: "#f1f2f2",
    },
    contentContainerStyle: {
        alignItems: 'center',

    },
    userThumbContainerStyle: {
        height: responsiveHeight(13.4),
        marginTop: responsiveHeight(8),
    },
    userThumbStyle: {
        width: responsiveHeight(13.4),
        height: responsiveHeight(13.4),
        borderRadius: responsiveHeight(6.7),
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
    },
    cameraIconStyle: {
        width: responsiveHeight(3.2),
        height: responsiveHeight(3.2),
    },
    detailContainerStyle: {
        width: '100%',
    },
    labelTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveHeight(2),
        color: "#414042",
        marginLeft: responsiveWidth(5),
        marginTop: responsiveHeight(2),
    },
    authContainerStyle: {
        paddingLeft: responsiveWidth(5),
        paddingRight: responsiveWidth(5),
    },
    subLabelStyle: {
        fontFamily: 'Kanit',
        color: '#808285',
        fontSize: responsiveFontSize(2),
    },
    switchContainerStyle: {

    },
    turnoffTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#e42526"

    },
    birthDatePickerStyle: {
        marginTop: responsiveHeight(1),
    },
    submitButtonStyle: {
        width: responsiveWidth(85),
        height: responsiveHeight(8),
        backgroundColor: "#6a85c3",
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        marginBottom: responsiveHeight(5),
        marginTop: responsiveHeight(2),
    },
    cancelButton: {
        backgroundColor: "#e42526",
        marginBottom: responsiveHeight(1),
    },
    submitButtonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    headersContainerStyle: {
        height: responsiveHeight(12),
        backgroundColor: 'transparent',
        borderBottomWidth: 1,
        borderColor: 'transparent',
        position:"absolute",
        top:responsiveHeight(4),
        zIndex:999
    },
    leftItemContainerStyle: {
        flex: 0.25,
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft:10
    },
    backTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveHeight(2.5),
        color: "#414042"
    }

}