module.exports = [
    {
      _id: Math.round(Math.random() * 1000000),
      text: 'โอเคครับ',
      createdAt: new Date(Date.UTC(2016, 7, 30, 17, 20, 0)),
      user: {
        _id: 1,
        name: 'User',
      },
      sent: true,
      received: true,
    },
    {
      _id: Math.round(Math.random() * 1000000),
      text: 'ทดสอบ Chat ESGN',
      createdAt: new Date(Date.UTC(2016, 7, 30, 17, 20, 0)),
      user: {
        _id: 2,
        name: 'ESGN',
      },
    },
    {
      _id: Math.round(Math.random() * 1000000),
      text: "You are officially rocking GiftedChat.",
      createdAt: new Date(Date.UTC(2016, 7, 30, 17, 20, 0)),
      system: true,
    },
  ];