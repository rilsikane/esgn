import axios from 'axios';
// import RNFetchBlob from 'react-native-fetch-blob'



//dev
//const endpoint = "http://103.208.27.13/eRegister/API/";
const endpoint = "https://dev-socket-apis.esgn-tc.com/v2/api/";
//const endpoint = "http://apiilease.ddns.net/eRegister/API/"





import store from 'react-native-simple-store';
import app from '../stores/app';
import {
    Alert,Linking,NetInfo,Platform
} from 'react-native';
import { NetworkInfo } from 'react-native-network-info';

export async function authen(path,param){
  
  let requestURL = `${endpoint}${path}`;
      try{
          let response = await  axios.post(requestURL, param);
          console.log(response);
          if(response.status=='200' && response.data.warning_code==0 && response.data.error_code==0){
           
            return response.data.data;
          }else{
                setTimeout(()=>{
                  Alert.alert(
                    response.data.title,
                    response.data.friendly_msg_th,
                    [
                    {text: 'OK', onPress: () => false},
                    ]
                  )
                },200)
               
              return false;
          }
      }catch(e){
            Alert.alert(
            'เกิดข้อผิดพลาด',
            'ไม่สามารถเชื่อมต่อระบบได้',
            [
            {text: 'OK', onPress: () =>false},
            ]
            )
          return false;
      }
}
export async function post(path,param){
  const connectionInfo = await getConnectionInfo()
  if(connectionInfo.type!="none"){
  const userData = await store.get("user");
  // var config = {
  //   headers: { 'token':userData.token},
  //   timeout:timeout
  // }
  
  let requestURL = `${endpoint}${path}`
      try{
        //   const response = await  axios.get(requestURL,{params:param},{ headers: { Authorization: "a1390243-ebcf-4ae7-bdaa-68bd77e19a7e" } });
          const response = await axios.post(requestURL,{params:param},{ headers:{'token':userData.token} })
          if(response.status=='200' && response.data.warning_code==0 && response.data.error_code==0){
            return response.data.data;
          }else{
              if("Expired Token" != response.data.error_name){
                setTimeout(()=>{
                  Alert.alert(
                      response.data.title,
                      response.data.friendly_msg_th,
                      [
                      {text: 'OK', onPress: () => false},
                      ]
                    )
                },200)
                return false;
              }else{
                setTimeout(()=>{
                  app.logout();
                  },500)
              }
          }
      }catch(e){
      console.log(e);
      Alert.alert(
        'เกิดข้อผิดพลาด',
        'ไม่สามารถเชื่อมต่อระบบได้',
        [
        {text: 'OK', onPress: () => console.log('OK Pressed!')},
        ]
      )
          return e;
      }
  }else{
    setTimeout(()=>{
    Alert.alert(
      'เกิดข้อผิดพลาด',
      'No Internet Connection',
      [
      {text: 'OK', onPress: () => false},
      ]
    )
  },200);
  }
}

export async function get(path,param){
  const connectionInfo = await getConnectionInfo()
  if(connectionInfo.type!="none"){
  const userData = await store.get("user");
  // var config = {
  //   headers: { 'token':userData.token},
  //   timeout:timeout
  // }
  console.log("TOKEN",userData.token);
  let requestURL = `${endpoint}${path}`
      try{
        //   const response = await  axios.get(requestURL,{params:param},{ headers: { Authorization: "a1390243-ebcf-4ae7-bdaa-68bd77e19a7e" } });
          const response = await axios.get(requestURL,{ headers:{'token':userData.token} })
          if(response.status=='200' && response.data.warning_code==0 && response.data.error_code==0){
            return response.data.data;
          }else{
              if("Expired Token" != response.data.error_name){
                setTimeout(()=>{
                  Alert.alert(
                      response.data.title,
                      response.data.friendly_msg_th,
                      [
                      {text: 'OK', onPress: () => false},
                      ]
                    )
                },200)
                return false;
              }else{
                setTimeout(()=>{
                  app.logout();
                },500)
              }
          }
      }catch(e){
      console.log(e);
      Alert.alert(
        'เกิดข้อผิดพลาด',
        'ไม่สามารถเชื่อมต่อระบบได้',
        [
        {text: 'OK', onPress: () => console.log('OK Pressed!')},
        ]
      )
          return e;
      }
  }else{
    setTimeout(()=>{
    Alert.alert(
      'เกิดข้อผิดพลาด',
      'No Internet Connection',
      [
      {text: 'OK', onPress: () => false},
      ]
    )
  },200);
  }
}
export async function getBasic(path,param){
  const connectionInfo = await getConnectionInfo()
  if(connectionInfo.type!="none"){
  let requestURL = `${endpoint}${path}`
      try{
        //   const response = await  axios.get(requestURL,{params:param},{ headers: { Authorization: "a1390243-ebcf-4ae7-bdaa-68bd77e19a7e" } });
          const response = await axios.get(requestURL,{params:param})
          if(response.status=='200' && response.data.warning_code==0 && response.data.error_code==0){
            return response.data.data;
          }else{
              setTimeout(()=>{
                Alert.alert(
                  'เกิดข้อผิดพลาด',
                  response.data.msg,
                  [
                  {text: 'OK', onPress: () => console.log('OK Pressed!')},
                  ]
                )
              },200)
              return false;
          }
      }catch(e){
      console.log(e);
      Alert.alert(
        'เกิดข้อผิดพลาด',
        'ไม่สามารถเชื่อมต่อระบบได้',
        [
        {text: 'OK', onPress: () => console.log('OK Pressed!')},
        ]
      )
          return e;
      }
  }else{
    setTimeout(()=>{
    Alert.alert(
      'เกิดข้อผิดพลาด',
      'No Internet Connection',
      [
      {text: 'OK', onPress: () => false},
      ]
    )
  },200);
  }
}
// export async function uploadFile(path,file,param){
//     const connectionInfo = await getConnectionInfo()
//     console.log(connectionInfo);
//     if(connectionInfo.type!="none"){
//     let requestURL = `${endpoint}${path}`;
//     const formData = [];
//     formData.push({
//       name: "file",
//       data: RNFetchBlob.wrap(file.path)
//     });
//     try{
//       let response = await RNFetchBlob.fetch(
//         "POST",
//         requestURL,
//         {
//           Accept: "application/json",
//           "Content-Type": "multipart/form-data"
//         },
//         formData
//       )
//       if(response){
//         let responseData = JSON.parse(response.data);
//         console.log(responseData);
//         if(responseData.RET.Complete){
//           return true;
//         }else{
//           Alert.alert(
//             `${I18n.t('Error')}`,
//             `${responseData.RET.Messag}`,
//             [
//             {text: 'OK', onPress: () => console.log('OK Pressed!')},
//             ]
//           )
//         }
//       }
   
//     }catch(e){
//       console.log(e);
//     }
//   }else{
//     setTimeout(()=>{
//       Alert.alert(
//         'เกิดข้อผิดพลาด',
//         'No Internet Connection',
//         [
//         {text: 'OK', onPress: () => false},
//         ]
//       )
//     },200);
//   }
// }
  const getConnectionInfo = async () => {
    if (Platform.OS === 'ios') {
      return new Promise((resolve, reject) => {
        const connectionHandler = connectionInfo => {
          NetInfo.removeEventListener('connectionChange', connectionHandler)
  
          resolve(connectionInfo)
        }
  
        NetInfo.addEventListener('connectionChange', connectionHandler)
      })
    }
  
    return NetInfo.getConnectionInfo()
}
