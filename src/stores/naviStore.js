/* @flow */

import { observable } from 'mobx';


export default class NaviStore {
  @observable navigation;
  @observable selectMenu;
}
