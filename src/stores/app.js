import { observable } from 'mobx';
import store from 'react-native-simple-store';
class AppStore {

  @observable root = undefined; // 'login' / 'after-login'

  constructor() {
    
  }
  async appInitialized() {
    let userData = await store.get("user");
    let skip = await store.get("tutorial");
    if (userData == null || userData==undefined) {
      if(skip){
        this.root = 'login';
      }else{
        this.root = 'intro';
      }
    }else{
      this.root = 'after-login'; 
    }
  }
  login() {
    this.root = 'after-login';
  }
  async logout() {
    await store.delete("user");
    this.root = 'login';
  }
  
 
}

export default new AppStore();