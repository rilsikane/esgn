/* @flow */

import { observable } from 'mobx';


export default class NotiStore {
  @observable notifications;
  @observable unwatch = 0;
}
