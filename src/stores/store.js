
import NaviStore from './naviStore'
import NotiStore from './notiStore'
export default {
    naviStore:new NaviStore(),
    notiStore:new NotiStore(),
};
