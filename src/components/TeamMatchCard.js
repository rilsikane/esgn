import React, { Component } from 'react'
import { View, Text, ImageBackground, Image } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { TeamCard } from './TeamCard'

class TeamMatchCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderContent() {
        const { coverImgUri, t1ImgUri, t1Name, t1Desc, t2ImgUri, t2Name, t2Desc, winner, hideCover,
            renderWinner, winnerName, winnerImgUri, winnerDesc
        } = this.props

        if (renderWinner) {
            return (
                <View style={styles.winnerContainerStyle}>
                    <ImageBackground
                        source={require('../sources/icons/winner_icon01.png')}
                        resizeMode='contain'
                        style={styles.winnerIconImgStyle}
                    >
                        <TeamCard
                            status={'winner'}
                            imgUri={winnerImgUri}
                            teamName={winnerName}
                            teamDesc={winnerDesc}
                        />
                    </ImageBackground>
                </View>
            )
        } else {
            return (
                <View style={[styles.teamMatchCardContainerStyle, hideCover && styles.hideCoverContainerStyle]}>
                    <ImageBackground
                        blurRadius={10}
                        style={[styles.matchCoverImgStyle, hideCover && styles.hideCoverContainerStyle]}
                        source={hideCover ? null : coverImgUri}
                    >
                        <TeamCard
                            status={winner != 't1' && 'lose'}
                            imgUri={t1ImgUri}
                            teamName={t1Name}
                            teamDesc={t1Desc}
                        />
                        <Image
                            source={require('../sources/icons/vs_icon01.png')}
                            resizeMode='contain'
                            style={styles.vsIconStyle}
                        />
                        <TeamCard
                            status={winner != 't2' && 'lose'}
                            imgUri={t2ImgUri}
                            teamName={t2Name}
                            teamDesc={t2Desc}
                        />
                    </ImageBackground>
                </View>
            )
        }

    }

    render() {
        return (
            <View>
                {this.renderContent()}
            </View>
        )

    }
}

const styles = {
    teamMatchCardContainerStyle: {
        width: responsiveWidth(100),
        height: responsiveHeight(31.6),
    },
    winnerContainerStyle: {
        width: responsiveWidth(100),
        height: responsiveHeight(31.6),
        justifyContent: 'center',
        alignItems: 'center',
    },
    winnerIconImgStyle: {
        height: responsiveHeight(25),
        width: responsiveWidth(100),
        justifyContent: 'center',
        alignItems: 'center',
    },
    hideCoverContainerStyle: {
        width: responsiveWidth(100),
        height: responsiveHeight(20),
    },
    matchCoverImgStyle: {
        width: responsiveWidth(100),
        height: responsiveHeight(31.6),
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    vsIconStyle: {
        width: responsiveWidth(9.1),
        height: responsiveHeight(3.8),
        marginLeft: responsiveWidth(7),
        marginRight: responsiveWidth(7),
    }
}
export { TeamMatchCard }
