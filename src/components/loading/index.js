import React from 'react';
import { StyleSheet, View, Image,Text } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
var SpinnerKit = require('react-native-spinkit');
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'


export default class Loading extends React.Component {
    render() {
        if(!this.props.mini)
        return  ( 
        <Spinner animation="fade" visible={this.props.visible} overlayColor="rgba(104, 104, 104, 0.4)">
          <View style={{flex:1,justifyContent:"center",alignItems:"center",marginTop:-50, backgroundColor:'transparent'}}>
            <SpinnerKit size={60} type={"FadingCircleAlt"} color="#f2f2f2" />
           </View>
        </Spinner>);
        else
        return (
            <View style={{flex:1,justifyContent:"center",alignItems:"center",marginTop:-50,backgroundColor:'transparent'}}>
                <SpinnerKit size={80} type={"Circle"} color="#f2f2f2" />
           </View>
        );
    }

}
const styles = StyleSheet.create({
  HeaderFont:{
    color:"#FFFF",
    fontFamily:"Kanit",
    fontSize:responsiveFontSize(1.3),
    backgroundColor:'transparent'
  }
});