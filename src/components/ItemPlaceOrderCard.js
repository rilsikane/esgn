import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Button } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { ItemInfoCard } from './ItemInfoCard'
import { LabelInput } from './LabelInput'

class ItemPlaceOrderCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { style, state, title, imgUri, rating, info, label, remark,onChangeText,placeOrder } = this.props

        return (
            <View style={[styles.itemDealCardContainerStyle, style, state == 'sell' && styles.sellContainerStyle]}>
                <View style={styles.itemDealCardSubContainerStyle}>
                    <ItemInfoCard
                        title={title}
                        imgUri={imgUri}
                        rating={rating}
                        info={info}
                    />
                    <Text style={styles.stateTitleTextStyle}>{state == 'buy' ? 'What price do you want to buy?' : 'What price do you want to Sell?'}</Text>
                    <LabelInput
                        label={label}
                        remark={remark}
                        keyboardType="number-pad"
                        onChangeText={(val)=>onChangeText(val)}
                    />
                    <Button onPress={()=>placeOrder()} rounded style={styles.submitButtonStyle}>
                        <Text style={styles.submitButtonTextStyle}>Place Order</Text>
                    </Button>
                </View>
            </View>
        )
    }
}

const styles = {
    itemDealCardContainerStyle: {
        width: responsiveWidth(93.8),
        height: responsiveHeight(62.9),
        borderRadius: 4,
        backgroundColor: "#009444",
        alignSelf: 'center',
    },
    sellContainerStyle: {
        backgroundColor: "#c35745",
    },
    itemDealCardSubContainerStyle: {
        flex: 1,
        backgroundColor: '#FFF',
        marginTop: responsiveHeight(2),
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 4,
        alignItems: 'center',
    },
    stateTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#414042",
        marginBottom: responsiveHeight(1),
    },
    submitButtonStyle: {
        width: responsiveWidth(85),
        height: responsiveHeight(8),
        backgroundColor: "#009444",
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        marginTop: responsiveHeight(2),
    },
    submitButtonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff"
    },

}

export { ItemPlaceOrderCard }
