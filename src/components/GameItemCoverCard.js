import React, { Component } from 'react'
import { View, Text, ImageBackground, TouchableOpacity } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

class GameItemCoverCard extends Component {

  constructor(props) {
    super(props)
    this.state = {
    }
  }

  render() {
    const { imgUri, style } = this.props

    return (
      <View style={[styles.gameItemCoverCardContainerStyle, style]}>
        <ImageBackground
          source={imgUri}
          style={styles.coverImgStyle}
        >
          <TouchableOpacity style={styles.viewAllButtonContainerStyle}>
            <Text style={styles.viewAllTextStyle}>View All ></Text>
          </TouchableOpacity>
        </ImageBackground>
      </View>
    )
  }
}

const styles = {
  gameItemCoverCardContainerStyle: {
    width: responsiveWidth(100),
    height: 163.9,
    marginBottom: responsiveHeight(1),
    alignSelf: 'center',
  },
  coverImgStyle: {
    width: responsiveWidth(100),
    height: 163.9,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  viewAllButtonContainerStyle: {
    width: responsiveWidth(18),
    height: responsiveHeight(3.1),
    borderRadius: 2,
    backgroundColor: "#e42526",
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: responsiveWidth(2.5),
    marginBottom: responsiveWidth(2.5),
  },
  viewAllTextStyle: {
    fontFamily: "Prompt",
    fontSize: responsiveFontSize(1.4),
    color: "#ffffff"
  },

}
export { GameItemCoverCard }
