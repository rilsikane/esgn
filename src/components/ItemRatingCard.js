import React, { Component } from 'react'
import { View, Text, Image } from 'react-native'
import { Item, Input, Button } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import FastImage from 'react-native-fast-image';
import StarRating from './react-native-star-rating'

class ItemRatingCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isSelect:false,
            questionList:[
                {
                    title: 'How accurate was the item description?'
                },
                {
                    title: 'How reasonable of the item price?'
                },
                {
                    title: 'How satisfied were you with the seller communication?'
                },
            ]
    
        }
    }
    selectedStart(index,rating){
        this.setState({isSelect:true});
        let tmp = [...this.state.questionList];
        tmp[index].rating = rating;
        
        this.setState({questionList:tmp})
        this.setState({isSelect:false});
    }
    renderQuestionList() {
        return this.state.questionList.map((data, index) =>
            <View key={index} style={styles.questionContainerStyle}>
                <Text style={styles.questionTitleTextStyle}>{data.title}</Text>
                <StarRating
                    maxStars={5}
                    rating={data.rating}
                    fullStarColor='#f7941d'
                    emptyStarColor='#a7a9ac'
                    starSize={responsiveHeight(4)}
                    halfStarEnabled
                    containerStyle={styles.starRatingStyle}
                    selectedStar={(rating)=>this.selectedStart(index,rating)}
                />
            </View>
        )
    }

    render() {
        const { title, imguri, rating, userCount, questionList,editRating,onSelectedStart,onChangeText } = this.props

        return (
            <View style={styles.itemRatingCardContainerStyle}>
                <Text style={styles.titleTextStyle}>{title}</Text>
                <View style={styles.itemContainerStyle}>
                    <FastImage
                        source={imguri}
                        style={styles.imgStyle}
                        borderRadius={3}
                    />
                    <View style={styles.ratingContainerStyle}>
                        <StarRating
                            selectedStar={(rating)=>onSelectedStart(rating)}
                            disabled={!editRating}
                            maxStars={5}
                            rating={rating}
                            fullStarColor='#f7941d'
                            emptyStarColor='#a7a9ac'
                            starSize={responsiveHeight(5)}
                            halfStarEnabled
                        />
                        <Text style={styles.totalUserTextStyle}>(From {userCount} Users)</Text>
                    </View>
                </View>
                <View style={styles.questionListContainerStyle}>
                    {!this.state.isSelect && this.renderQuestionList(questionList)}
                    <Item style={styles.inputContainerStyle}>
                        <Input
                            onChangeText={(val)=>onChangeText(val)}
                            placeholder='Tell us more....'
                            placeholderTextColor='#bcbec0'
                            style={styles.inputStyle}
                        />
                    </Item>
                    <Button rounded style={styles.submitButtonStyle} onPress={()=>this.props.confirm()}>
                        <Text style={styles.submitButtonTextStyle}>Submit</Text>
                    </Button>
                </View>
            </View>
        )
    }
}

const styles = {
    itemRatingCardContainerStyle: {
        width: responsiveWidth(95),
        height: responsiveHeight(79.7),
        alignSelf: 'center',
        alignItems: 'center',
        borderRadius: 4,
        backgroundColor: "#ffffff",
        padding: responsiveWidth(5),
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#414042",
        marginBottom: responsiveHeight(2),
    },
    itemContainerStyle: {
        flexDirection: 'row',
        height: responsiveHeight(14.9),
        alignItems: 'center',
        width: '100%',
        marginBottom: responsiveHeight(1),
    },
    imgStyle: {
        height: responsiveHeight(14.9),
        width: responsiveHeight(14.9),
    },
    ratingContainerStyle: {
        marginLeft: responsiveWidth(4),
    },
    totalUserTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.5),
        color: "#414042"
    },
    questionListContainerStyle: {
        width: '100%',
    },
    starRatingStyle: {
        width: responsiveWidth(50),
    },
    questionContainerStyle: {
        marginBottom: responsiveHeight(2),
    },
    questionTitleTextStyle: {
        fontSize: responsiveFontSize(2),
        fontFamily: 'Kanit',
        color: "#414042"
    },
    inputContainerStyle: {
        marginTop: responsiveHeight(1),
    },
    inputStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#bcbec0"
    },
    submitButtonStyle: {
        width: responsiveWidth(85),
        height: responsiveHeight(8),
        backgroundColor: "#1b75bc",
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        marginTop: responsiveHeight(2),
    },
    submitButtonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff"
    },
}

export { ItemRatingCard }
