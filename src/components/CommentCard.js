import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Card, Thumbnail } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

class CommentCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { userImgUri, userName, timeLeft, desc, style } = this.props
        return (
            <Card style={[styles.commentCardContainerStyle, style]}>
                <Thumbnail
                    source={userImgUri}
                    size={responsiveHeight(6.4)}
                    style={styles.thumbnailStyle}
                />
                <View style={styles.detailContainerStyle}>
                    <View style={styles.titleContainerStyle}>
                        <Text style={styles.userNameTextStyle}>{userName}</Text>
                        <Text style={styles.timeLeftTextStyle}>{timeLeft}</Text>
                    </View>
                    <Text numberOfLines={3} style={styles.descTextStyle}>{desc}</Text>
                </View>
            </Card>
        )
    }
}

const styles = {
    commentCardContainerStyle: {
        width: responsiveWidth(95),
        height: responsiveHeight(11.6),
        borderRadius: 2,
        backgroundColor: "#ffffff",
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: responsiveWidth(2.5),
        paddingRight: responsiveWidth(2.5),
    },
    thumbnailStyle: {

    },
    detailContainerStyle: {
        flex: 1,
        marginLeft: responsiveWidth(2.5),
        justifyContent: 'center',
    },
    titleContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    userNameTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#6e6f71"
    },
    timeLeftTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(1.5),
        color: "#989798"
    },
    descTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.5),
        color: "#818181"
    },

}
export { CommentCard }
