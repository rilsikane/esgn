import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

class ContactActionConfirmCard extends Component {

  constructor(props) {
    super(props)
    this.state = {
    }
  }

  render() {
    const { title, desc } = this.props

    return (
      <View style={styles.contactActionConfirmCard}>
        <Image
          source={require('../sources/icons/user_icon02.png')}
          style={styles.thumbStyle}
        />
        <Text style={styles.titleTextStyle}>{title}</Text>
        <Text style={styles.descTextStyle}>{desc}</Text>
        <TouchableOpacity style={styles.buttonStyle}>
          <Text style={styles.textButtonStyle}>DELETE</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = {
  contactActionConfirmCard: {
    width: responsiveWidth(75.6),
    height: responsiveHeight(46.7),
    borderRadius: 2,
    backgroundColor: "#ffffff",
    alignSelf: 'center',
    alignItems: 'center',
    padding: responsiveWidth(2.5),
  },
  thumbStyle: {
    marginTop: responsiveHeight(2),
    width: responsiveHeight(13.9),
    height: responsiveHeight(13.9),
    borderRadius: responsiveHeight(6.95),
  },
  titleTextStyle: {
    fontFamily: "Kanit",
    fontSize: responsiveFontSize(2),
    color: "#58595b",
    textAlign: 'center',
    marginTop: responsiveHeight(3),
    marginBottom: responsiveHeight(3),
  },
  descTextStyle: {
    fontFamily: "Kanit",
    fontSize: responsiveFontSize(1.8),
    color: "#939598",
    textAlign: 'center',
    marginBottom: responsiveHeight(5),
    width: responsiveWidth(53),
  },
  buttonStyle: {
    width: responsiveWidth(21.3),
    height: responsiveHeight(5),
    borderRadius: 2,
    backgroundColor: "#e42526",
    justifyContent: 'center',
    alignItems: 'center',
  },
  textButtonStyle: {
    fontFamily: "Kanit",
    fontSize: responsiveFontSize(1.8),
    color: "#ffffff"
  },

}

export { ContactActionConfirmCard }
