import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

class LiveHeader extends Component {
    constructor(props) {
        super(props)

    }

    renderLeftItem(leftName) {
        if (leftName == 'home') {
            return (
                <TouchableOpacity>
                    <Image
                        source={require('../sources/icons/app_icon04.png')}
                        resizeMode='contain'
                        style={styles.appIconStyle}
                    />
                </TouchableOpacity>
            )
        } else if (leftName == 'back') {
            return (
                <TouchableOpacity>
                    <Text style={styles.backTextStyle}>{'<'} Back</Text>
                </TouchableOpacity>
            )
        }
    }

    renderCenterItem(title) {
        if (title != undefined) {
            return (
                <Text style={styles.titleTextStyle}>{title}</Text>
            )
        }
    }

    renderRightAbsoluteItem(rightName) {
        if (rightName == 'searchLive') {
            return (
                <View style={styles.rightAbsoluteItemSubContainerStyle}>
                    <TouchableOpacity>
                        <Image
                            source={require('../sources/icons/search_icon01.png')}
                            resizeMode='contain'
                            style={styles.searchIconStyle}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.userIconContainerStyle}>
                        <Image
                            source={require('../sources/icons/user_icon01.png')}
                            resizeMode='contain'
                            style={styles.userIconStyle}
                        />
                    </TouchableOpacity>
                </View>
            )
        } else if (rightName == 'searchData') {
            return (
                <View style={styles.rightAbsoluteItemSubContainerStyle}>
                    <TouchableOpacity>
                        <Image
                            source={require('../sources/icons/search_icon01.png')}
                            resizeMode='contain'
                            style={styles.searchIconStyle}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.userIcon02ContainerStyle}>
                        <Image
                            source={require('../sources/icons/user_icon02.png')}
                            resizeMode='contain'
                            style={styles.userIcon02Style}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.settingIconContainerStyle}>
                        <Image
                            source={require('../sources/icons/setting_icon01.png')}
                            resizeMode='contain'
                            style={styles.userIconStyle}
                        />
                    </TouchableOpacity>
                </View>
            )
        } else if (rightName == 'share') {
            return (
                <View style={styles.rightAbsoluteItemSubContainerStyle}>
                    <TouchableOpacity>
                        <Image
                            source={require('../sources/icons/share_icon01.png')}
                            resizeMode='contain'
                            style={styles.shareIconStyle}
                        />
                    </TouchableOpacity>
                </View>
            )
        }
    }

    render() {
        const { leftName, title, rightName } = this.props

        return (
            <View style={styles.headersContainerStyle}>
                <View style={styles.leftItemContainerStyle}>
                    {this.renderLeftItem(leftName)}
                </View>
                <View style={styles.centerItemsContainerStyle}>
                    {this.renderCenterItem(title)}
                </View>
                <View style={styles.rightItemContainerStyle} />
                <View style={styles.absoluteRightItemContainerStyle}>
                    {this.renderRightAbsoluteItem(rightName)}
                </View>
            </View>
        )
    }
}

const styles = {
    headersContainerStyle: {
        height: responsiveHeight(10),
        backgroundColor: '#2b405a',
        borderBottomWidth: 1,
        borderColor: '#0F0E0E',
        flexDirection: 'row',
    },
    leftItemContainerStyle: {
        flex: 0.2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    centerItemsContainerStyle: {
        flex: 0.6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    rightItemContainerStyle: {
        flex: 0.2,
    },
    absoluteRightItemContainerStyle: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    rightAbsoluteItemSubContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    searchIconStyle: {
        height: responsiveHeight(2.5),
        width: responsiveHeight(2.5),
    },
    userIconContainerStyle: {
        marginRight: responsiveWidth(4),
        marginLeft: responsiveWidth(6),
    },
    userIcon02ContainerStyle: {
        marginLeft: responsiveWidth(3),
    },
    settingIconContainerStyle: {
        marginRight: responsiveWidth(3),
        marginLeft: responsiveWidth(3),
    },
    userIconStyle: {
        height: responsiveHeight(2.5),
        width: responsiveHeight(2.5),
    },
    userIcon02Style: {
        height: responsiveHeight(4),
        width: responsiveHeight(4),
    },
    backTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveHeight(2.5),
        color: "#ffffff"
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveHeight(2.7),
        color: "#ffffff"
    },
    appIconStyle: {
        height: responsiveHeight(4),
    },
    shareIconStyle:{
        width: responsiveHeight(3.4),
        height: responsiveHeight(3.4),
        marginRight: responsiveWidth(2.5),
    },

}

export { LiveHeader }