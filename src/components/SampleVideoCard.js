import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import { observer, inject } from 'mobx-react';
import FastImage from 'react-native-fast-image';
import {convertNumber} from './utils/number'
@inject('naviStore')
@observer
class SampleVideoCard extends Component {
    constructor(props) {
        super(props)

    }
    
    render() {
        const { imgUri, desc, casterName, viewCount, timePast, casterImg } = this.props
        return (
            <TouchableOpacity style={styles.sampleVideoCardContainerStyle} onPress={()=>this.props.goToVideo()}>
                <View style={styles.clipImgContainerStyle}>
                    <FastImage
                        source={imgUri}
                        style={styles.clipImgStyle}
                        borderRadius={3}
                    />
                </View>
                <Text style={styles.descTextStyle}>{desc}</Text>
                <View style={styles.casterNameContainerStyle}>
                    <FastImage
                        source={casterImg}
                        resizeMode='contain'
                        style={styles.casterIconStyle}
                    />
                    <Text style={styles.casterNameTextStyle}>{casterName}</Text>
                </View>
                <Text style={styles.viewTextStyle}>{convertNumber(viewCount)} views • {timePast}</Text>
            </TouchableOpacity>
        )
    }
}

const styles = {
    sampleVideoCardContainerStyle: {
        margin: responsiveWidth(1.5),
        width: responsiveWidth(44),
    },
    clipImgContainerStyle: {
        width: responsiveWidth(44),
        height: responsiveHeight(14),
    },
    clipImgStyle: {
        width: responsiveWidth(43),
        height: responsiveHeight(14),
    },
    descTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.6),
        color: "#ffffff"
    },
    casterNameContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    casterIconStyle: {
        height: responsiveHeight(1.5),
        width: responsiveHeight(1.5),
    },
    casterNameTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.6),
        color: "#bcbec0",
        marginLeft: responsiveWidth(2),
    },
    viewTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.4),
        color: "#bcbec0"
    }
}
export { SampleVideoCard }