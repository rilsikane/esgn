import React, { Component } from 'react'
import { View, Text, FlatList } from 'react-native'
import { Card } from 'native-base'

import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

class TournamentDetailCard extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderDetailList(detailList) {
        return (
            <FlatList
                data={detailList}
                renderItem={this._renderItem}
                keyExtractor={(item, index) => index.toString()}
            />
        )

    }

    _renderItem = ({ item }) => (
        <View style={styles.detailItemContainerStyle}>
            <Text style={styles.detailTextStyle}>{item.title} {item.desc}</Text>
        </View>
    )

    render() {
        const { detailList } = this.props

        return (
            <Card style={styles.tournamentDetailCardContainerStyle}>
                <Text style={styles.titleTextStyle}>TOURNAMENT DETAILS :</Text>
                <View style={styles.detailItemContainerStyle}>
                    <Text style={styles.detailTextStyle}>{item.title} {item.desc}</Text>
                </View>
            </Card>
        )
    }
}

const styles = {
    tournamentDetailCardContainerStyle: {
        borderWidth: 0.5,
        borderColor: "#cdcccc",
        borderRadius: 1.8,
        backgroundColor: "#ffffff",
        height: responsiveHeight(30),
        width: responsiveWidth(95),
        padding: responsiveWidth(2),
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#212221"
    },
    detailItemContainerStyle:{
        marginBottom: responsiveHeight(1),
    },
    detailTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.5),
        color: "#212221"
    }

}
export { TournamentDetailCard }
