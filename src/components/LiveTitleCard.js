import React, { Component } from 'react'
import { View, Text, Image, ImageBackground, TouchableOpacity } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import FastImage from 'react-native-fast-image';
import {convertNumber} from './utils/number'
class LiveTitleCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { imgUri, casterName, casterImgUri, desc, viewer, state, onPress } = this.props

        return (
            <TouchableOpacity onPress={()=>onPress?onPress():null} style={styles.liveTitleCardContainerStyle}>
                {state == 'live' &&
                    <Image
                        source={require('../sources/icons/live_icon03.png')}
                        resizeMode='contain'
                        style={styles.liveTopIconStyle}
                    />
                }
                <FastImage
                    source={imgUri}
                    style={styles.coverImgStyle}
                >
                    <View style={styles.titleContainerStyle}>
                        <Image
                            source={casterImgUri}
                            style={styles.casterImgStyle}
                        />
                        <View style={styles.detailContainerStyle}>
                            <Text style={styles.casterNameTextStyle}>{casterName}</Text>
                            <Text numberOfLines={1}  style={styles.descTextStyle}>{desc}</Text>
                        </View>
                        <View style={styles.viewerContainerStyle}>
                            <Text style={styles.viewTextStyle}>{convertNumber(viewer)}  </Text>
                            <Image
                                source={require('../sources/icons/view_icon01.png')}
                                resizeMode='contain'
                                style={styles.viewerIconStyle}
                            />
                        </View>
                    </View>
                </FastImage>
                <View style={styles.liveIconContainerStyle}>
                    <Image
                        source={require('../sources/icons/live_icon04.png')}
                        resizeMode='contain'
                        style={styles.liveIconStyle}
                    />
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = {
    liveTitleCardContainerStyle: {
        width: responsiveWidth(95),
        height: responsiveHeight(30),
        marginBottom: responsiveHeight(2),
    },
    liveTopIconStyle: {
        width: responsiveHeight(5.9),
        height: responsiveHeight(5.9),
        position: 'absolute',
        alignSelf: 'flex-end',
        zIndex: 10,
        right: responsiveWidth(2.5)
    },
    coverImgStyle: {
        width: responsiveWidth(100),
        height: responsiveHeight(30),
        justifyContent: 'flex-end',
    },
    liveIconContainerStyle: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    liveIconStyle: {
        width: responsiveHeight(9.4),
        height: responsiveHeight(9.4),
    },
    titleContainerStyle: {
        width: responsiveWidth(100),
        height: responsiveHeight(6.1),
        backgroundColor: "#2b405a",
        flexDirection: 'row',
        alignItems: 'center',
    },
    casterImgStyle: {
        width: responsiveHeight(4.8),
        height: responsiveHeight(4.8),
        borderRadius: responsiveHeight(2.4),
        marginLeft: responsiveWidth(2.5),
    },
    detailContainerStyle: {
        flex: 1,
        marginLeft: responsiveWidth(5)
    },
    casterNameTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#f5f7f9"
    },
    descTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.4),
        color: "#a7a9ac"
    },
    viewerContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        flex: 1,
        paddingRight: responsiveWidth(10),
    },
    viewerIconStyle: {
        width: responsiveHeight(2.5),
        height: responsiveHeight(2.5)
    },
    viewTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.4),
        color: "#f5f7f9"
    },

}

export { LiveTitleCard }
