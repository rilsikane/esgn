import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { Item, Input } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Dropdown } from './react-native-material-dropdown'

class ErrorInput extends Component {

  constructor(props) {
    super(props)

  }

  getIcon(iconName) {
    if (iconName == 'pen') {
      return require('../sources/icons/edit_icon01.png')
    } else if (iconName == 'view') {
      return require('../sources/icons/view_icon02.png')
    }

  }

  renderContent() {
    const { error, value, onChangeText, placeholder, errorText, style, iconName, editable, inputType, dropdownData } = this.props
    if (inputType == 'dropdown') {
      return (
        <View style={[styles.errorInputContainerStyle, style, styles.dropdownStyle]}>
          <View style={styles.dropdownInputStyle}>
            <Dropdown
              value={value}
              data={dropdownData}
              inputContainerStyle={[styles.dropdownInputContainerStyle, error && { borderBottomColor: '#e42526', }]}
              textColor='#808285'
              itemTextStyle={styles.dropdownTextStyle}
              disabled={!editable}
              onChangeText={onChangeText}
              disabledLineType='none'
              label='GENDER'
              labelTextStyle={styles.labelTextStyle}
            />
          </View>
          {error ? <Text style={styles.errorTextStyle}>{errorText}</Text> : null}
        </View>
      )

    } else if (inputType == 'button') {
      return (
        <TouchableOpacity onPress={() => alert('press')} style={[styles.errorInputContainerStyle, style]}>
          <View style={[styles.buttonInputStyle, error && styles.errorButtonStyle]}>
            <View style={styles.buttonContainerStyle}>
              <Text style={styles.buttonValueStyle}>{value != '' ? value : placeholder}</Text>
            </View>
            {iconName && editable &&
              <Image
                source={this.getIcon(iconName)}
                resizeMode='contain'
                style={styles.iconStyle}
              />
            }
          </View>
          {error ? <Text style={styles.errorTextStyle}>{errorText}</Text> : null}
        </TouchableOpacity>
      )
    } else {
      return (
        <View style={[styles.errorInputContainerStyle, style]}>
          <Item error={error}>
            <Input
              {...this.props}
              value={value}
              onChangeText={onChangeText}
              placeholder={placeholder}
              placeholderTextColor={styles.inputStyle.color}
              style={styles.inputStyle}
              editable={editable}
            />
            {iconName && editable &&
              <Image
                source={this.getIcon(iconName)}
                resizeMode='contain'
                style={styles.iconStyle}
              />
            }
          </Item>
          {error ? <Text style={styles.errorTextStyle}>{errorText}</Text> : null}
        </View>
      )
    }


  }

  render() {
    return (
      <View>
        {this.renderContent()}
      </View>
    )
  }
}

const styles = {
  errorInputContainerStyle: {
    width: responsiveWidth(90),
    alignSelf: 'center',
  },
  buttonInputStyle: {
    borderBottomWidth: 1,
    borderColor: '#d9d5dc',
    backgroundColor: 'transparent',
    flexDirection: 'row',
    alignItems: 'center',

  },
  errorButtonStyle: {
    borderBottomColor: '#e42526'
  },
  dropdownInputStyle: {
    width: '100%',
    height: responsiveHeight(8),
    justifyContent: 'center',
  },
  dropdownStyle: {
    marginBottom: responsiveHeight(2),
  },
  inputStyle: {
    fontFamily: 'Kanit',
    color: '#808285',
    fontSize: responsiveFontSize(2),
    paddingLeft: 0,
    height: responsiveHeight(8),
  },
  buttonContainerStyle: {
    flex: 1,
    justifyContent: 'center',
    height: responsiveHeight(8),
  },
  buttonValueStyle: {
    fontFamily: 'Kanit',
    color: '#808285',
    fontSize: responsiveFontSize(2),
  },
  errorTextStyle: {
    fontFamily: "Kanit",
    fontSize: responsiveFontSize(1.6),
    color: "#e42526",
    marginTop: responsiveHeight(0.5),
  },
  iconStyle: {
    width: responsiveHeight(2.2),
    height: responsiveHeight(2.2),
  },
  dropdownInputContainerStyle: {
    borderBottomColor: '#d1d3d4',
    borderBottomWidth: 1,
  },
  dropdownTextStyle: {
    fontFamily: 'Kanit'
  },
  labelTextStyle: {
    fontFamily: 'Kanit',
    color: '#808285',
    fontSize: responsiveFontSize(2),
  },

}

export { ErrorInput }