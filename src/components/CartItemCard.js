import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { PrizeTag } from './PrizeTag'

class CartItemCard extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { imgUri, title, total, prize, prizeUnit, index, style } = this.props

        return (
            <View style={[styles.cartItemCardContainerStyle, Number.isInteger(index * 0.5) && styles.cartItemCard2ContainerStyle, style]}>
                <TouchableOpacity style={styles.removeIconStyle}>
                    <Image
                        source={require('../sources/icons/close_icon01.png')}
                        resizeMode='contain'
                        style={styles.removeIconStyle}
                    />
                </TouchableOpacity>
                <View style={styles.imgContainerStyle}>
                    <Image
                        source={imgUri}
                        style={styles.imgStyle}
                    />
                </View>
                <View style={styles.detailContainerStyle}>
                    <Text style={styles.titleTextStyle}>{title}</Text>
                    <View style={styles.totalContainerStyle}>
                        <Text style={styles.prizeTextStyle}>{prize} x </Text>
                        <Text style={styles.totolTextStyle}>{total}</Text>
                    </View>
                </View>
                <PrizeTag
                    prize={prize}
                    prizeUnit={prizeUnit}
                    style={styles.prizeTagStyle}
                />
            </View>
        )
    }
}

const styles = {
    cartItemCardContainerStyle: {
        flexDirection: 'row',
        width: responsiveWidth(95),
        height: responsiveHeight(14.1),
        padding: responsiveWidth(3),
    },
    removeIconStyle: {
        width: responsiveHeight(3.5),
        height: responsiveHeight(3.5),
        position: 'absolute',
        zIndex: 5,
        margin: responsiveHeight(0.2),
    },
    cartItemCard2ContainerStyle: {
        backgroundColor: '#e6e7e8',
    },
    imgContainerStyle: {
        height: responsiveHeight(10.5),
        justifyContent: 'center',
    },
    imgStyle: {
        width: responsiveHeight(10.5),
        height: responsiveHeight(10.5),
    },
    detailContainerStyle: {
        flex: 1,
        paddingLeft: responsiveWidth(3),
        justifyContent: 'center',
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#58595b"
    },
    totalContainerStyle: {
        flexDirection: 'row',
    },
    prizeTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#58595b"
    },
    totolTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#e42526"
    },
    prizeTagStyle: {
        alignSelf: 'center',
    },

}

export { CartItemCard }
