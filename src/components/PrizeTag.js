import React, { Component } from 'react'
import { View, Text, ImageBackground } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

class PrizeTag extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { prize, prizeUnit, style, type } = this.props

        return (
            <ImageBackground
                source={type == 'sell' ? require('../sources/icons/prize_tag_icon02.png') : require('../sources/icons/prize_tag_icon01.png')}
                resizeMode='contain'
                style={[styles.prizeTagContainerStyle, style]}
            >
                <Text style={styles.prizeUnitTextStyle}>{prizeUnit}</Text>
                <Text style={styles.prizeTextStyle}>{prize}</Text>
            </ImageBackground>
        )
    }
}

const styles = {
    prizeTagContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flex:1,
        height: responsiveHeight(3),
    },
    prizeUnitTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff",
    },
    prizeTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff",
    }

}

export { PrizeTag }