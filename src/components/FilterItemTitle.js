import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

class FilterItemTitle extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { title, viewType, style,isInventory } = this.props

        return (
            <View style={[styles.filterItemTitleContainerStyle, style]}>
                <Text style={styles.titleTextStyle}>{title}</Text>
                <View style={styles.buttonGroupContainerStyle}>
                    {isInventory &&<TouchableOpacity onPress={()=>this.props.addItem? this.props.addItem():null}>
                        <Image
                            source={require('../sources/icons/add_icon01.png') }
                            resizeMode='contain'
                            style={styles.buttonIconStyle}
                        />
                    </TouchableOpacity>}
                    <TouchableOpacity onPress={()=>this.props.onChangeViewType()}>
                        <Image
                            source={viewType == 'item' ? require('../sources/icons/list_icon02.png') : require('../sources/icons/list_icon01.png')}
                            resizeMode='contain'
                            style={styles.buttonIconStyle}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.props.onFilter? this.props.onFilter():null}>
                        <Image
                            source={require('../sources/icons/filter_icon01.png')}
                            resizeMode='contain'
                            style={styles.buttonIconStyle}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = {
    filterItemTitleContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff"
    },
    buttonGroupContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    buttonIconStyle: {
        width: responsiveHeight(2.8),
        height: responsiveHeight(2.8),
        marginLeft: responsiveWidth(4),
    }
}

export { FilterItemTitle }
