import React, { Component } from 'react'
import { View, Text, Image } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import { Button } from 'native-base'

import { LabelInput } from './LabelInput'
import { observer, inject } from 'mobx-react';
@inject('naviStore')
@observer
class ItemNotifyCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { style, headerColor, title, desc, imgUri, viewType,code } = this.props

        return (
            <View style={[styles.cardContainerStyle, style, headerColor && { backgroundColor: headerColor }]}>
                <View style={styles.cardSubContainerStyle}>
                    <Text style={styles.titleTextStyle}>{title}</Text>
                    <View style={styles.lineStyle} />
                    <View style={styles.imgStyle}>
                        <Image
                            source={imgUri}
                            style={styles.imgStyle}
                            borderRadius={3}
                        />
                    </View>
                    {viewType == 'check-code' &&
                        <View style={styles.promoCodeContainerStyle}>
                            <Text style={styles.codeTitleTextStyle}>Serial CODE</Text>
                            <LabelInput disabled={true} value={code}/>
                        </View>
                    }
                    <Text style={[styles.descTextStyle, viewType != 'check-code' && styles.descText2Style]}>{desc}</Text>
                    <View style={styles.lineStyle} />
                    <Button rounded style={styles.submitButtonStyle} onPress={()=>{
                        this.props.naviStore.navigation.dismissLightBox();
                        this.props.complete();
                    }}>
                        <Text style={styles.submitButtonTextStyle}>{viewType == 'use' ? 'USE' : 'OK'}</Text>
                    </Button>
                </View>
            </View>
        )
    }
}

const styles = {
    cardContainerStyle: {
        width: responsiveWidth(95),
        height: responsiveHeight(66.9),
        backgroundColor: "#445982",
        alignSelf: 'center',
        borderRadius: 4,
        paddingTop: responsiveHeight(2.5),
    },
    cardSubContainerStyle: {
        flex: 1,
        backgroundColor: '#FFF',
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 4,
        padding: responsiveWidth(2.5),
        alignItems: 'center',
    },
    lineStyle: {
        width: '100%',
        height: 1,
        backgroundColor: "#bcbec0",
        marginTop: responsiveHeight(2.5),
        marginBottom: responsiveHeight(2.5),
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#414042",
        textAlign: 'center',
    },
    imgStyle: {
        width: responsiveHeight(14.9),
        height: responsiveHeight(14.9)
    },
    promoCodeContainerStyle: {
        marginTop: responsiveHeight(2),
    },
    codeTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#414042",
        textAlign: 'center'
    },
    descTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#414042",
        textAlign: 'center',
        width: responsiveWidth(65),
    },
    descText2Style: {
        width: responsiveWidth(85),
        marginTop: responsiveHeight(2),
        marginBottom: responsiveHeight(4),
    },
    submitButtonStyle: {
        width: responsiveWidth(85),
        height: responsiveHeight(8),
        backgroundColor: "#445982",
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
    },
    submitButtonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff"
    },

}
export { ItemNotifyCard }
