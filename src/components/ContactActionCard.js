import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

class ContactActionCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderTitle(actionType, title) {
        if (actionType == 'user') {
            return (
                <View style={styles.titleContainerStyle}>
                    <Text style={styles.labelTextStyle}>Action for this user</Text>
                    <Text style={styles.titleTextStyle}>  {title}</Text>
                </View>
            )
        }
    }

    renderActionList(actionType) {
        let userAction = [];
        if(this.props.isFriend){
            userAction = [ {
                title: 'Block Friend',
                iconUri: require('../sources/icons/block_icon01.png'),
                value:"block"
            },
           {
                title: 'Delete Friend',
                iconUri: require('../sources/icons/delete_icon02.png'),
                value:"delete"
            },]
        }else{
            userAction=[
                {
                    title: 'Invite Friend',
                    iconUri: require('../sources/icons/invite_icon01.png'),
                    value:"invite"
                },
            ]
        }
        

        if (actionType == 'user') {
            return userAction.map((data, index) =>
                <View key={index}>
                    <View style={styles.lineStyle} />
                    <TouchableOpacity style={styles.buttonStyle} onPress={()=>this.props.onSelect(data.value)}>
                        <Image
                            source={data.iconUri}
                            resizeMode='contain'
                            style={styles.iconStyle}
                        />
                        <Text style={styles.buttonTitleTextStyle}>{data.title}</Text>
                    </TouchableOpacity>
                </View>
            )
        }
    }

    render() {
        const { actionType, title } = this.props

        return (
            <View style={styles.contactActionCardContainerStyle}>
                {this.renderTitle(actionType, title)}
                {this.renderActionList(actionType)}
            </View>
        )
    }
}

const styles = {
    contactActionCardContainerStyle: {
        width: responsiveWidth(100),
        height: responsiveHeight(33.4),
        backgroundColor: "#f6f6f6",
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
        padding: responsiveWidth(2.5),
    },
    titleContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: responsiveWidth(4),
        marginBottom: responsiveHeight(2.5),
    },
    labelTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#212221"
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#212221"
    },
    buttonStyle: {
        flexDirection: 'row',
        marginLeft: responsiveWidth(4),
        paddingBottom: responsiveHeight(2.5),
        paddingTop: responsiveHeight(2.5),
    },
    iconStyle: {
        width: responsiveHeight(2.9),
        height: responsiveHeight(2.9),
    },
    buttonTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#212221",
        marginLeft: responsiveWidth(10),
    },
    lineStyle: {
        height: 1,
        width: '100%',
        backgroundColor: "#212221"
    },

}

export { ContactActionCard }
