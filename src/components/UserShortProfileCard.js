import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { Thumbnail } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import {convertCurrency,convertNumber} from './utils/number';
class UserShortProfileCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { buttonType, imgUri, name, desc, friendCount, videoCount, creditCount } = this.props

        return (
            <View style={styles.userShortProfileCardContainerStyle}>
                <View style={styles.thumbContainerStyle}>
                    <Thumbnail
                        source={imgUri}
                        size={responsiveHeight(7.5)}
                    />
                </View>
                <View style={styles.detailContainerStyle}>
                    <View style={styles.userTitleContainerStyle}>
                        <View>
                            <Text style={styles.userNameTextStyle}>{name}</Text>
                            <Text style={styles.userDescTextStyle}>{desc}</Text>
                        </View>
                        {!!buttonType &&
                            <TouchableOpacity>
                                <Image
                                    source={buttonType == 'setting' ? require('../sources/icons/setting_icon02.png') : require('../sources/icons/camera_icon01.png')}
                                    resizeMode='contain'
                                    style={styles.iconStyle}
                                />
                            </TouchableOpacity>
                        }
                    </View>
                    <View style={styles.detailSectionStyle}>
                        <View style={styles.detailSubSectionStyle}>
                            <Text style={styles.labelTextStyle}>Friend</Text>
                            <Text style={styles.sectionTextStyle}>{convertNumber(friendCount)}</Text>
                        </View>
                        <View style={styles.detailSubSectionStyle}>
                            <Text style={styles.labelTextStyle}>Video</Text>
                            <Text style={styles.sectionTextStyle}>{convertNumber(videoCount)}</Text>
                        </View>
                        <View style={styles.detailSubSectionStyle}>
                            <Text style={styles.labelTextStyle}>Credit</Text>
                            <Text style={styles.sectionTextStyle}>{convertCurrency(creditCount)}</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = {
    userShortProfileCardContainerStyle: {
        flexDirection: 'row',
        padding: responsiveWidth(2.5),
        height: responsiveHeight(15),
        borderBottomWidth: 1,
        borderColor: '#231f20',
    },
    thumbContainerStyle: {
        justifyContent: 'center',
        flex: 0.25,
        alignItems: 'center',
        marginRight: responsiveWidth(2.5),
    },
    detailContainerStyle: {
        flex: 0.75,
        justifyContent: 'center',
    },
    userTitleContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    userNameTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    userDescTextStyle: {
        fontFamily: "Prompt-Italic",
        fontSize: responsiveFontSize(1.5),
        color: "#d1d3d4"
    },
    iconStyle: {
        width: responsiveHeight(2.5),
        height: responsiveHeight(2.5),
    },
    detailSectionStyle: {
        flexDirection: 'row',
    },
    detailSubSectionStyle: {
        flex: 1,
        alignItems: 'center',
    },
    labelTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(1.3),
        color: "#d1d3d4"
    },
    sectionTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(1.5),
        color: "#d1d3d4"
    },

}

export { UserShortProfileCard }
