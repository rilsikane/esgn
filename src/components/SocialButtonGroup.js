import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

class SocialButtonGroup extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderButtonList() {
        let button = [
            {
                iconUri: require('../sources/icons/facebook_icon01.png')
            },
            {
                iconUri: require('../sources/icons/google_icon01.png')
            },
            {
                iconUri: require('../sources/icons/twitter_icon01.png')
            },
            {
                iconUri: null,
            },
        ]

        return button.map((data, index) =>
            <TouchableOpacity key={index} style={styles.socialButtonContainerStyle}>
                <Image
                    source={data.iconUri}
                    resizeMode='contain'
                    style={styles.socialButtonStyle}
                />
            </TouchableOpacity>
        )
    }

    render() {
        const { style } = this.props

        return (
            <View style={[styles.socialButtonGroupContainerStyle, style]}>
                {this.renderButtonList()}
                <TouchableOpacity style={styles.socialLinkContainerStyle}>
                    <Text numberOfLines={1} style={styles.socialLinkTextStyle}>https://goo.gl/vmQuxF</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = {
    socialButtonGroupContainerStyle: {
        height: responsiveHeight(7.6),
        backgroundColor: "#e6e7e8",
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    socialButtonContainerStyle: {
        width: responsiveHeight(5.9),
        height: responsiveHeight(5.9),
        borderRadius: 2.9,
        backgroundColor: "#f5f7f9",
        justifyContent: 'center',
        alignItems: 'center',
    },
    socialButtonStyle: {
        width: responsiveHeight(3.7),
        height: responsiveHeight(3.7),
    },
    socialLinkContainerStyle: {
        width: responsiveWidth(42.5),
        height: responsiveHeight(5.9),
        borderRadius: 2.9,
        backgroundColor: "#f5f7f9",
        justifyContent: 'center',
        alignItems: 'center',
    },
    socialLinkTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#231f20"
    }

}
export { SocialButtonGroup }
