import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

class MembershipPlanCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderBenefitList(benefits) {
        return benefits.map((data, index) =>
            <View key={index} style={styles.benefitListContainerStyle}>
                <Image
                    source={data.type == 'checked' ? require('../sources/icons/checkbox_icon01.png') : require('../sources/icons/lock_icon01.png')}
                    resizeMode='contain'
                    style={styles.benefitIconStyle}
                />
                <Text style={[styles.benefitTitleTextStyle, data.highlight && styles.highlightTextStyle]}>  {data.title}</Text>
            </View>
        )
    }

    render() {
        const { free, title, iconUri, owned, benefits, prize, isFavorite } = this.props

        return (
            <View style={styles.membershipPlanCardContainerStyle}>
                {free &&
                    <View style={styles.freeMarkContainerStyle}>
                        <View style={styles.freeMarkSubContainerStyle}>
                            <Text style={styles.freeTextStyle}>FREE</Text>
                        </View>
                    </View>
                }
                {isFavorite &&
                    <Image
                        source={require('../sources/icons/favor_icon01.png')}
                        resizeMode='contain'
                        style={styles.favorIconStyle}
                    />

                }
                <View style={styles.iconContainerStyle}>
                    {!owned &&
                        <Text style={styles.titleTextStyle}>{title}</Text>
                    }
                    <View style={styles.iconStyle}>
                        <Image
                            source={iconUri}
                            resizeMode='contain'
                            style={styles.iconStyle}
                        />
                    </View>
                    {owned ?
                        <Text style={styles.titleTextStyle}>{title}</Text> :
                        <View style={styles.upgradeContainerStyle}>
                            <TouchableOpacity style={styles.upgradeButtonStyle}>
                                <Text style={styles.titleTextStyle}>UPGRADE</Text>
                            </TouchableOpacity>
                            <Text style={styles.prizeTextStyle}>{prize}</Text>
                        </View>
                    }
                </View>
                <View style={styles.benefitContainerStyle}>
                    <Text style={styles.benefitLabelStyle}>Get Benefit!</Text>
                    {this.renderBenefitList(benefits)}
                </View>
            </View>
        )
    }
}

const styles = {
    membershipPlanCardContainerStyle: {
        backgroundColor: '#212221',
        flexDirection: 'row',
        paddingTop: responsiveWidth(2.5),
        paddingBottom: responsiveWidth(2.5),
        borderBottomWidth: 1,
        borderColor: '#231f20',
    },
    iconContainerStyle: {
        flex: 0.25,
        alignItems: 'center',
    },
    favorIconStyle: {
        width: responsiveHeight(6.4),
        height: responsiveHeight(6.4),
        position: 'absolute',
        zIndex: 10,
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.5),
        color: "#ffffff"
    },
    prizeTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.2),
        color: "#ffffff",
        marginBottom: responsiveHeight(1),
    },
    iconStyle: {
        width: responsiveHeight(10.6),
        height: responsiveHeight(10.6),
    },
    benefitContainerStyle: {
        flex: 0.75,
    },
    benefitLabelStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#ffffff"
    },
    freeMarkContainerStyle: {
        width: '100%',
        position: 'absolute',
        alignItems: 'flex-end',
        zIndex: 5,
    },
    freeMarkSubContainerStyle: {
        width: responsiveWidth(10),
        height: responsiveHeight(3.1),
        borderRadius: 1.3,
        backgroundColor: "#6a85c3",
        justifyContent: 'center',
        alignItems: 'center',
    },
    freeTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.4),
        color: "#fcb54e"
    },
    upgradeContainerStyle: {
        alignItems: 'center',
        marginTop: responsiveHeight(1),
    },
    upgradeButtonStyle: {
        width: responsiveWidth(16.4),
        height: responsiveHeight(3.4),
        borderRadius: 1,
        backgroundColor: "#6a85c3",
        justifyContent: 'center',
        alignItems: 'center',
    },
    benefitListContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    benefitTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.5),
        color: "#ffffff"
    },
    highlightTextStyle:{
        color: "#f8981d"
    },
    benefitIconStyle: {
        width: responsiveHeight(1.8),
        height: responsiveHeight(1.8),
    },

}

export { MembershipPlanCard }
