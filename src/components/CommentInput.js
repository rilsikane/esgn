import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import { Input } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

class CommentInput extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { style, textColor } = this.props

        return (
            <View style={[styles.commentInputContainerStyle, style]}>
                <Input
                    placeholder='Write your Comment.'
                    placeholderTextColor={textColor || '#FFF'}
                    style={[styles.inputStyle, textColor && { color: textColor }]}
                />
                <View style={styles.optionButtonContainerStyle}>
                    <TouchableOpacity>
                        <Image
                            source={require('../sources/icons/keyboard_icon01.png')}
                            resizeMode='contain'
                            style={styles.keyboardIconStyle}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image
                            source={require('../sources/icons/emoji_icon01.png')}
                            resizeMode='contain'
                            style={styles.iconStyle}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = {
    commentInputContainerStyle: {
        width: responsiveWidth(95),
        height: responsiveHeight(8),
        borderRadius: 2,
        backgroundColor: "#bcbec0",
        flexDirection: 'row',
        alignItems: 'center',
    },
    inputStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#ffffff"
    },
    optionButtonContainerStyle: {
        flexDirection: 'row',
    },
    keyboardIconStyle: {
        width: responsiveHeight(4),
        height: responsiveHeight(4),
    },
    iconStyle: {
        width: responsiveHeight(4),
        height: responsiveHeight(4),
        marginLeft: responsiveWidth(1),
        marginRight: responsiveWidth(1),
    },

}

export { CommentInput }
