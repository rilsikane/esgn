import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { Button } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import FastImage from 'react-native-fast-image'
import {convertCurrency,convertNumber} from './utils/number';
import Loading from './loading';
class UserShortMemberCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { imgUri, name, status, desc, walletCount, daimonCount, goldCount, level, memberClass, classImgUri } = this.props

        return (
            <View style={styles.userShortMemberCardContainerStyle}>
                <View style={styles.settingButtonContainerStyle}>
                    <TouchableOpacity onPress={()=>this.props.onPress()} style={styles.settingButtonSectionStyle}>
                        <FastImage
                            source={require('../sources/icons/setting_icon02.png')}
                            resizeMode='contain'
                            style={styles.setttingIconStyle}
                        />
                    </TouchableOpacity>
                </View>
                <TouchableOpacity style={styles.avatarContainerStyle}>
                    <FastImage
                        source={imgUri ? imgUri : require('../sources/icons/avatar_icon02.png')}
                        style={[styles.avartarIconStyle, imgUri && styles.avartarIconImgStyle]}
                    />
                </TouchableOpacity>
                <Text style={styles.userNameTextStyle}>{name}</Text>
                <View style={styles.statusContainerStyle}>
                    <Text style={[styles.statusTextStyle, status != 'Active' && styles.statusInActiveStyle]}>{status}</Text>
                    <View style={styles.centerLineStyle} />
                    <Text style={styles.descTextStyle}>{desc}</Text>
                </View>
                <View style={styles.walletContainerStyle}>
                    <View style={styles.walletSectionStyle}>
                        <View style={styles.walletSubSectionStyle}>
                            <Text style={styles.labelTextStyle}>Wallet</Text>
                            <FastImage
                                source={require('../sources/icons/money_icon02.png')}
                                resizeMode='contain'
                                style={styles.walletIconStyle}
                            />
                            <Text style={styles.walletCountTextStyle}>{convertCurrency(walletCount)}</Text>
                        </View>
                        <View style={styles.walletSubSectionStyle}>
                            <Text style={styles.labelTextStyle}>Diamond</Text>
                            <FastImage
                                source={require('../sources/icons/daimon_icon01.png')}
                                resizeMode='contain'
                                style={styles.walletIconStyle}
                            />
                            <Text style={styles.walletCountTextStyle}>{convertNumber(daimonCount)}</Text>
                        </View>
                        <View style={styles.walletSubSectionStyle}>
                            <Text style={styles.labelTextStyle}>Gold</Text>
                            <FastImage
                                source={require('../sources/icons/gold_icon01.png')}
                                resizeMode='contain'
                                style={styles.walletIconStyle}
                            />
                            <Text style={styles.walletCountTextStyle}>{convertNumber(goldCount)}</Text>
                        </View>
                    </View>
                    {/* {!this.props.isFriend && <Button rounded style={styles.submitButtonStyle}>
                        <Text style={styles.submitButtonTextStyle}>Top Up</Text>
                    </Button>} */}
                </View>
                <View style={styles.memberContainerStyle}>
                    <View style={styles.memberSectionStyle}>
                        <Text style={styles.memberLabelTextStyle}>Level</Text>
                        <Text style={styles.levelTextStyle}>{level}</Text>
                    </View>
                    <View style={styles.memberSectionStyle}>
                        <Text style={styles.memberLabelTextStyle}>Membership</Text>
                        <View style={styles.memberLevelContainerStyle}>
                            <FastImage
                                source={classImgUri}
                                resizeMode='contain'
                                style={styles.memberIconStyle}
                            />
                            <Text style={styles.memberClassTextStyle}>{memberClass}</Text>
                        </View>
                    </View>
                </View>
                <Loading visible={!name} />
            </View>
        )
    }
}

const styles = {
    userShortMemberCardContainerStyle: {
        backgroundColor: '#212221',
        width: responsiveWidth(100),
        alignItems: 'center',
    },
    settingButtonContainerStyle: {
        position: 'absolute',
        alignSelf: 'flex-end',
    },
    settingButtonSectionStyle: {
        margin: responsiveWidth(2.5),
    },
    avatarContainerStyle: {
        height: responsiveHeight(13.4),
    },
    avartarIconStyle: {
        height: responsiveHeight(13.4),
        width: responsiveHeight(13.4),
    },
    avartarIconImgStyle: {
        borderRadius: responsiveHeight(6.7)
    },
    setttingIconStyle: {
        width: responsiveHeight(2.5),
        height: responsiveHeight(2.5),

    },
    userNameTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(3.5),
        color: "#ffffff"
    },
    statusContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
    },
    statusTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.5),
        color: "#00a651",
        width: responsiveWidth(20),
        textAlign: 'right'
    },
    centerLineStyle: {
        width: 1,
        height: responsiveHeight(2.7),
        backgroundColor: "#939598",
        marginLeft: responsiveWidth(2),
        marginRight: responsiveWidth(2),
    },
    statusInActiveStyle: {
        color: "#e42526"
    },
    descTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.5),
        color: "#808285",
        width: responsiveWidth(20),
    },
    walletContainerStyle: {
        width: responsiveWidth(100),
        height: responsiveHeight(31),
        backgroundColor: "#e6e7e8",
        marginTop: responsiveHeight(2),
        marginBottom: responsiveHeight(2),
        padding: responsiveWidth(2),
    },
    walletSectionStyle: {
        flexDirection: 'row',
    },
    walletSubSectionStyle: {
        flex: 1,
        alignItems: 'center',
    },
    labelTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#212221"
    },
    walletIconStyle: {
        width: responsiveHeight(9.4),
        height: responsiveHeight(9.4),
        marginTop: responsiveHeight(1),
        marginBottom: responsiveHeight(1),
    },
    walletCountTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.9),
        color: "#f7941d"
    },
    submitButtonStyle: {
        width: responsiveWidth(85),
        height: responsiveHeight(8),
        backgroundColor: "#1b75bc",
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        marginTop: responsiveHeight(1),
    },
    submitButtonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff"
    },
    memberContainerStyle: {
        flexDirection: 'row',
    },
    memberSectionStyle: {
        flex: 1,
        alignItems: 'center',
    },
    memberLabelTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.4),
        color: "#f8981d"
    },
    levelTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(3),
        color: "#d1d3d4"
    },
    memberLevelContainerStyle: {
        flexDirection: 'row',
    },
    memberIconStyle: {
        width: responsiveHeight(3.7),
        height: responsiveHeight(3.7),
    },
    memberClassTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#d1d3d4"
    },


}

export { UserShortMemberCard }
