import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image, ScrollView } from 'react-native'
import { Button } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

class ImageCustom extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderButtonList(imgList) {
        return imgList.map((data, index) =>
            <View key={index} style={styles.imageCustomContainerStyle}>
                <View style={styles.imgStyle}>
                    <Image
                        source={data.imgUri}
                        style={styles.imgStyle}
                    />
                </View>
                <Text numberOfLines={1} style={styles.titleTextStyle}>{data.title}</Text>
                <Text style={styles.sizeTextStyle}>Size {data.size}</Text>
                <TouchableOpacity style={styles.deleteIconStyle}>
                    <Image
                        source={require('../sources/icons/delete_icon01.png')}
                        resizeMode='contain'
                        style={styles.deleteIconStyle}
                    />
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        const { imgList } = this.props

        return (
            <View>
                <ScrollView horizontal contentContainerStyle={styles.contentContainerStyle}>
                    {this.renderButtonList(imgList)}
                    <View style={styles.addImgButtonContainerStyle}>
                        <TouchableOpacity style={styles.addIconStyle}>
                            <Image
                                source={require('../sources/icons/add_icon02.png')}
                                resizeMode='contain'
                                style={styles.addIconStyle}
                            />
                        </TouchableOpacity>
                        <Text style={styles.addImgTextStyle}>Add Image</Text>
                    </View>
                </ScrollView>
                <Button style={styles.submitButtonStyle}>
                    <Text style={styles.submitButtonTextStyle}>Submit</Text>
                </Button>
                <Text style={styles.remarkTextStyle}>Your match submit can be edit until 00.00 of Next Day.</Text>
            </View>
        )
    }
}

const styles = {
    contentContainerStyle: {
        marginTop: responsiveHeight(1),
    },
    addImgButtonContainerStyle: {
        width: responsiveWidth(36.7),
        height: responsiveHeight(22.3),
        borderRadius: 2,
        backgroundColor: "#ffffff",
        justifyContent: 'center',
        alignItems: 'center',
        margin: responsiveWidth(1),
    },
    imageCustomContainerStyle: {
        width: responsiveWidth(36.7),
        height: responsiveHeight(22.3),
        borderRadius: 2,
        backgroundColor: "#ffffff",
        justifyContent: 'center',
        margin: responsiveWidth(1),
    },
    deleteIconStyle: {
        width: responsiveFontSize(4),
        height: responsiveFontSize(4),
        alignSelf: 'flex-end',
        marginRight: responsiveWidth(1),
    },
    addIconStyle: {
        width: responsiveHeight(10.3),
        height: responsiveHeight(10.3),
    },
    addImgTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#414042",
        marginTop: responsiveHeight(2),
    },
    imgStyle: {
        width: responsiveWidth(32),
        height: responsiveHeight(10.7),
        alignSelf: 'center',
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#414042",
        marginLeft: responsiveWidth(2.5),
    },
    sizeTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.6),
        color: "#2b405a",
        marginLeft: responsiveWidth(2.5),

    },
    submitButtonStyle: {
        width: responsiveWidth(95),
        height: responsiveHeight(8),
        backgroundColor: "#e42526",
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        marginBottom: responsiveHeight(2),
        marginTop: responsiveHeight(2),
    },
    submitButtonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    remarkTextStyle: {
        fontFamily: "BentonSans",
        fontSize: responsiveFontSize(1.4),
        color: "#f6f6f6",
        marginLeft: responsiveWidth(2.5),
    },

}
export { ImageCustom }
