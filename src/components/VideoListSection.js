import React, { Component } from 'react'
import { View, Text, TouchableOpacity, FlatList } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { SampleVideoCard } from './SampleVideoCard'

class VideoListSection extends Component {

    constructor(props) {
        super(props)

    }

    renderSampleVideoList(videoList) {
        return (
            <FlatList
                scrollEnabled={false}
                data={videoList}
                numColumns={2}
                renderItem={this.renderSampleVideoItem}
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }

    renderSampleVideoItem = ({ item, index }) => (
        <SampleVideoCard
            imgUri={{uri:item.thumbnail}}
            title={item.title}
            desc={item.title}
            casterName={item.caster_name}
            viewCount={item.view_count}
            timePast={item.timePast}
            casterImg={{uri:item.caster_image}}
            goToVideo={()=>this.props.onPress(item)}
        />
    )

    render() {
        const { title, desc, style, videoList } = this.props
        return (
            <View style={[styles.videoListSectionContainerStyle, style]}>
                <View style={styles.titleContainerStyle}>
                    <Text style={styles.titleTextStyle}>{title}</Text>
                    <TouchableOpacity onPress={()=>this.props.seeAllVideo()}>
                        <Text style={styles.seeAllTextStyle}>SEE ALL</Text>
                    </TouchableOpacity>
                </View>
                <Text style={styles.descTextStyle}>{desc}</Text>
                <View style={styles.sampleListContainerStyle}>
                    {this.renderSampleVideoList(videoList)}
                </View>
            </View>
        )
    }
}

const styles = {
    videoListSectionContainerStyle: {
        flex: 1,
    },
    titleContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff",
    },
    seeAllTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#5782c2"
    },
    descTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#bcbec0"
    },
    sampleListContainerStyle: {
        alignItems: 'center',
    }
}

export { VideoListSection }