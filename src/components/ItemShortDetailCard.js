import React, { Component } from 'react'
import { View, Text, ImageBackground, TouchableOpacity, Image } from 'react-native'
import { Card,Button } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import FastImage from 'react-native-fast-image';
import StarRating from './react-native-star-rating'
import { PrizeTag } from './PrizeTag'

class ItemShortDetailCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderContent(coverImgUri, gameName, ratingStar, ratingCount, ratingTotal, prize, itemName, itemDesc, itemInfo, timeLeft, viewType, status,isCaster) {
        return (
            <View style={styles.sellingContainerStyle}>
                <View style={styles.coverImgStyle}>
                    <FastImage
                        source={coverImgUri}
                        style={styles.coverImgStyle}
                    >
                        {!!timeLeft &&
                            <Card style={styles.countdownTimeContainerStyle}>
                                <View style={styles.countdownTimeBackgroundStyle} />
                                <Image
                                    source={require('../sources/icons/clock_icon01.png')}
                                    resizeMode='contain'
                                    style={styles.clockIconStyle}
                                />
                                <View>
                                    <Text style={styles.countdownLabelTextStyle}>Time Left</Text>
                                    <Text numberOfLines={1} style={styles.countdownTextStyle}>{timeLeft}</Text>
                                </View>
                            </Card>
                        }
                    </FastImage>
                </View>}
                <View style={styles.itemDetailContainerStyle}>
                    <View style={styles.titleContainerStyle}>
                        <View style={styles.ratingSectionStyle}>
                            {!isCaster && <Text style={styles.gameNameTextStyle}>Use in GAME : {gameName}</Text>}
                            {!isCaster && <View style={styles.ratingContainerStyle}>
                                <StarRating
                                    disabled
                                    maxStars={5}
                                    rating={ratingStar}
                                    fullStarColor='#f7941d'
                                    emptyStarColor='#a7a9ac'
                                    starSize={responsiveHeight(1.6)}
                                    halfStarEnabled
                                />
                                {ratingTotal ? <Text style={styles.ratingCountTextStyle}>  ({ratingCount}/{ratingTotal})</Text>:null}
                            </View>}
                            {viewType == 'selling' ?
                                <PrizeTag
                                    prize={prize}
                                    prizeUnit='฿'
                                /> :
                                <View style={styles.itemCodeContainerStyle}>
                                    <TouchableOpacity>
                                        <Image
                                            source={require('../sources/icons/use_icon01.png')}
                                            resizeMode='contain'
                                            style={styles.codeIconStyle}
                                        />
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={()=>this.props.promoPress()} style={styles.codeButtonStyle}>
                                        <Text style={styles.codeTextStyle}> CODE</Text>
                                    </TouchableOpacity>
                                </View>
                            }
                        </View>
                        <View style={styles.gameNameSectionStyle}>
                            <Text numberOfLines={1} style={styles.itemNameTextStyle}>{itemName}</Text>
                            <Text numberOfLines={1} style={styles.itemDescTextStyle}>{itemDesc}</Text>
                            {viewType != 'selling' ?
                            <View style={{flexDirection:'row'}}>
                            {status != 'selling'&&<TouchableOpacity onPress={this.props.onPress ? ()=>this.props.onPress():null} style={viewType == 'selling' ? styles.cartButtonStyle : styles.sellButtonStyle}>
                                {viewType == 'selling' &&
                                    <Image
                                        source={require('../sources/icons/cart_icon01.png')}
                                        resizeMode='contain'
                                        style={styles.cartIconStyle}
                                    />
                                }
                                <Text style={viewType == 'selling' ? styles.cartTextStyle : styles.sellTextStyle}>{viewType == 'selling' ? ' ADD TO CART' : 'SELL'}</Text>
                            </TouchableOpacity>}
                            {status != 'selling'&&<TouchableOpacity onPress={this.props.deletePress ? ()=>this.props.deletePress():null} style={viewType == 'selling' ? styles.cartButtonStyle : styles.sellButtonStyle}>
                            <Text style={styles.sellTextStyle}>{'DELETE'}</Text>
                            </TouchableOpacity>}
                            {status == 'selling'&&<TouchableOpacity onPress={this.props.cancelPress ? ()=>this.props.cancelPress():null} style={viewType == 'selling' ? styles.cartButtonStyle : styles.sellButtonStyle}>
                            <Text style={styles.sellTextStyle}>{'Selling'}</Text>
                            </TouchableOpacity>}
                        </View>
                            :<View style={[styles.cartButtonStyle,{backgroundColor:"transparent"}]}/>}
                        </View>
                    </View>
                    <Text style={styles.itemInfoTextStyle}>{itemInfo}</Text>
                    {this.props.canBuy && <View style={{flexDirection: 'row',marginLeft: 10,marginRight: 10,marginTop:20}}>
                        <Button style={styles.submitButtonStyle} onPress={()=>this.props.dealConfirm()}>
                            <Text style={styles.submitButtonTextStyle}>Buy Now!</Text>
                        </Button>
                        <Button style={[styles.submitButtonStyle,{backgroundColor:"#009444"}]} onPress={()=>this.props.placeOrder()}>
                            <Text style={styles.submitButtonTextStyle}>Place order</Text>
                        </Button>
                    </View>}
                    {isCaster && <View style={{flexDirection: 'row',marginLeft: 10,marginRight: 10,marginTop:20}}>
                        <Button style={styles.submitButtonStyle} onPress={()=>this.props.dealConfirm()}>
                            <Text style={styles.submitButtonTextStyle}>Buy Now!</Text>
                        </Button>
                    </View>}

                </View>
                
            </View>
        )
    }

    render() {
        const { coverImgUri, gameName, ratingStar, ratingCount, ratingTotal, prize, itemName, itemDesc,
            itemInfo, timeLeft, viewType,status,isCaster
        } = this.props

        return (
            <View>
                {this.renderContent(coverImgUri, gameName, ratingStar, ratingCount, ratingTotal, prize, itemName, itemDesc, itemInfo, timeLeft, viewType, status,isCaster)}
            </View>
        )
    }
}

const styles = {
    itemShortDetailCardContainerStyle: {

    },
    coverImgStyle: {
        width: responsiveWidth(100),
        height: responsiveHeight(37),
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
    },
    countdownTimeContainerStyle: {
        flexDirection: 'row',
        borderColor: 'transparent',
        backgroundColor: 'transparent',
        height: responsiveHeight(6),
        width: responsiveWidth(40),
        marginRight: responsiveWidth(2),
        marginBottom: responsiveWidth(2),
        alignItems: 'center',
        justifyContent: 'center',
    },
    countdownTimeBackgroundStyle: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        backgroundColor: '#6d6e71',
        opacity: 0.8,
        borderRadius: 2,
    },
    clockIconStyle: {
        height: responsiveHeight(3.2),
        width: responsiveHeight(3.2),
        marginRight: responsiveWidth(2),
    },
    countdownLabelTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(1.4),
        color: "#f5f7f9",
        textAlign: 'center',
    },
    countdownTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(2),
        color: "#f5f7f9",
        textAlign: 'center',
        width: responsiveWidth(30)
    },
    itemDetailContainerStyle: {
        width: responsiveWidth(100),
        backgroundColor: "#ffffff"
    },
    titleContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: responsiveWidth(2.5),
        paddingRight: responsiveWidth(2.5),
    },
    ratingSectionStyle: {
        flex: 0.4,
        justifyContent: 'center',
    },
    itemCodeContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    codeIconStyle: {
        width: responsiveHeight(4.1),
        height: responsiveHeight(4.1),
    },
    codeButtonStyle: {
        width: responsiveWidth(17.8),
        height: responsiveHeight(3.9),
        borderRadius: 2.5,
        backgroundColor: "#2b405a",
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: responsiveWidth(4),
    },
    codeTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(1.4),
        color: "#ffffff"
    },
    gameNameSectionStyle: {
        flex: 0.6,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    gameNameTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.4),
        color: "#6d6e71"
    },
    ratingContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: responsiveHeight(1),
        marginBottom: responsiveHeight(1),
    },
    ratingCountTextStyle: {
        fontFamily: "SegoeUI",
        fontSize: responsiveFontSize(1.6),
        color: "#b4b4b4"
    },
    itemNameTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(3),
        color: "#333333",
    },
    itemDescTextStyle: {
        fontFamily: "SegoeUI",
        fontSize: responsiveFontSize(2.5),
        color: "#b4b4b4",
        marginBottom: responsiveHeight(1),
    },
    sellingContainerStyle: {

    },
    cartButtonStyle: {
        width: responsiveWidth(39),
        height: responsiveHeight(4),
        backgroundColor: "#1b75bc",
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    sellButtonStyle: {
        width: responsiveWidth(23.6),
        height: responsiveHeight(5.2),
        backgroundColor: "#e42526",
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 3.3,
        justifyContent: 'center',
        marginLeft:5,
    },
    cartIconStyle: {
        width: responsiveHeight(3.5),
        height: responsiveHeight(3.5),
    },
    cartTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.4),
        color: "#ffffff"
    },
    sellTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    itemInfoTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#58595b",
        marginLeft: responsiveWidth(2.5),
        marginRight: responsiveWidth(2.5),
        marginTop: responsiveHeight(2),
    },
    submitButtonStyle: {
        flex:1,
        backgroundColor: "#e42526",
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        margin: 10,
    },
    submitButtonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },

}

export { ItemShortDetailCard }
