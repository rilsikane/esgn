import React, { Component } from 'react'
import { View, Text, Image, ScrollView } from 'react-native'
import { Tabs, Tab, TabHeading, ScrollableTab } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

class MainTabs extends Component {
    constructor(props) {
        super(props)

    }

    renderTabHeader(tabData, child, headingType, contentContainerStyle, headerStyle,isNotScroll) {
        return tabData.map((data, i) =>
            <Tab
                key={i}
                heading={
                    <TabHeading style={[styles.tabHeadingContainerStyle, headerStyle]}>
                        {this.renderTabHeading(data, headingType)}
                    </TabHeading>
                }
            >
                {!isNotScroll ? <ScrollView style={styles.tabContentStyle} contentContainerStyle={[styles.contentContainerStyle, contentContainerStyle]}>
                    {child[i]}
                </ScrollView>:<View style={styles.tabContentStyle} contentContainerStyle={[styles.contentContainerStyle, contentContainerStyle]}>
                    {child[i]}
                </View>}
            </Tab>
        )
    }

    renderTabHeading(data, headingType) {
        if (headingType == 'image') {
            return (
                <Image
                    source={data.iconUri}
                    resizeMode='contain'
                    style={styles.tabIconStyle}
                />
            )
        } else if (headingType == 'text') {
            return (
                <Text style={styles.tabHeaderLabelTextStyle}>{data.title}</Text>
            )
        } else if (headingType == 'text-icon') {
            return (
                <View style={styles.textIconContainerStyle}>
                    <Image
                        source={data.iconUri}
                        style={styles.textIconStyle}
                        resizeMode='contain'
                    />
                    <Text style={styles.tabHeaderLabelTextStyle}>  {data.title}</Text>
                </View>
            )
        }
    }

    render() {
        const { tabData, child, headingType, contentContainerStyle, headerStyle, scrollable,isNotScroll } = this.props

        return (
            <Tabs
                tabBarUnderlineStyle={styles.tabBarUnderlineStyle}
                tabContainerStyle={styles.tabsContainerStyle}
                renderTabBar={scrollable ? () => <ScrollableTab /> : undefined}
                onChangeTab={(index) => {
                    // if(index.i==3){
                    //   this.setState({isLoading:true});
                    //   await this.props.searchStaff();
                    //   this.setState({isLoading:false});
                    // }
                    // this.setState({ tabIndex: index.i })
                    if(this.props.onChangeTab){
                        this.props.onChangeTab(index.i);
                    }else{
                        return null;
                    }
                  
                  }}
            >
                {this.renderTabHeader(tabData, child, headingType, contentContainerStyle, headerStyle,isNotScroll)}
            </Tabs >
        )
    }
}

const styles = {
    tabBarUnderlineStyle: {
        backgroundColor: '#FFF',
    },
    tabsContainerStyle: {
        height: responsiveHeight(8),
    },
    tabHeadingContainerStyle: {
        backgroundColor: '#212221',
    },
    tabIconStyle: {
        height: responsiveHeight(3),
    },
    tabContentStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    contentContainerStyle: {
        alignItems: 'center',
        paddingTop: responsiveHeight(2),
    },
    tabHeaderLabelTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#ffffff"
    },
    textIconContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    textIconStyle: {
        width: responsiveHeight(2.2),
        height: responsiveHeight(2.2),
    },

}
export { MainTabs }