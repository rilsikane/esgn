import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { Thumbnail } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import FastImage from 'react-native-fast-image';
class FriendContactCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderActionButton(actionType,status,badge) {
        if (actionType == 'setting') {
            return (
                <TouchableOpacity style={{width:60,height:30,alignItems:"flex-end"}} onPress={()=>this.props.openSetting()}>
                    <Image
                        source={require('../sources/icons/setting_icon01.png')}
                        resizeMode='contain'
                        style={styles.settingIconStyle}
                    />
                </TouchableOpacity>
            )
        } else if (actionType == 'follow') {
            return (
                <TouchableOpacity onPress={()=>this.props.follow()} style={styles.followButtonStyle}>
                    <Text style={styles.buttonTextStyle}>Follow</Text>
                </TouchableOpacity>
            )
        } else if (actionType == 'subscribe') {
            return (
                <TouchableOpacity style={styles.subscribeButtonStyle}>
                    <Text style={styles.buttonTextStyle}>Subscribe</Text>
                </TouchableOpacity>
            )
        } else if (actionType == 'request') {
            return (
                <View style={styles.requestTypeContainerStyle}>
                    <TouchableOpacity onPress={this.props.confirm} style={[styles.buttonStyle, styles.confirmButtonStyle]}>
                        <Text style={styles.buttonTextStyle}>Confirm</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.props.reject} style={[styles.buttonStyle, styles.deleteButtonStyle]}>
                        <Text style={styles.buttonTextStyle}>Reject</Text>
                    </TouchableOpacity>
                </View>
            )
        }else if (actionType == 'add') {
            return (
                <View style={styles.requestTypeContainerStyle}>
                    {status!="friend" ?
                    (<TouchableOpacity onPress={()=>status!="invited"?this.props.add():this.props.cancel()} 
                        style={[styles.buttonStyle, styles.confirmButtonStyle,{marginRight: 5},
                        status=="invited"&&{width: responsiveWidth(24),backgroundColor: "#e42526"}]}>
                        <Text style={styles.buttonTextStyle}>{status!="invited"?'Add':'Cancel Request'}</Text>
                    </TouchableOpacity>)
                    
                    :<Text style={styles.buttonTextStyle}>You are now friend</Text>}
                </View>
            )
        }else if(actionType=='chat'){
            return (badge!=0&&badge!="0") ? (
                <View style={styles.badgeTextContainerStyle}>
                    <Text style={styles.badgeTextStyle}>{`${badge}`}</Text>
                </View>
            ):null
        }

    }

    render() {
        const { imgUri, name, desc, lastActive, actionType, isOnLine, users,status,badge } = this.props

        return (
            <TouchableOpacity onPress={()=>this.props.onPress ? this.props.onPress():null} style={styles.friendContactCardContainerStyle}>
                <View>
                    {isOnLine && <View style={styles.onLineDotStyle} />}
                    <FastImage
                        source={imgUri && imgUri.uri!=""?imgUri:require("../sources/icons/avatar_icon02.png")}
                        size={responsiveHeight(6.5)}
                        style={[lastActive && styles.offLineThumbStyle
                            ,{width:responsiveHeight(6.5),height:responsiveHeight(6.5)
                            ,borderRadius:responsiveHeight(6.5)/2}]}
                        defaultSource={require("../sources/icons/avatar_icon02.png")}
                    />
                </View>
                <View style={styles.detailContainerStyle}>
                    <View style={styles.nameContainerStyle}>
                        <Text style={[styles.userNameTextStyle, lastActive && styles.offLineTextStyle]}>{name}</Text>
                        {users && <Text style={styles.usersTextStyle}>  ({users})</Text>}
                    </View>
                    <View style={styles.descContainerStyle}>
                        <Text numberOfLines={1} style={[styles.descTextStyle, lastActive && styles.offLineTextStyle]}>{desc}</Text>
                        {lastActive && <Text style={[styles.descTextStyle, styles.lastActiveTextStyle, lastActive && styles.offLineTextStyle]}>Last Active : {lastActive}</Text>}
                    </View>
                </View>
                <View style={styles.buttonContainerStyle}>
                    {this.renderActionButton(actionType,status,badge)}
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = {
    friendContactCardContainerStyle: {
        flexDirection: 'row',
        height: responsiveHeight(12),
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: '#231f20',
    },
    onLineDotStyle: {
        position: 'absolute',
        width: responsiveHeight(1.4),
        height: responsiveHeight(1.4),
        borderRadius: responsiveHeight(0.7),
        backgroundColor: "#00a651",
        zIndex: 5,
    },
    offLineThumbStyle: {
        opacity: 0.8,
    },
    detailContainerStyle: {
        marginLeft: responsiveWidth(2.5),
        justifyContent: 'center',
        flex: 1,
    },
    nameContainerStyle:{
        flexDirection: 'row',
        alignItems: 'center',
    },
    userNameTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#ffffff"
    },
    usersTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.6),
        color: "#bcbec0"
    },
    offLineTextStyle: {
        color: '#bcbec0'
    },
    descContainerStyle: {
        flexDirection: 'row',
    },
    descTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.4),
        color: "#ffffff",
        flex:0.4
    },
    lastActiveTextStyle: {
        marginLeft: responsiveWidth(5),
    },
    settingIconStyle: {
        width: responsiveHeight(2.8),
        height: responsiveHeight(2.8),

    },
    followButtonStyle: {
        width: responsiveWidth(17.9),
        height: responsiveHeight(3.8),
        borderRadius: 1.1,
        backgroundColor: "#6a85c3",
        justifyContent: 'center',
        alignItems: 'center',
    },
    requestTypeContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    buttonStyle: {
        width: responsiveWidth(17.9),
        height: responsiveHeight(3.8),
        borderRadius: 1.1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    confirmButtonStyle: {
        backgroundColor: "#6a85c3"
    },
    deleteButtonStyle: {
        backgroundColor: "#e42526",
        marginLeft: responsiveWidth(2),
    },
    subscribeButtonStyle: {
        width: responsiveWidth(17.9),
        height: responsiveHeight(3.8),
        borderRadius: 1.1,
        backgroundColor: "#f8981d",
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.5),
        color: "#ffffff"
    },
    buttonContainerStyle: {
        justifyContent: 'center',
    },
    badgeTextContainerStyle:{
        backgroundColor: '#fbb040',
        width: responsiveHeight(3.5),
        height: responsiveHeight(3.5),
        borderRadius: responsiveHeight(3.5),
        justifyContent: 'center',
        zIndex: 5,
        marginLeft: responsiveWidth(5),

    },
    badgeTextStyle:{
        color: '#FFF',
        textAlign: 'center',
        textAlignVertical: 'center',


    },

}

export { FriendContactCard }
