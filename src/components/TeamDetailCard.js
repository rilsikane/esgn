import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

class TeamDetailCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { desc } = this.props

        return (
            <View style={styles.teamDetailCardContainerStyle}>
                <Text style={styles.titleTextStyle}>TEAM DETAILS :</Text>
                <Text style={styles.teamDescTextStyle}>{desc}</Text>
            </View>
        )
    }
}

const styles = {
    teamDetailCardContainerStyle: {
        width: responsiveWidth(95),
        height: responsiveHeight(36.3),
        borderRadius: 1.8,
        backgroundColor: "#ffffff",
        borderStyle: "solid",
        borderWidth: 0.5,
        borderColor: "#cdcccc",
        padding: responsiveWidth(2.5),
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#212221",
        marginBottom: responsiveHeight(1),
    },
    teamDescTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.7),
        color: "#231f20"
    },

}

export { TeamDetailCard }
