import React, { Component } from 'react'
import { View, Text, Image } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import { Button } from 'native-base'

import { ItemInfoCard } from './ItemInfoCard'
import { LabelInput } from './LabelInput'
import StarRating from './react-native-star-rating'
import CheckBox from './react-native-check-box'
import { observer, inject } from 'mobx-react';
import {convertCurrency,convertNumber} from './utils/number';
@inject('naviStore')
@observer
class ItemSellingCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isChecked :false
        }
    }

    render() {
        const { style, headerColor, title, imgUri, itemDesc, rating, feeCharge, feeTotal, isChecked, totalPrice } = this.props

        return (
            <View style={[styles.cardContainerStyle, style, headerColor && { backgroundColor: headerColor }]}>
                <View style={styles.cardSubContainerStyle}>
                    <ItemInfoCard
                        title={title}
                        imgUri={imgUri}
                        rating={rating}
                        info={itemDesc}
                    />
                    <Text style={styles.titleTextStyle}>What price do you want to Sell?</Text>
                    <LabelInput
                        onChangeText={(val) => this.props.onChangeText(val)}
                        label={'THB'}
                        remark={`Fee of charge. ${feeCharge}`}
                        keyboardType="numbers-and-punctuation"
                    />
                    <View style={styles.pinContainerStyle}>
                        <View style={styles.pinSectionStyle}>
                            <Image
                                source={require('../sources/icons/pin_icon01.png')}
                                resizeMode='contain'
                                style={styles.iconStyle}
                            />
                            <Text style={styles.pinTextStyle}> Pin</Text>
                        </View>
                        <View style={styles.pinSectionStyle}>
                            <StarRating
                                disabled
                                maxStars={1}
                                rating={0}
                                fullStarColor='#f7941d'
                                emptyStarColor='#a7a9ac'
                                starSize={responsiveHeight(2.6)}
                                halfStarEnabled
                            />
                            <Text style={styles.pinTextStyle}> Highlight</Text>
                        </View>
                    </View>
                    <View style={styles.feeContainerStyle}>
                        <Text style={styles.pinTextStyle}> FEE : {convertCurrency(feeTotal)}</Text>
                        <Text style={styles.unitTextStyle}>THB </Text>
                    </View>
                    <View style={styles.priceContainerStyle}>
                        <Text style={styles.pinTextStyle}> Total Price : {convertCurrency(totalPrice)}</Text>
                        <Text style={styles.totalUnitTextStyle}>THB </Text>
                    </View>
                    <CheckBox
                        isChecked={this.state.isChecked}
                        rightText='Accept terms of selling.'
                        rightTextStyle={styles.checkBoxTextStyle}
                        style={styles.checkboxContainerStyle}
                        checkBoxColor='#d1d3d4'
                        onClick={() => {this.setState({isChecked:!this.state.isChecked})}}
                    />
                    <View style={styles.lineStyle} />
                    <Button disabled={!this.state.isChecked} rounded style={[styles.submitButtonStyle,!this.state.isChecked&&{backgroundColor: "gray"}]} onPress={()=>{
                          this.props.naviStore.navigation.dismissLightBox({
                            animationType: 'slide-down' // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
                          });
                        this.props.sell();
                    }}>
                        <Text style={styles.submitButtonTextStyle}>Sell</Text>
                    </Button>
                </View>
            </View>
        )
    }
}

const styles = {
    cardContainerStyle: {
        width: responsiveWidth(95),
        height: responsiveHeight(85),
        backgroundColor: "#445982",
        alignSelf: 'center',
        borderRadius: 4,
        paddingTop: responsiveHeight(2.5),
    },
    cardSubContainerStyle: {
        flex: 1,
        backgroundColor: '#FFF',
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 4,
        padding: responsiveWidth(2.5),
        alignItems: 'center',
    },
    titleContainerStyle: {

    },
    titleLabelStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#0a2042",
        textAlign: 'center'
    },
    lineStyle: {
        width: '100%',
        height: 1,
        backgroundColor: "#bcbec0",
        marginTop: responsiveHeight(2.5),
        marginBottom: responsiveHeight(2.5),
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#333333",
        textAlign: 'center',
        marginBottom: responsiveHeight(2),
    },
    pinContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        paddingLeft: responsiveWidth(4),
        paddingRight: responsiveWidth(4),
    },
    pinSectionStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    iconStyle: {
        height: responsiveHeight(2.6),
        width: responsiveHeight(2.6),
    },
    pinTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#414042",
    },
    unitTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#939598"
    },
    totalUnitTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#FFF"
    },
    feeContainerStyle: {
        width: '90%',
        height: responsiveHeight(5.5),
        backgroundColor: "#e6e7e8",
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    priceContainerStyle: {
        width: '90%',
        height: responsiveHeight(5.5),
        backgroundColor: "#d1d3d4",
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    checkBoxTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#414042"
    },
    checkboxContainerStyle: {
        marginTop: responsiveHeight(2),
    },
    submitButtonStyle: {
        width: responsiveWidth(85),
        height: responsiveHeight(8),
        backgroundColor: "#445982",
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
    },
    submitButtonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff"
    },

}

export { ItemSellingCard }
