import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

class MenuButtonGroup extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderButtonList(buttonList, headingType) {
        if (headingType == 'image') {
            return buttonList.map((data, index) =>
                <TouchableOpacity key={index} style={styles.iconContainerStyle}>
                    <Image
                        source={data.iconUri}
                        resizeMode='contain'
                        style={styles.iconStyle}
                    />
                </TouchableOpacity>
            )
        } else if (headingType == 'text') {

        } else if (headingType == 'text-icon') {

        }

    }

    render() {
        const { buttonList, headingType, style } = this.props

        return (
            <View style={[styles.menuButtonGroupContainerStyle, style]}>
                {this.renderButtonList(buttonList, headingType)}
            </View>
        )
    }
}

const styles = {
    menuButtonGroupContainerStyle: {
        height: responsiveHeight(10),
        flexDirection: 'row',
        alignItems: 'center',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: '#231f20',
    },
    iconStyle: {
        height: responsiveHeight(3),
        width: responsiveHeight(3),
    },
    iconContainerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },


}
export { MenuButtonGroup }
