import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import {Input} from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import { observer, inject } from 'mobx-react';
import store from 'react-native-simple-store';
import FastImage from 'react-native-fast-image';
import * as Animatable from 'react-native-animatable';
import { ifIphoneX, isIphoneX } from 'react-native-iphone-x-helper'
@inject('naviStore')
@observer
class Headers extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user:{},showSearch:false,searchVal:""
        }
    }
    async componentDidMount(){
        let user = await store.get("user");
        this.setState({user:user});
    }
    gotoMenu(link){
        if(link){
            this.props.naviStore.navigation.resetTo({
                screen: link, // unique ID registered with Navigation.registerScreen
                title: undefined, // navigation bar title of the pushed screen (optional)
                titleImage: undefined, // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
                animated: true, // does the push have transition animation or does it happen immediately (optional)
                backButtonTitle: undefined, // override the back button title (optional)
                backButtonHidden: false, // hide the back button altogether (optional)
                animationType: 'fade'
            })
        }
    }
    renderLeftItem(leftName) {
        if (leftName == 'home') {
            return (
                <TouchableOpacity 
                    onPress={()=> this.props.naviStore.navigation.toggleDrawer({
                    side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
                    animated: true, // does the toggle have transition animation or does it happen immediately (optional)
                    to: 'open' // optional, 'open' = open the drawer, 'closed' = close it, missing = the opposite of current state
                  })}>
                    <Image
                        source={require('../sources/icons/app_icon03.png')}
                        resizeMode='contain'
                        style={styles.appIconStyle}
                    />
                </TouchableOpacity>
            )
        } else if (leftName == 'back') {
            return (
                <TouchableOpacity onPress={(e)=> this.props.naviStore.navigation.pop()}>
                    <Text style={styles.backTextStyle}>{'<'} Back</Text>
                </TouchableOpacity>
            )
        }else if (leftName == 'cancel') {
            return (
                <TouchableOpacity onPress={(e)=> this.props.cancel()}>
                    <Text style={styles.backTextStyle}>{'<'} Back</Text>
                </TouchableOpacity>
            )
        }
        
    }

    renderCenterItem(title) {
        if (title != undefined) {
            return (
                <Text numberOfLines={1} style={[styles.titleTextStyle,this.props.fontSize && {fontSize:this.props.fontSize}]}>{title}</Text>
            )
        }
    }
    onShowSearch(){
        if(!this.state.showSearch){
            this.refs.header.transitionTo({height: responsiveHeight(isIphoneX()?20:17)},50,true,100);
            this.refs.search.fadeInLeft(500);
        }else{
            this.refs.header.transitionTo({height: responsiveHeight(isIphoneX()?13:10)},50,true,100);
        }
        this.setState({showSearch:!this.state.showSearch})
    }
    onSearch(){
        this.props.onSearch(this.state.searchVal);
    }

    renderRightAbsoluteItem(rightName,onSearch) {
        if (rightName == 'searchLive') {
            return (
                <View style={styles.rightAbsoluteItemSubContainerStyle}>
                    {onSearch && 
                    <TouchableOpacity onPress={()=>this.onShowSearch()}>
                        <Image
                            source={require('../sources/icons/search_icon01.png')}
                            resizeMode='contain'
                            style={styles.searchIconStyle}
                        />
                    </TouchableOpacity>}
                    <TouchableOpacity style={styles.userIconContainerStyle}>
                        <Image
                            source={require('../sources/icons/user_icon01.png')}
                            resizeMode='contain'
                            style={styles.userIconStyle}
                        />
                    </TouchableOpacity>
                </View>
            )
        } else if (rightName == 'searchData') {
            return (
                <View style={styles.rightAbsoluteItemSubContainerStyle}>
                    {onSearch && <TouchableOpacity onPress={()=>this.onShowSearch()}>
                        <Image
                            source={require('../sources/icons/search_icon01.png')}
                            resizeMode='contain'
                            style={styles.searchIconStyle}
                        />
                    </TouchableOpacity>}
                    <TouchableOpacity onPress={()=>this.gotoMenu('esgn.UserProfileScreen')} style={styles.userIcon02ContainerStyle}>
                        <FastImage
                            source={{uri:this.state.user.picture}}
                            resizeMode='contain'
                            style={styles.userIcon02Style}
                        />
                    </TouchableOpacity>
                    {/* <TouchableOpacity style={styles.settingIconContainerStyle}>
                        <Image
                            source={require('../sources/icons/setting_icon01.png')}
                            resizeMode='contain'
                            style={styles.userIconStyle}
                        />
                    </TouchableOpacity> */}
                    <View style={styles.settingIconContainerStyle}></View>
                </View>
            )
        }
    }

    render() {
        const { leftName, title, rightName, onSearch} = this.props

        return (
            <Animatable.View ref="header" style={styles.headersContainerStyle}>
                <View style={{flexDirection:'row',flex:1}}>
                    <View style={styles.leftItemContainerStyle}>          
                        {this.renderLeftItem(leftName)}
                    </View>
                    <View style={styles.centerItemsContainerStyle}>
                        {this.renderCenterItem(title)}
                    </View>
                    <View style={styles.rightItemContainerStyle} />
                    <View style={styles.absoluteRightItemContainerStyle}>
                        {this.renderRightAbsoluteItem(rightName,onSearch)}
                    </View>
                </View>
                <Animatable.View ref="search" style={this.state.showSearch && {flex:0.5,marginLeft:20,marginRight:20}}>
                    {this.state.showSearch ?<Input
                        placeholder="ค้นหา"
                        returnKeyLabel="search"
                        returnKeyType="search"
                        value={this.state.searchVal}
                        onChangeText={(val) => this.setState({searchVal:val})}
                        style={styles.inputStyle}
                        onSubmitEditing={()=>this.onSearch()}
                    />:null}
                </Animatable.View>
            </Animatable.View>
        )
    }
}

const styles = {
    headersContainerStyle: {
        height: responsiveHeight(isIphoneX() ? 13:10),
        backgroundColor: '#212221',
        borderBottomWidth: 1,
        borderColor: '#0F0E0E',
        
    },
    leftItemContainerStyle: {
        flex: 0.25,
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft:10
    },
    centerItemsContainerStyle: {
        flex: 0.5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    rightItemContainerStyle: {
        flex: 0.25,
    },
    absoluteRightItemContainerStyle: {
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    rightAbsoluteItemSubContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    searchIconStyle: {
        height: responsiveHeight(2.5),
        width: responsiveHeight(2.5),
    },
    userIconContainerStyle: {
        marginRight: responsiveWidth(4),
        marginLeft: responsiveWidth(6),
    },
    userIcon02ContainerStyle: {
        marginLeft: responsiveWidth(3),
    },
    settingIconContainerStyle: {
        marginRight: responsiveWidth(3),
        marginLeft: responsiveWidth(3),
    },
    userIconStyle: {
        height: responsiveHeight(2.5),
        width: responsiveHeight(2.5),
    },
    userIcon02Style: {
        height: responsiveHeight(4),
        width: responsiveHeight(4),
    },
    backTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveHeight(2.5),
        color: "#ffffff"
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveHeight(2.7),
        color: "#ffffff"
    },
    appIconStyle: {
        height: responsiveHeight(4),
    },
    inputStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#212221",
        backgroundColor:"#fff",
        borderRadius: 5,
    },
}

export { Headers }