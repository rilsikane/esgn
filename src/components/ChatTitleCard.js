import React, { Component } from 'react'
import { View, Text, FlatList } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import Accordion from '@ercpereda/react-native-accordion'
import { FriendContactCard } from './FriendContactCard'
import { observer, inject } from 'mobx-react';
@inject('naviStore')
@observer
class ChatTitleCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
        this._renderHeader = this._renderHeader.bind(this);
        this._renderContent = this._renderContent.bind(this);
    }

    _renderHeader(){
        const { chatTitle, total } = this.props
        return (
            <View style={styles.titleContainerStyle}>
                <Text style={styles.chatTitleTextStyle}>{chatTitle}</Text>
                <Text style={styles.totalTextStyle}>  {total&&`(${total})`}</Text>
            </View>
        )

    }
   
    _renderItem(item,isFriend,isRequest,isChat){
             return <FriendContactCard
                onPress={()=>this.props.openChat ? this.props.openChat(item):null}
                imgUri={{uri:!isFriend ?item.image:item.picture}}
                name={!isFriend ?item.name:item.nickname}
                desc={item.desc}
                lastActive={item.lastActive}
                isOnLine={item.isOnLine}
                users={item.users}
                actionType={!isChat?(isRequest?'request':'setting'):null}
                confirm={()=>this.props.confirm(item.friend_invite_id)}
                reject={()=>this.props.reject(item.friend_invite_id)}
                openSetting={()=>this.props.openSetting(item)}
            />
    }
    
    _renderContent(){
        const { chatList,isFriend,isRequest,isChat} = this.props
        return <FlatList
                data={chatList}
                renderItem={({item,index})=>this._renderItem(item,isFriend,isRequest,isChat)}
                keyExtractor={(item, index) => index.toString()}
        />
    }
   

    render() {
        const { canExpand,expanded } = this.props

        return (
            <Accordion
                sections={[{}]}
                header={({ isOpen }) => this._renderHeader()}
                content={this._renderContent()}
                disabled={!canExpand}
                duration={50}
                easing="easeOutCubic"
                expanded={expanded}
            />
        )
    }
}

const styles = {
    chatTitleCardContainerStyle: {

    },
    titleContainerStyle:{
        flexDirection: 'row',
        alignItems: 'center',
        height: responsiveHeight(8),
        paddingTop: responsiveHeight(2),
        paddingBottom: responsiveHeight(2),
        borderBottomWidth: 1,
        borderColor: '#231f20',
    },
    chatTitleTextStyle:{
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    totalTextStyle:{
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.6),
        color: "#bcbec0"
    },

}

export { ChatTitleCard }
