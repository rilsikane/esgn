import React, { Component } from 'react'
import { View, Text, Image } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

class ExpRewardTitleCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { title, expCount } = this.props

        return (
            <View style={styles.expRewardTitleCardContainerStyle}>
                <Text style={styles.titleTextStyle}>{title}</Text>
                <View style={styles.expCountContainerStyle}>
                    <Image
                        source={require('../sources/icons/exp_icon02.png')}
                        resizeMode='contain'
                        style={styles.expIconStyle}
                    />
                    <Text style={styles.titleTextStyle}>{expCount}</Text>
                </View>
            </View>
        )
    }
}

const styles = {
    expRewardTitleCardContainerStyle: {
        height: responsiveHeight(8),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: responsiveWidth(2.5),
        paddingBottom: responsiveWidth(2.5),
        paddingRight: responsiveWidth(2.5),
        borderBottomWidth: 1,
        borderColor: '#231f20',
    },
    expIconStyle: {
        width: responsiveHeight(4.6),
        height: responsiveHeight(4.6)
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#ffffff"
    },
    expCountContainerStyle:{
        alignItems: 'center',
    },

}

export { ExpRewardTitleCard }
