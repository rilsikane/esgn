import React, { Component } from 'react'
import { View, Text, Image } from 'react-native'
import { Button } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import * as Animatable from 'react-native-animatable'

class SubmitResultCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { iconUri, title, style, buttonTitle, buttonColor, headerColor } = this.props

        return (
            <View style={[styles.submitResultCardContainerStyle, style, headerColor && { backgroundColor: headerColor }]}>
                <View style={[styles.sectionStyle, headerColor && styles.headerStyle]}>
                    <Animatable.View animation='bounceIn' style={[styles.cardContainerStyle]}>
                        {buttonTitle && <Text style={styles.resultTextStyle}>{title}</Text>}
                        <Image
                            source={iconUri}
                            resizeMode='contain'
                            style={styles.iconStyle}
                        />
                        <View style={styles.borderLineStyle} />
                        {buttonTitle ?
                            <Button rounded style={[styles.submitButtonStyle, buttonColor && { backgroundColor: buttonColor }]}>
                                <Text style={styles.submitButtonTextStyle}>{buttonTitle}</Text>
                            </Button> :
                            <Text style={styles.resultTextStyle}>{title}</Text>
                        }
                    </Animatable.View>
                </View>
            </View>
        )
    }
}

const styles = {
    submitResultCardContainerStyle: {
        alignSelf: 'center',
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
    },
    sectionStyle: {
        backgroundColor: '#FFF',
        borderRadius: 4,
    },
    cardContainerStyle: {
        width: responsiveWidth(95),
        //height: responsiveHeight(50.1),
        borderRadius: 4,
        backgroundColor: "#ffffff",
        alignItems: 'center',
        padding: responsiveWidth(2.5),
    },
    headerStyle: {
        marginTop: responsiveHeight(2),
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
    },
    iconStyle: {
        height: responsiveHeight(29),
        marginTop: responsiveHeight(5),
    },
    borderLineStyle: {
        height: 1,
        backgroundColor: '#bcbec0',
        width: '100%',
        marginTop: responsiveHeight(5),
        marginBottom: responsiveHeight(3),
    },
    resultTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#414042",
    },
    submitButtonStyle: {
        width: responsiveWidth(85),
        height: responsiveHeight(8),
        backgroundColor: "#e42526",
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        marginBottom: responsiveHeight(2),
    },
    submitButtonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },

}

export { SubmitResultCard }
