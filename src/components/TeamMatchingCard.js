import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, FlatList } from 'react-native'
import { Button } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import Accordion from './react-native-collapsible/Accordion'
import { CommentCard } from './CommentCard'
import FastImage from 'react-native-fast-image'
class TeamMatchingCard extends Component {

    constructor(props) {
        super(props)

    }

    renderRightStatus(status, statusTime, t2Score, showImg) {
        if (status == 'predict') {
            return (
                <View style={[styles.predictButtonContainerStyle, showImg && styles.showImgRendererStyle]}>
                    <Text style={styles.statusTimeTextStyle}>{statusTime}</Text>
                    <Button full style={styles.predictButtonStyle}>
                        <Text style={styles.predictTextStyle}>Predict</Text>
                    </Button>
                </View>
            )
        } else if (status == 'living') {
            return (
                <TouchableOpacity>
                    <Image
                        source={require('../sources/icons/live_icon02.png')}
                        resizeMode='contain'
                        style={[styles.liveIconStyle, showImg && styles.liveIconShowImgStyle]}
                    />
                </TouchableOpacity>
            )
        } else if (status == 'ended') {
            return (
                <View style={[styles.endedStatusContainerStyle, showImg && styles.endedStatusShowImgContainerStyle]}>
                    <Image
                        source={require('../sources/icons/clock_icon01.png')}
                        resizeMode='contain'
                        style={styles.clockIconStyle}
                    />
                    <Text style={styles.timeEndedTextStyle}>{statusTime}</Text>
                </View>
            )
        } else if (status == 'score') {
            return (
                <View style={styles.t2ScoreTextContainerStyle}>
                    <Text style={styles.scoreTextStyle}>{t2Score}</Text>
                </View>
            )
        }
    }

    renderLeftStatus(status, date, time, gameCount, t1Score, gameImg, showImg) {
        if (showImg) {
            return (
                <View style={styles.gameImgContainerStyle}>
                    <Image
                        source={gameImg}
                        style={styles.gameImgStyle}
                    />
                </View>
            )
        } else if (status == 'coming') {
            return (
                <View style={styles.comingContainerStyle}>
                    <View style={styles.dateContainerStyle}>
                        <Text style={styles.dateTextStyle}>{date}</Text>
                        <View style={styles.timeContainerStyle}>
                            <Image
                                source={require('../sources/icons/clock_icon01.png')}
                                resizeMode='contain'
                                style={styles.clockComingIconStyle}
                            />
                            <Text style={styles.timeEndedTextStyle}>{time}</Text>
                        </View>
                    </View>
                    <View style={styles.gameCountCotainerStyle}>
                        <Text style={styles.gameLabelTextStyle}>GAME</Text>
                        <Text style={styles.gameCountTextStyle}>{gameCount}</Text>
                    </View>
                </View>
            )
        } else if (status == 'score') {
            return (
                <View style={styles.t1ScoreContainerStyle}>
                    <View style={styles.gameCountCotainerStyle}>
                        <Text style={styles.gameLabelTextStyle}>GAME</Text>
                        <Text style={styles.gameCountTextStyle}>{gameCount}</Text>
                    </View>
                    <View style={styles.sectionLineStyle} />
                    <View style={styles.t1ScoreTextContainerStyle}>
                        <Text style={styles.scoreTextStyle}>{t1Score}</Text>
                    </View>
                </View>
            )
        }
    }

    _renderHeader = (content, index, isActive, sections) => {

        const { t1Img, t2Img, t1Name, t2Name, t1Rank, t2Rank, status, statusTime, date, time, gameCount,
            t1Score, t2Score, gameImg, showImg
        } = this.props

        return (
            <View style={styles.teamMatchingCardContainerStyle}>
                {this.renderLeftStatus(status, date, time, gameCount, t1Score, gameImg, showImg)}
                <View style={styles.teamDetailContainerStyle}>
                    <FastImage
                        source={t1Img}
                        resizeMode='contain'
                        style={styles.team1IconStyle}
                    />
                    <View>
                        <Text numberOfLines={1} style={styles.teamNameTextStyle}>{t1Name}</Text>
                        <Text style={styles.teamRankTextStyle}>{t1Rank}</Text>
                    </View>
                </View>
                <FastImage
                    source={require('../sources/icons/vs_icon01.png')}
                    resizeMode='contain'
                    style={[styles.vsIconStyle, !showImg && styles.vsShowImgStyle]}
                />
                <View style={styles.teamDetailContainerStyle}>
                    <View>
                        <Text numberOfLines={1} style={styles.teamNameTextStyle}>{t2Name}</Text>
                        <Text style={styles.teamRankTextStyle}>{t2Rank}</Text>
                    </View>
                    <FastImage
                        source={t2Img}
                        resizeMode='contain'
                        style={[styles.team2IconStyle, showImg && styles.team2IconShowImgStyle]}
                    />
                </View>
                {this.renderRightStatus(status, statusTime, t2Score, showImg)}
            </View>
        )
    }

    _renderContent = (content, index, isActive, sections) => {
        return (
            <View>
                {this.renderCommentCardList()}
            </View>
        )
    }

    renderCommentCardList() {
        const { commentList } = this.props

        return (
            < FlatList
                data={commentList}
                renderItem={this.renderCommentItem}
                keyExtractor={(item, index) => index.toString()}
            />
        )

    }

    renderCommentItem = ({ item }) => (
        <CommentCard
            userImgUri={item.userImgUri}
            userName={item.userName}
            timeLeft={item.timeLeft}
            desc={item.desc}
        />
    )

    render() {
        const { canExpand,isHome,t1Img, t2Img, t1Name, t2Name, t1Rank, t2Rank, status, statusTime, date, time, gameCount,
            t1Score, t2Score, gameImg, showImg } = this.props
        return isHome==false ?(
                <Accordion
                    sections={[{}]}
                    renderHeader={this._renderHeader}
                    renderContent={this._renderContent}
                    disabled={!canExpand}
                />
        ):( <TouchableOpacity onPress={()=> this.props.onPress ? this.props.onPress():null} style={styles.teamMatchingCardContainerStyle}>
            {this.renderLeftStatus(status, date, time, gameCount, t1Score, gameImg, showImg)}
            <View style={styles.teamDetailContainerStyle}>
                <FastImage
                    source={t1Img}
                    resizeMode='contain'
                    style={styles.team1IconStyle}
                />
                <View style={{flex:1}}>
                    <Text numberOfLines={1} style={styles.teamNameTextStyle}>{t1Name}</Text>
                    <Text style={styles.teamRankTextStyle}>{t1Rank}</Text>
                </View>
            </View>
            <Image
                source={require('../sources/icons/vs_icon01.png')}
                resizeMode='contain'
                style={[styles.vsIconStyle, !showImg && styles.vsShowImgStyle]}
            />
            <View style={styles.teamDetailContainerStyle}>
                <View style={{flex:1}}>
                    <Text numberOfLines={1} style={styles.teamNameTextStyle}>{t2Name}</Text>
                    <Text style={styles.teamRankTextStyle}>{t2Rank}</Text>
                </View>
                <FastImage
                    source={t2Img}
                    resizeMode='contain'
                    style={[styles.team2IconStyle, showImg && styles.team2IconShowImgStyle]}
                />
            </View>
            {this.renderRightStatus(status, statusTime, t2Score, showImg)}
        </TouchableOpacity>
        )
    }
}

const styles = {
    teamMatchingCardContainerStyle: {
        flexDirection: 'row',
        paddingBottom: responsiveHeight(1),
        marginTop: responsiveHeight(1),
        borderBottomWidth: 1,
        borderBottomColor: '#231f20',
        alignItems: 'center',
        width:responsiveWidth(95)
    },
    teamDetailContainerStyle: {
        flexDirection: 'row',
        flex:0.4
    },
    team1IconStyle: {
        height: responsiveHeight(4),
        width: responsiveHeight(4),
        marginRight: responsiveWidth(2),
    },
    team2IconStyle: {
        height: responsiveHeight(4),
        width: responsiveHeight(4),
        marginLeft: responsiveWidth(2),
    },
    team2IconShowImgStyle: {
        marginRight: responsiveWidth(2),
    },
    teamNameTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(1.6),
        color: "#ffffff"
    },
    teamRankTextStyle: {
        fontFamily: "BentonSans",
        fontSize: responsiveFontSize(1.2),
        color: "#989898"
    },
    vsIconStyle: {
        height: responsiveHeight(3.5),
        width: responsiveHeight(3.5),
        marginLeft: responsiveWidth(4),
        marginRight: responsiveWidth(4),
    },
    vsShowImgStyle: {
        marginLeft: responsiveWidth(6),
        marginRight: responsiveWidth(6),
    },
    statusTimeTextStyle: {
        fontFamily: "BentonSans",
        fontSize: responsiveFontSize(1.2),
        color: "#ffffff",
        textAlign: 'center',
    },
    showImgRendererStyle: {
        marginLeft: 0,
    },
    predictButtonContainerStyle: {
        marginLeft: responsiveWidth(10),
    },
    predictButtonStyle: {
        height: responsiveHeight(3),
        width: responsiveWidth(12.8),
        backgroundColor: "#3c5e89",
        justifyContent: 'center',
        alignItems: 'center',
    },
    predictTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(1),
        fontStyle: "italic",
        color: "#f27e3d"
    },
    liveIconStyle: {
        height: responsiveHeight(4),
        width: responsiveHeight(4),
        marginLeft: responsiveWidth(13),
    },
    liveIconShowImgStyle: {
        marginLeft: responsiveWidth(4),
    },
    clockIconStyle: {
        height: responsiveHeight(2),
        width: responsiveHeight(2),
        marginRight: responsiveWidth(1),
    },
    clockComingIconStyle: {
        height: responsiveHeight(1.4),
        width: responsiveHeight(1.4),
        marginRight: responsiveWidth(1),
    },
    endedStatusContainerStyle: {
        flexDirection: 'row',
        marginLeft: responsiveWidth(11),
        alignItems: 'center',
    },
    endedStatusShowImgContainerStyle: {
        marginLeft: responsiveWidth(1),
    },
    timeEndedTextStyle: {
        fontFamily: "BentonSans",
        fontSize: responsiveFontSize(1),
        color: "#ffffff"
    },
    t1ScoreContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    gameCountCotainerStyle: {
        alignItems: 'center',
    },
    gameLabelTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(1.2),
        color: "#ffffff"
    },
    gameCountTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(1),
        color: "#ffffff"
    },
    sectionLineStyle: {
        marginLeft: responsiveWidth(1),
        marginRight: responsiveWidth(1),
        borderLeftWidth: 1,
        borderLeftColor: '#FFF',
        height: responsiveHeight(4),
    },
    t1ScoreTextContainerStyle: {
        justifyContent: 'center',
        width: responsiveHeight(3),
        height: responsiveHeight(3),
        borderRadius: 2,
        backgroundColor: "#1a1b1a",
        alignItems: 'center',
        marginRight: responsiveWidth(1.5),
    },
    t2ScoreTextContainerStyle: {
        justifyContent: 'center',
        width: responsiveHeight(3),
        height: responsiveHeight(3),
        borderRadius: 2,
        backgroundColor: "#1a1b1a",
        alignItems: 'center',
        marginLeft: responsiveWidth(1.5),
    },
    scoreTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(1.5),
        color: "#ffffff",
    },
    comingContainerStyle: {
        flexDirection: 'row',
        borderRightWidth: 1,
        borderRightColor: '#FFF',
        paddingRight: responsiveWidth(1.5),
        marginRight: responsiveWidth(1.5),
    },
    dateContainerStyle: {
        alignItems: 'center',
        marginRight: responsiveWidth(1.5),
    },
    dateTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(1.2),
        color: "#ffffff"
    },
    timeContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    gameImgContainerStyle: {
        marginRight: responsiveWidth(2),
    },
    gameImgStyle: {
        height: responsiveHeight(4),
        width: responsiveWidth(10),
    },
    commentLabelTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff"
    }

}

export { TeamMatchingCard }