import React, { Component } from 'react'
import { View, Text, Image, ScrollView } from 'react-native'
import { Tabs, Tab, TabHeading } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

class MarketTabs extends Component {
    constructor(props) {
        super(props)

    }

    renderTabHeader(tabData, child, headingType, contentContainerStyle, activeIndex, headerStyle) {
        return tabData.map((data, i) =>
            <Tab
                key={i}
                heading={
                    <TabHeading style={[styles.tabHeadingContainerStyle, headerStyle, activeIndex == i && styles.activeTabStyle]}>
                        {this.renderTabHeading(data, headingType, activeIndex, i)}
                    </TabHeading>
                }
            >
                <ScrollView style={styles.tabContentStyle} contentContainerStyle={[styles.contentContainerStyle, contentContainerStyle]}>
                    {child[i]}
                </ScrollView>
            </Tab>
        )
    }

    renderTabHeading(data, headingType, activeIndex, index) {
        if (headingType == 'image') {
            return (
                <Image
                    source={data.iconUri}
                    resizeMode='contain'
                    style={styles.tabIconStyle}
                />
            )
        } else if (headingType == 'text') {
            return (
                <Text style={[styles.tabHeaderLabelTextStyle, activeIndex != index && styles.activeTextStyle]}>{data.title}</Text>
            )
        } else if (headingType == 'text-icon') {
            return (
                <View style={styles.textIconContainerStyle}>
                    <Image
                        source={data.iconUri}
                        style={styles.textIconStyle}
                        resizeMode='contain'
                    />
                    <Text style={[styles.tabHeaderLabelTextStyle, activeIndex != index && styles.activeTextStyle]}>  {data.title}</Text>
                </View>
            )
        }
    }

    render() {
        const { tabData, child, headingType, contentContainerStyle, activeIndex, headerStyle } = this.props

        return (
            <Tabs
                tabBarUnderlineStyle={styles.tabBarUnderlineStyle}
                tabContainerStyle={styles.tabsContainerStyle}
            >
                {this.renderTabHeader(tabData, child, headingType, contentContainerStyle, activeIndex, headerStyle)}
            </Tabs>
        )
    }
}

const styles = {
    tabBarUnderlineStyle: {
        backgroundColor: 'transparent',
        borderColor: 'transparent',
    },
    tabsContainerStyle: {
        width: responsiveWidth(100),
        height: responsiveHeight(8),
        alignSelf: 'center',
        backgroundColor: 'transparent',
        elevation: 0,
        borderBottomWidth: 0,
    },
    tabHeadingContainerStyle: {
        backgroundColor: '#d1d3d4',
        marginLeft: responsiveWidth(0.5),
        marginRight: responsiveWidth(0.5),
    },
    activeTabStyle: {
        backgroundColor: '#2b405a',
    },
    tabIconStyle: {
        height: responsiveHeight(3),
    },
    tabContentStyle: {
        flex: 1,
        backgroundColor: '#212221',
    },
    contentContainerStyle: {
        alignItems: 'center',
        paddingTop: responsiveHeight(2),
    },
    tabHeaderLabelTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    activeTextStyle: {
        color: "#2b405a"
    },
    textIconContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    textIconStyle: {
        width: responsiveHeight(2.2),
        height: responsiveHeight(2.2),
    },

}
export { MarketTabs }