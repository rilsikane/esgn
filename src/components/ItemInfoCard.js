import React, { Component } from 'react'
import { View, Text, Image } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import StarRating from './react-native-star-rating'

class ItemInfoCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { title, imgUri, rating, info, } = this.props

        return (
            <View style={styles.itemInfoCardContainerStyle}>
                <Text style={styles.itemTitleTextStyle}>{title}</Text>
                <View style={styles.lineStyle} />
                <View style={styles.itemInfoContainerStyle}>
                    <View style={styles.imgContainerStyle}>
                        <Image
                            source={imgUri}
                            style={styles.itemImgStyle}
                            borderRadius={3}
                        />
                    </View>
                    <View style={styles.infoContainerStyle}>
                        <View style={styles.ratingContainerStyle}>
                            <Text style={styles.detailLabelTextStyle}>รายละเอียด :</Text>
                            <StarRating
                                disabled
                                maxStars={5}
                                rating={rating}
                                fullStarColor='#f7941d'
                                emptyStarColor='#a7a9ac'
                                starSize={responsiveHeight(1.6)}
                                halfStarEnabled
                            />
                        </View>
                        <Text style={styles.infoTextStyle}>{info}</Text>
                    </View>
                </View>
                <View style={styles.lineStyle} />
            </View>
        )
    }
}

const styles = {
    itemInfoCardContainerStyle: {
        alignItems: 'center',
        width: '100%',
        paddingLeft: responsiveWidth(2.5),
        paddingRight: responsiveWidth(2.5),
    },
    itemTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#414042",
        marginTop: responsiveHeight(1),
    },
    lineStyle: {
        width: '100%',
        height: 1,
        backgroundColor: "#bcbec0",
        marginTop: responsiveHeight(2),
        marginBottom: responsiveHeight(2),
    },
    itemInfoContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    imgContainerStyle: {
        width: responsiveHeight(14.9),
        height: responsiveHeight(14.9),
        alignItems: 'center',
        flex: 0.4,
    },
    itemImgStyle: {
        width: responsiveHeight(14.9),
        height: responsiveHeight(14.9)
    },
    infoContainerStyle: {
        flex: 0.6
    },
    ratingContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    detailLabelTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.5),
        color: "#e42526"
    },
    infoTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.5),
        color: "#58595b",
        width: responsiveWidth(50),
    },

}

export { ItemInfoCard }
