import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

class NotifyCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { imgUri, title, desc, isOnline } = this.props

        return (
            <View style={styles.notifyCardContainerStyle}>
                <TouchableOpacity onPress={()=>this.props.onPress()} style={styles.notifyContainerStyle}>
                    <View style={styles.imgContainerStyle}>
                        <Image
                            source={imgUri}
                            style={styles.imgStyle}
                        />
                    </View>
                    <View style={styles.detailContainerStyle}>
                        <Text style={styles.titleTextStyle}>{title}</Text>
                        <Text numberOfLines={1} style={styles.descTextStyle}>  {desc}</Text>
                    </View>
                    <View style={styles.onlineContainerStyle}>
                        {isOnline && <View style={styles.onlineStyle} />}
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = {
    notifyCardContainerStyle: {
        paddingTop: responsiveHeight(2),
        paddingBottom: responsiveHeight(2),
        borderBottomWidth: 1,
        borderColor: '#231f20',
        height: responsiveHeight(10),
    },
    notifyContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    imgContainerStyle: {
        flex: 0.2,
        alignItems: 'center',
    },
    imgStyle: {
        width: responsiveHeight(6.7),
        height: responsiveHeight(6.7),
        borderRadius: responsiveHeight(3.35),
    },
    detailContainerStyle: {
        flex: 0.7,
        flexDirection: 'row',
        alignItems: 'center',
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#ffffff",
    },
    descTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.4),
        color: "#d1d3d4",
        flex: 1
    },
    onlineContainerStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 0.1,
    },
    onlineStyle: {
        width: responsiveHeight(1),
        height: responsiveHeight(1),
        borderRadius: responsiveHeight(0.5),
        backgroundColor: "#46b349"
    },

}
export { NotifyCard }
