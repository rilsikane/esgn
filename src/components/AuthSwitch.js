import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { Switch } from './react-native-switch'

class AuthSwitch extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderSwitch(state, onValueChange, disabled) {
        return (
            <View style={[styles.switchStyle, disabled && styles.disabledStyle]}>
                <Switch
                    value={state}
                    backgroundActive={'#FFF'}
                    backgroundInactive={'#FFF'}
                    circleActiveColor={'#84c669'}
                    circleInActiveColor={'#6d6e71'}
                    barBorderColor='#6d6e71'
                    circleSize={responsiveHeight(4)}
                    barHeight={responsiveHeight(5.5)}
                    circleBorderWidth={0}
                    switchLeftPx={responsiveWidth(0.32)}
                    barWidth={responsiveWidth(20)}
                    onValueChange={onValueChange}
                    disabled={disabled}
                />
            </View>
        )
    }

    render() {
        const { labelText, state, onValueChange, disabled } = this.props

        return (
            <View style={styles.authSwitchContainerStyle}>
                <Text style={styles.subLabelStyle}>{labelText}</Text>
                <View style={styles.switchContainerStyle}>
                    <Text style={[styles.turnoffTextStyle, state == true && styles.turnonTextStyle]}>{state == true ? 'TURN ON' : 'TURN OFF'}</Text>
                    {this.renderSwitch(state, onValueChange, disabled)}
                </View>

            </View>
        )
    }
}

const styles = {
    authSwitchContainerStyle: {
        paddingLeft: responsiveWidth(5),
        paddingRight: responsiveWidth(5),
    },
    subLabelStyle: {
        fontFamily: 'Kanit',
        color: '#808285',
        fontSize: responsiveFontSize(2),
    },
    switchContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    turnoffTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#e42526"
    },
    turnonTextStyle: {
        color: "#84c669"
    },
    switchStyle: {
        //width: responsiveWidth(21),
    },
    disabledStyle: {
        opacity: 0.5,
    }
}
export { AuthSwitch }
