import React, { Component } from 'react'
import { View, Text, Image, ScrollView } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

class LeaderListCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderLeaderList() {
        let leaders = [
            {
                name: 'Goldram',
                desc: '#kaizer0681',
                point: '1.836',
                state: 'up',
            },
            {
                name: 'Pelham',
                desc: '#kaizer0681',
                point: '1.812',
                state: 'down',
            },
            {
                name: 'Toutoies',
                desc: '#kaizer0681',
                point: '1.800',
                state: 'still',
            }
        ]

        return leaders.map((data, index) =>
            <View key={index} style={styles.leaderItemContainerStyle}>
                <View style={styles.rankContainerStyle}>
                    {index == 0 &&
                        <Image
                            source={require('../sources/icons/leader_icon01.png')}
                            resizeMode='contain'
                            style={styles.leaderIconStyle}
                        />
                    }
                    <Text style={styles.rankTextStyle}>{index + 1}</Text>
                </View>
                <View style={styles.titleContainerStyle}>
                    <Text style={styles.nameTextStyle}>{data.name}</Text>
                    <Text style={styles.descTextStyle}>{data.desc}</Text>
                </View>
                <Image
                    source={data.state == 'still' ? undefined : (data.state == 'up' ? require('../sources/icons/rank_up_icon01.png') : require('../sources/icons/rank_down_icon01.png'))}
                    resizeMode='contain'
                    style={styles.rankStateIconStyle}
                />
                <Text style={styles.pointTextStyle}>{data.point}</Text>
            </View>
        )
    }

    render() {
        return (
            <View style={styles.leaderListCardContainerStyle}>
                <View style={styles.coverImgContainerStyle}>
                    <Image
                        source={require('../sources/images/leader_img01.png')}
                        resizeMode='contain'
                        style={styles.coverImgStyle}
                    />
                </View>
                <View style={styles.detailContainerStyle}>
                    <ScrollView>
                        {this.renderLeaderList()}
                    </ScrollView>
                </View>
            </View>
        )
    }
}

const styles = {
    leaderListCardContainerStyle: {
        width: responsiveWidth(100),
        height: responsiveHeight(22),
        backgroundColor: "#1b2838",
        flexDirection: 'row',
        alignItems: 'center',
    },
    coverImgContainerStyle: {
        flex: 0.35,
        justifyContent: 'center',
        alignItems: 'center',
    },
    detailContainerStyle: {
        flex: 0.65,
    },
    coverImgStyle: {
        width: responsiveHeight(14.2),
        height: responsiveHeight(14.2)
    },
    leaderItemContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    rankContainerStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        width: responsiveWidth(10),
    },
    leaderIconStyle: {
        width: responsiveHeight(1.8),
        height: responsiveHeight(1.8),
    },
    rankTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.4),
        color: "#ffffff"
    },
    titleContainerStyle: {
        width: responsiveWidth(30),
    },
    nameTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    descTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#a7a9ac"
    },
    rankStateIconStyle: {
        width: responsiveHeight(1.3),
        height: responsiveHeight(1.3),
    },
    pointTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.2),
        color: "#ffffff",
        marginLeft: responsiveWidth(5),
    },

}
export { LeaderListCard }
