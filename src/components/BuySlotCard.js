import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import { Button } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { LabelInput } from './LabelInput'
import { observer, inject } from 'mobx-react';
@inject('naviStore')
@observer
class BuySlotCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { style, credit, headerColor } = this.props

        return (
            <View style={[styles.buySlotCardContainerStyle, style, headerColor && { backgroundColor: headerColor }]}>
                <View style={styles.buySlotCardStyle}>
                    <View style={styles.titleContainerStyle}>
                        <Text style={styles.titleLabelStyle}>Your Credit for :</Text>
                        <Text style={styles.creditTextStyle}>{credit}</Text>
                        <TouchableOpacity style={styles.topupButtonContainerStyle}>
                            <Image
                                source={require('../sources/icons/topup_icon01.png')}
                                resizeMode='contain'
                                style={styles.topupIconStyle}
                            />
                            <Text style={styles.topupTextStyle}> Topup</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.lineStyle} />
                    <Text style={styles.titleTextStyle}>Buy More Slot</Text>
                    <Image
                        source={require('../sources/icons/slot_icon01.png')}
                        resizeMode='contain'
                        style={styles.slotIconStyle}
                    />
                    <View style={styles.inputSectionStyle}>
                        <Text style={styles.inputLabelStyle}>Amount</Text>
                        <LabelInput
                            label={'Slot'}
                        />
                    </View>
                    <View style={styles.inputSectionStyle}>
                        <Text style={styles.inputLabelStyle}>Cost</Text>
                        <LabelInput
                            label={'THB'}
                        />
                    </View>
                    <View style={styles.lineStyle} />
                    <Button rounded style={styles.submitButtonStyle} onPress={()=>{
                        this.props.naviStore.navigation.dismissLightBox();
                    }}>
                        <Text style={styles.submitButtonTextStyle}>Buy</Text>
                    </Button>
                </View>
            </View>
        )
    }
}

const styles = {
    buySlotCardContainerStyle: {
        width: responsiveWidth(95),
        height: responsiveHeight(71.2),
        backgroundColor: "#445982",
        alignSelf: 'center',
        borderRadius: 4,
        paddingTop: responsiveHeight(2.5),
    },
    buySlotCardStyle: {
        flex: 1,
        backgroundColor: '#FFF',
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 4,
        padding: responsiveWidth(2.5),
        alignItems: 'center',
    },
    titleContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    titleLabelStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#0a2042"
    },
    creditTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.4),
        color: "#f7941d",
        marginLeft: responsiveWidth(4),
        marginRight: responsiveWidth(4),
    },
    topupButtonContainerStyle: {
        width: responsiveWidth(19.8),
        height: responsiveHeight(4.5),
        borderRadius: responsiveHeight(5),
        backgroundColor: "#e42526",
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    topupIconStyle: {
        width: responsiveHeight(1.9),
        height: responsiveHeight(1.9),
    },
    topupTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(1.2),
        color: "#ffffff"
    },
    lineStyle: {
        width: '100%',
        height: 1,
        backgroundColor: "#bcbec0",
        marginTop: responsiveHeight(2.5),
        marginBottom: responsiveHeight(2.5),
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(3.5),
        color: "#333333"
    },
    slotIconStyle: {
        width: responsiveHeight(18.5),
        height: responsiveHeight(18.5),
        marginBottom: responsiveHeight(2.5),
    },
    inputSectionStyle: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'center',
    },
    inputLabelStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#445982",
        textAlign: 'right',
        width: responsiveWidth(20),
        marginRight: responsiveWidth(5),
    },
    submitButtonStyle: {
        width: responsiveWidth(85),
        height: responsiveHeight(8),
        backgroundColor: "#445982",
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
    },
    submitButtonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff"
    },

}

export { BuySlotCard }
