import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { Item, Input } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import FontAwesome from 'react-native-vector-icons/FontAwesome'

class PhonePicker extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { error, errorText, countryValue, telValue, onCountryPress, onTelChangeText, style, editable } = this.props

        return (
            <View style={[styles.phonePickerContainerStyle, style]}>
                <Text style={styles.titleTextStyle}>Telephone</Text>
                <Item error={error}>
                    <TouchableOpacity disabled={!editable} onPress={onCountryPress} style={styles.countryContainerStyle}>
                        <Text style={styles.labelTextStyle}>{countryValue != '' ? countryValue : 'Country'}</Text>
                        <FontAwesome name='caret-down' color='#818181' size={responsiveFontSize(2)} />
                    </TouchableOpacity>
                    <Input
                        {...this.props}
                        value={telValue}
                        onChangeText={onTelChangeText}
                        placeholder='Tel No.'
                        placeholderTextColor={styles.inputStyle.color}
                        style={styles.inputStyle}
                        keyboardType='phone-pad'
                        maxLength={10}
                    />
                </Item>
                {error ? <Text style={styles.errorTextStyle}>{errorText}</Text> : null}
            </View>
        )
    }
}

const styles = {
    phonePickerContainerStyle: {
        width: responsiveWidth(90),
        alignSelf: 'center',
    },
    titleTextStyle: {
        fontFamily: 'Kanit',
        color: '#808285',
        fontSize: responsiveFontSize(2),
    },
    countryContainerStyle: {
        height: responsiveHeight(8),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        flex: 0.3
    },
    telContainerStyle: {
        height: responsiveHeight(8),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        flex: 0.7,
        marginLeft: responsiveWidth(3),
    },
    labelTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.5),
        color: "#a7a9ac"
    },
    inputStyle: {
        fontFamily: 'Kanit',
        color: '#a7a9ac',
        fontSize: responsiveFontSize(1.5),
        paddingLeft: 0,
        marginLeft: responsiveWidth(3),
        flex: 0.7,
    },
    errorTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.6),
        color: "#e42526",
        marginTop: responsiveHeight(0.5),
    },
}

export { PhonePicker }