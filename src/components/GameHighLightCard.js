import React, { Component } from 'react'
import { View, Text, TouchableOpacity, ImageBackground, Image } from 'react-native'
import { Card } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import FastImage from 'react-native-fast-image';
import {convertNumber} from './utils/number';
class GameHighLightCard extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { imgUri, iconUri, prize, countdownTime, style, disabled,gotoDetail } = this.props

        return (
            <TouchableOpacity onPress={()=>gotoDetail()}
            disabled={disabled} style={[styles.gameHighLightCardContainerStyle, style]}>
                <FastImage
                    source={imgUri}
                    style={[styles.imgBackgroundStyle, style]}
                >
                    <Card style={styles.prizeContainerStyle}>
                        <Text style={styles.prizeTitleTextStyle}>PRIZE{'\n'}POOL</Text>
                        <Text style={styles.prizeTextStyle}>{convertNumber(prize)} ฿</Text>
                    </Card>
                    <View style={styles.timeIconContainerStyle}>
                        <View>
                            {iconUri ?
                                <FastImage
                                    defaultSource={require('../sources/icons/app_icon01.png')}
                                    source={iconUri}
                                    resizeMode='contain'
                                    style={styles.gameIconUriStyle}
                                /> : null}
                        </View>
                        {countdownTime && <Card style={styles.countdownTimeContainerStyle}>
                            <View style={styles.countdownTimeBackgroundStyle} />
                            <Image
                                source={require('../sources/icons/clock_icon01.png')}
                                resizeMode='contain'
                                style={styles.clockIconStyle}
                            />
                            <View>
                                <Text style={styles.countdownLabelTextStyle}>Count Down</Text>
                                <Text numberOfLines={1} style={styles.countdownTextStyle}>{countdownTime}</Text>
                            </View>
                        </Card>}
                    </View>
                </FastImage>
            </TouchableOpacity>
        )
    }
}

const styles = {
    gameHighLightCardContainerStyle: {
        height: responsiveHeight(30),
        width: responsiveWidth(100),
        marginBottom: responsiveHeight(1),
    },
    imgBackgroundStyle: {
        height: responsiveHeight(30),
        width: responsiveWidth(100),
        justifyContent: 'space-between',
    },
    prizeContainerStyle: {
        height: responsiveHeight(6),
        width: responsiveWidth(40),
        borderRadius: 2,
        backgroundColor: "#e42526",
        borderColor: 'transparent',
        marginLeft: responsiveWidth(2),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    prizeTitleTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(1.5),
        color: "#f5f7f9",
        height: '100%',
    },
    prizeTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(2.5),
        color: "#f5f7f9",
        marginLeft: responsiveWidth(2),
    },
    gameIconUriStyle: {
        height: responsiveHeight(7),
        width: responsiveHeight(7),
    },
    countdownTimeContainerStyle: {
        flexDirection: 'row',
        alignSelf: 'flex-end',
        borderColor: 'transparent',
        backgroundColor: 'transparent',
        height: responsiveHeight(6),
        width: responsiveWidth(40),
        marginRight: responsiveWidth(2),
        marginBottom: responsiveWidth(2),
        alignItems: 'center',
        justifyContent: 'center',
    },
    timeIconContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: responsiveWidth(2)
    },
    countdownTimeBackgroundStyle: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        backgroundColor: '#000',
        opacity: 0.6,
        borderRadius: 2,
    },
    clockIconStyle: {
        height: responsiveHeight(3.2),
        width: responsiveHeight(3.2),
        marginRight: responsiveWidth(2),
    },
    countdownLabelTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(1.4),
        color: "#f5f7f9",
        textAlign: 'center',
    },
    countdownTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(2),
        color: "#f5f7f9",
        textAlign: 'center',
        width: responsiveWidth(30)
    }

}

export { GameHighLightCard }
