import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Textarea, Button } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { ErrorInput } from './ErrorInput'

class ReportForm extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        return (
            <View style={styles.reportFormContainerStyle}>
                <ErrorInput
                    // error={}
                    // value={}
                    // onChangeText={}
                    placeholder='Tournament Name'
                //errorText={}
                />
                <ErrorInput
                    // error={}
                    // value={}
                    // onChangeText={}
                    placeholder='Telephone'
                //errorText={}
                />
                <Text style={styles.mainLabelTextStyle}>Description</Text>
                <Textarea
                    rowSpan={5}
                    bordered
                    style={styles.descInputStyle}
                    //onChangeText={}
                    numberOfLines={5}
                />
                <Button style={styles.submitButtonStyle}>
                    <Text style={styles.submitButtonTextStyle}>Confirm</Text>
                </Button>
            </View>
        )
    }
}

const styles = {
    reportFormContainerStyle: {
        alignItems: 'center',
    },
    mainLabelTextStyle: {
        fontFamily: 'Kanit',
        color: '#FFF',
        fontSize: responsiveFontSize(2),
        marginTop: responsiveHeight(1),
        marginLeft: responsiveWidth(2.5),
        alignSelf: 'flex-start',
    },
    descInputStyle: {
        backgroundColor: '#e6e7e8',
        borderRadius: 3,
        marginTop: responsiveHeight(1),
        fontFamily: "Kanit",
        height: responsiveHeight(16.7),
        fontSize: responsiveFontSize(1.7),
        color: "#808285",
        paddingTop: responsiveHeight(1),
        width: '95%',
    },
    submitButtonStyle: {
        width: responsiveWidth(95),
        height: responsiveHeight(8),
        backgroundColor: "#e42526",
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        marginBottom: responsiveHeight(2),
        marginTop: responsiveHeight(2),
    },
    submitButtonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },

}
export { ReportForm }
