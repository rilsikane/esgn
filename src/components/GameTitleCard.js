import React, { Component } from 'react'
import { View, Text, Image,TouchableOpacity } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import FastImage from 'react-native-fast-image'
class GameTitleCard extends Component {
    constructor(props) {
        super(props)

    }

    render() {
        const { imgUri, title, count, countUnit, iconUri, style } = this.props

        return (
            <View style={{flex:1}}>
            <TouchableOpacity onPress={()=>this.props.onPress && this.props.onPress()} style={[styles.gameTitleCardContainerStyle, style]}>
                <View style={styles.gameCoverImgContainerStyle}>
                    <FastImage
                        resizeMode="contain"
                        source={imgUri}
                        style={styles.gameCoverImgContainerStyle}
                    />
                </View>
                <Text numberOfLines={1} style={styles.titleTextStyle}>{title}</Text>
                {/* <View style={styles.trophyContainerStyle}>
                    <FastImage
                        source={iconUri}
                        resizeMode='contain'
                        style={styles.trophyIconStyle}
                    />
                    <Text style={styles.trophyCountTextStyle}>{count} {countUnit}</Text>
                </View> */}
            </TouchableOpacity>
            </View>
        )
    }
}

const styles = {
    gameTitleCardContainerStyle: {
        flex:1
    },
    gameCoverImgContainerStyle: {
        height: responsiveHeight(15),
        width: responsiveWidth(26),
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#ffffff"
    },
    trophyContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    trophyIconStyle: {
        height: responsiveHeight(1.6),
        width: responsiveHeight(1.6),
    },
    trophyCountTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.5),
        color: "#ffffff"
    }

}
export { GameTitleCard }