import React, { Component } from 'react'
import { View, Text, Image } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { PrizeTag } from '../components/PrizeTag'

class ItemHistoryCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { imgUri, title, date, prize, type } = this.props

        return (
            <View style={styles.itemHistoryCardContainerStyle}>
                <View style={styles.imgContainerStyle}>
                    <Image
                        source={imgUri}
                        borderRadius={3}
                        style={styles.imgStyle}
                    />
                </View>
                <View style={styles.detailContainerStyle}>
                    <Text style={styles.titleTextStyle}>{type == 'buy' ? 'คุณได้ขาย' : 'คุณได้ซื้อ'} {title}</Text>
                    <View style={styles.dateContainerStyle}>
                        <Image
                            source={require('../sources/icons/calendar_icon01.png')}
                            resizeMode='contain'
                            style={styles.calendarIconStyle}
                        />
                        <Text style={styles.dateTextStyle}>  {date}</Text>
                    </View>
                </View>
                <PrizeTag
                    prize={prize}
                    prizeUnit={'฿'}
                    type={type}
                    style={styles.prizeTagStyle}
                />
            </View>
        )
    }
}

const styles = {
    itemHistoryCardContainerStyle: {
        height: responsiveHeight(12),
        paddingTop: responsiveWidth(2.5),
        paddingBottom: responsiveWidth(2.5),
        borderBottomWidth: 1,
        borderColor: '#231f20',
        flexDirection: 'row',
        alignItems: 'center',
    },
    imgContainerStyle: {
        flex: 0.3,
        alignItems: 'center',
    },
    imgStyle: {
        width: responsiveHeight(10.5),
        height: responsiveHeight(10.5)
    },
    detailContainerStyle: {
        flex: 0.7,
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#ffffff",
        marginBottom: responsiveHeight(1.5),
    },
    dateContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    calendarIconStyle: {
        width: responsiveHeight(2.2),
        height: responsiveHeight(2.2),
    },
    dateTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.4),
        color: "#ffffff"
    },
    prizeTagStyle: {

    },

}

export { ItemHistoryCard }
