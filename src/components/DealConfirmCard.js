import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Button } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import {convertCurrency,convertNumber} from './utils/number';
class DealConfirmCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderPrizeList(prizeList) {
        return prizeList.prizes.map((data, index) =>
            <View key={index} style={[styles.rowTitle1ContainerStyle, Number.isInteger(index * 0.5) && styles.rowTitle2ContainerStyle]}>
                <Text style={[styles.listLabel1Style, Number.isInteger(index * 0.5) && styles.listLabel2Style]}>{data.title}</Text>
                <Text style={[styles.listLabel1Style, Number.isInteger(index * 0.5) && styles.listLabel2Style]}>{prizeList.unit}{convertCurrency(data.prize)}</Text>
            </View>
        )
    }

    render() {
        const { prizeList } = this.props

        return (
            <View style={styles.dealConfirmCardStyle}>
                <View style={styles.dealConfirmCardContainerStyle}>
                    <View style={styles.rowTitle1ContainerStyle}>
                        <Text style={styles.titleTextStyle}>สรุปราคา</Text>
                    </View>
                    {this.renderPrizeList(prizeList)}
                    <View style={[styles.rowTitle1ContainerStyle, Number.isInteger(prizeList.prizes.length * 0.5) && styles.rowTitle2ContainerStyle]}>
                        <Text style={[styles.listLabel1Style, Number.isInteger(prizeList.prizes.length * 0.5) && styles.listLabel2Style]}>ค่าธรรมเนียม</Text>
                        <Text style={[styles.listLabel1Style, Number.isInteger(prizeList.prizes.length * 0.5) && styles.listLabel2Style]}>{prizeList.unit}{convertCurrency(prizeList.fee)}</Text>
                    </View>
                    <View style={[styles.rowTitle1ContainerStyle, !Number.isInteger(prizeList.prizes.length * 0.5) && styles.rowTitle2ContainerStyle]}>
                        <Text style={[styles.listLabel1Style, !Number.isInteger(prizeList.prizes.length * 0.5) && styles.listLabel2Style]}>ราคาสุทธิ</Text>
                        <Text style={[styles.listLabel1Style, !Number.isInteger(prizeList.prizes.length * 0.5) && styles.listLabel2Style]}>{prizeList.unit}{convertCurrency(prizeList.total)}</Text>
                    </View>
                </View>
                <Button style={styles.submitButtonStyle} onPress={()=>this.props.buy()}>
                    <Text style={styles.submitButtonTextStyle}>Confirm</Text>
                </Button>
            </View>
        )
    }
}

const styles = {
    dealConfirmCardStyle:{
        marginTop: responsiveHeight(2),
    },
    dealConfirmCardContainerStyle: {
        alignSelf: 'center',
    },
    rowTitle1ContainerStyle: {
        width: responsiveWidth(67.7),
        height: responsiveHeight(4.4),
        backgroundColor: "#e6e7e8",
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.6),
        color: "#445982"
    },
    rowTitle2ContainerStyle: {
        backgroundColor: "#445982",
    },
    listLabel1Style: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.6),
        color: "#445982",
        flex: 1,
        textAlign: 'center',
    },
    listLabel2Style: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.6),
        color: "#FFF"
    },
    submitButtonStyle: {
        width: responsiveWidth(85),
        height: responsiveHeight(8),
        backgroundColor: "#1b75bc",
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        marginBottom: responsiveHeight(5),
        marginTop: responsiveHeight(2),
        marginLeft: 5,
        marginRight: 5,
    },
    submitButtonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },

}

export { DealConfirmCard }
