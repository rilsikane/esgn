import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import {convertCurrency,convertNumber} from './utils/number';
class ItemDealDetailCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderTitle(status, reqCount, prizeAt, prizeUnit) {
        if (status == 'buy') {
            return (
                <View style={styles.titleContainerStyle}>
                    <Text style={styles.reqTextStyle}>{reqCount}</Text>
                    <Text style={styles.titleTextStyle}> for sale starting at </Text>
                    <Text style={styles.reqTextStyle}>{prizeUnit}{prizeAt}</Text>
                </View>
            )
        } else if (status == 'sell') {
            return (
                <View style={styles.titleContainerStyle}>
                    <Text style={styles.reqTextStyle}>{reqCount}</Text>
                    <Text style={styles.titleTextStyle}> requests to buy at </Text>
                    <Text style={styles.reqTextStyle}>{prizeUnit}{prizeAt}</Text>
                    <Text style={styles.titleTextStyle}> or lower</Text>
                </View>
            )
        }
    }

    renderButton(status) {
        if (status == 'buy') {
            return (
                <TouchableOpacity style={styles.buyButtonStyle}>
                    <Text style={styles.buttonTextStyle}>BUY</Text>
                </TouchableOpacity>
            )

        } else if (status == 'sell') {
            return (
                <TouchableOpacity style={styles.sellButtonStyle}>
                    <Text style={styles.buttonTextStyle}>SELL</Text>
                </TouchableOpacity>
            )
        }
    }

    renderPrizeList(prizeList) {
        return (
            <View style={styles.prizeListContainerStyle}>
                <View style={styles.rowTitleContainerStyle}>
                    <Text style={styles.listLabel1Style}>Price</Text>
                    <Text style={styles.listLabel1Style}>Quantity</Text>
                </View>
                {prizeList.map((data, index) =>
                    <View key={index} style={[styles.rowTitleContainerStyle, Number.isInteger(index * 0.5) && styles.rowTitle2ContainerStyle]}>
                        <Text style={[styles.listLabel1Style, Number.isInteger(index * 0.5) && styles.listLabel2Style]}>฿ {convertNumber(data.price)}</Text>
                        <Text style={[styles.listLabel1Style, Number.isInteger(index * 0.5) && styles.listLabel2Style]}>{data.number_offer}</Text>
                    </View>
                )}
            </View>
        )
    }

    render() {
        const { status, reqCount, prizeAt, prizeUnit, prizeList } = this.props

        return (
            <View style={styles.itemDealDetailCardContainerStyle}>
                {/* {this.renderTitle(status, reqCount, prizeAt, prizeUnit)}
                {this.renderButton(status)} */}
                <ScrollView style={{ marginTop: responsiveHeight(2) }}>
                    {this.renderPrizeList(prizeList)}
                </ScrollView>
            </View>
        )
    }
}

const styles = {
    itemDealDetailCardContainerStyle: {
        width: responsiveWidth(100),
        height: responsiveHeight(49.1),
        // borderRadius: 1.8,
        backgroundColor: "#ffffff",
        // borderStyle: "solid",
        // borderWidth: 0.5,
        // borderColor: "#cdcccc",
        marginBottom: responsiveHeight(1),
        alignItems: 'center',
        padding: responsiveWidth(2.5),
    },
    titleContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: responsiveHeight(2),
    },
    reqTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.6),
        color: "#231f20"
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.6),
        color: "#939598"
    },
    buyButtonStyle: {
        width: responsiveWidth(47.8),
        height: responsiveHeight(7.4),
        borderRadius: 2.8,
        backgroundColor: "#009444",
        justifyContent: 'center',
        alignItems: 'center',
    },
    sellButtonStyle: {
        width: responsiveWidth(47.8),
        height: responsiveHeight(7.4),
        borderRadius: 2.8,
        backgroundColor: "#c35745",
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(3),
        color: "#ffffff"

    },
    prizeListContainerStyle: {
        width: responsiveWidth(95),
        alignItems: 'center',
    },
    rowTitleContainerStyle: {
        width: responsiveWidth(90),
        height: responsiveHeight(4.4),
        backgroundColor: "#e6e7e8",
        flexDirection: 'row',
        alignItems: 'center',
    },
    rowTitle2ContainerStyle: {
        backgroundColor: "#445982",
    },
    listLabel1Style: {
        flex: 0.5,
        textAlign: 'center',
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.6),
        color: "#231f20"

    },
    listLabel2Style: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.6),
        color: "#FFF"
    },
}

export { ItemDealDetailCard }
