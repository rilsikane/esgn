import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { Item } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import FontAwesome from 'react-native-vector-icons/FontAwesome'

class BirthDatePicker extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        const { error, errorText, dateValue, monthValue, yearValue, onDatePress, onMonthPress, onYearPress, style } = this.props

        return (
            <View style={[styles.datePickerContainerStyle, style]}>
                <Text style={styles.titleTextStyle}>Birth-Date</Text>
                <Item error={error}>
                    <TouchableOpacity onPress={onDatePress} style={styles.dateContainerStyle}>
                        <Text style={styles.labelTextStyle}>{dateValue != '' ? dateValue : 'Date'}</Text>
                        <FontAwesome name='caret-down' color='#818181' size={responsiveFontSize(2)} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={onMonthPress} style={styles.monthContainerStyle}>
                        <Text style={styles.labelTextStyle}>{monthValue != '' ? monthValue : 'Month'}</Text>
                        <FontAwesome name='caret-down' color='#818181' size={responsiveFontSize(2)} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={onYearPress} style={styles.yearContainerStyle}>
                        <Text style={styles.labelTextStyle}>{yearValue != '' ? yearValue : 'Year'}</Text>
                        <FontAwesome name='caret-down' color='#818181' size={responsiveFontSize(2)} />
                    </TouchableOpacity>
                </Item>
                {error ? <Text style={styles.errorTextStyle}>{errorText}</Text> : null}
            </View>
        )
    }
}

const styles = {
    datePickerContainerStyle: {
        width: responsiveWidth(90),
        alignSelf: 'center',
    },
    titleTextStyle: {
        fontFamily: 'Kanit',
        color: '#808285',
        fontSize: responsiveFontSize(2),
    },
    dateContainerStyle: {
        height: responsiveHeight(8),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        flex: 0.3
    },
    monthContainerStyle: {
        height: responsiveHeight(8),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        flex: 0.4,
        marginLeft: responsiveWidth(3),
        marginRight: responsiveWidth(3),
    },
    yearContainerStyle: {
        height: responsiveHeight(8),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        flex: 0.3
    },
    labelTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.5),
        color: "#a7a9ac"
    },
    errorTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.6),
        color: "#e42526",
        marginTop: responsiveHeight(0.5),
    },
}

export { BirthDatePicker }