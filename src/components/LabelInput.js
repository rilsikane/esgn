import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Input } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

class LabelInput extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { label, remark, style,keyboardType } = this.props

        return (
            <View style={style}>
                <View style={styles.labelInputContainerStyle}>
                    <Input
                        disabled={this.props.disabled}
                        value={this.props.value}
                        onChangeText={(val) => this.props.onChangeText ? this.props.onChangeText(val):null}
                        style={styles.inputStyle}
                        keyboardType={keyboardType}
                    />
                    <Text style={styles.labelTextStyle}>{label}</Text>
                </View>
                <Text style={styles.remarkTextStyle}>{remark}</Text>
            </View>
        )
    }
}

const styles = {
    labelInputContainerStyle: {
        width: 183,
        height: 31,
        borderRadius: 4,
        backgroundColor: "#414042",
        flexDirection: 'row',
        alignItems: 'center',
    },
    inputStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    labelTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#ffffff",
        marginRight: responsiveWidth(1),
    },
    remarkTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.6),
        color: "#e42526"
    },

}
export { LabelInput }
