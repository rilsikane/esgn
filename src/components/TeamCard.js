import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import FastImage from 'react-native-fast-image'
class TeamCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { status, imgUri, teamName, teamDesc, style } = this.props
        return (
            <TouchableOpacity onPress={ ()=>this.props.onPress && this.props.onPress()}
                style={[styles.teamCardContainerStyle, status == 'lose' && styles.loseTeamStyle, style]}>
                <FastImage
                    source={imgUri}
                    resizeMode='contain'
                    style={styles.teamImgStyle}
                />
                <Text numberOfLines={1} style={styles.teamNameTextStyle}>{teamName}</Text>
                <Text numberOfLines={1} style={styles.teamDescTextStyle}>{teamDesc}</Text>
            </TouchableOpacity>
        )
    }
}

const styles = {
    teamCardContainerStyle: {
        width: responsiveWidth(30),
        height: responsiveHeight(15.5),
        borderRadius: 4,
        borderStyle: "solid",
        borderColor: '#cbcbcb',
        borderWidth: 0.3,
        backgroundColor: "#FFF",
        alignItems: 'center',
        justifyContent: 'center',
    },
    loseTeamStyle: {
        backgroundColor: "#b3b3b3"
    },
    teamImgStyle: {
        width: responsiveWidth(24),
        height: responsiveHeight(6.7)
    },
    teamNameTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.5),
        color: "#58595b"
    },
    teamDescTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.3),
        color: "#929497"
    }
}

export { TeamCard }
