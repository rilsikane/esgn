import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image, ImageBackground } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import FastImage from 'react-native-fast-image';
import StarRating from './react-native-star-rating'
import { ifIphoneX, isIphoneX } from 'react-native-iphone-x-helper'
class ItemTitleCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderRatingFeedback(bought, prize, rating, moduleType,isLive,status) {
        if (moduleType == 'inventory') {
            return (
                <View style={styles.itemViewCodeContainerStyle}>
                    <Text style={styles.codeTextStyle}>CODE</Text>
                    <View style={styles.itemViewTypeSellContainerStyle}>
                        {/* <Image
                            source={require('../sources/icons/use_icon01.png')}
                            resizeMode='contain'
                            style={styles.useIconStyle}
                        /> */}
                        <View style={styles.useIconStyle}></View>
                        <TouchableOpacity style={styles.sellButtonStyle} onPress={()=>this.props.onBuySellPress ? this.props.onBuySellPress():null}>
                            <Text style={styles.sellButtonTextStyle}>{status!="selling"?'Sell':'Selling'}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        } else {
            return (
                <View>
                    <View style={styles.ratingContainerStyle}>
                        <Text style={styles.prizeTextStyle}>{prize} ฿</Text>
                        {!isLive && <StarRating
                            disabled
                            maxStars={5}
                            rating={rating}
                            fullStarColor='#f7941d'
                            emptyStarColor='#a7a9ac'
                            starSize={responsiveHeight(1.6)}
                            halfStarEnabled
                        />}
                    </View>
                    {!!bought &&
                        <TouchableOpacity onPress={()=>this.props.feedback()} style={styles.feedbackButtonContainerStyle}>
                            <Image
                                source={require('../sources/icons/feedback_icon01.png')}
                                resizeMode='contain'
                                style={styles.feedbackIconStyle}
                            />
                            <Text style={styles.feedbackLabelStyle}>Give a Feedback</Text>
                        </TouchableOpacity>
                    }
                </View>
            )
        }


    }

    renderMark(mark) {
        return (
            <ImageBackground
                source={require('../sources/icons/mark_icon01.png')}
                resizeMode='contain'
                style={styles.markIconStyle}
            >
                <Text numberOfLines={1} style={styles.markTextStyle}>{mark}</Text>
            </ImageBackground>
        )
    }

    renderContent(style, imgUri, title, desc, isFavorite, prize, rating, mark, bought, viewType, moduleType, timeLeft,isOwner,isLive,status) {
        if (viewType == 'item') {
            return (
                <TouchableOpacity onPress={()=>this.props.onPress ? this.props.onPress():null} style={[styles.itemTitleCardContainerStyle, style,!title &&{backgroundColor:"transparent"}]}>
                    {title && <View>
                    {!!mark && this.renderMark(mark)}
                    {this.props.buyMore && <View style={{position: 'absolute',zIndex: 5,}}>
                        <Image
                                source={require('../sources/images/buy-more-slot.png')}
                                style={styles.imgStyle}
                                borderTopLeftRadius={3}
                                borderTopRightRadius={3}
                            />
                    </View>}
                    <View style={styles.imgStyle}>
                        <FastImage
                            source={imgUri}
                            style={styles.imgStyle}
                            borderTopLeftRadius={3}
                            borderTopRightRadius={3}
                            resizeMode="stretch"
                        />
                        {moduleType == 'inventory' && timeLeft &&
                            <View style={styles.timeLeftContainerStyle}>
                                <Text style={styles.timeLeftTextStyle}>Time left : {timeLeft} </Text>
                            </View>
                        }
                    </View>
                    <View style={styles.titleContainerStyle}>
                        <View style={{flex:0.8}}>
                            <Text numberOfLines={1} style={styles.titleTextStyle}>{title}</Text>
                            <Text numberOfLines={1} style={styles.descTextStyle}>{desc}</Text>
                        </View>
                        {moduleType == 'inventory' ? null :
                            (!bought && !isLive && !isOwner) &&
                            <TouchableOpacity onPress={()=>this.props.wishlist()}>
                                <Image
                                    source={isFavorite ? require('../sources/icons/heart_icon01.png') : require('../sources/icons/heart_icon02.png')}
                                    resizeMode='contain'
                                    style={styles.favorIconStyle}
                                />
                            </TouchableOpacity>
                        }
                    </View>
                    {!this.props.isReview && this.renderRatingFeedback(bought, prize, rating, moduleType,isLive,status)}
                    </View>}
                </TouchableOpacity>
            )
        } else if (viewType == 'list') {
            return (
                <View style={[styles.listItemContainerStyle, style]}>
                    <TouchableOpacity onPress={()=>this.props.onPress ? this.props.onPress():null} style={styles.listItemSectionStyle}>
                        <View style={styles.imgContainerStyle}>
                            <FastImage
                                source={imgUri}
                                style={styles.listImgStyle}
                                borderRadius={3}
                            />
                            {moduleType == 'inventory' && timeLeft &&
                                <Text style={styles.timeLeftTextStyle}>Time left : {timeLeft}</Text>
                            }
                        </View>
                        <View style={{width:responsiveWidth(isIphoneX()?43:52)}}>
                            <Text numberOfLines={1} style={styles.listTitleTextStyle}>{title}</Text>
                            <Text numberOfLines={1} style={styles.listDescTextStyle}>{desc}</Text>
                            {moduleType == 'inventory' ?
                                <Image
                                    source={require('../sources/icons/use_icon02.png')}
                                    resizeMode='contain'
                                    style={styles.useIconStyle}
                                /> :
                                <View style={styles.listRatingContainerStyle}>
                                    <StarRating
                                        disabled
                                        maxStars={5}
                                        rating={rating}
                                        fullStarColor='#f7941d'
                                        emptyStarColor='#a7a9ac'
                                        starSize={responsiveHeight(1.6)}
                                        halfStarEnabled
                                    />
                                    {!isOwner &&<TouchableOpacity style={styles.listFavorIconStyle} onPress={()=>this.props.wishlist()}>
                                        <Image
                                            source={isFavorite ? require('../sources/icons/heart_icon01.png') : require('../sources/icons/heart_icon02.png')}
                                            resizeMode='contain'
                                            style={styles.favorIconStyle}
                                        />
                                    </TouchableOpacity>}
                                </View>}
                        </View>
                    </TouchableOpacity>
                    {!this.props.isReview && !isOwner && <View style={styles.buyContainerStyle}>
                        <Text style={styles.listPrizeTextStyle}>{moduleType == 'inventory' ? 'CODE' : prize + ' ฿'}</Text>
                        <TouchableOpacity style={styles.buyButtonStyle} onPress={()=>this.props.onBuySellPress ? this.props.onBuySellPress():null}>
                            <Text style={styles.buyTextStyle}>{moduleType == 'inventory' ? 'Sell' : 'Buy'}</Text>
                        </TouchableOpacity>
                    </View>}
                </View>
            )
        }

    }

    render() {
        const { style, imgUri, title, desc, isFavorite, prize, rating, mark, bought, viewType,
            moduleType, timeLeft, isOwner,isLive,status
        } = this.props

        return (
            <View style={[styles.cardContainerStyle, viewType == 'list' && styles.listViewContainerStyle]}>
                
                {this.renderContent(style, imgUri, title, desc, isFavorite, prize, rating, mark, bought, viewType, moduleType, timeLeft,isOwner,isLive,status)}
            </View>
        )
    }
}

const styles = {
    cardContainerStyle: {
        borderBottomWidth: 1,
        borderColor: '#231f20',
    },
    listViewContainerStyle: {
        paddingTop: responsiveWidth(2.5),
        paddingBottom: responsiveWidth(2.5),
    },
    itemTitleCardContainerStyle: {
        width: responsiveWidth(44.9),
        //height: responsiveHeight(29.4),
        backgroundColor: "#FFF",
        borderRadius: 3,
        paddingBottom: responsiveHeight(1),
    },
    imgStyle: {
        height: responsiveHeight(16.6),
        width: responsiveWidth(44.9),
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
    },
    timeLeftContainerStyle: {
        position: 'absolute',
    },
    timeLeftTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1),
        color: "#ffffff"
    },
    titleContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: responsiveHeight(2),
        paddingLeft: responsiveWidth(1),
        paddingRight: responsiveWidth(1),
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2),
        color: "#333333",
        flex:0.8
    },
    descTextStyle: {
        fontFamily: "SegoeUI",
        fontSize: responsiveFontSize(1.6),
        color: "#b4b4b4"
    },
    favorIconStyle: {
        width: responsiveHeight(2.6),
        height: responsiveHeight(2.6),
    },
    listFavorIconStyle: {
        marginLeft: responsiveWidth(10),
    },
    ratingContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: responsiveWidth(1),
        paddingRight: responsiveWidth(1),
        alignItems: 'center',
    },
    prizeTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(2),
        color: "#c35745"
    },
    feedbackButtonContainerStyle: {
        width: responsiveWidth(40.3),
        height: responsiveHeight(3.5),
        borderRadius: 6,
        backgroundColor: "#1b75bc",
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: responsiveHeight(1),
    },
    feedbackIconStyle: {
        width: responsiveHeight(2.4),
        height: responsiveHeight(2.4),
    },
    feedbackLabelStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#ffffff",
        marginLeft: responsiveWidth(1),
    },
    markIconStyle: {
        width: responsiveHeight(12.4),
        height: responsiveHeight(12.4),
        alignSelf: 'flex-end',
        position: 'absolute',
        zIndex: 5,
        alignItems: 'flex-end',
    },
    markTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff",
        marginRight: responsiveWidth(2),
    },
    listItemContainerStyle: {
        //height: responsiveHeight(12),
        flexDirection: 'row',
    },
    listItemSectionStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    imgContainerStyle: {
        marginLeft: 1
    },
    listImgStyle: {
        width: responsiveHeight(10.6),
        height: responsiveHeight(10.6),
        marginRight: responsiveWidth(10),
    },
    listTitleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.8),
        color: "#ffffff"
    },
    listDescTextStyle: {
        fontFamily: "SegoeUI",
        fontSize: responsiveFontSize(1.6),
        color: "#ffffff",
        marginBottom: responsiveHeight(1.5),
    },
    listRatingContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    buyContainerStyle: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    listPrizeTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(2),
        color: "#ffffff"
    },
    buyButtonStyle: {
        width: responsiveWidth(14.1),
        height: responsiveHeight(3.1),
        borderRadius: 2,
        backgroundColor: "#e42526",
        justifyContent: 'center',
        alignItems: 'center',
    },
    buyTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(1.4),
        color: "#ffffff"
    },
    itemViewCodeContainerStyle: {
        paddingLeft: responsiveWidth(1),
        paddingRight: responsiveWidth(1),
    },
    codeTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(1.6),
        color: "#6d6e71",
        textAlign: 'right',
    },
    itemViewTypeSellContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    useIconStyle: {
        width: responsiveHeight(3.7),
        height: responsiveHeight(3.7),
    },
    sellButtonStyle: {
        width: responsiveWidth(14.1),
        height: responsiveHeight(3.1),
        borderRadius: 2,
        backgroundColor: "#e42526",
        justifyContent: 'center',
        alignItems: 'center',
    },
    sellButtonTextStyle: {
        fontFamily: "Prompt",
        fontSize: responsiveFontSize(1.4),
        color: "#ffffff"
    },

}
export { ItemTitleCard }
