export function convertCurrency(number) {
    let num
    if(!number){
        number = 0;
    }
    if(!isNumeric(number)){
        num = parseFloat(number);
    }else{
        num = number
    }
    return Number(num).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    
}
export function convertNumber(number) {
    let num
    if(!number){
        return; 
    }
    if(!isNumeric(number)){
        num = parseFloat(number);
    }else{
        num = number
    }
    return Number(num).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,').replace(".00","");
    
}
function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}