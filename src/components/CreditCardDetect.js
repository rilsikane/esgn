import React, { Component } from 'react'
import { View, Text, Image } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

class CreditCardDetect extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    renderCardList(cardType) {

        let activeIndex = null
        if (cardType == 'visa') {
            activeIndex = 0
        } else if (cardType == 'mastercard') {
            activeIndex = 1
        } else if (cardType == 'amex') {
            activeIndex = 2
        }

        let data = [
            { title: 'visa', iconUri: require('../sources/icons/visa_icon01.png') },
            { title: 'master card', iconUri: require('../sources/icons/mastercard_icon01.png') },
            { title: 'american express', iconUri: require('../sources/icons/americanexpress_icon01.png') },
            { title: 'paypal', iconUri: require('../sources/icons/paypal_icon01.png') },
        ]


        return data.map((data, i) =>
            <View key={i} style={[styles.creditCardContainerStyle, activeIndex == i && styles.activeCardStyle]}>
                <Image
                    source={data.iconUri}
                    style={i == 1 ? styles.largeIconStyle : styles.creditCardIconStyle}
                    resizeMode='contain'
                />
            </View>
        )
    }

    render() {
        const { style, cardType } = this.props

        return (
            <View style={[styles.creditCardDetectContainerStyle, style]}>
                {this.renderCardList(cardType)}
            </View>
        )
    }
}

const styles = {
    creditCardDetectContainerStyle: {
        flexDirection: 'row',
        width: responsiveWidth(100),
        justifyContent: 'space-around',
        alignSelf: 'center',
    },
    creditCardContainerStyle: {
        width: responsiveWidth(20),
        height: responsiveHeight(7),
        borderRadius: 4,
        backgroundColor: "#ffffff",
        borderStyle: "solid",
        borderWidth: 0.5,
        borderColor: "#cdcccc",
        alignItems: 'center',
        justifyContent: 'center',
    },
    largeIconStyle: {
        height: responsiveHeight(1),
    },
    creditCardIconStyle: {
        height: responsiveHeight(1.5),
    },
    activeCardStyle: {
        borderWidth: 3,
        borderColor: '#6a85c3',
    },

}

export { CreditCardDetect }