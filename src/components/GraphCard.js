import React, { Component } from 'react'
import { View, Text, Image } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

class GraphCard extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { imgUri } = this.props

        return (
            <View style={styles.graphCardContainerStyle}>
                <Image
                    source={imgUri}
                    resizeMode='stretch'
                    style={styles.imgStyle}
                />
            </View>
        )
    }
}

const styles = {
    graphCardContainerStyle: {
        width: responsiveWidth(95),
        height: responsiveHeight(39),
        borderRadius: 1.8,
        backgroundColor: "#ffffff",
        borderStyle: "solid",
        borderWidth: 0.5,
        borderColor: "#cdcccc",
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    imgStyle:{
        width: responsiveWidth(90),
        height: responsiveHeight(34),
    },

}
export { GraphCard }
