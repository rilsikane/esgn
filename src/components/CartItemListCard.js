import React, { Component } from 'react'
import { View, Text, ScrollView } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import { Button } from 'native-base'

import { CartItemCard } from '../components/CartItemCard'
import { observer, inject } from 'mobx-react';
@inject('naviStore')
@observer
class CartItemListCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderItemList(itemList) {
        return itemList.items.map((data, index) =>
            <CartItemCard
                index={index}
                imgUri={data.imgUri}
                title={data.title}
                total={data.total}
                prize={data.prize}
                prizeUnit={itemList.unit}
                key={index}
            />
        )
    }

    render() {
        const { style, itemList } = this.props

        return (
            <View style={[styles.cartItemListCardContainerStyle, style]}>
                <Text style={styles.titleTextStyle}>คุณมีสินค้าในตะกร้า</Text>
                <Text style={styles.totalItemTextStyle}>{itemList.items.length} ชิ้น</Text>
                <ScrollView>
                    {this.renderItemList(itemList)}
                </ScrollView>
                <View style={styles.totalCostContainerStyle}>
                    <Text style={styles.totalLabelTextStyle}>ยอดรวมทั้งสิ้น</Text>
                    <Text style={styles.totalPrizeTextStyle}>{itemList.totalCost} {itemList.unit}</Text>
                </View>
                <View style={styles.lineStyle} />
                <Button rounded style={styles.submitButtonStyle} onPress={()=>this.props.confirm()}>
                    <Text style={styles.submitButtonTextStyle}>Confirm</Text>
                </Button>
            </View>
        )
    }
}

const styles = {
    cartItemListCardContainerStyle: {
        width: responsiveWidth(95),
        borderRadius: 4,
        backgroundColor: "#ffffff",
        alignSelf: 'center',
        alignItems: 'center',
    },
    titleTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#414042"
    },
    totalItemTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#e42526"
    },
    totalCostContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        paddingLeft: responsiveWidth(2.5),
        paddingRight: responsiveWidth(2.5),
    },
    totalLabelTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#414042"
    },
    totalPrizeTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#e42526"
    },
    lineStyle: {
        width: '95%',
        height: 1,
        backgroundColor: "#bcbec0",
        alignSelf: 'center',
        marginTop: responsiveHeight(2),
        marginBottom: responsiveHeight(2),
    },
    submitButtonStyle: {
        width: responsiveWidth(85),
        height: responsiveHeight(8),
        backgroundColor: "#e42526",
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        marginBottom: responsiveHeight(2),
    },
    submitButtonTextStyle: {
        fontFamily: "Kanit",
        fontSize: responsiveFontSize(2.5),
        color: "#ffffff"
    },

}

export { CartItemListCard }
